<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Administrador_model extends CI_model {
	private $database = '';
	private $filter = []; //filtro
	private $conn;
	
	function __construct() {
		parent::__construct();
		$this->load->library('MongoDB');
		$this->conn = $this->mongodb->getConn();
	}

	function get_by_tipo_filtro($value, $id_entidad)
	{
		$this->filter = ['$and' => [['name'=>$value], ['entity.id'=>$id_entidad]]];
		$options = ['projection' => ['columns'=>'1']];
		try {
			$query = new MongoDB\Driver\Query($this->filter, $options);
			$result = $this->conn->executeQuery($this->database.'.entities_filters', $query)->toArray();

			if (count($result)>0 && count($result[0]->columns)) {
				$columns = $result[0]->columns;
				$group = [];
				if (count($columns)>1) {
					for ($i=0; $i < count($columns); $i++) { 
						$group[] = array('$'.$columns[$i] => $columns[$i]);
					}
					$pipeline = [
					    [ '$match' => ['entity.id' => $id_entidad] ],
					    [ '$group' => ['_id' => $group] ]
					];
				}else{
					$pipeline = [
					    [ '$match' => ['entity.id' => $id_entidad] ],
					    [ '$group' => ['_id' => '$'.$columns[0]] ]
					];
				}	
				$result = $this->collection_model->get_aggregate('patients', $pipeline);
				
				if ($result){
				   	return $result;
				}
			}
			
			return array();
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching entities: ' . $ex->getMessage(), 500);
		}
	}
}