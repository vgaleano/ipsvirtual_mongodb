<?php
class Pacientes_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();	            
    }
    /*
    primer nombre -> 12 - 1
    segundo nombre ->  13 - 1
    primer apellido -> 10 - 2
    segundo apellido -> 11 - 2
    municipio -> 7 - 7
    departamento -> 6 - 70
    fecha registro normal
    ips -> 2 - 66
    */
    public function allPacientes($busqueda=FALSE,$id_entidad=false, $ids){
    	if ($id_entidad!=false) {           

            if ($busqueda=='' || $busqueda==FALSE) {
            	$this->db->select('count(distinct(numero_paciente)) as total');
			    $this->db->where('id_entidad',$id_entidad);
			    $q2 = $this->db->get('columnas_valor');
                //print_r($this->db->last_query());
			    if(!$q2)return 0;
			    $query= $q2->result();
			    return $query[0]->total;
            }else{
                //nombre, apellido, direccion, telefono, municipio, departamento
                if (count($ids)>0) {
                	$this->db->select('count(distinct(numero_paciente)) as total');
				    $this->db->where_in('id_columnas ',$ids);
				    $this->db->where('id_entidad',$id_entidad);
				    $this->db->like('valor',$busqueda);
				    $q2 = $this->db->get('columnas_valor');
				    if(!$q2)return 0;
				    $query= $q2->result();
				    return $query[0]->total;
                }else{
                	return 0;					
            	}
            }
            //print $this->db->last_query();
    	}else{
    		return  0;
    	}
    }
    public function getPacientes($buscar=false,$id_entidad=false,$ids=[],$limit=null,$offset=NULL){
    	if ($id_entidad!=false) {
            if (count($ids)==0){
                return 'bad';
            }
		    $this->db->select('distinct(numero_paciente) as numero_paciente');
		    if (count($ids)>0 && $buscar!=false) {
		    	$this->db->where_in('id_columnas ',$ids);
		    }
		    $this->db->where('id_entidad',$id_entidad);
		    if ($buscar!=false) {
		    	$this->db->like('valor',$buscar);
		    }
		    $this->db->group_by('numero_paciente');
		    $this->db->order_by('numero_paciente','asc');
		    if ($limit!=false) {
		        $this->db->limit($limit, $offset);
		    } 
		    $q2 = $this->db->get('columnas_valor');
		    
		    if(!$q2)return false;
		    return $q2->result();

    	}else{
    		return  false;
    	}
    }
    public function getEstados(){
    	$query = $this->db->get('estado');
    	if(!$query)return false;
        return $query->result();
    }
    public function getPaciente($id){
    	$this->db->select('id,nombre');
    	$query= $this->db->get_where('pacientes',array('id'=>$id),1);
    	if(!$query)return false;
        return $query->result();
    }
    public function getDetallePaciente($id){
        //id, sexo, nombre, apellido, numero_documento, fecha_nacimiento, celular
        print_r($this->session->userdata('array_select'));
    	//$this->db->select('pacientes.*,municipios.municipio, municipios.departamento,(select estado.adicional FROM historico_datos_paciente join estado on historico_datos_paciente.id_estado = estado.id where historico_datos_paciente.numero_identificacion=pacientes.numero_documento order by fecha_registro desc limit 1) as estado');
    	$this->db->select('id, sexo, nombre, apellido, numero_documento, fecha_nacimiento, celular');
    	//$this->db->join('municipios','pacientes.municipio = municipios.codigo');
    	$this->db->where('id',$id);
    	$query= $this->db->get('pacientes');
    	if(!$query)return false;
        return $query->result();
    }
    public function getHistoricoPaciente($cedula){
    	$this->db->where('numero_identificacion',$cedula);
    	$this->db->order_by('fecha_registro','desc');
    	//$this->db->limit(1);
    	$query= $this->db->get('historico_datos_paciente');
    	if(!$query)return false;
        return $query->result();
    }
    public function getHistoricoMensajesPersonalizado($id){
    	$this->db->select('pacientes.imagen, log_personalizado.mensaje,pacientes.nombre as receptor');
    	$this->db->join('pacientes', 'pacientes.id = log_personalizado.id_paciente');
    	$this->db->where('id_paciente',$id);
    	$query= $this->db->get('log_personalizado');
    	if(!$query)return false;
        return $query->result();
    }
    public function getHistoricoMensajesGeneral($id_estado){
    	$this->db->select('mensaje, id_canal as imagen,"masivo" as receptor');
    	$this->db->where('id_estado_pacientes',$id_estado);
    	$query= $this->db->get('log_general');
    	if(!$query)return false;
        return $query->result();
    }
    public function getEstadosColores($id_estado, $id_entidad=false){
    	//$query=$this->db->query('select count(distinct(historico_datos_paciente.numero_identificacion))  as total, historico_datos_paciente.id_estado from historico_datos_paciente join pacientes on  historico_datos_paciente.numero_identificacion = pacientes.numero_documento group by historico_datos_paciente.id_estado');
    	if ($id_entidad!=false) {
    		$this->db->select('total');
			$this->db->where('id_estado',$id_estado);
			$this->db->where('id_entidad', $id_entidad);
        	$query= $this->db->get('colores_cantidad');
        	if(!$query)return false;
        	$total = $query->result();
	        return (count($total)>0)?$total[0]->total:'0';
    	}else{
    		return false;
    	}		        	
    }
    public function getRojoIntenso(){
    	$this->db->select('count(numero_identificacion) as total');
		$this->db->where('id_estad','2');
		$this->db->group_by('numero_identificacion');
		$this->db->order_by('fecha_registro', 'DESC');
    	$query= $this->db->get('historico_datos_paciente');
    	if(!$query)return false;
    	$total = $query->result();
        return count($total);
    }
    public function getRojo(){
    	$this->db->select('count((select estado.id FROM historico_datos_paciente join estado on historico_datos_paciente.id_estado = estado.id where historico_datos_paciente.numero_identificacion=pacientes.numero_documento and estado.id=2 order by fecha_registro desc limit 1)) as total');
    	$query= $this->db->get('pacientes');
    	if(!$query)return false;
        return $query->result();
    }
    public function getAmarillo(){
    	$this->db->select('count((select estado.id FROM historico_datos_paciente join estado on historico_datos_paciente.id_estado = estado.id where historico_datos_paciente.numero_identificacion=pacientes.numero_documento and estado.id=3 order by fecha_registro desc limit 1)) as total');
    	$query= $this->db->get('pacientes');
    	if(!$query)return false;
        return $query->result();
    }
    public function getVerde(){
    	$this->db->select('count((select estado.id FROM historico_datos_paciente join estado on historico_datos_paciente.id_estado = estado.id where historico_datos_paciente.numero_identificacion=pacientes.numero_documento and estado.id=4 order by fecha_registro desc limit 1)) as total');
    	$query= $this->db->get('pacientes');
    	if(!$query)return false;
        return $query->result();
    }
    public function getPacienteAll($id_entidad=false){
    	if ($id_entidad!=false) {
    		$this->db->select('count(id) as total');
    		$this->db->where('id_entidad',$id_entidad);
        	$query= $this->db->get('pacientes');
        	if(!$query)return false;
	        return $query->result();
    	}else{
    		return false;
    	}
    }
    public function getRecienteEncuestaSMS($id){
    	$this->db->select('Max(fecha_registro) as fe');
    	$this->db->order_by('fecha_registro','desc');
    	$this->db->where('id_paciente',$id);
    	$query= $this->db->get('respuesta_paciente');
    	if(!$query)return false;
        return $query->result();
    }
    public function getRecienteEncuestaLlamada($id){
    	$this->db->select('Max(fecha_registro) as fe');
    	$this->db->order_by('fecha_registro','desc');
    	$this->db->where('id_paciente',$id);
    	$query= $this->db->get('respuesta_paciente_llamada');
    	if(!$query)return false;
        return $query->result();
    }
    public function getRespuestaEncuesta($fecha,$tabla,$id){
    	$this->db->select('id_pregunta, id_respuesta');
    	$this->db->where('fecha_registro',$fecha);
    	$this->db->where('id_paciente',$id);
    	$query= $this->db->get($tabla);
    	if(!$query)return false;
        return $query->result();
    }
    public function getNoMedico($id){
    	$this->db->select('id,observacion');
    	$this->db->where('id_paciente',$id);
    	$this->db->where('visto','0');
    	$this->db->limit(1);
    	$query= $this->db->get('datos_no_medicos');
    	if(!$query)return false;
        return $query->result();
    }
    public function getNoMedicoVista($id){
    	$data = array('visto'=>1);
    	$this->db->where('id',$id);
    	$query= $this->db->update('datos_no_medicos',$data);
    	/*if(!$query)return false;
        return $query->result();*/
    }
    public function getHistorialAntecedente($id){
    	$this->db->select('id_antecedente');
    	$this->db->where('id_paciente',$id);
    	$this->db->order_by('id_antecedente','asc');
    	$query= $this->db->get('historial_antecedentes_relevantes');
    	if(!$query)return false;
        return $query->result();
    }
     public function getCondicionesTrazadoras($id){
     	$this->db->where('id_paciente',$id);
     	$this->db->order_by('id_condicion_trazadora','asc');
    	$query= $this->db->get('historial_condiciones_trazadoras');
    	if(!$query)return false;
        return $query->result();
    }
    public function getHistoricoPacienteUltimo($cedula){
    	$this->db->select('score, fumador, edad, filtracion_glomerular, diagnostico_diabetes, tension_arterial_sistolica, colesterol_total, fecha_ult_control, id_estado');
    	$this->db->where('numero_identificacion',$cedula);
    	$this->db->order_by('fecha_registro','desc');
    	$this->db->limit(1);
    	$query= $this->db->get('historico_datos_paciente');
    	if(!$query)return false;
        return $query->result();
    }   
    public function getEstadoPaciente($id)
    {
    	$this->db->select('estado.adicional');
    	$this->db->join('estado','estado.id=historico_datos_paciente.id_estado','left');
    	$this->db->join('pacientes','pacientes.numero_documento=historico_datos_paciente.numero_identificacion','left');
    	$this->db->where('pacientes.id',$id);
    	$this->db->order_by('historico_datos_paciente.fecha_registro', 'DESC');
    	$this->db->limit(1);
    	$query=$this->db->get('historico_datos_paciente');
    	if(!$query)return false;
        return $query->result();
    }
    public function getHistoricoPacienteFecha($cedula, $mes, $age, $tipo)
    {
    	if ($tipo=='1') {//ultimo control
    		$this->db->select('tension_arterial_sistolica, tension_arterial_diastolica, peso, imc, talla');
    	}
    	if ($tipo=='2') {//datos hemoglobina
    		$this->db->select('hemoglobina_glicosilaba');
    	}
    	if ($tipo=='3') {//datos colesterol total
    		$this->db->select('colesterol_total');
    	}
    	if ($tipo=='4') {//datos colesterol ldl
    		$this->db->select('colesterol_total, triglicerios, hdl, ldl_calculado');
    	}
    	if ($tipo=='5') {//datos colesterol hdl
    		$this->db->select('hdl');
    	}
    	if ($tipo=='6') {//datos triglicerios
    		$this->db->select('triglicerios');
    	}
    	if ($tipo=='7') {//datos creatinina
    		$this->db->select('creatinina');
    	}
    	if ($tipo=='1') {//ultimo control
    		$this->db->where('month(fecha_ult_control)',$mes);
    		$this->db->where('year(fecha_ult_control)',$age);
    	}
    	if ($tipo=='2') {//datos hemoglobina
    		$this->db->where('month(fecha_hemoglobina)',$mes);
    		$this->db->where('year(fecha_hemoglobina)',$age);
    	}
    	if ($tipo=='3') {//datos colesterol total
    		$this->db->where('month(fecha_colesterol)',$mes);
    		$this->db->where('year(fecha_colesterol)',$age);
    	}
    	if ($tipo=='4') {//datos colesterol ldl
    		$this->db->where('month(fecha_ldl)',$mes);
    		$this->db->where('year(fecha_ldl)',$age);
    	}
    	if ($tipo=='5') {//datos colesterol hdl
    		$this->db->where('month(fecha_hdl)',$mes);
    		$this->db->where('year(fecha_hdl)',$age);
    	}
    	if ($tipo=='6') {//datos triglicerios
    		$this->db->where('month(fecha_triglicerios)',$mes);
    		$this->db->where('year(fecha_triglicerios)',$age);
    	}
    	if ($tipo=='7') {//datos creatinina
    		$this->db->where('month(fecha_creatinina)',$mes);
    		$this->db->where('year(fecha_creatinina)',$age);
    	}
    	$this->db->where('numero_identificacion',$cedula);
    	
    	$query=$this->db->get('historico_datos_paciente');

    	if(!$query)return false;
        return $query->result();
    }
    public function getPacientesDuplicados()
    {
    	$query=$this->db->query('SELECT columnas_valor.valor as numero_documento, count(columnas_valor.valor) as total from columnas_valor join columnas on columnas.id=columnas_valor.id_columnas join columnas_configuracion on columnas_configuracion.id_columnas=columnas.id where columnas_configuracion.id_columnas_variables=8 group by columnas_valor.numero_paciente HAVING COUNT(columnas_valor.valor) > 1;');
    	if(!$query)return false;
        return $query->result();
    }
    public function getPacientesDuplicados1($limit=false,$offset=false)
    {
    	$sql='SELECT numero_documento, nombre, apellido, count(numero_documento) as total FROM pacientes GROUP BY numero_documento HAVING COUNT(numero_documento) > 1';            	
    	if($limit != false){
           $sql.=" LIMIT ".$limit." OFFSET ".$offset."";
        }
    	$query=$this->db->query($sql);
    	if(!$query)return false;
        return $query->result();
    }
    public function getEntidadesPacientes($numero_documento)
    {
    	$this->db->select('entidad.nombre as entidad');
    	$this->db->join('entidad','entidad.id=pacientes.id_entidad');
    	$this->db->where('pacientes.numero_documento',$numero_documento);
    	$query=$this->db->get('pacientes');
    	if(!$query)return false;
        return $query->result();
    }
    public function getCountDMSinHtaErcParametro($parametro, $fecha=false, $valor=false)
    {
        if ($parametro=='tfg') {
            $this->db->select('pacientes_controles.id_paciente, pacientes_controles.id_tipo_control, pacientes_controles.id_periocidad_control, pacientes_controles.tfg_crokoft');
        }else{
            $this->db->select('COUNT(pacientes_controles.id) AS total, tipo_control.tipo_control, periocidad_control.periocidad_control, pacientes_controles.age_control');
        }    	   
        $this->db->join('pacientes_historicos','pacientes_historicos.id_paciente = pacientes_controles.id_paciente', 'LEFT');
        $this->db->join('tipo_control', 'tipo_control.id=pacientes_controles.id_tipo_control', 'LEFT');
        $this->db->join('periocidad_control', 'periocidad_control.id=pacientes_controles.id_periocidad_control','LEFT');
    	$this->db->where('pacientes_historicos.diabetes_mellitus','Si');
    	$this->db->where('pacientes_historicos.hipertension_arterial','No');
    	$this->db->where('pacientes_historicos.erc','No');        
        $this->db->where('pacientes_controles.fecha_control!="0000-00-00"');

    	if ($parametro=='presion_arterial') { //<140/90 ultimo semestre
    		$this->db->where('pacientes_controles.tas<140');
            $this->db->where('pacientes_controles.tad<90');
    	}
        if ($parametro=='hemoglobina_glicosilada') {
            $this->db->where('pacientes_controles.hemoglobina_glicosilada is not null');            
            if ($valor!=false) {
                $this->db->where("pacientes_controles.hemoglobina_glicosilada < $valor ");
            }
        }
        if ($parametro=='colesterol_ldl') {
            $this->db->where('pacientes_controles.colesterol_ldl is not null');
            $this->db->where('pacientes_controles.colesterol_ldl !="NA"');
            if ($valor!=false) {
                $this->db->where("(pacientes_controles.colesterol_ldl < $valor or pacientes_controles.colesterol_ldl = $valor)");
            }
        }
        if ($parametro=='creatinina') {
            $this->db->where('pacientes_controles.creatinina_sangre is not null');
            $this->db->where('pacientes_controles.creatinina_sangre !="NA"');
        }
        if ($parametro=='albuminuria') {
            $this->db->where('pacientes_controles.microalbuminuria is not null');
        }
        if ($parametro=='imc') {
             $this->db->where("((pacientes_controles.imc > 20 and pacientes_controles.imc < 25) or pacientes_controles.imc = 20 or pacientes_controles.imc = 25)");
        }
        if ($fecha!=false) {
            $this->db->where("(pacientes_controles.fecha_control > '$fecha' || pacientes_controles.fecha_control = '$fecha')");
        }
        if ($parametro=='tfg') {
            $this->db->where("pacientes_controles.tfg_crokoft is not null");
            $this->db->where("pacientes_controles.tfg_crokoft != 0");
            $this->db->where("pacientes_controles.tfg_crokoft > 0");
            $this->db->order_by("pacientes_controles.id_paciente, pacientes_controles.id_periocidad_control asc");
        }else{
            $this->db->group_by('pacientes_controles.id_tipo_control , pacientes_controles.id_periocidad_control');
            $this->db->order_by('pacientes_controles.id_tipo_control , pacientes_controles.age_control , pacientes_controles.id_periocidad_control', 'asc');
        }
        
    	$query=$this->db->get('pacientes_controles');
        /*if ($parametro=='tfg') {
            print_r($this->db->last_query());
        } */      
    	if(!$query)return array();
        return $query->result();
    }

    public function getCountDM()
    {
    	$this->db->select('count(id) as total');
    	$this->db->where('diabetes_mellitus','Si');
        $this->db->where('hipertension_arterial','No');
        $this->db->where('erc','No'); 
    	$query=$this->db->get('pacientes_historicos');
    	if(!$query)return false;
        return $query->result();
    }

    public function getCountDMIMC($fecha=false)
    {
        $this->db->select('pacientes_controles.id_paciente, count(pacientes_controles.id_paciente) as total');
        $this->db->join('pacientes_historicos','pacientes_historicos.id_paciente = pacientes_controles.id_paciente', 'LEFT');
        $this->db->join('tipo_control', 'tipo_control.id=pacientes_controles.id_tipo_control', 'LEFT');
        $this->db->join('periocidad_control', 'periocidad_control.id=pacientes_controles.id_periocidad_control','LEFT');
        $this->db->where('pacientes_historicos.diabetes_mellitus','Si');
        $this->db->where('pacientes_historicos.hipertension_arterial','No');
        $this->db->where('pacientes_historicos.erc','No');        
        $this->db->where('pacientes_controles.fecha_control!="0000-00-00"');
        $this->db->where("pacientes_controles.tfg_crokoft is not null");
        $this->db->where("pacientes_controles.tfg_crokoft != 0");
        $this->db->where("pacientes_controles.tfg_crokoft > 0");
        if ($fecha!=false) {
            $this->db->where("(pacientes_controles.fecha_control > '$fecha' || pacientes_controles.fecha_control = '$fecha')");
        }
        $this->db->group_by('pacientes_controles.id_paciente');
        $this->db->order_by("pacientes_controles.id_paciente, pacientes_controles.id_periocidad_control asc");        
        $query=$this->db->get('pacientes_controles');

        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresPadres($id_entidad, $limit=false, $offset=false)
    {
        $this->db->select('id, titulo');
        $this->db->where('id_entidad',$id_entidad);
        if ($limit!=false) {
             $this->db->limit($limit, $offset);
        }       
        $query=$this->db->get('indicadores_titulos');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresSub($id_padre)
    {
        $this->db->select('id, subtitulo, descripcion, posicion, id_tipo_periodicidad');
        $this->db->where('id_indicador_titulo',$id_padre);
        $query=$this->db->get('indicadores_sub');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresFormulas($id_padre)
    {
        $this->db->select('indicadores_formula.id, indicadores_formula.operador, indicadores_formula.valor, indicadores_formula.operador_1, indicadores_formula.valor_1, indicadores_formula.operador_2,  indicadores_formula.valor_2, indicadores_formula.tipo_fecha, columnas_configuracion.id_columnas, columnas_variables.columna_variable, indicadores_formula.id_columna_variable, indicadores_formula.tipo_formula');
        $this->db->join('columnas_configuracion', 'columnas_configuracion.id_columnas_variables=indicadores_formula.id_columna_variable', 'left');
        $this->db->join('columnas_variables', 'columnas_variables.id=indicadores_formula.id_columna_variable', 'left');
        $this->db->where('indicadores_formula.num_grupo',$id_padre);
        $query=$this->db->get('indicadores_formula');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresGlobales($id_padre)
    {
         $this->db->select('indicadores_globales.id, indicadores_globales.operador, indicadores_globales.valor, indicadores_globales.operador_1, indicadores_globales.valor_1, indicadores_globales.operador_2, indicadores_globales.valor_2, indicadores_globales.tipo_fecha, columnas_configuracion.id_columnas, indicadores_globales.id_columna_variable');
        $this->db->join('columnas_configuracion', 'columnas_configuracion.id_columnas_variables=indicadores_globales.id_columna_variable', 'left');
        $this->db->where('indicadores_globales.id_indicador_sub',$id_padre);
        $query=$this->db->get('indicadores_globales');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresCategorias($id_padre)
    {
        $this->db->where('id_sub',$id_padre);
        $query=$this->db->get('indicadores_categorias');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresGrupos($id_padre)
    {
        $this->db->select('indicadores_grupos.id, indicadores_grupos.num_grupo,  indicadores_grupos.id_tipo_indicador, indicadores_grupos.nombre_grupo');//, tipo_indicador.tipo_indicador
        //$this->db->join('tipo_indicador', 'tipo_indicador.id=indicadores_grupos.id_tipo_indicador');
        $this->db->where('indicadores_grupos.id_indicador_categoria',$id_padre);
        $query=$this->db->get('indicadores_grupos');
        if(!$query)return array();
        return $query->result();
    }
    public function getTotalPadres($id_entidad)
    {
        $this->db->where('id_entidad',$id_entidad);
        $query = $this->db->count_all_results('indicadores_titulos');
        if(!$query)return 0;
        return $query;
    }
    public function getIndicadoresGraficas($id_padre)
    {
        $this->db->where('id_indicador_titulo',$id_padre);
        $query=$this->db->get('indicadores_sub');
        if(!$query)return array();
        return $query->result();
    }
     public function getTotalGraficas($id_entidad)
    {
        $this->db->where('id_entidad',$id_entidad);
        $query = $this->db->count_all_results('indicadores_sub');
        if(!$query)return 0;
        return $query;
    }
    public function getIndicadoresGraciasT($id_entidad, $publicar=false, $limit=false, $offset=false)
    {
        if ($publicar!=false) {
            $this->db->where('publicar','1');
        }
        $this->db->where('id_entidad',$id_entidad);
        if ($limit!=false) {
             $this->db->limit($limit, $offset);
        }       
        $query=$this->db->get('indicadores_sub');
        if(!$query)return array();
        return $query->result();
    }
    public function getGrafica($id)
    {
        $this->db->where('id',$id);
        $query=$this->db->get('indicadores_sub');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadorById($id)
    {
        $this->db->where('id',$id);
        $query=$this->db->get('indicadores_sub');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresGlobalesInfo($id_padre)
    {
         $this->db->select('indicadores_globales.id, indicadores_globales.operador, indicadores_globales.valor, indicadores_globales.operador_1, indicadores_globales.valor_1, indicadores_globales.operador_2, indicadores_globales.valor_2, indicadores_globales.tipo_fecha, columnas_variables.columna_variable, indicadores_globales.id_columna_variable');
        $this->db->join('columnas_variables', 'columnas_variables.id=indicadores_globales.id_columna_variable', 'left');
        $this->db->where('indicadores_globales.id_indicador_sub',$id_padre);
        $query=$this->db->get('indicadores_globales');
        if(!$query)return array();
        return $query->result();
    }
    public function getIndicadoresTipoRiesgo($id_padre)
    {
        $this->db->select('tipo_riesgo_evento.*, indicadores_tipo_riesgo.operador, indicadores_tipo_riesgo.valor, indicadores_tipo_riesgo.operador1, indicadores_tipo_riesgo.valor1, indicadores_tipo_riesgo.operador2, indicadores_tipo_riesgo.valor2, indicadores_tipo_riesgo.id_tipo_indicador');
        $this->db->join('indicadores_tipo_riesgo', 'indicadores_tipo_riesgo.id_tipo_riesgo_evento=tipo_riesgo_evento.id', 'right');
        $this->db->where('indicadores_tipo_riesgo.id_indicador_sub',$id_padre);
        $query=$this->db->get('tipo_riesgo_evento');
        if(!$query)return array();
        return $query->result();
    }
    public function getPacienteById($ids_paciente, $array_select, $array_join)
    {
        $sql = "SELECT a.numero_paciente, a.fecha_registro, ".implode(', ', $array_select)." from columnas_valor a ".implode(' ', $array_join)." where a.numero_paciente in (".implode(',', $ids_paciente).") order by a.numero_paciente;";
        //print $sql;
        $query = $this->db->query($sql);
        if(!$query)return array();
        return $query->result();
    }

    public function getCountConfigVariables($id_entidad=false)
    {
        if ($id_entidad==false) {
            return 0;
        }
        $this->db->select('count(columnas_configuracion.id) as total');
        $this->db->join('columnas_variables','columnas_variables.id=columnas_configuracion.id_columnas_variables');
        $this->db->where('columnas_variables.id_entidad',$id_entidad);
        $query=$this->db->get('columnas_configuracion');
        if(!$query)return 0;
        $query = $query->result();
        return $query[0]->total;
    }

    public function getColumnas($id_entidad=false)
    {
        if ($id_entidad==false) {
            return 0;
        }
        $this->db->select('count(id) as total');
        $this->db->where('id_entidad',$id_entidad);
        $query=$this->db->get('columnas');
        if(!$query)return 0;
        $query = $query->result();
        return $query[0]->total;
    }

    public function getIndicadoresClasificaciones($id_sub)
    {
        $this->db->select('id, name, tipo_regla, id_tipo_indicador');
        $this->db->where('id_sub', $id_sub);
        $query=$this->db->get('indicadores_clasificacion');
        if(!$query)return array();
        return $query->result();
    }

    public function getIndicadoresClasificacionesParametros($id_padre)
    {
        $this->db->select('indicadores_clasificacion_formula.id, indicadores_clasificacion_formula.operador, indicadores_clasificacion_formula.valor, indicadores_clasificacion_formula.operador_1, indicadores_clasificacion_formula.valor_1, indicadores_clasificacion_formula.operador_2,  indicadores_clasificacion_formula.valor_2, columnas_configuracion.id_columnas, columnas_variables.columna_variable, indicadores_clasificacion_formula.id_columna_variable');
        $this->db->join('columnas_configuracion', 'columnas_configuracion.id_columnas_variables=indicadores_clasificacion_formula.id_columna_variable', 'left');
        $this->db->join('columnas_variables', 'columnas_variables.id=indicadores_clasificacion_formula.id_columna_variable', 'left');
        $this->db->where('indicadores_clasificacion_formula.id_indicador_clasificacion',$id_padre);
        $query=$this->db->get('indicadores_clasificacion_formula');
        if(!$query)return array();
        return $query->result();
    }
}
