<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
class Backend_administrador extends CI_model {
	private $database = '';
	private $col_roles = 'roles';
	private $col_users = 'users';
	private $conn;
	
	function __construct() {
		parent::__construct();
		$this->load->library('MongoDB');
		$this->conn = $this->mongodb->getConn();
	}

	function get_permisos_by_view_rol($id_vista, $_id)
	{
		$this->filter = ['$and'=>[['_id'=>new MongoDB\BSON\ObjectId($_id)], ['views.id'=>$id_vista]]];
		try {
			$command = new MongoDB\Driver\Command([
				'count' => $this->col_roles, 
				'query' =>$this->filter
			]);
			$cursor = $this->conn->executeCommand($this->database, $command)->toArray();
			return $cursor[0]->n;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching entities: ' . $ex->getMessage(), 500);
		}
	}

	function get_user_login($email, $password)
	{
		try {
			$filter = ['email' => $email, 'password'=>$password, 'state'=>'1'];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->col_users, $query)->toArray();
			
			if (count($result)>0) {
				return $result[0];
			}
						
			return NULL;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching entities: ' . $ex->getMessage(), 500);
		}
	}
}