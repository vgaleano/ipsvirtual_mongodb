<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Estado: 1->activo, 0->desactivado, 2->eliminado
*/
date_default_timezone_set('America/Bogota');
class Collection_model extends CI_model {
	private $database = '';
	private $filter = []; //filtro
	private $array_collection = array('users', 'process', 'columns_configuration', 'indicators', 'entities_indicators', 'entities_periodicity', 'message_banck', 'messaging_campaigns', 'log_envios_personalizados', 'patients');
	private $conn;
	
	function __construct() {
		parent::__construct();
		$this->load->library('MongoDB');
		$this->conn = $this->mongodb->getConn();
	}

	function get_total($collection, $data_filter=false)
	{
		$this->filter = ($data_filter==false)? [] : $data_filter;
		$cmd = ($data_filter==false) ? array('count' => $collection) : array('count' => $collection, 'query' =>$this->filter);
		try {
			$command = new MongoDB\Driver\Command($cmd);
			$cursor = $this->conn->executeCommand($this->database, $command)->toArray();
			return $cursor[0]->n;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching entities: ' . $ex->getMessage(), 500);
		}
	}

	//name_collection, filter, options
	function get_collection($collection, $data_filter=false, $options=false)
	{	
		$this->filter = ($data_filter==false)? [] : $data_filter;

		$collection = $this->conn->ipsvirtual->users;
		$cursor = $collection->find(['state' => '1']);
		return $cursor;
		/*try {
			if ($options!==false) {
				$query = new MongoDB\Driver\Query($this->filter, $options);
			}else{
			 	$query = new MongoDB\Driver\Query($this->filter);
			}
			$result = $this->conn->executeQuery($this->database.'.'.$collection, $query)->toArray();
			return $result;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching entities: ' . $ex->getMessage(), 500);
		} */
	}

	function update_collection($_id, $collection, $data, $other_filter=false)
	{
		try {
			$query = new MongoDB\Driver\BulkWrite();
			if ($other_filter==false) {
				$query->update(['_id' => new MongoDB\BSON\ObjectId($_id)], ['$set' => $data]);
			}else{
				$query->update(['$and'=>[['_id' => new MongoDB\BSON\ObjectId($_id)], $other_filter]], ['$set' => $data]);
			}						
			$result = $this->conn->executeBulkWrite($this->database.'.'.$collection, $query);
			
			if($result->getModifiedCount() == 1 || $result->getMatchedCount() == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}
	//add array field of document
	function update_push_collection($_id=false, $collection, $data, $other_filter=false)
	{
		if ($_id) {
			$filter = (is_array($_id))? $_id : ['_id' => new MongoDB\BSON\ObjectId($_id)];
			$filter=($other_filter==false)? $filter : ['$and'=>[$filter, $other_filter]];
		}else{
			$filter=$other_filter;
		}		
		try {
			$query = new MongoDB\Driver\BulkWrite();
			$query->update($filter, ['$push' => $data]);
			$result = $this->conn->executeBulkWrite($this->database.'.'.$collection, $query);

			if($result->getModifiedCount() == 1 || $result->getMatchedCount() == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}
	//remove array field of document
	function update_pull_collection($_id, $collection, $data, $other_filter=false)
	{
		$filter=($other_filter==false)? ['_id' => new MongoDB\BSON\ObjectId($_id)]: ['$and'=>[['_id' => new MongoDB\BSON\ObjectId($_id)], $other_filter]];
		try {
			$query = new MongoDB\Driver\BulkWrite();
			$query->update($filter, ['$pull' => $data]);			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$collection, $query);

			if($result->getModifiedCount() == 1 || $result->getMatchedCount() == 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}

	function insert_collection($collection, $data)
	{
		if (in_array($collection, $this->array_collection)) {
			$orig_date = new DateTime();
			$orig_date=$orig_date->getTimestamp();
			$data = array_merge($data,array('registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)));
		}
		
		try {
			$query = new MongoDB\Driver\BulkWrite();
			$_id=$query->insert($data);			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$collection, $query);
			
			if($result->getInsertedCount() == 1) {
				return $_id;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}

	function insert_many_collection($collection, $data)
	{
		$orig_date = new DateTime();
		$orig_date=$orig_date->getTimestamp();
		
		try {
			$query = new MongoDB\Driver\BulkWrite();
			for ($i=0; $i < count($data); $i++) { 
				$data[$i] = array_merge($data[$i],array('registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)));
				$query->insert($data[$i]);
			}						
			$result = $this->conn->executeBulkWrite($this->database.'.'.$collection, $query);

			if($result->getInsertedCount() > 1) {
				return TRUE;
			}
			
			return FALSE;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while deleting users: ' . $ex->getMessage(), 500);
		}
	}

	function get_aggregate($collection, $pipeline)
	{
		try {
			$command = new MongoDB\Driver\Command([
	    		'aggregate' => $collection,
	    		'pipeline' => $pipeline,
	    		'cursor' => new stdClass,
			]);
			$cursor = $this->conn->executeCommand($this->database, $command)->toArray();
			return $cursor;
		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching entities: ' . $ex->getMessage(), 500);
		}
	}
}