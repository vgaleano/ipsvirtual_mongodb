<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Author: https://www.roytuts.com
*/

$config['host'] = 'localhost';
$config['port'] = 27017;
$config['username'] = '';
$config['password'] = '';
$config['authenticate'] = FALSE;
/*
$config['host'] = '52.54.183.114';//'34.201.217.223';
$config['port'] = 27017;
$config['username'] = 'root';
$config['password'] = 'y3vQou7amqie';
$config['authenticate'] = TRUE; */