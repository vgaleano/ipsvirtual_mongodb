<?php
//NAVBAR
$lang['navbar1'] = 'Home';
$lang['navbar2'] = 'What do we do';
$lang['navbar3'] = 'Contact us';
$lang['navbar4'] = 'Blog';
$lang['navbar5'] = 'Login';

//INDEX
$lang['titulo1'] = 'Home';
//seccion header
$lang['text_header1'] = 'Tracking';
$lang['text_header2'] = 'VIRTUAL';
$lang['text_header3'] = 'Of cronical patients';
$lang['text_header4'] = 'Control and manage the risk of this population, visualize indicators of progress, adherence and treatment';
$lang['text_header_button1'] = 'Contac Us';
$lang['text_header_button2'] = 'CALL TO ACTION 02';

//seccion 2
$lang['text_title_2'] = 'What are the advantages?';
//ventaja 1
$lang['text_description_2_1'] = 'User knowledge through the characterization and georeference of the population.';
//ventaja 2
$lang['text_description_2_2'] = 'Adaptability to particular scheduling needs.';
//ventaja 3
$lang['text_description_2_3'] = 'Portable clinical information for users and medical institutions.';
//ventaja 3
$lang['text_description_2_4'] = 'Security and accessibility of information.';

//seccion 3
$lang['text_title_3'] = 'Our remote healthcare and tracking platform.';
$lang['text_description_3'] = 'Properly handle of communication and medical information with patients with chronics and high-cost diseases, analyzing every detail of their attention, managing with high impact their clinic treatment and improving their quality of life. <br><br> In turn, we control and manage population risk through clear advance indicator, adherence to treatments and georeferencing of patients. With this, we save millions of dollars in care cost and improve users perception with their health care provider.';

//seccion 4
//parte 1
$lang['text_description_4_1'] = 'Timely diagnosis and appropriate clinical treatment based on the characterization of the health population';

//seccion 5
$lang['text_title_5'] = 'Know our platform';
$lang['text_subtitle_5'] = '¿Why we are the <span class="label-green">best choice?</span>';

$lang['text_list_5'] = '<ul><li>Classification and <span class="label-blue">risk management</span></li><li>Population characterization and <span class="label-blue">georeferencing</span></li><li>Generation of <span class="label-blue">risk reports</span></li><li>Real time and automated <span class="label-blue">contactability </span>processes</li><li>Management of complications and comorbidities</li><li>Patient traceability</li><li><span class="label-blue">Patient to patient process</spab> reduction</li><li>Clinical and technical knowledge in the development of the solution</li><li><span class="label-blue">Portable clinical</span> information for users</li><li>Information security</li><li>Adaptability to particular assurance needs</li></ul>';

//seccion 6
$lang['text_title_6'] = 'Testimonies';
$lang['text_description_6_1'] = 'Make patients and at-risk population aware of diseases to enable them to make healthy choices and reduce the impact on their lives.';

//seccion 7
$lang['text_title_7'] = 'Our Blog';

$lang['text_blog_title_7_1'] = 'Text Header 1';
$lang['text_bolg_description_7_1'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$langd['text_blog_fecha_7_1']= 'June 1, 2018';

$lang['text_blog_title_7_2'] = 'Text Header 2';
$lang['text_bolg_description_7_2'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$langd['text_blog_fecha_7_2']= 'June 1, 2018';

$lang['text_blog_title_7_3'] = 'Text Header 3';
$lang['text_bolg_description_7_3'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$langd['text_blog_fecha_7_3']= 'June 1, 2018';

$lang['text_blog_title_7_4'] = 'Text Header 4';
$lang['text_bolg_description_7_4'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$langd['text_blog_fecha_7_4']= 'June 1, 2018';

//seccion8
$lang['text_title_8'] = 'INTEGRATIONS WITH:';
$lang['text_servicio_8_1'] = 'SMS';
$lang['text_servicio_8_2'] = 'E-MAIL';
$lang['text_servicio_8_3'] = 'VOICE';
$lang['text_servicio_8_4'] = 'CRM';
$lang['text_servicio_8_5'] = 'ERP';
$lang['text_servicio_8_6'] = 'WHATSAPP';

//SECCION9
$lang['text_title_9'] = 'Implementation Cases';

//footer
$lang['text_description_10'] = 'Our promise to you is responsive communication and transparency. When you call or e-mail us, we respond right away and explain all clearly.';
$lang['text_subtitle_10_1'] = 'What do we do';
$lang['text_subtitle_10_2'] = 'Contact us';
$lang['text_subtitle_10_3'] = 'Blog';
$lang['text_subtitle_10_4'] = 'Login';

$lang['text_subtitle_10_5'] = 'Subscribe to the blog';
$lang['text_subtitle_10_6'] = 'E-mail';
$lang['text_subtitle_10_7'] = 'FullCare.live, 2020';
$lang['text_subtitle_10_8'] = 'Follow us';

//ADMINISTRADOR
//login
$lang['titulo_login'] = '¿Do you have an account?';
$lang['subtitulo_login'] = 'Register and start using FullCare.Live';
$lang['registrar_btn'] = 'Register';
$lang['bienvenido_login'] = 'Welcome';
$lang['placeholder_email'] = 'E-mail';
$lang['placeholder_password'] = 'Password';
$lang['olvido_password'] = '¿Forgot your password?';

$lang['titulo_registro'] = 'Register';
$lang['descripcion_registro'] = 'Contact to admin and request your login user.';
$lang['subtitulo_registro'] = '¿Do you already have an account?';
$lang['subtitulo_2_registro'] = 'Login and visit your profile';
$lang['btn_ingresar'] = 'ok';
$lang['btn_entrar'] = 'Login';

$lang['cerrar_sesion'] = 'Close session';

//admin - navbar
$lang['nav_1'] = 'Patient list';
$lang['nav_2'] = 'Patient reminders';
$lang['nav_3'] = 'Bulk Load';
$lang['nav_4'] = 'Campaigns';
$lang['nav_5'] = 'Bank messages';
$lang['nav_6'] = 'Heatmap';
$lang['nav_7'] = 'Indicator display';
$lang['nav_8'] = 'User Settings';
$lang['nav_9'] = 'Entities configuration';
$lang['nav_10'] = 'Parameter settings';
$lang['nav_11'] = 'Indicator Settings';

//admin - modal -  sms
$lang['new_sms'] = 'New message';
$lang['mensaje'] = 'Message';
$lang['res_sms'] = 'Characters remaining';
$lang['bad_mensaje'] = 'message field is required';
$lang['ok_mensaje'] = 'Message sent succesfully.';

//admin - modal -  llamada
$lang['new_llamada'] = 'New call';
$lang['ok_llamada'] = 'Call sent successfully.';

//admin - modal -  whatsapp
$lang['new_whatsapp'] = 'New Whatsapp message';
$lang['ok_whatsapp'] = 'Whatsapp sent successfully.';

//admin - nuevo pacicente
$lang['bad_nombre_paciente'] = 'Forgot to add patient name.';
$lang['bad_apellido_paciente'] = "Forgot to add the patient&#39;s last name.";
$lang['bad_celular_paciente'] = 'Forgot to add patient cell phone.';
$lang['bad_tipo_doc_paciente'] = 'Forgot to select type of document.';
$lang['bad_cedula_paciente'] = 'Forgot to add ID.';
$lang['bad_estado_paciente'] = 'Forgot to add patient status.';
$lang['bad_municipio_paciente'] = 'Forgot to select municipality.';

//admin - dashboard
$lang['total_pacientes'] = 'Total patients';
$lang['name_estado_1'] = 'Dark red';
$lang['name_estado_2'] = 'Red';
$lang['name_estado_3'] = 'Yellow';
$lang['name_estado_4'] = 'Green';

$lang['paciente_duplicado'] = 'Duplicate Patients';
$lang['entidad'] = 'Entity';
$lang['llamar'] = 'Call';
$lang['sms'] = 'SMS';
$lang['WHATSAPP'] = 'Whatsapp';
$lang['llamada'] = 'Call';
$lang['mensual'] = 'Monthly';

$lang['search_dashboard'] = 'Name, Surname, State, Address...';
$lang['bad_search_dashboard'] = 'No results were found.';

//mensaje general
$lang['mensaje_masivo'] = 'Mass message';
$lang['estado_paciente'] = 'Patients status';

//perfil - datos paciente
$lang['cedula'] = 'ID number';
$lang['direccion'] = 'Adress';
$lang['edad'] = 'Age';
$lang['estado'] = 'State';

$lang['historial'] = 'Medical history';
$lang['estadisticas'] = 'Statistics';
$lang['historial_mensajes'] = 'Message History';
$lang['ultima_observacion'] = 'LAST OBSERVATION';
$lang['bad_observacion_text'] = 'There are no observations records.';
$lang['titulo_condiciones'] = 'DRAWING CONDITIONS - RELEVANT BACKGROUND';
$lang['titulo_trazadora'] = 'DRAWING CONDITIONS';
$lang['rta_1'] = 'Yes';
$lang['rta_2'] = 'No';
$lang['observaciones'] = 'Observations';
$lang['year'] = 'Year';
$lang['text_observacion'] = 'Observation';
$lang['text_registro'] = 'Registro';
$lang['titulo_relevantes'] = 'RELEVANT BACKGROUND';
$lang['relevante_1'] = 'Cardiopatía Isquemica';
$lang['relevante_2'] = 'Enf. Arterial Perifeca';
$lang['relevante_3'] = 'Enf. Carotidea';
$lang['relevante_4'] = 'Enf. Carotidea Evento Cerebrovascular';
$lang['paciente'] = 'Patient';
$lang['doctor'] = 'Doctor';
$lang['observacion_consulta'] = 'Comments about patient';

$lang['controles_realizados'] = 'CONTROLS CARRIED OUT';
$lang['ultimos_mensajes'] = 'LAST MESSAGES SENT';
$lang['bad_ultimos_mensajes'] = 'There are no messages sent';
$lang['bad_estadisticas'] = 'Click on any of the buttons at the top to graph this clinical data.';

//modal - condicion trazadora
$lang['nuevo_incidente'] = 'New incident';
$lang['fecha'] = 'Date';

//modal - score
$lang['score'] = 'Score';
$lang['detalle_puntaje'] = 'Score Detail';
$lang['fumador'] = 'Smoker';
$lang['genero'] = 'Gender';
$lang['colesterol'] = 'Cholesterol';
$lang['tension_arterial'] = 'Blood pressure';
$lang['detalle_estado'] = 'State detail';
$lang['filtracion'] = 'Filtration';
$lang['diabetes'] = 'Diabetes';

//modal - datos relevantes
$lang['datos_no_medicos'] = 'Non-medical data';


//CARGA PACIENTE MENSAJERIA
$lang['titulo_recordatorio'] = 'Patient reminders';
$lang['numero_documento'] = 'ID Number';
$lang['nombre'] = 'Name';
$lang['telefono'] = 'Phone';
$lang['fecha_cita'] = 'Appointment date';
$lang['fecha_envio'] = 'Send date';
$lang['estado_envio'] = 'Sent status';
$lang['activo'] = 'Active';
$lang['desactivado'] = 'Disabled';
$lang['bad_recordatorio_table'] = 'There are no reminders for patients.';

//CARGA MASIVA
$lang['titulo_masiva'] = 'Bulk Load';
$lang['subtitulo_masiva'] = 'Massive patient bedding';
$lang['pacientes_masivo'] = 'Load patients';
$lang['adjunte_excel'] = 'Attach an excel file';
$lang['recordatorio_masivo'] = 'Massive bed sheet of reminders';
$lang['descargar_formato'] = 'Download format';
$lang['cargar_recordatorios'] = 'Load reminders';
$lang['cargar_archivo'] = 'Attach an excel file with maximum 5000 records';
$lang['bad_cargar_archivo'] = 'You must attach an .xlsx or .xls file to load the patients';
$lang['bad_cargar_archivo_1'] = 'You must attach an .xlsx or .xls file to load patient notifications';
$lang['ok_cargar_archivo'] = 'Data loaded successfully';
$lang['ok_cargar_archivo_1'] = 'File loaded successfully. The data will be loaded in the next 5 minutes. ';

//MENSAJERIA
$lang['titulo_campana'] = 'Messaging Campaigns';
$lang['nueva_campana'] = 'New campaign';
$lang['nombre_campana'] = 'Campaign name';
$lang['canal'] = 'Channel';
$lang['periodicidad'] = 'Periodicity';
$lang['fecha_creacion'] = 'Creation date';
$lang['acciones'] = 'Actions';
$lang['bad_mensajeria_tabla'] = 'There are no messaging campaigns available.';
$lang['confirm_mensajeria'] = 'Are you sure to delete this campaign? Remember, this process is irreversible.';

//NUEVA MENSAJERIA
$lang['titulo_nueva_campana'] = 'New messaging campaign';
$lang['name_titulo_campana'] = 'Campaign title';
$lang['dia_campana'] = 'Day or date of shipment';
$lang['descripcion_dia_campana'] = 'You must click on the field and select a calendar date.';
$lang['campana_text_1'] = 'You must add a day (number) of the month.';
$lang['campana_text_2'] = 'Use message bank';
$lang['campana_text_3'] = 'Message Bank';
$lang['campana_text_4'] = 'Clinical data';
$lang['campana_text_5'] = 'Aggregate conditions';
$lang['campana_text_6'] = 'Current characters';

$lang['campana_bad_1'] = 'Campaign title field is required';
$lang['campana_bad_2'] = 'Campaign type field is required';
$lang['campana_bad_3'] = 'You must add a valid shipping day.';
$lang['campana_bad_4'] = 'You must add a shipping day.';
$lang['campana_bad_5'] = 'You must select a date';
$lang['campana_bad_6'] = 'You must add a status option.';
$lang['campana_bad_7'] = 'You must select an entity.';
$lang['campana_bad_8'] = 'You must select a clinical data.';
$lang['campana_bad_9'] = 'You must select a message bank option';
$lang['campana_bad_10'] = 'You must add a message';
$lang['campana_bad_11'] = 'You must select an entity to continue campaign creation.';

//EDITAR CAMPAÑA
$lang['titulo_editar_campana'] = 'Edit Messaging Campaign';
$lang['tipo_campana'] = 'Campaign type';

$lang['editar_campana_bad_1'] = 'You must add a channel.';

//VER MENSAJERIA
$lang['titulo_ver_mensajeria'] = 'Campaign detail';
$lang['titulo'] = 'Title';
$lang['mensajeria_text_1'] = 'Message or message bank used';
$lang['mensajeria_text_2'] = 'Campaign Conditions';

//BANCO MENSAJES
$lang['titulo_banco'] = 'Message Bank';
$lang['nuevo_banco'] = 'New bank';
$lang['banco'] = 'Bank';
$lang['bad_banco_tabla'] = 'There are no messaging campaigns available.';
$lang['editar_banco'] = 'Edit Bank';
$lang['confirm_banco'] = 'Are you sure to delete this bank? Remember, this process is irreversible ';

$lang['banco_bad_1'] = 'You must add a title.';
$lang['banco_bad_2'] = 'You must select a state.';
$lang['banco_bad_3'] = 'You must select an entity.';
$lang['banco_bad_4'] = 'You must select an entity.';

//GET BANCO
$lang['bad_table_banco'] = 'There are no messages available';
$lang['editar_mensaje'] = 'Edit Message';
$lang['confirm_banco_mensaje'] = 'Are you sure to delete this message from the bank? Remember, this process is irreversible.';

//UBICACION GEOGRAFICA
$lang['titulo_ubicacion'] = 'Geographic location';

$lang['ubicacion_bad_1'] = 'You must select the status of patients.';
$lang['ubicacion_bad_2'] = 'You must select an entity.';

//INDICADORES
$lang['indicadores_text_1'] = 'EVALUATION OF CLINICAL RESULTS';
$lang['desde'] = 'From';
$lang['hasta'] = 'To';
$lang['sede'] = 'Campus';
$lang['todos'] = 'All';
$lang['profesional'] = 'Professional';
$lang['especialidad'] = 'Specialty';
$lang['eventos'] = 'Events';
$lang['estandar'] = 'Standard';
$lang['diagnostico'] = 'Diagnosis';
$lang['discapacidad'] = 'Disability';
$lang['procedencia'] = 'Origin';
$lang['area'] = 'Area';
$lang['total_registros'] = 'Total records';
$lang['total_registros_sin_fecha'] = 'Total records without dates';
$lang['total_registros_otros'] = 'Total records denied by filters';
$lang['total_registros_ok'] = 'Total records ok';

$lang['indicadores_bad_1'] = 'You must select the entity';
$lang['indicadores_bad_2'] = 'The start date must be less than the end date';
$lang['indicadores_bad_3'] = 'The start date must be less than the end date';

$lang['descargar_grafica'] = 'Download graphic';
$lang['descargar_sabana'] = 'Download data sheet';
$lang['exportar_excel'] = 'Export to Excel';
$lang['bajo'] = 'Low';
$lang['medio'] = 'Medium';
$lang['alto'] = 'High';

//CONFIGURACIÓN USUARIOS
$lang['config_user_1'] = 'User Settings';
$lang['config_user_2'] = 'Manage Roles';
$lang['config_user_3'] = 'New user';
$lang['correo'] = 'Mail';
$lang['rol'] = 'Role';
$lang['bad_table_user'] = 'There are no users.';
$lang['editar_usuario'] = 'Edit User';

$lang['confirm_user'] = 'Are you sure to delete this user? Remember, this process is irreversible.';

$lang['user_bad_1'] = 'You must add a name for the user';
$lang['user_bad_2'] = 'You must add an email to the user';
$lang['user_bad_3'] = 'Invalid email format, please try again';
$lang['user_bad_4'] = 'You must add a password for the user';
$lang['user_bad_5'] = 'You must add an entity for the user';
$lang['user_bad_6'] = 'You must add a role and configure the view permissions';

//CONFIGURACION ROLES
$lang['config_rol_1'] = 'Role Settings';
$lang['nuevo_rol'] = 'New role';
$lang['config_rol_2'] = 'Edit Role';

$lang['bad_table_rol'] = 'There are no roles configured.';

$lang['confirm_rol'] = 'Are you sure to delete this role? Remember, this process is irreversible.';

$lang['rol_bad_1'] = 'You must add a name for the role';

//CONFIGURACION VISTAS
$lang['config_vista_1'] = 'View Settings';
$lang['vista'] = 'View';
$lang['permiso'] = 'Permission';
$lang['bad_table_vista'] = 'NO APPLICATION FULFILLS THE SEARCH CONDITIONS';

//CONFIGURACION ENTIDADES
$lang['config_entidad_1'] = 'Entity Configuration';
$lang['nueva_entidad'] = 'New entity';

$lang['bad_table_entidad'] = 'There are no entities.';

$lang['editar_entidad'] = 'Edit Entity';

$lang['confirm_entidad'] = 'Are you to delete this entity? Remember, this process is irreversible.';

$lang['entidad_bad_1'] = 'You must add a name for the entity';

//CONFIGURACION PARAMETROS
$lang['config_parametro_1'] = 'Parameter Settings';
$lang['config_parametro_2'] = 'Search parameter name';
$lang['config_parametro_3'] = 'New Parameter';
$lang['config_parametro_4'] = 'Generic name';
$lang['config_parametro_5'] = 'Parameter name';
$lang['config_parametro_6'] = 'Add related columns';
$lang['config_parametro_7'] = 'Related columns';
$lang['config_parametro_8'] = 'Select column';
$lang['config_parametro_9'] = 'Remove tag';
$lang['config_parametro_10'] = 'Search not found';
$lang['config_parametro_11'] = 'You must select an entity';

$lang['confirm_parametro'] = 'Are you sure to delete this parameter? Remember, this process is irreversible.';

$lang['parametro_bad_1'] = 'You must add an identifier name for related columns';
$lang['parametro_bad_2'] = 'You must select a column from Excel';
$lang['parametro_bad_3'] = 'It was saved correctly';
$lang['parametro_bad_4'] = 'You must configure the parameters of the excel.';

//dashboard
$lang['dashboard_bad_1'] = 'You must select the group of patients to whom the message will be directed.';
$lang['dashboard_bad_2'] = 'You must select a survey.';
$lang['dashboard_bad_3'] = 'Messages sent successfully.';
$lang['dashboard_bad_4'] = 'You must select the group of patients to whom the message will be directed.';
$lang['dashboard_bad_5'] = 'Calls sent successfully.';
$lang['dashboard_bad_6'] = 'You currently do not have permissions to access this page, please request them with your administrator.';

//CONFIGURACIÓN GENERAL
$lang['config_general_1'] = 'General Indicator Settings';
$lang['config_general_2'] = 'Type of indicators';
$lang['config_general_3'] = 'Indicator Filters';
$lang['config_general_4'] = 'Parameter name';
$lang['config_general_5'] = 'Add related columns';
$lang['config_general_6'] = 'Related columns';

$lang['confirm_general'] = 'Are you sure to delete this option? Remember, this process is irreversible and would affect the configured graphics.';

$lang['general_bad_1'] = 'You must select an entity to continue with the configuration.';

//CONFIGFURACION INDICADORES
$lang['config_indicador_1'] = 'Indicator Settings';
$lang['config_indicador_2'] = 'Configure filters';
$lang['config_indicador_3'] = 'New Indicator';
$lang['indicador'] = 'Indicator';
$lang['descripcion'] = 'Description';
$lang['dashboard'] = 'Dashboard';
$lang['publicacion'] = 'Publication';
$lang['borrador'] = 'Draft';
$lang['publicado'] = 'Published';
$lang['editar_indicador'] = 'Edit Indicator';
$lang['eliminar_indicador'] = 'Remove Indicator';

$lang['bad_table_indicadores'] = 'There are no Indicators configured.';

$lang['confirm_indicadores'] = 'Are you sure to delete this graphic? Remember, this process is irreversible.';

//EDITAR INDICADOR
$lang['indicador_text_1'] = 'CONFIGURATION - Edit Indicator';
$lang['indicador_text_2'] = 'Add event information';
$lang['indicador_text_3'] = 'Enter the title';
$lang['indicador_text_4'] = 'Description of the graph';
$lang['indicador_text_5'] = 'Add Global Standard';
$lang['indicador_text_6'] = 'Add Global Parameters';
$lang['indicador_text_7'] = 'Add rules';
$lang['indicador_text_8'] = 'Add Category';
$lang['indicador_text_9'] = 'Additional conditions';
$lang['indicador_text_10'] = 'Save Indicator Father with his children';
$lang['indicador_text_11'] = 'Save draft';
$lang['indicador_text_12'] = 'Indicator Type';
$lang['indicador_text_13'] = 'Value 1';
$lang['indicador_text_14'] = 'Select the parameter';
$lang['indicador_text_15'] = 'Value 2';
$lang['indicador_text_16'] = 'Name of the data category';
$lang['indicador_text_17'] = 'Rule Type';
$lang['indicador_text_18'] = 'Add Parameters';
$lang['indicador_text_19'] = 'Delete Control Group';
$lang['indicador_text_20'] = "It&#39;s date type";
$lang['indicador_text_21'] = 'Delete parameter';
$lang['indicador_text_22'] = 'Remove standard';
$lang['indicador_text_23'] = 'Add Groups';
$lang['indicador_text_24'] = 'Remove category';

//NUEVO INDICADOR
$lang['new_indicador_text_1'] = 'CONFIGURATION - New Indicator';

//modal nuevo paciente
$lang['paciente_text_1'] = 'New patient';
$lang['paciente_text_2'] = 'Column Name - Excel';
$lang['paciente_text_3'] = 'Value';
$lang['paciente_text_4'] = 'New field';
$lang['paciente_text_5'] = 'You must select an entity.';
$lang['paciente_text_6'] = 'Edit patient';

//paginacion
$lang['ir_primero'] = 'Go to first';
$lang['atras'] = 'Back';
$lang['siguiente'] = 'Next';
$lang['ultimo'] = 'Go to last';

//botones
$lang['btn_guardar'] = 'Save';
$lang['btn_cancelar'] = 'Cancel';
$lang['btn_enviar'] = 'Sent';
$lang['btn_cerrar'] = 'Close';
$lang['btn_cargar'] = 'Load';
$lang['btn_aceptar'] = 'Accept';
$lang['btn_filtrar'] = 'FILTER';

$lang['select_one'] = '- Select one -';
$lang['inactivo'] = 'Inactive';
$lang['inicio'] = 'Start';
$lang['select'] = '- Select -';
$lang['mensaje_ok_blog'] = 'Your email has been successfully registered';
$lang['mensaje_blog_bad_1'] = 'You must enter an email.';
$lang['mensaje_blog_bad_2'] = 'You must enter a valid email';
$lang['mensaje_ok_parametros'] = 'Parámetro guardado';