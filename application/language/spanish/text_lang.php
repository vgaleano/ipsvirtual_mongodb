<?php
//NAVBAR
$lang['navbar1'] = 'Inicio';
$lang['navbar2'] = 'Qué hacemos';
$lang['navbar3'] = 'Contáctanos';
$lang['navbar4'] = 'Blog';
$lang['navbar5'] = 'Ir a la Plataforma';

//INDEX
$lang['titulo1'] = 'Home';
//seccion header
$lang['text_header1'] = 'Seguimiento';
$lang['text_header2'] = 'VIRTUAL';
$lang['text_header3'] = 'De pacientes crónicos';
$lang['text_header4'] = 'Controla y gestiona el riesgo de esta población, visualiza indicadores de avance, adherencia y tratamiento';
$lang['text_header_button1'] = 'Contáctanos';
$lang['text_header_button2'] = 'CALL TO ACTION 02';

//seccion 2
$lang['text_title_2'] = 'Cuales son las ventajas';
//ventaja 1
$lang['text_description_2_1'] = 'Conocimiento de los usuarios a través de la caracterización y georeferenciación de la población';
//ventaja 2
$lang['text_description_2_2'] = 'Adaptabilidad a necesidades particulares de aseguramiento.';
//ventaja 3
$lang['text_description_2_3'] = 'Información clínica portable para usuarios e  instituciones de salud.';
//ventaja 3
$lang['text_description_2_4'] = 'Seguridad y accesibilidad de la información.';

//seccion 3
$lang['text_title_3'] = 'Nuestra plataforma de Atención y Seguimiento Remoto.';
$lang['text_description_3'] = 'Maneja adecuadamente la comunicación e información médica con los pacientes con enfermedades crónicas y de alto costo, analizando cada detalle de su atención, gestionando con mayor impacto su tratamiento clínico y mejorando su calidad de vida. <br><br> A su vez, controlamos y gestionamos e riesgo de esta problación a través de indicadores claros de avance, adherencia a tratamientos y la georeferenciación de los pacientes. Con esto, ahorramos millones de dolares en costos de atención y mejoramos la percepción del usuario con su prestador de salud.';

//seccion 4
//parte 1
$lang['text_description_4_1'] = 'Diagnóstico oportuno y tratamientos clínicos adecuados a partir de la caracterización de la población salud.';

//seccion 5
$lang['text_title_5'] = 'Conoce nuestra plataforma';
$lang['text_subtitle_5'] = '¿Porque somos la <span class="label-green">mejor opción?</span>';

$lang['text_list_5'] = '<ul><li>Clasificación y <span class="label-blue">gestión del riesgo</span></li><li>Caracterización y <span class="label-blue">georeferenciación</span> de la población</li><li><span class="label-blue">Generación de reportes</span> de riesgo</li><li>Población de <span class="label-blue">contactabilidad,</span> automatizados y en tiempo real</li><li>Gestión de complicaciones y comorbilidades</li><li>Trazabilidad de pacientes</li><li>Reducción de <span class="label-blue">procesos entre pacientes y atención</span></li><li>Conocimiento clínico y técnico en el desarrollo de la solución</li><li>Información <span class="label-blue">clínica portable</span> para usuarios</li><li>Seguridad de la información</li><li>Adaptabilidad a necesidades particulares de aseguramiento</li></ul>';

//seccion 6
$lang['text_title_6'] = 'Testimonios';
$lang['text_description_6_1'] = 'Hacer que los pacientes y la población en riesgo tome conciencia de las enfermedades para permitirles tomar decisiones saludables y reducir el impacto en sus vidas.';

//seccion 7
$lang['text_title_7'] = 'Nuestro Blog';

$lang['text_blog_title_7_1'] = 'Encabezado del  texto 1';
$lang['text_bolg_description_7_1'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$lang['text_blog_fecha_7_1']= '1 Junio 2018';

$lang['text_blog_title_7_2'] = 'Encabezado del  texto 2';
$lang['text_bolg_description_7_2'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$lang['text_blog_fecha_7_2']= '1 Junio 2018';

$lang['text_blog_title_7_3'] = 'Encabezado del  texto 3';
$lang['text_bolg_description_7_3'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$lang['text_blog_fecha_7_3']= '1 Junio 2018';

$lang['text_blog_title_7_4'] = 'Encabezado del  texto 4';
$lang['text_bolg_description_7_4'] = 'Lorem impsum dor sit amet, and consencutor adipiscing elit. amount April and has already received';
$lang['text_blog_fecha_7_4']= '1 Junio 2018';

//seccion8
$lang['text_title_8'] = 'INTEGRACIONES CON:';
$lang['text_servicio_8_1'] = 'SMS';
$lang['text_servicio_8_2'] = 'E-MAIL';
$lang['text_servicio_8_3'] = 'VOZ';
$lang['text_servicio_8_4'] = 'CRM';
$lang['text_servicio_8_5'] = 'ERP';
$lang['text_servicio_8_6'] = 'WHATSAPP';
$lang['text_button_8'] = 'Ir a la plataforma';

//SECCION9
$lang['text_title_9'] = 'Casos de implementación';

//footer
$lang['text_description_10'] = 'Nuestra promesa es una comunicación receptiva y transparente. Cuando llames o envíe un e-mail, respondemos de inmediato y explicamos todo claramente.';
$lang['text_subtitle_10_1'] = 'Qué hacemos';
$lang['text_subtitle_10_2'] = 'Contáctanos';
$lang['text_subtitle_10_3'] = 'Blog';
$lang['text_subtitle_10_4'] = 'Ir a la Plataforma';

$lang['text_subtitle_10_5'] = 'Suscribete al blog';
$lang['text_subtitle_10_6'] = 'Correo electrónico';
$lang['text_subtitle_10_7'] = 'FullCare.live, 2020';
$lang['text_subtitle_10_8'] = 'Siguenos';

//ADMINISTRADOR
//login
$lang['titulo_login'] = '¿Tienes una cuenta?';
$lang['subtitulo_login'] = 'Registrate y empieza a usar FullCare.Live';
$lang['registrar_btn'] = 'Registrar';
$lang['bienvenido_login'] = 'Bienvenido';
$lang['placeholder_email'] = 'Correo electrónico';
$lang['placeholder_password'] = 'Contraseña';
$lang['olvido_password'] = '¿Olvidó su contraseña?';

$lang['titulo_registro'] = 'Registro';
$lang['descripcion_registro'] = 'Comunícate con el administrador de la plataforma y solicita tu usuario de ingreso.';
$lang['subtitulo_registro'] = '¿Ya tienes una cuenta?';
$lang['subtitulo_2_registro'] = 'Ingresa y visita tu perfil';
$lang['btn_ingresar'] = 'Ingresar';
$lang['btn_entrar'] = 'Entrar';

$lang['cerrar_sesion'] = 'Cerrar sesion';

//admin - navbar
$lang['nav_1'] = 'Lista de pacientes';
$lang['nav_2'] = 'Recordatorios de pacientes';
$lang['nav_3'] = 'Carga masiva';
$lang['nav_4'] = 'Campañas';
$lang['nav_5'] = 'Banco mensajes';
$lang['nav_6'] = 'Mapa de calor';
$lang['nav_7'] = 'Visualización de indicadores';
$lang['nav_8'] = 'Configuración usuarios';
$lang['nav_9'] = 'Configuración entidades';
$lang['nav_10'] = 'Configuración parametros';
$lang['nav_11'] = 'Configuración indicadores';

//admin - dashboard
$lang['total_pacientes'] = 'Total pacientes';
$lang['name_estado_1'] = 'Rojo intenso';
$lang['name_estado_2'] = 'Rojo';
$lang['name_estado_3'] = 'Amarillo';
$lang['name_estado_4'] = 'Verde';

$lang['paciente_duplicado'] = 'Pacientes duplicados';
$lang['entidad'] = 'Entidad';
$lang['llamar'] = 'Llamar';
$lang['sms'] = 'SMS';
$lang['WHATSAPP'] = 'Whatsapp';
$lang['llamada'] = 'Llamada';
$lang['mensual'] = 'Mensual';

//admin - modal -  sms
$lang['new_sms'] = 'Nuevo mensaje';
$lang['mensaje'] = 'Mensaje';
$lang['res_sms'] = 'Caracteres restantes';
$lang['bad_mensaje'] = 'Debe agregar un mensaje';
$lang['ok_mensaje'] = 'Mensaje enviado con éxito.';

//admin - modal -  llamada
$lang['new_llamada'] = 'Nueva llamada';
$lang['ok_llamada'] = 'Llamada enviada con éxito.';

//admin - modal -  whatsapp
$lang['new_whatsapp'] = 'Nuevo mensaje a Whatsapp';
$lang['ok_whatsapp'] = 'Mensaje de Whatsapp enviado con éxito.';


//admin - nuevo pacicente
$lang['bad_nombre_paciente'] = 'Olvidó agregar nombre del paciente.';
$lang['bad_apellido_paciente'] = 'Olvidó agregar apellido del paciente.';
$lang['bad_celular_paciente'] = 'Olvidó agregar celular del paciente.';
$lang['bad_tipo_doc_paciente'] = 'Olvidó seleccionar tipo de documento.';
$lang['bad_cedula_paciente'] = 'Olvidó la cédula.';
$lang['bad_estado_paciente'] = 'Olvidó agregar estado del paciente.';
$lang['bad_municipio_paciente'] = 'Olvidó seleccionar municipio.';

//admin - dashboard
$lang['total_pacientes'] = 'Total de pacientes';
$lang['name_estado_1'] = 'Rojo intenso';
$lang['name_estado_2'] = 'Rojo';
$lang['name_estado_3'] = 'Amarillo';
$lang['name_estado_4'] = 'Verde';

$lang['paciente_duplicado'] = 'Pacientes duplicados';
$lang['entidad'] = 'Entidad';
$lang['llamar'] = 'Llamar';

$lang['search_dashboard'] = 'Nombre, Apellido, Estado, Dirección...';
$lang['bad_search_dashboard'] = 'No se encontro ningún resultado.';

//mensaje general
$lang['mensaje_masivo'] = 'Mensaje masivo';
$lang['estado_paciente'] = 'Estado pacientes';

//perfil - datos paciente
$lang['cedula'] = 'Cédula de Ciudadanía';
$lang['direccion'] = 'Dirección';
$lang['edad'] = 'Edad';
$lang['estado'] = 'Estado';

$lang['historial'] = 'Historial';
$lang['estadisticas'] = 'Estadisticas';
$lang['historial_mensajes'] = 'Historial mensajes';
$lang['ultima_observacion'] = 'ÚLTIMA OBSERVACIÓN';
$lang['bad_observacion_text'] = 'No hay registros de observaciones.';
$lang['titulo_condiciones'] = 'CONDICIONES TRAZADORAS - ANTECEDENTES RELEVANTES';
$lang['titulo_trazadora'] = 'CONDICIONES TRAZADORAS';

$lang['rta_1'] = 'Si';
$lang['rta_2'] = 'No';
$lang['observaciones'] = 'Observaciones';
$lang['year'] = 'Año';
$lang['text_observacion'] = 'Observación';
$lang['text_registro'] = 'Registro';
$lang['titulo_relevantes'] = 'ANTECEDENTES RELEVANTES';
$lang['relevante_1'] = 'Cardiopatía Isquemica';
$lang['relevante_2'] = 'Enf. Arterial Perifeca';
$lang['relevante_3'] = 'Enf. Carotidea';
$lang['relevante_4'] = 'Enf. Carotidea Evento Cerebrovascular';
$lang['paciente'] = 'Paciente';
$lang['doctor'] = 'Doctor';
$lang['observacion_consulta'] = 'Observaciones consulta';

$lang['controles_realizados'] = 'CONTROLES REALIZADOS';
$lang['ultimos_mensajes'] = 'ÚLTIMOS MENSAJES ENVIADOS';
$lang['bad_ultimos_mensajes'] = 'No hay mensajes enviados';
$lang['bad_estadisticas'] = 'Da clic sobre alguno de los botones de la parte superior para graficar este dato clínico.';

//modal - condicion trazadora
$lang['nuevo_incidente'] = 'Nuevo incidente';
$lang['fecha'] = 'Fecha';

//modal - score
$lang['score'] = 'Puntaje';
$lang['detalle_puntaje'] = 'Detalle puntaje';
$lang['fumador'] = 'Fumador';
$lang['genero'] = 'Género';
$lang['colesterol'] = 'Colesterol';
$lang['tension_arterial'] = 'Tensión arterial';
$lang['detalle_estado'] = 'Detalle estado';
$lang['filtracion'] = 'Filtración';
$lang['diabetes'] = 'Diabetes';

//modal - datos relevantes
$lang['datos_no_medicos'] = 'Datos no médicos';


//CARGA PACIENTE MENSAJERIA
$lang['titulo_recordatorio'] = 'Recordatorios de pacientes';
$lang['numero_documento'] = 'Número de documento';
$lang['nombre'] = 'Nombre';
$lang['telefono'] = 'Teléfono';
$lang['fecha_cita'] = 'Fecha cita';
$lang['fecha_envio'] = 'Fecha envío';
$lang['estado_envio'] = 'Estado envío';
$lang['activo'] = 'Activo';
$lang['desactivado'] = 'Desactivado';
$lang['bad_recordatorio_table'] = 'No hay recordatorios para pacientes.';

//CARGA MASIVA
$lang['titulo_masiva'] = 'Carga Masiva';
$lang['subtitulo_masiva'] = 'Carga masiva sabana de pacientes';
$lang['pacientes_masivo'] = 'Cargar pacientes';
$lang['adjunte_excel'] = 'Adjunte un archivo en excel';
$lang['recordatorio_masivo'] = 'Carga masiva sabana de recordatorios';
$lang['descargar_formato'] = 'Descargar formato';
$lang['cargar_recordatorios'] = 'Cargar recordatorios';
$lang['cargar_archivo'] = 'Adjunte un archivo en excel con máximo de 5000 regitros';
$lang['bad_cargar_archivo'] = 'Debe adjuntar un .xlsx o .xls para cargar los pacientes';
$lang['bad_cargar_archivo_1'] = 'Debe adjuntar un .xlsx o .xls para cargar las notificaciones de pacientes';
$lang['ok_cargar_archivo'] = 'Datos cargados con éxito';
$lang['ok_cargar_archivo_1'] = 'Archivo cargado con éxito. Los datos será cargado en los proximos 5 minutos.';

//MENSAJERIA
$lang['titulo_campana'] = 'Campañas de Mensajería';
$lang['nueva_campana'] = 'Nueva campaña';
$lang['nombre_campana'] = 'Nombre campaña';
$lang['canal'] = 'Canal';
$lang['periodicidad'] = 'Periodicidad';
$lang['fecha_creacion'] = 'Fecha creación';
$lang['acciones'] = 'Acciones';
$lang['bad_mensajeria_tabla'] = 'No hay campañas de mensajería disponibles.';
$lang['confirm_mensajeria'] = '¿Esta seguro que desea eliminar esta campaña? Recuerde, este proceso es irreversible';

//NUEVA MENSAJERIA
$lang['titulo_nueva_campana'] = 'Nueva campaña de mensajería';
$lang['name_titulo_campana'] = 'Titulo campaña';
$lang['dia_campana'] = 'Día o fecha de envío';
$lang['descripcion_dia_campana'] = 'Debe dar clic sobre el campo y seleccionar una fecha del calendario.';
$lang['campana_text_1'] = 'Debe agregar un día (número) del mes.';
$lang['campana_text_2'] = 'Usar banco de mensajes';
$lang['campana_text_3'] = 'Banco de mensajes';
$lang['campana_text_4'] = 'Datos clinicos';
$lang['campana_text_5'] = 'Condiciones agregadas';
$lang['campana_text_6'] = 'Caracteres actuales';

$lang['campana_bad_1'] = 'Debe agregar un titulo tipo de campaña.';
$lang['campana_bad_2'] = 'Debe agregar tipo de campaña.';
$lang['campana_bad_3'] = 'Debe agregar un día de envío valido.';
$lang['campana_bad_4'] = 'Debe agregar un día de envío.';
$lang['campana_bad_5'] = 'Debe seleccionar una fecha';
$lang['campana_bad_6'] = 'Debe agregar una opción de estado.';
$lang['campana_bad_7'] = 'Debe seleccionar una entidad.';
$lang['campana_bad_8'] = 'Debe seleccionar un dato clinicos.';
$lang['campana_bad_9'] = 'Debe seleccionar una opción del banco de mensajes';
$lang['campana_bad_10'] = 'Debe agregar un mensaje';
$lang['campana_bad_11'] = 'Debe seleccionar una entidad para continuar con la creación de campaña.';

//EDITAR CAMPAÑA
$lang['titulo_editar_campana'] = 'Editar campaña de mensajería';
$lang['tipo_campana'] = 'Tipo campaña';

$lang['editar_campana_bad_1'] = 'Debe agregar un canal.';

//VER MENSAJERIA
$lang['titulo_ver_mensajeria'] = 'Detalle campaña';
$lang['titulo'] = 'Titulo';
$lang['mensajeria_text_1'] = 'Mensaje o banco de mensajes usado';
$lang['mensajeria_text_2'] = 'Condiciones de la campaña';

//BANCO MENSAJES
$lang['titulo_banco'] = 'Banco de mensajes';
$lang['nuevo_banco'] = 'Nuevo banco';
$lang['banco'] = 'Banco';
$lang['bad_banco_tabla'] = 'No hay campañas de mensajería disponibles.';
$lang['editar_banco'] = 'Editar banco';
$lang['confirm_banco'] = '¿Esta seguro que desea eliminar este banco? Recuerde, este proceso es irreversible';

$lang['banco_bad_1'] = 'Debe agregar un titulo.';
$lang['banco_bad_2'] = 'Debe seleccionar un estado.';
$lang['banco_bad_3'] = 'Debe seleccionar una entidad.';
$lang['banco_bad_4'] = 'Debe seleccionar una entidad.';

//GET BANCO
$lang['bad_table_banco'] = 'No hay mensajes disponibles';
$lang['editar_mensaje'] = 'Editar mensaje';
$lang['confirm_banco_mensaje'] = '¿Esta seguro que desea eliminar este mensaje del banco? Recuerde, este proceso es irreversible';

//UBICACION GEOGRAFICA
$lang['titulo_ubicacion'] = 'Ubicación geográfica de los pacientes';

$lang['ubicacion_bad_1'] = 'Debe seleccionar el estado de pacientes.';
$lang['ubicacion_bad_2'] = 'Debe seleccionar una entidad.';

//INDICADORES
$lang['indicadores_text_1'] = 'EVALUACIÓN DE RESULTADOS CLÍNICOS';
$lang['desde'] = 'Desde';
$lang['hasta'] = 'Hasta';
$lang['sede'] = 'Sede';
$lang['todos'] = 'Todos';
$lang['profesional'] = 'Profesional';
$lang['especialidad'] = 'Especialidad';
$lang['eventos'] = 'Eventos';
$lang['estandar'] = 'Estándar';
$lang['diagnostico'] = 'Diagnóstico';
$lang['discapacidad'] = 'Discapacidad';
$lang['procedencia'] = 'Procedencia';
$lang['area'] = 'Área';
$lang['total_registros'] = 'Total registros';
$lang['total_registros_sin_fecha'] = 'Total registros sin fechas';
$lang['total_registros_otros'] = 'Total registros negado por filtros';
$lang['total_registros_ok'] = 'Total registros ok';

$lang['indicadores_bad_1'] = 'Debe seleccionar la entidad';
$lang['indicadores_bad_2'] = 'La fecha inicio debe ser menor a la fecha fin';
$lang['indicadores_bad_3'] = 'La fecha inicio debe ser menor a la fecha fin';

$lang['descargar_grafica'] = 'Descargar gráfica';
$lang['descargar_sabana'] = 'Descargar sabana de datos';
$lang['exportar_excel'] = 'Exportar excel';
$lang['bajo'] = 'Bajo';
$lang['medio'] = 'Medio';
$lang['alto'] = 'Alto';

//CONFIGURACIÓN USUARIOS
$lang['config_user_1'] = 'Configuración de usuarios';
$lang['config_user_2'] = 'Gestionar Roles';
$lang['config_user_3'] = 'Nuevo Usuario';
$lang['correo'] = 'Correo';
$lang['rol'] = 'Rol';
$lang['bad_table_user'] = 'No hay usuarios.';
$lang['editar_usuario'] = 'Editar Usuario';

$lang['confirm_user'] = '¿Esta seguro que desea eliminar este usuario? Recuerde, este proceso es irreversible';

$lang['user_bad_1'] = 'Debe agregar un nombre para el usuario';
$lang['user_bad_2'] = 'Debe agregar un correo para el usuario';
$lang['user_bad_3'] = 'Formato de correo no valido, por favor, inténtelo de nuevo';
$lang['user_bad_4'] = 'Debe agregar una cotnraseña para el usuario';
$lang['user_bad_5'] = 'Debe agregar una entidad para el usuario';
$lang['user_bad_6'] = 'Debe agregar un rol y configurar los permisos de vistas';

//CONFIGURACION ROLES
$lang['config_rol_1'] = 'Configuración de roles';
$lang['nuevo_rol'] = 'Nuevo Rol';
$lang['config_rol_2'] = 'Editar Rol';

$lang['bad_table_rol'] = 'No hay roles configurados.';

$lang['confirm_rol'] = '¿Esta seguro que desea eliminar este rol? Recuerde, este proceso es irreversible';

$lang['rol_bad_1'] = 'Debe agregar un nombre para el rol';

//CONFIGURACION VISTAS
$lang['config_vista_1'] = 'Configuración de vistas';
$lang['vista'] = 'Vista';
$lang['permiso'] = 'Permiso';
$lang['bad_table_vista'] = 'NINGUNA SOLICITUD CUMPLE CON LAS CONDICIONES DE BÚSQUEDA';

//CONFIGURACION ENTIDADES
$lang['config_entidad_1'] = 'Configuración de entidades';
$lang['nueva_entidad'] = 'Nueva entidad';

$lang['bad_table_entidad'] = 'No hay entidades.';

$lang['editar_entidad'] = 'Editar Entidad';

$lang['confirm_entidad'] = '¿Esta seguro que desea eliminar esta entidad? Recuerde, este proceso es irreversible';

$lang['entidad_bad_1'] = 'Debe agregar un nombre para la entidad';

//CONFIGURACION PARAMETROS
$lang['config_parametro_1'] = 'Configuración de parametros';
$lang['config_parametro_2'] = 'Buscar nombre del parámetro';
$lang['config_parametro_3'] = 'Nuevo Parámetro';
$lang['config_parametro_4'] = 'Nombre genérico';
$lang['config_parametro_5'] = 'Nombre del parámetro';
$lang['config_parametro_6'] = 'Añadir columnas relacionadas';
$lang['config_parametro_7'] = 'Columnas relacionadas';
$lang['config_parametro_8'] = 'Selecciona la columna';
$lang['config_parametro_9'] = 'Eliminar tag';
$lang['config_parametro_10'] = 'Busqueda no encontrado';
$lang['config_parametro_11'] = 'Debe seleccionar una entidad';

$lang['confirm_parametro'] = '¿Esta seguro que desea eliminar este parametro? Recuerde, este proceso es irreversible';

$lang['parametro_bad_1'] = 'Debe agregar un nombre identificador para las columnas relacionadas';
$lang['parametro_bad_2'] = 'Debe seleccionar una columna del excel';
$lang['parametro_bad_3'] = 'Se guardo correctamente';
$lang['parametro_bad_4'] = 'Debe configurar los parametros del excel.';

//dashboard
$lang['dashboard_bad_1'] = 'Debe seleccionar al grupo de pacientes al cual ira dirigido el mensaje.';
$lang['dashboard_bad_2'] = 'Debe seleccionar una encuesta.';
$lang['dashboard_bad_3'] = 'Mensajes enviados con éxito.';
$lang['dashboard_bad_4'] = 'Debe seleccionar al grupo de pacientes al cual ira dirigido el mensaje.';
$lang['dashboard_bad_5'] = 'Llamadas enviadas con éxito.';
$lang['dashboard_bad_6'] = 'Actualmente usted no tiene permisos para acceder a esta página, por favor, Solicitelos con su administrador.';

//CONFIGURACIÓN GENERAL
$lang['config_general_1'] = 'Configuración general de los Indicadores';
$lang['config_general_2'] = 'Tipo de indicadores';
$lang['config_general_3'] = 'Filtros de Indicadores';
$lang['config_general_4'] = 'Nombre del parámetro';
$lang['config_general_5'] = 'Añadir columnas relacionadas';
$lang['config_general_6'] = 'Columnas relacionadas';

$lang['confirm_general'] = '¿Esta seguro que desea eliminar esta opción? Recuerde, este proceso es irreversible y afectaría a las gráficas configuradas';

$lang['general_bad_1'] = 'Debe seleccionar una entidad para continuar con la configuración.';

//CONFIGFURACION INDICADORES
$lang['config_indicador_1'] = 'Configuración de Indicadores';
$lang['config_indicador_2'] = 'Configurar filtros';
$lang['config_indicador_3'] = 'Nuevo Indicador';
$lang['indicador'] = 'Indicador';
$lang['descripcion'] = 'Descripción';
$lang['dashboard'] = 'Dashboard';
$lang['publicacion'] = 'Publicación';
$lang['borrador'] = 'Borrador';
$lang['publicado'] = 'Publicado';
$lang['editar_indicador'] = 'Editar Indicador';
$lang['eliminar_indicador'] = 'Eliminar Indicador';

$lang['bad_table_indicadores'] = 'No hay Indicadores configurados.';

$lang['confirm_indicadores'] = '¿Esta seguro que desea eliminar esta gráfica? Recuerde, este proceso es irreversible';

//EDITAR INDICADOR
$lang['indicador_text_1'] = 'CONFIGURACIÓN - Editar Indicador';
$lang['indicador_text_2'] = 'Agregar información del evento';
$lang['indicador_text_3'] = 'Ingrese el titulo';
$lang['indicador_text_4'] = 'Descripción de la gráfica';
$lang['indicador_text_5'] = 'Agregar estándar Globales';
$lang['indicador_text_6'] = 'Agregar parámetros Globales';
$lang['indicador_text_7'] = 'Agregar reglas';
$lang['indicador_text_8'] = 'Agregar Categoría';
$lang['indicador_text_9'] = 'Condiciones adicionales';
$lang['indicador_text_10'] = 'Guardar Indicador Padre con sus hijos';
$lang['indicador_text_11'] = 'Guardar BORRADOR';
$lang['indicador_text_12'] = 'Tipo indicador';
$lang['indicador_text_13'] = 'Valor 1';
$lang['indicador_text_14'] = 'Selecciona el parámetro';
$lang['indicador_text_15'] = 'Valor 2';
$lang['indicador_text_16'] = 'Nombre de la categoría de datos';
$lang['indicador_text_17'] = 'Tipo de regla';
$lang['indicador_text_18'] = 'Agregar Parámetros';
$lang['indicador_text_19'] = 'Eliminar Grupo Control';
$lang['indicador_text_20'] = 'Es tipo fecha';
$lang['indicador_text_21'] = 'Eliminar parámetro';
$lang['indicador_text_22'] = 'Eliminar estándar';
$lang['indicador_text_23'] = 'Agregar Grupos';
$lang['indicador_text_24'] = 'Eliminar categoria';

//NUEVO INDICADOR
$lang['new_indicador_text_1'] = 'CONFIGURACIÓN - Indicador Nuevo';

//modal nuevo paciente
$lang['paciente_text_1'] = 'Nuevo paciente';
$lang['paciente_text_2'] = 'Nombre de la columna - Excel';
$lang['paciente_text_3'] = 'Valor';
$lang['paciente_text_4'] = 'Nuevo campo';
$lang['paciente_text_5'] = 'Debe seleccionar una entidad.';
$lang['paciente_text_6'] = 'Editar paciente';

//paginacion
$lang['ir_primero'] = 'Ir al primero';
$lang['atras'] = 'Atras';
$lang['siguiente'] = 'Siguiente';
$lang['ultimo'] = 'Ir al último';

//botones
$lang['btn_guardar'] = 'Guardar';
$lang['btn_cancelar'] = 'Cancelar';
$lang['btn_enviar'] = 'Enviar';
$lang['btn_cerrar'] = 'Cerrar';
$lang['btn_cargar'] = 'Cargar';
$lang['btn_aceptar'] = 'Aceptar';
$lang['btn_filtrar'] = 'FILTRAR';

$lang['select_one'] = '- Seleccione uno -';
$lang['inactivo'] = 'Inactivo';
$lang['inicio'] = 'Inicio';
$lang['select'] = '- Selecciona -';
$lang['mensaje_ok_blog'] = 'Se ha registrado su correo electrónico exitósamente';
$lang['mensaje_blog_bad_1'] = 'Debe ingresar un correo electrónico';
$lang['mensaje_blog_bad_2'] = 'Debe ingresar un correo electrónico válido';
$lang['mensaje_ok_parametros'] = 'Parámetro guardado';