	<header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
            <div class="navbar-brand">
              <a href="<?=base_url()?>"><img class="img-responsive" width="150px" src="<?=base_url()?>img/logo_header.png"></a>
            </div>
          </div>

          <div class="navbar-collapse collapse">
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">                
                <li role="presentation">
                  <select title="" class="my-image-selectpicker d-block d-sm-none">
                    <option value="spanish" <?= ($this->session->userdata('site_lang')=='spanish') ? 'selected':''?> data-thumbnail="img/logo_espana.png"></option>
                    <option value="english" <?= ($this->session->userdata('site_lang')=='english') ? 'selected':''?> data-thumbnail="img/eeuu.png"></option>
                  </select>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </header>