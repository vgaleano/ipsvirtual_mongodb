  <header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse.collapse" aria-controls="">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand">
              <a href="index.html"><img class="img-responsive" width="150px" src="<?=base_url()?>img/logo_header.png"></a>
            </div>
          </div>

          <div class="navbar-collapse collapse">
            <!-- <div class="menu"> -->
              <ul class="nav navbar-nav pull-right">
                <li role="presentation">
                  <a href="<?=base_url()?>" class="active"><?=$this->lang->line('navbar1')?></a>
                </li>
                <li role="presentation">
                  <a href="about.html"><?=$this->lang->line('navbar2')?></a>
                </li>
                <li role="presentation">
                  <a href="services.html"><?=$this->lang->line('navbar3')?></a>
                </li>
                <!-- <li role="presentation">
                  <a href="portfolio.html"><?=$this->lang->line('navbar4')?></a>
                </li> -->
                <li role="presentation">
                  <a class="btn btn-primary m-0" href="<?=base_url()?>web/login"><?=$this->lang->line('navbar5')?></a>
                </li>
                <li role="presentation">
                  <select title="" class="my-image-selectpicker d-block d-sm-none">
                    <option value="spanish" <?= ($this->session->userdata('site_lang')=='spanish') ? 'selected':''?> data-thumbnail="img/logo_espana.png"></option>
                    <option value="english" <?= ($this->session->userdata('site_lang')=='english') ? 'selected':''?> data-thumbnail="img/eeuu.png"></option>
                  </select>
                </li>
              </ul>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </nav>
  </header>