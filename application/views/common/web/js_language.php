
  <script src="https://unpkg.com/bootstrap-select@1.13.8/dist/js/bootstrap-select.min.js"></script>
  <script src="<?php echo base_url(); ?>js/scripts.js" type="text/javascript"></script>

   <script type="text/javascript">

    const BASE_URL = base_url;
    const $_SELECT_PICKER = $('.my-image-selectpicker');
    $_SELECT_PICKER.find('option').each((idx, elem) => {
      const $OPTION = $(elem);
      const IMAGE_URL = $OPTION.attr('data-thumbnail');
      if (IMAGE_URL) {
          $OPTION.attr('data-content', "<img class='img-fluid' width='18px' src='%i'/> %s".replace(/%i/, BASE_URL + IMAGE_URL).replace(/%s/, $OPTION.text()))
      }
      console.warn('option:', idx, $OPTION)
    });
    $_SELECT_PICKER.selectpicker('val', "<?=$this->session->userdata('site_lang')?>");

    $_SELECT_PICKER.on('change', function () {
      var valor = $(this).val();
      ajax('LanguageSwitcher/switchLang/'+valor, {}, function (data) {
        window.location.href = data.url;
      });
    })
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
       $_SELECT_PICKER.selectpicker('mobile');
    }

    $(document).ready(function(){
    	if ("<?=$this->session->userdata('site_lang')?>"=='') {
    		$_SELECT_PICKER.selectpicker('val', 'spanish');
    	}
    })

    /*$('.list-language>li').on('click', function () {
      var valor = $(this).attr('data-value');
      ajax('LanguageSwitcher/switchLang/'+valor, {}, function (data) {
        window.location.href = data.url;
      });
    })*/


  </script>