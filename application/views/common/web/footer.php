  <footer>
    <div class="footer px-0 py-4">
      <div class="container">
        <div class="col-xs-12 col-md-4">
          <img src="<?=base_url()?>img/public/logo_fotter.png">
          <p>Our promise to you is responsive communication and transparency. When you call or e-mail us, we respond right away and explain all clearly.</p>
        </div>

        <div class="col-xs-12 col-md-4">
          <ul class="doble">
            <li><a class="label-white" href="#"><?=$this->lang->line('text_subtitle_10_1')?></a></li>
            <li><a class="label-white" href="#"><?=$this->lang->line('text_subtitle_10_2')?></a></li>
            <!-- <li><?=$this->lang->line('text_subtitle_10_3')?></li> -->
            <li><a class="label-white" href="<?=base_url()?>web/login"><?=$this->lang->line('text_subtitle_10_4')?></a></li>
          </ul>
        </div>

        <div class="col-xs-12 col-md-4">
          <label><?=$this->lang->line('text_subtitle_10_5')?></label>
          <div class="input-group btn btn-blue p-0">
            <input name="email1" class="form-control label-white" placeholder="<?=$this->lang->line('text_subtitle_10_6')?>">
            <span class="input-group-addon sent-email"><img src="<?=base_url()?>img/public/icon_suscribirse.png"></span>
          </div>
        </div>
      </div>

      <div class="pull-right">
        <a href="#home" class="scrollup"><i class="fa fa-angle-up fa-3x"></i></a>
      </div>
      <hr width="100%">
      <div class="container">
        <div class="col-xs-12 col-md-6 text-left">
          <h4>IPS VIRTUAL, 2020</h4>
        </div>
        <div class="col-xs-12 col-md-6 text-right">
          <div class="social-icon">          
            <div class="col-xs-12 col-md-12 xs-p-0">
              <ul class="social-network">
                <li><?=$this->lang->line('text_subtitle_10_8')?></li>
                <li><a href="#" class="fb tool-tip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter tool-tip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="gplus tool-tip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>