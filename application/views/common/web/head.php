<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo APP_NAME." | ".(isset($titulo) ? $titulo : "" ); ?></title>

  <!-- Bootstrap -->
  <link href="<?=base_url();?>css/bootstrap/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?=base_url();?>css/public/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url();?>css/public/animate.css">
  <!-- <link href="<?=base_url();?>css/public/prettyPhoto.css" rel="stylesheet"> -->
  <link href="<?=base_url();?>css/public/style.css" rel="stylesheet" />
  <link href="<?=base_url();?>css/public/estilos.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel=stylesheet href="https://unpkg.com/bootstrap-select@1.13.8/dist/css/bootstrap-select.css" />
  <link rel="stylesheet" href="<?=base_url()?>css/public/select_img.css">