    <div id="navbar-wrapper">
        <header>
            <nav class="navbar navbardefault navbar-fixed-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url()?>admin/dashboard"><img src="<?php echo base_url()?>img/logo_header.png"></a>
                    </div>
                    <div id="navbar-collapse" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right" style="display: flex; align-items: center;">
                            <li><?php echo $this->session->rol;?>&nbsp;&nbsp; </li>
                            <li class="dropdown">
                                <a id="user-profile" href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="https://s3.amazonaws.com/ipsvirtual/user_default.png" class="img-responsive img-circle"> <?php echo $this->session->nombre;?> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-block" role="menu">
                                    <!-- <li><a href="#">Editar perfil</a></li> -->
                                    <li><a href="<?php echo base_url()?>usuarios/logout"><?=$this->lang->line('cerrar_sesion')?></a></li>
                                </ul>
                            </li>
                            <li role="presentation">
                                <select title="" class="my-image-selectpicker d-block d-sm-none">
                                    <option value="spanish" <?= ($this->session->userdata('site_lang')=='spanish') ? 'selected':''?> data-thumbnail="img/logo_espana.png"></option>
                                    <option value="english" <?= ($this->session->userdata('site_lang')=='english') ? 'selected':''?> data-thumbnail="img/eeuu.png"></option>
                                </select>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    </div>
    <div class="main-menu">
        <ul>
            <li class="<?php echo($active=='dashboard')?'active':''; ?>">
                <a href="<?php echo base_url()?>admin/dashboard">
                    <i class="fa fa-users fa-2x"></i>
                    <span class="nav-text"><?=$this->lang->line('nav_1')?></span>
                </a>
            </li>
            <?php if($this->session->userdata('id_rol') != "5e3307d8fd61ee8807387413") { ?>
                 <li class="has-subnav <?php echo($active=='pacienteMensajeria')?'active':''; ?>">
                    <a href="<?php echo base_url()?>admin/pacienteMensajeria">
                       <i class="fa fa-bell-o fa-2x"></i>
                        <span class="nav-text"><?=$this->lang->line('nav_2')?></span>
                    </a>
                </li>
                <li class="has-subnav <?php echo($active=='carga_masiva')?'active':''; ?>">
                    <a href="<?php echo base_url()?>admin/cargaMasiva">
                       <i class="fa fa-upload fa-2x"></i>
                        <span class="nav-text"><?=$this->lang->line('nav_3')?></span>
                    </a>
                </li>                
                <li class="has-subnav <?php echo($active=='mensajeria')?'active':''; ?>">
                    <a href="<?php echo base_url()?>admin/mensajeria">
                       <i class="fa fa-comments-o fa-2x"></i>
                        <span class="nav-text"><?=$this->lang->line('nav_4')?></span>
                    </a>
                </li>
                <li class="has-subnav <?php echo($active=='bancoMensajes')?'active':''; ?>">
                    <a href="<?php echo base_url()?>admin/bancoMensajes">
                       <i class="fa fa-archive fa-2x"></i>
                        <span class="nav-text"><?=$this->lang->line('nav_5')?></span>
                    </a>
                </li> 
            <?php } ?>                   
            <li class="has-subnav <?php echo($active=='ubicacion')?'active':''; ?>">
                <a href="<?php echo base_url()?>admin/ubicacion">
                   <i class="fa fa-map-marker fa-2x"></i>
                    <span class="nav-text"><?=$this->lang->line('nav_6')?></span>
                </a>
            </li> 
            <li class="has-subnav <?php echo($active=='indicadores')?'active':''; ?>">
                <a href="<?php echo base_url()?>admin/indicadores">
                   <i class="fa fa-area-chart fa-2x"></i>
                    <span class="nav-text"><?=$this->lang->line('nav_7')?></span>
                </a>
            </li>                
             
            <?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
                <li class="has-subnav <?php echo($active=='configuracionUsuarios')?'active':''; ?>">
                    <a href="<?php echo base_url()?>admin/configuracionUsuarios">
                       <i class="fa fa fa-cogs fa-2x"></i>
                        <span class="nav-text"><?=$this->lang->line('nav_8')?></span>
                    </a>
                </li> 
            <?php } ?> 
            <?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
                <li class="has-subnav <?php echo($active=='configuracionEntidades')?'active':''; ?>">
                    <a href="<?php echo base_url()?>admin/configuracionEntidades">
                       <i class="fa fa fa-building fa-2x"></i>
                        <span class="nav-text"><?=$this->lang->line('nav_9')?></span>
                    </a>
                </li> 
            <?php } ?>
            <?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
                <li class="has-subnav <?php echo($active=='configuracionParametros')?'active':''; ?>">
                    <a href="<?php echo base_url()?>admin/configuracionParametros">
                       <i class="fa fa fa-cubes fa-2x"></i>
                        <span class="nav-text"><?=$this->lang->line('nav_10')?></span>
                    </a>
                </li> 
            <?php } ?>    
            <li class="has-subnav <?php echo($active=='configuracionIndicadores')?'active':''; ?>">
                <a href="<?php echo base_url()?>admin/configuracionIndicadores">
                   <i class="fa fa fa-area-chart fa-2x"></i>
                    <span class="nav-text"><?=$this->lang->line('nav_11')?></span>
                </a>
            </li>      
        </ul>

        <ul class="logout">
            <li>
               <a href="<?php echo base_url()?>usuarios/logout">
                     <i class="fa fa-power-off fa-2x"></i>
                    <span class="nav-text"><?=$this->lang->line('cerrar_sesion')?></span>
                </a>
            </li>  
        </ul>
    </div>
   
    <!-- NUEVO MENSAJE PERSONALIZADO-->
    <div class="modal fade" id="mensaje" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-comments-o fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('new_sms')?> <span id="nombre_paciente"></span></h4>
                </div>
                <form action="" class="formSMS" method="POST" onsubmit="return false">
                    <div class="modal-body">                    
                        <input type="hidden"  id="id_paciente" class="">
                        <input type="hidden"id="sms_entidad">
                        <input type="hidden"id="sms_telefono">
                        <!-- <div class="form-group">
                            <input type="checkbox" name="encuesta" value="1" id="encuesta_msj_personalizado"> Enviar encuesta
                        </div>
                        <div class="form-group" id="encuesta-box_personalizado" style="display:none">
                            <label>Encuesta</label>
                            <select class="form-control " id="encuesta_opcion_personalizado">
                                <option value="" style="display: none;">-Seleccione uno-</option>
                                <?php foreach ($encuesta as $e) { ?>
                                <option value="<?php echo $e->id?>"><?php echo $e->encuesta?></option>
                                <?php } ?>
                            </select>
                        </div> -->
                        <div class="form-group">
                             <label><?=$this->lang->line('new_sms')?> &nbsp;</label>
                             <textarea class="form-control " rows="4" cols="50" id="mensaje_personalizado" maxlength="159"></textarea>
                        </div>
                        <div class="col-md-6 pull-right" style="text-align: right;">
                            <span><?=$this->lang->line('res_sms')?> </span><span class="CaracteresRestantes">160</span> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn limpiar btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
                        <button type="button" class="btn mas btn-green" id="enviar_personalizado"><?=$this->lang->line('btn_enviar')?></button> 
                    </div>
                </form>
            </div>
        </div>
    </div>

        <!-- VOZ PERSONALIZADO-->
    <div class="modal fade" id="voz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" class="formLlamada" method="POST" onsubmit="return false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-phone fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('new_llamada')?> <span id="nombre_paciente_voz"></span></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_paciente_voz">
                    <input type="hidden"id="voz_entidad">
                    <input type="hidden"id="voz_telefono">
                    <div class="form-group">
                        <label><?=$this->lang->line('mensaje')?></label>
                        <textarea rows="4" cols="50" id="voz_personalizado" class="form-control "></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
                    <button type="button" class="btn mas btn-green" id="enviar_voz_personalizado"><?=$this->lang->line('btn_enviar')?></button> 
                </div>
            </form>
            </div>
          </div>
        </div>

        <!-- WHATSAPP PERSONALIZADO-->
    <div class="modal fade" id="modal_whatsapp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" class="formWhatsapp" method="POST" onsubmit="return false">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-whatsapp fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('new_whatsapp')?> <span id="nombre_paciente_voz"></span></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id_paciente_whatsapp">
                    <input type="hidden"id="whatsapp_entidad">
                    <input type="hidden"id="whatsapp_telefono">
                    <div class="form-group">
                        <label><?=$this->lang->line('mensaje')?></label>
                        <textarea rows="4" cols="50" id="whatsapp_personalizado" class="form-control "></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
                    <button type="button" class="btn mas btn-green" id="enviar_whatsapp_personalizado"><?=$this->lang->line('btn_enviar')?></button> 
                </div>
            </form>
            </div>
          </div>
        </div>

    </div>

    <form style="display: none" action="/the/url" method="POST" id="form_super_formulario">
      <input type="hidden" id="super_formulario" name="id" value=""/>
    </form>