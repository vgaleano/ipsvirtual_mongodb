<div class="modal-carga"></div>
<script type="text/javascript">var base_url = "<?php print base_url(); ?>"; </script>
<script type="text/javascript" src="<?php echo base_url();?>js/jquery-1.12.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/scripts.js"></script>
<script src="<?php echo base_url();?>/js/redirectpost.js"></script>
<script src="https://unpkg.com/bootstrap-select@1.13.8/dist/js/bootstrap-select.min.js"></script>
<script type="text/javascript">
	var valor_superformulario = '';
	const BASE_URL = base_url;
    const $_SELECT_PICKER = $('.my-image-selectpicker');
    $_SELECT_PICKER.find('option').each((idx, elem) => {
      const $OPTION = $(elem);
      const IMAGE_URL = $OPTION.attr('data-thumbnail');
      if (IMAGE_URL) {
          $OPTION.attr('data-content', "<img class='img-fluid' width='18px' src='%i'/> %s".replace(/%i/, BASE_URL + IMAGE_URL).replace(/%s/, $OPTION.text()))
      }
      console.warn('option:', idx, $OPTION)
    });
    $_SELECT_PICKER.selectpicker('val', "<?=$this->session->userdata('site_lang')?>");

    $_SELECT_PICKER.on('change', function () {
      var valor = $(this).val();
      ajax('LanguageSwitcher/switchLang/'+valor, {}, function (data) {
      	var ulr_ = data.url;
      	console.log(ulr_.indexOf('perfil'))
      	if (ulr_.indexOf('perfil') !== -1) {
      		$("#form_super_formulario").attr('action', ulr_);
			$("#super_formulario").val(valor_superformulario);
			$("#form_super_formulario").submit();
      	}else{
      		window.location.href = data.url;
      	}
      });
    })
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
       $_SELECT_PICKER.selectpicker('mobile');
    }

    $(document).ready(function(){
    	if ("<?=$this->session->userdata('site_lang')?>"=='') {
    		$_SELECT_PICKER.selectpicker('val', 'spanish');
    	}
    })
		$(document.body).on("click", ".limpiar", function () {
			limpiar();
		});

		$('#encuesta_msj_personalizado').on('change',function(){
			if(document.getElementById("encuesta_msj_personalizado").checked == true){
				$('#encuesta-box_personalizado').css('display','block');
			}else{
				$('#encuesta-box_personalizado').css('display','none');
			}
		});

		$(document.body).on("click", ".mensaje", function () {
		    document.getElementById('nombre_paciente').innerHTML=$(this).attr('data-patient');
		    $('#id_paciente').val($(this).attr('data-id'))
		    $('#sms_entidad').val($(this).attr('data-id_entidad'))
		    $('#sms_telefono').val($(this).attr('data-telefono'))
		    $('#mensaje').modal('show');
		});

		$('#enviar_personalizado').on('click',function(){
	    	/*if(document.getElementById("encuesta_msj_personalizado").checked == true && $('#encuesta_opcion_personalizado').val()==''){
	    		return mensaje('Debe seleccionar una encuesta.');
	    	}*/
	    	if($('#mensaje_personalizado').val()==''){
	    		return mensaje('Debe agregar un mensaje');
	    	}
	    	$('#mensaje').modal('hide');
	    	ajax('mensajeria/personalizado',
		    {   
		        id: $('#id_paciente').val(),
		        mensaje:$('#mensaje_personalizado').val(),
		        //encuesta:$('#encuesta_opcion_personalizado').val(),
		        tipo: 'sms',
		        id_entidad: $('#sms_entidad').val(),
		        telefono: $('#sms_telefono').val()
		    },
		    function(data){
		        if(data.res=="ok"){
		        	mensaje("<?=$this->lang->line('ok_mensaje')?>");
		            limpiar();                                    
		        }else{
		            mensaje(data.msj);
		        }
		    },10000);
		});	

		$(document.body).on("click", ".llamada", function () {
		    document.getElementById('nombre_paciente_voz').innerHTML=$(this).attr('data-patient');
		    $('#id_paciente_voz').val($(this).attr('data-id'))
		    $('#voz_entidad').val($(this).attr('data-id_entidad'))
		    $('#voz_telefono').val($(this).attr('data-telefono'))
		    $('#voz').modal('show');
		});

		$(document.body).on("click", ".whatsapp", function () {
		    document.getElementById('nombre_paciente_voz').innerHTML=$(this).attr('data-patient');
		    $('#id_paciente_whatsapp').val($(this).attr('data-id'))
		    $('#whatsapp_entidad').val($(this).attr('data-id_entidad'))
		    $('#whatsapp_telefono').val($(this).attr('data-telefono'))
		    $('#modal_whatsapp').modal('show');
		});

		$('#enviar_voz_personalizado').on('click',function(){
	    	if($('#voz_personalizado').val()==''){
	    		return mensaje("<?=$this->lang->line('bad_mensaje')?>");
	    	}
	    	$('#voz').modal('hide');
	    	ajax('mensajeria/personalizado',
		    {   
		        id: $('#id_paciente_voz').val(),
		        mensaje:$('#voz_personalizado').val(),
		        tipo: 'llamada',
		        id_entidad: $('#voz_entidad').val(),
		        telefono: $('#voz_telefono').val()
		    },
		    function(data){
		        if(data.res=="ok"){
		        	$('.formLlamada')[0].reset()
		        	mensaje("<?=$this->lang->line('ok_llamada')?>");		            
		        }else{
		            mensaje(data.msj);
		        }
		    },10000);
		});

		$('#enviar_whatsapp_personalizado').on('click',function(){
	    	if($('#whatsapp_personalizado').val()==''){
	    		return mensaje("<?=$this->lang->line('bad_mensaje')?>");
	    	}
	    	$('#modal_whatsapp').modal('hide');
	    	ajax('mensajeria/personalizado',
		    {   
		        id: $('#id_paciente_whatsapp').val(),
		        mensaje:$('#whatsapp_personalizado').val(),
		        tipo: 'Whatsapp',
		        id_entidad: $('#whatsapp_entidad').val(),
		        telefono: $('#whatsapp_telefono').val()
		    },
		    function(data){
		        if(data.res=="ok"){
		        	$('.formWhatsapp')[0].reset()
		        	mensaje("<?=$this->lang->line('ok_whatsapp')?>");		            
		        }else{
		            mensaje(data.msj);
		        }
		    },10000);
		});
		
		   
	 	$('#nuevo_paciente').on('click',function(){
	    	if($('#nombre').val() == ''){
	    		return mensaje("<?=$this->lang->line('bad_nombre_paciente')?>");
	    	}
	    	if($('#apellido').val() == ''){
	    		return mensaje("<?=$this->lang->line('bad_apellido_paciente')?>");
	    	}
	    	if($('#celular').val() == ''){
	    		return mensaje("<?=$this->lang->line('bad_celular_paciente')?>");
	    	}
	    	if($('#tipoDoc_paciente').val() == ''){
	    		return mensaje("<?=$this->lang->line('bad_tipo_doc_paciente')?>");
	    	}
	    	if($('#cedula').val() == ''){
	    		return mensaje("<?=$this->lang->line('bad_cedula_paciente')?>");
	    	}
	    	if($('#estado_paciente').val() == ''){
	    		return mensaje("<?=$this->lang->line('bad_estado_paciente')?>");
	    	}
	    	if($('#municipios_paciente').val() == ''){
	    		return mensaje("<?=$this->lang->line('bad_municipio_paciente')?>");
	    	}
	    	$('#nuevo_usuario').modal('hide');
	    	ajax('pacientes/nuevoPaciente',
		    {   
		        nombre: $('#nombre').val(),
		        apellido:$('#apellido').val(),
		        celular:$('#celular').val(),
		        tipoDoc_paciente:$('#tipoDoc_paciente').val(),
		        cedula:$('#cedula').val(),
		        municipios_paciente:$('#municipios_paciente').val(),
		        estado:$('#estado_paciente').val()
		    },
		    function(data){
		        if(data.res=="ok"){
		        	location.reload();                                   
		        }else{
		        	$('#mensaje').modal('hide');
		            mensaje(data.msj);
		        }
		    },10000);
		});
</script>