	<?php $this->load->view('common/web/head'); ?>
  <link href="<?=base_url();?>css/public/carousel.css" rel="stylesheet" />
</head>
<body class="index">
	<?php $this->load->view('common/web/navbar'); ?>
	<section id="main-slider" class="no-margin">
    <div class="carousel slide">
      <div class="carousel-inner">
        <div class="item active d-flex align-items-center">
          <div class="container">
            <div class="row slide-margin m-0 d-flex align-items-center">
              <div class="col-xs-12 col-md-6 xs-p-0">
                <div class="carousel-content mt-0">
                  <h2 class="animation animated-item-1 m-0"><?=$this->lang->line('text_header1')?></h2>
                  <h1 class="animation animated-item-1 m-0"><?=$this->lang->line('text_header2')?> <img src="<?=base_url()?>img/public/linea_title.png"></h1>
                  <h2 class="animation animated-item-1 m-0"><?=$this->lang->line('text_header3')?></h2>
                  <br>
                  <p class="animation animated-item-2 m-0"><?=$this->lang->line('text_header4')?></p>
                  <a class="btn btn-slide animation animated-item-3" href="#"><?=$this->lang->line('text_header_button1')?></a> <!--<a class="btn btn-slide animation animated-item-3" href="#"><?=$this->lang->line('text_header_button2')?></a>-->
                </div>
              </div>

              <div class="col-xs-12 col-md-6 hidden-xs animation animated-item-4">
                <div class="slider-img">
                  <img src="<?=base_url()?>img/public/img_hear.png" class="img-responsive">
                </div>
              </div>

            </div>
          </div>
        </div>
        <!--/.item-->
      </div>
      <!--/.carousel-inner-->
    </div>
    <!--/.carousel-->
  </section>
  <!--/#main-slider-->

  <div class="feature background-light">
    <div class="container">
      <div class="text-center">
      	<div class="col-xs-12 col-md-12 mb-2">
      		<h2><?=$this->lang->line('text_title_2')?></h2>
      	</div>
        <div class="col-xs-12 col-md-3 p-4">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown d-row justify-content-center align-items-center" data-wow-duration="1000ms" data-wow-delay="300ms">
            <img class="img-responsive m-auto mb-2" src="<?=base_url();?>img/public/ventaja1.png">
            <p><?=$this->lang->line('text_description_2_1')?></p>
          </div>
        </div>
        <div class="col-xs-12 col-md-3 p-4">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown d-row justify-content-center align-items-center" data-wow-duration="1000ms" data-wow-delay="600ms">
            <img class="img-responsive m-auto mb-2" src="<?=base_url();?>img/public/ventaja2.png">
            <p><?=$this->lang->line('text_description_2_2')?></p>
          </div>
        </div>
        <div class="col-xs-12 col-md-3 p-4">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown d-row justify-content-center align-items-center" data-wow-duration="1000ms" data-wow-delay="900ms">
            <img class="img-responsive m-auto mb-2" src="<?=base_url();?>img/public/ventaja3.png">
            <p><?=$this->lang->line('text_description_2_3')?></p>
          </div>
        </div>
        <div class="col-xs-12 col-md-3 p-4">
          <div class="hi-icon-wrap hi-icon-effect wow fadeInDown d-row justify-content-center align-items-center" data-wow-duration="1000ms" data-wow-delay="1200ms">
            <img class="img-responsive m-auto mb-2" src="<?=base_url();?>img/public/ventaja4.png">
            <p><?=$this->lang->line('text_description_2_4')?></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="about position-relative">
    <div class="container">
      <div class="hidden-xs col-md-6 wow fadeInDown position-absolute b-0 l-0 pl-0" data-wow-duration="1000ms" data-wow-delay="300ms">
        <img src="<?=base_url()?>img/public/img_seguimiento.png" class="img-responsive mb-0" />
      </div>
      <div class="col-xs-12 col-xs-offset-0 col-md-offset-6 col-md-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <h2><?=$this->lang->line('text_title_3')?></h2>
        <p><?=$this->lang->line('text_description_3')?></p>
      </div>
    </div>
  </div>

  <div class="opiniones"> <!-- carrousel -->
    <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="bs-carousel">
      <!-- Overlay -->
      <!-- <div class="overlay"></div> -->

      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
        <!-- <li data-target="#bs-carousel" data-slide-to="1"></li>
        <li data-target="#bs-carousel" data-slide-to="2"></li> -->
      </ol>
  
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item slides active">
          <div class="slide-1"></div>
          <div class="hero">
            <hgroup>     
              
                <h2 class="label-white hidden-xs"><p><img src="<?=base_url()?>img/public/comilla.png"><?=$this->lang->line('text_description_4_1')?><img src="<?=base_url()?>img/public/comilla1.png"></p></h2>

                <h2 class="label-white hidden-sm hidden-md hidden-lg"><img src="<?=base_url()?>img/public/comilla.png"><p><?=$this->lang->line('text_description_4_1')?></p><img src="<?=base_url()?>img/public/comilla1.png"></h2>

            </hgroup>
          </div>
        </div>
        <!-- <div class="item slides">
          <div class="slide-2"></div>
          <div class="hero">        
            <hgroup>
                <h1>We are smart</h1>        
                <h3>Get start your next awesome project</h3>
            </hgroup>       
            <button class="btn btn-hero btn-lg" role="button">See all features</button>
          </div>
        </div>
        <div class="item slides">
          <div class="slide-3"></div>
          <div class="hero">        
            <hgroup>
                <h1>We are amazing</h1>        
                <h3>Get start your next awesome project</h3>
            </hgroup>
            <button class="btn btn-hero btn-lg" role="button">See all features</button>
          </div>
        </div> -->
      </div> 
    </div>
  </div>

  <div class="ventajas">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12 col-md-12 text-center mb-4">
          <h2><?=$this->lang->line('text_title_5')?></h2>
          <h3><?=$this->lang->line('text_subtitle_5')?></h3>
        </div>
        <div class="col-xs-12 col-md-6 xs-pr-2"><?=$this->lang->line('text_list_5')?></div>
        <div class="col-xs-12 col-md-6 pr-0 d-flex justify-content-end xs-pt-2">
          <img class="img-responsive mr-0" src="<?=base_url();?>img/public/img_plataforma.png">
        </div>
      </div>
    </div>
  </div>

  <div class="testimonios position-relative">
    <div class="container">
      <div class="row">        
        <div class="col-md-offset-4 col-md-8">
          <h3 class="titulo"><?=$this->lang->line('text_title_6')?></h3>

          <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
              <div class="carousel-caption">
                <img src="<?=base_url()?>img/public/comilla_testimonios.png"><p><?=$this->lang->line('text_description_6_1')?></p>
              </div>
              <a href="#" class="foto-carousel hidden-sm hidden-md hidden-lg">
                <img src="<?=base_url()?>img/public/ana_maria_ospina_galeano.jpg" class="img-responsive" width="50px">
                <br>
                <small>Ana Maria Ospina Galeano</small>
              </a>
            </div>
            <!-- End Item -->
            <!-- <div class="item">
              <div class="carousel-caption">
                <img src="<?=base_url()?>img/public/comilla_testimonios.png"><p><?=$this->lang->line('text_description_6_1')?></p>
              </div>
              <a class="foto-carousel" href="#">
                <img src="<?=base_url()?>img/public/foto2.png">
                <small>Ana Maria Ospina</small>
              </a>
            </div> -->
            <!-- End Item -->
            <!-- <div class="item">
                <div class="carousel-caption">
                  <img src="<?=base_url()?>img/public/comilla_testimonios.png"><p><?=$this->lang->line('text_description_6_1')?></p>
                </div>
                <a class="foto-carousel" href="#">
                  <img src="<?=base_url()?>img/public/foto1.png">
                  <small>Juan Diego Higuera</small>
                </a>
            </div> -->
            <!-- End Item -->
        </div>
        <!-- End Carousel Inner -->
        <ul class="nav nav-pills nav-justified hidden-xs">
            <li data-target="#myCarousel" data-slide-to="0" class="active">
              <a href="#" class="foto-carousel">
                <img src="<?=base_url()?>img/public/ana_maria_ospina_galeano.jpg" class="img-responsive" style="height: 56px;">
                <small>Ana Maria Ospina Galeano</small>
              </a>
            </li>
            <!-- <li data-target="#myCarousel" data-slide-to="1">
              <a class="foto-carousel" href="#">
                <img src="<?=base_url()?>img/public/foto2.png">
                <small>Ana Maria Ospina</small>
              </a></li>
            <li data-target="#myCarousel" data-slide-to="2">
              <a class="foto-carousel" href="#">
                <img src="<?=base_url()?>img/public/foto1.png">
                <small>Juan Diego Higuera</small>
              </a>
            </li> -->
        </ul>
    </div>

        </div>
      </div>
    </div>
  </div>
<!--
  <div class="lates">
    <div class="container xs-p-0">
      <div class="text-center">
        <h2><?=$this->lang->line('text_title_7')?></h2>
      </div>
      <div class="col-md-3 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
        <img width="100%" src="<?=base_url()?>img/public/blog1.png" class="img-responsive" />
        <h4 class="m-0"><?=$this->lang->line('text_blog_title_7_1')?></h4>
        <p class="m-0"><?=$this->lang->line('text_bolg_description_7_1')?></p>
        <p class="d-flex justify-concnet-between border-blue"><span>1 JUNE 2018</span> <span><i class="fa fa-eye"></i> 343</span></p>
      </div>

      <div class="col-md-3 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <img width="100%" src="<?=base_url()?>img/public/blog2.png" class="img-responsive" />
        <h4 class="m-0"><?=$this->lang->line('text_blog_title_7_2')?></h4>
        <p class="m-0"><?=$this->lang->line('text_bolg_description_7_2')?></p>
        <p class="d-flex justify-concnet-between border-blue"><span>1 JUNE 2018</span> <span><i class="fa fa-eye"></i> 343</span></p>
      </div>

      <div class="col-md-3 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
        <img width="100%" src="<?=base_url()?>img/public/blog1.png" class="img-responsive" />
        <h4 class="m-0"><?=$this->lang->line('text_blog_title_7_3')?></h4>
        <p class="m-0"><?=$this->lang->line('text_bolg_description_7_3')?></p>
        <p class="d-flex justify-concnet-between border-blue"><span>1 JUNE 2018</span> <span><i class="fa fa-eye"></i> 343</span></p>
      </div>

      <div class="col-md-3 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
        <img width="100%" src="<?=base_url()?>img/public/blog2.png" class="img-responsive" />
        <h4 class="m-0"><?=$this->lang->line('text_blog_title_7_4')?></h4>
        <p class="m-0"><?=$this->lang->line('text_bolg_description_7_4')?></p>
        <p class="d-flex justify-concnet-between border-blue"><span>1 JUNE 2018</span> <span><i class="fa fa-eye"></i> 343</span></p>
      </div>
    </div>
  </div>
-->
  <section id="conatcat-info">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-3">
          <h3><?=$this->lang->line('text_title_8')?></h3>
        </div>
        <div class="col-xs-12 col-md-6">
          <div class="col-xs-6 col-md-2 text-center xs-left">
            <img src="<?=base_url()?>img/public/icon_sms.png">
            <span><?=$this->lang->line('text_servicio_8_1')?></span>
          </div>
          <div class="col-xs-6 col-md-2 text-center xs-left">
            <img src="<?=base_url()?>img/public/icon_mail.png">
            <span><?=$this->lang->line('text_servicio_8_2')?></span>
          </div>
          <div class="col-xs-6 col-md-2 text-center xs-left">
            <img src="<?=base_url()?>img/public/icon_voz.png">
            <span><?=$this->lang->line('text_servicio_8_3')?></span>
          </div>
          <div class="col-xs-6 col-md-2 text-center xs-left">
            <img src="<?=base_url()?>img/public/icon_CRM.png">
            <span><?=$this->lang->line('text_servicio_8_4')?></span>
          </div>
          <div class="col-xs-6 col-md-2 text-center xs-left">
            <img src="<?=base_url()?>img/public/icon_ERP.png">
            <span><?=$this->lang->line('text_servicio_8_5')?></span>
          </div>
          <div class="col-xs-6 col-md-2 text-center xs-left">
            <img src="<?=base_url()?>img/public/icon_what.png">
            <span class="what"><?=$this->lang->line('text_servicio_8_6')?></span>
          </div>
        </div>
        <div class="col-xs-12 col-md-3 text-center">
          <a href="<?=base_url()?>web/login" class="btn btn-white xs-mt-2"><?=$this->lang->line('text_button_8')?></a>
        </div>
      </div>
    </div>
    <!--/.container-->
  </section>
  <!--/#conatcat-info-->

  <section id="partner">
    <div class="container">
      <div class="center wow fadeInDown">
        <h2><?=$this->lang->line('text_title_9')?></h2>
      </div>
      <div class="row hidden-sm hidden-md hidden-lg">
        <div class="col-xs-12">
          <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="<?=base_url()?>img/public/logos/8.png"></a>
        </div>
        <div class="col-xs-12">
          <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="<?=base_url()?>img/public/logos/9.png"></a>
        </div>
        <div class="col-xs-12">
          <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="<?=base_url()?>img/public/logos/10.png"></a>
        </div>
        <div class="col-xs-12">
          <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="<?=base_url()?>img/public/logos/11.png"></a>
        </div>
        <div class="col-xs-12">
          <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="<?=base_url()?>img/public/logos/12.png"></a>
        </div>
      </div>
      <div class="partners hidden-xs"> <!-- debe ser carousel -->
        <ul>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms" src="<?=base_url()?>img/public/logos/8.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms" src="<?=base_url()?>img/public/logos/9.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="900ms" src="<?=base_url()?>img/public/logos/10.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1200ms" src="<?=base_url()?>img/public/logos/11.png"></a></li>
          <li> <a href="#"><img class="img-responsive wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="1500ms" src="<?=base_url()?>img/public/logos/12.png"></a></li>
        </ul>
      </div>
    </div>
    <!--/.container-->
  </section>
  <!--/#partner-->
 <?php $this->load->view('common/web/footer') ?>
  <?php $this->load->view('common/web/js') ?>

  <script type="text/javascript">
    $(document).ready( function() {
      $('#myCarousel').carousel({
          interval:   4000
      });
      
      var clickEvent = false;
      $('#myCarousel').on('click', '#myCarousel .nav a', function() {
        clickEvent = true;
        $('#myCarousel .nav li').removeClass('active');
        $(this).parent().addClass('active');    
      }).on('slid.bs.carousel', function(e) {
        console.log(clickEvent);
        if(!clickEvent) {
          var count = $('#myCarousel .nav').children().length -1;
          var current = $('#myCarousel .nav li.active');
          console.log(count);
          console.log(current)
          current.removeClass('active').next().addClass('active');
          var id = parseInt(current.data('slide-to'));
          console.log(id)
          if(count == id) {
            $('#myCarousel .nav li').first().addClass('active');  
          }
        }
        clickEvent = false;
      });
    });
  </script>

</body>

</html>