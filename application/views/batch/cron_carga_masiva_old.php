<?php
//sube los datos del paciente
date_default_timezone_set('America/Bogota');
require_once '/var/www/html/ips_virtual_3/application/third_party/simplexlsx.class.php';
$usuario = "dgualdron";
$password = "91532004ab";
$servidor = "apps.ccsmckgi9ggi.us-east-1.rds.amazonaws.com";
$basededatos = "ips_virtual_3";
// creación de la conexión a la base de datos con mysql_connect()
$conexion = mysqli_connect($servidor, $usuario, $password) or die ("No se ha podido conectar al servidor de Base de datos");
$db = mysqli_select_db($conexion, $basededatos) or die ( "Upps! Pues va a ser que no se ha podido conectar a la base de datos" );
// establecer y realizar consulta. guardamos en variable.


$score = "SELECT * FROM score";
$resultado1 = mysqli_query($conexion, $score) or die ( "Algo ha ido mal en la consulta a la base de datos");
$tabla_score = array();
while($row = $resultado1->fetch_array()){
	$temp['fumador'] =$row['fumador'];
	$temp['genero'] =$row['genero'];
	$temp['edad'] =$row['edad'];
	$temp['colesterol'] =$row['colesterol'];
	$temp['tension_arterial'] =$row['tension_arterial'];
	$temp['score'] =$row['score'];
	//$temp['id'] =$row['id'];
	array_push($tabla_score, $temp);
}

$tipo_documento=array();
$consulta = "SELECT * FROM tipo_documento;";
$resultado = mysqli_query($conexion, $consulta) or die ( "Algo ha ido mal en la consulta a la base de datos");
// Bucle while que recorre cada registro y muestra cada campo en la tabla.
while ($columna = mysqli_fetch_array($resultado)){
	$tipo_documento[$columna['tipo_documento']]=$columna['id'];
}

$consulta = "SELECT * FROM procesos where estado = 1 and tipo=1;";
$resultado = mysqli_query($conexion, $consulta) or die ( "Algo ha ido mal en la consulta a la base de datos");
// Bucle while que recorre cada registro y muestra cada campo en la tabla.
while ($columna = mysqli_fetch_array($resultado)){
	$id_documento = $columna['id'];
	$archivotmp = $columna['ruta'];
	$id_entidad  = $columna['id_entidad'];
	$xlsx = new SimpleXLSX($archivotmp);
	
	$consulta1 = "SELECT numero_documento, direccion, municipio FROM pacientes where estado_direccion='1' and id_entidad='".$id_entidad."'";
	$resultado1 = mysqli_query($conexion, $consulta1) or die ( "Algo ha ido mal en la consulta a la base de datos");
	$cedula_existentes = array();
	$info=array();
	while($row = $resultado1->fetch_array()){
		array_push($cedula_existentes, $row['numero_documento']);
		$info[$row['numero_documento']]['direccion']=$row['direccion'];
		$info[$row['numero_documento']]['municipio']=$row['municipio'];
	}
	
	/*exit();*/
	$cons='select numero_identificacion, id_estado, fecha_ult_control from pacientes_estados where id_entidad="'.$id_entidad.'"';
	$resultado1 = mysqli_query($conexion, $cons) or die ( "Algo ha ido mal en la consulta a la base de datos");
	$cedula_estados = array();	
	$info_estados=array();
	while($row = $resultado1->fetch_array()){
		array_push($cedula_estados, $row['numero_identificacion']);
		$info_estados[$row['numero_identificacion']]['id_estado']=$row['id_estado'];
		$info_estados[$row['numero_identificacion']]['fecha_ult_control']=$row['fecha_ult_control'];
	}
	
	if ($xlsx->success()) {
		/*if ($xlsx = SimpleXLSX::parse($archivotmp)){*/
		$arrayNew=$xlsx->rows();
		$grupoHistorico=array();
		$nuevoPaciente=array();

		for ($i=1; $i < count($arrayNew); $i++){
			/*echo "<pre>";
			print_r($arrayNew[$i]);
			echo "</pre>";*/

			if($arrayNew[$i][0] != ""){
				//CALCULO DEL SCORE DE CADA PACIENTE
				//$fumador = 'no';
				$fumador = (isset($arrayNew[$i][83]))?$arrayNew[$i][83]:'NO';
				if($fumador == 'NO'){
					$fumador='0';
				}else{
					$fumador='1';
				}
				$cvd['fumador'] = $fumador;
				//columna sexo
				$genero =$arrayNew[$i][10];
				if($genero == 'F'){
					$cvd['genero'] ='1';
				}else{
					$cvd['genero'] ='2';
				}
				//columna edad
				$edad = intval($arrayNew[$i][19]);
				if($edad >= 40 && $edad <= 49){
					$cvd['edad'] ='1';
				}else if($edad >= 50 && $edad <= 54){
					$cvd['edad'] ='2';
				}else if($edad >= 55 && $edad <= 59){
					$cvd['edad'] ='3';
				}else if($edad >= 60 && $edad <= 64){
					$cvd['edad'] ='4';
				}else if($edad >= 65){
					$cvd['edad'] ='5';
				}
				//columna colesterol total
				$colesterol =round(intval($arrayNew[$i][32]) * 0.026);
				if($colesterol >= 8){
					$colesterol='8';
				}else if($colesterol < 4){
					$colesterol='4';
				}
				$cvd['colesterol'] =$colesterol;
				//columna tensión arterial sistólica
				$tension_arterial =$arrayNew[$i][50];
				if($tension_arterial <= 120){
					$tension_arterial='1';
				}else if($tension_arterial >= 121 && $tension_arterial <= 140){
					$tension_arterial='2';
				}else if($tension_arterial >= 141 && $tension_arterial <= 160){
					$tension_arterial='3';
				}else if($tension_arterial >= 161){
					$tension_arterial='4';
				}
				$cvd['tension_arterial'] =$tension_arterial;
				//$cvd_json=json_encode($cvd);
				// fumador|genero|edad|colesterol|tension
				$cvd_json=implode('|',$cvd);
				//echo $cvd_json;
				$score ='';
				foreach($tabla_score as $t){
					$result=array_diff_assoc($t,$cvd);
					$llaves = array_keys($result);
					if(count($llaves) == 1 && $llaves[0] == 'score'){
						$score = $result['score'];
						break;
					}
				}
				//CALCULO DE FILTRACION, FORMULA Cockcroft-Gault
				$peso = intval($arrayNew[$i][20]);
				$talla = intval($arrayNew[$i][21]);
				$creatinina = floatval($arrayNew[$i][24]);
				if($genero == 'M'){
					if($creatinina != 0){
						$filtracion = round(((140 - $edad ) * $peso) / (72 * $creatinina)); 
					}else{
						$filtracion ='';
					}
				}else{
					if($creatinina != 0){
						$filtracion = round((((140 - $edad ) * $peso) / (72 * $creatinina)) * 0.85); 
					}else{
						$filtracion ='';
					}
					
				}
				//DIABETES
				$diabetes =$arrayNew[$i][16];
				if($diabetes == '1' || $diabetes == '2'){
					$diabetes='1';
				}else{
					$diabetes='0';
				}
				//CALCULO DEL ESTADO
				if($filtracion < 30 || $score >= 10){
					//rojo intenso
					$estado_paciente='1';
				}else if(($filtracion >= 30 && $filtracion <= 59) || ($score >= 5 && $score < 10) || $diabetes == '1'){
					$estado_paciente='2';
				}else if($score >= 1 && $score < 5){
					$estado_paciente='3';
				}else if($score < 1){
					$estado_paciente='4';
				}
				//CALCULO DE LDL
				$colesterolTotal = intval($arrayNew[$i][32]);
				$hdl = intval($arrayNew[$i][34]);
				$tg = intval($arrayNew[$i][38]);
				$ldl_calculado = $colesterolTotal - ($hdl + ($tg / 5));
				$ldl_calculado = round($ldl_calculado);
				$direccion=(isset($arrayNew[$i][82]))?$arrayNew[$i][82]:'';
				//busco cedulas nuevas en el excel
				if(!in_array($arrayNew[$i][8],$cedula_existentes)){
					//print $arrayNew[$i][4];
					if($arrayNew[$i][4] =='NONE'){
						$arrayNew[$i][4]='';
					}
					if ($arrayNew[$i][7]!='' && array_key_exists($arrayNew[$i][7], $tipo_documento)==true) {
						$arrayNew[$i][7]=$tipo_documento[$arrayNew[$i][7]];
					}
					//(nombre, apellido,id_tipo_documento,numero_documento, celular, activo, direccion, imagen, municipio, ips, latitud, longitud, fecha_nacimiento, sexo)
					$nuevoPaciente[$i-1]= '("'.$arrayNew[$i][3].' '.$arrayNew[$i][4].'","'.$arrayNew[$i][5].' '.$arrayNew[$i][6].'","'.$arrayNew[$i][7].'","'.$arrayNew[$i][8].'","'.$arrayNew[$i][12].'","1","'.$direccion.'","https://s3.amazonaws.com/ipsvirtual/default.png","'.$arrayNew[$i][11].'","'.$arrayNew[$i][2].'","","","'.$arrayNew[$i][9].'","'.$arrayNew[$i][10].'","'.$id_entidad.'","1")';
				}else{					
					$estado_direccion='0';
					if ($direccion!=$info[$arrayNew[$i][8]]['direccion'] || $arrayNew[$i][11]!=$info[$arrayNew[$i][8]]['municipio']) {
						$estado_direccion='1';
					}
					$sql3="UPDATE pacientes SET celular = '".$arrayNew[$i][12]."', direccion = '".$direccion."', municipio ='".$arrayNew[$i][11]."', estado_direccion='".$estado_direccion."' WHERE numero_documento='".$arrayNew[$i][8]."';";
					insertarRegistros($sql3,$id_documento,$archivotmp,$conexion);
				}
				//aqui hay que agregarle el estado y meterlo en el temp
				$temp ='("'.$cvd_json.'","'.$fumador.'",'.$arrayNew[$i][8].','.$estado_paciente.',"'.$score.'","'.$filtracion.'","'.$ldl_calculado.'",';
				//print $score;

				$myDateTime = DateTime::createFromFormat('d/m/Y', $arrayNew[$i][23]);
				$newDateString = date('Y-m-d',$myDateTime);


				/*print $arrayNew[$i][8].' '.$newDateString.' '.$arrayNew[$i][23];
				echo '<br>';*/

				for ($j=13; $j < 65 ; $j++) { 
					//condiciones de celdas eliminadas en el excel
					if($j < 40 && ($j != 30 && $j!= 31) || $j > 49 && ($j != 30 && $j!= 31)){
						if($j !=64 ){
							$temp .='"'.$arrayNew[$i][$j].'",';

						}else if($j == 64){
							$temp .='"'.$arrayNew[$i][$j].'")';
						}
					}
				} 
				$grupoHistorico[$i-1] = $temp;
				/*echo '<pre>';
				print_r($temp);
				echo '</pre>';*/
			}
		}
		$grupoFinal = join(",",$grupoHistorico);
		$grupoNuevos = join(",",$nuevoPaciente);
		$sql="INSERT INTO historico_datos_paciente (calculo,fumador,numero_identificacion,id_estado,score,filtracion_glomerular,ldl_calculado, ingreso_programa_nefroproteccion,diagnostico_confirmado_hta,fecha_diagnostico_hta,diagnostico_diabetes, fecha_diagnostico_diabetes, etiologia_erc, edad, peso, talla, imc, fecha_ult_control, creatinina, fecha_creatinina, hemoglobina_glicosilaba, fecha_hemoglobina, albuminuria, fecha_albuminuria, colesterol_total, fecha_colesterol, hdl, fecha_hdl, ldl, fecha_ldl, triglicerios, fecha_triglicerios, tension_arterial_sistolica, tension_arterial_diastolica, tasa_filtracion_glomerular, estadio_erc, inhibidor_enzima_ieca, receptores_araii, diagnostico_renal_cronica, fecha_erc_estudio_5, programa_atencion_erc, dialisis, fecha_dialisis, nefroproteccion, fecha_nefroproteccion, transplantes, glucometro_casa) VALUES ".$grupoFinal;
		//print $sql;
		insertarRegistros($sql,$id_documento,$archivotmp,$conexion);
		if($grupoNuevos != ""){
			$sql2="INSERT INTO pacientes (nombre, apellido,id_tipo_documento,numero_documento, celular, activo, direccion, imagen, municipio, ips, latitud, longitud, fecha_nacimiento, sexo, id_entidad, estado_direccion) VALUES ".$grupoNuevos;
			insertarRegistros($sql2,$id_documento,$archivotmp,$conexion);
		}		
		$sql4="UPDATE procesos set estado='0' where id='".$id_documento."';";
		insertarRegistros($sql4,$id_documento,$archivotmp,$conexion);

		//se calcula la cantidad de pacientes que hay para cada estado
		$sql='SELECT historico_datos_paciente.id_estado, historico_datos_paciente.numero_identificacion,historico_datos_paciente.fecha_ult_control FROM historico_datos_paciente left join pacientes on pacientes.numero_documento=historico_datos_paciente.numero_identificacion where pacientes.id_entidad="'.$id_entidad.'" GROUP BY historico_datos_paciente.numero_identificacion HAVING COUNT(historico_datos_paciente.id) > 0 order by historico_datos_paciente.fecha_registro  desc;';
		$resultado = mysqli_query($conexion, $sql);
		//inserta o actualiza los estados del paciente
		while ($columna = mysqli_fetch_array($resultado)){
			if(!in_array($columna['numero_identificacion'],$cedula_estados)){
				//nuevo
				$sql1='INSERT INTO pacientes_estados (id, numero_identificacion, id_estado, id_entidad, fecha_ult_control) VALUES (NULL,"'.$columna['numero_identificacion'].'", "'.$columna['id_estado'].'", "'.$id_entidad.'", "'.$columna['fecha_ult_control'].'");';
	    		mysqli_query($conexion, $sql1);
			}else{
				//existe
				if ($columna['id_estado']!=$info_estados[$columna['numero_identificacion']]['id_estado'] || $columna['fecha_ult_control']!=$info_estados[$columna['numero_identificacion']]['fecha_ult_control']) {
					$sql1='UPDATE pacientes_estados set id_estado="'.$columna['id_estado'].'", fecha_ult_control="'.$columna['fecha_ult_control'].'"  where numero_documento="'.$columna['numero_identificacion'].'"';
		     		mysqli_query($conexion, $sql1);
				}
	    		
			} 
		}
			
		//hace el conteo
		$sql7='Delete FROM colores_cantidad where id_entidad="'.$id_entidad.'"';
		mysqli_query($conexion, $sql7);
		$sql5='SELECT count(id_estado) as total, id_estado from pacientes_estados WHERE id_entidad="'.$id_entidad.'" group by id_estado';
		$resultado = mysqli_query($conexion, $sql5);
		while ($columna = mysqli_fetch_array($resultado)){
			$sql6='INSERT INTO colores_cantidad (id, id_estado, total, id_entidad) values (null, "'.$columna['id_estado'].'", "'.$columna['total'].'", "'.$id_entidad.'")';
			mysqli_query($conexion, $sql6);
		}
	}
}


function insertarRegistros($sql,$id_documento,$archivotmp,$conexion){
	//insertar los registros
	$runcheck = mysqli_query($conexion, $sql);
	if (!$runcheck) {
		echo $sql;
		die('No se pudo hacer la carga masiva: '.mysql_error());
	}else{
		//print 'ok';
	}
}


/*echo '<pre>';
print_r($grupoFinal);
echo '</pre>';*/

    
mysqli_close( $conexion );



