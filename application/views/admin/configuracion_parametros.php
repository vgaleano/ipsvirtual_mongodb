<?php $this->load->view("common/admin/head")?>		
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/typeahead.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12 no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('config_parametro_1')?></h2>
	    			</div>
	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
							<form id="formEntidad" action="<?=base_url();?>admin/configuracionParametros" method="POST" onsubmit="return false">
								<div class="input-group">
					      			<div class="input-group-addon">
					      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
				      				</div>
									<select class="form-control" name="searchentidad">
								    	<?php $ent = $this->session->userdata('searchentidad'); if(!$ent) $ent=""; ?>
								        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
								        <?php foreach ($entidad as $r) { ?>
											<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
										<?php } ?>
								    </select>
							    </div>																		
							</form>
						<?php } ?>
						<form id="formSystem" action="<?php echo base_url(); ?>admin/configuracionParametros" method="post">
			                <div class="input-group ml-1">
			                    <!-- USE TWITTER TYPEAHEAD JSON WITH API TO SEARCH -->
			                    <?php $search = $this->session->userdata('searchSystem'); if(!$search) $search=""; ?>
			                    <input class="form-control" id="system-search" name="searchSystem" placeholder="<?=$this->lang->line('config_parametro_2')?>" value="<?php echo $search; ?>" required>
			                    <span class="input-group-btn">
			                        <button type="submit" class="btn btn-blue btn-search"><i class="glyphicon glyphicon-search"></i></button>
			                    </span>
			                </div>
			            </form>
						<button class="btn btn-green ml-1" onclick="addRow()"><i class="fa fa-plus"></i> <?=$this->lang->line('config_parametro_3')?></button>
					</div>
	    		</div>

	    		<div class="row  mt-20">
	    			<div class="col-xs-12 col-md-12">

		    			<div class="table-responsive table-color">
		    				<table class="table table-hover table-bordered text-center table-list-search">
								<thead>
									<tr>
										<th width="15%"><?=$this->lang->line('config_parametro_4')?></th>
										<th width="15%"><?=$this->lang->line('config_parametro_5')?></th>
										<th width="30%"><?=$this->lang->line('config_parametro_6')?></th>
										<th width="30%"><?=$this->lang->line('config_parametro_7')?></th>
										<th width="10%"><?=$this->lang->line('acciones')?></th>
									</tr>
								</thead>
								<tbody class="addNew">
									<?php if (!is_null($id_entidad)) {
										if(isset($columnas_configuracion) && count($columnas_configuracion) > 0) {
										foreach ($columnas_configuracion as $key => $c_v) { ?>
										<tr>
											<td>
												<?php  if ($c_v->required=='1') {
													//echo "----";
													echo $c_v->name; ?>
													<input type="hidden" name="name_parametro" data-id="<?php echo $c_v->_id; ?>" value="<?php echo $c_v->name; ?>" data-obligatorio="<?php echo $c_v->required; ?>">
												<?php }else{
													$name_generico = (isset($c_v->name_generico)) ? $c_v->name_generico : ''; ?>
													<input class="form-control" placeholder="<?=$this->lang->line('config_parametro_4')?>" name="name_generico" data-id="<?=$c_v->_id;?>" value="<?=$name_generico;?>" data-obligatorio="<?=$c_v->required?>">
												<?php } ?>
											</td>
											<td><?php if ($c_v->required=='1') { 
												echo $c_v->name; ?>
												<input type="hidden" name="name_parametro" data-id="<?php echo $c_v->_id; ?>" value="<?php echo $c_v->name; ?>" data-obligatorio="<?php echo $c_v->required; ?>">
											<?php }else{ ?>
												<input class="form-control" type="" name="name_parametro" placeholder="<?=$this->lang->line('config_parametro_5')?>" data-id="<?php echo $c_v->_id; ?>" value="<?php echo $c_v->name; ?>" data-obligatorio="<?php echo $c_v->required; ?>">
											<?php } ?>	
											</td>
											<td>
												<div  class="typeahead">
													<input type="text" class="form-control typeahead-input tags_auto" name="tags_auto" placeholder="<?=$this->lang->line('config_parametro_8')?>">
												</div>	
											</td>
											<td>
												<div class="divtags">
													<?php if (count($c_v->columns)>0) { 
													foreach ($c_v->columns as $key_aa => $aa) { ?>
													 	<div class="btn  tagspan tag<?=$key_aa; ?> deleteTag" data-toggle="bottom" title="<?=$this->lang->line('config_parametro_9')?>" id="tag<?=$key_aa; ?>"><?=$aa;?></div>
													 	<input class="tag<?=$key_aa; ?>" type="hidden" data-id="<?=$key_aa; ?>" name="tag" value="<?=$aa;?>">
													<?php }} ?>
												</div>											
											</td>
											<td>
												<button class="btn btn-eraser mx-05 borrar"><i class="fa fa-eraser"></i></button>
												<?php if ($c_v->required=='0') { ?>
													<button class="btn btn-danger mx-05 eliminar"><i class="fa fa-trash"></i></button>
												<?php } ?>
												<button class="btn btn-green guardarTag mx-05"><i class="fa fa-save"></i></button>
											</td>
										</tr>
										<?php }}else{ ?>
											<tr>
												<td class="search-sf" colspan="4"><?=$this->lang->line('config_parametro_10')?></td>
											</tr>	
										<?php }}else{ ?>
										<tr>
											<td colspan="4"><?=$this->lang->line('config_parametro_11')?></td>
										</tr>
									<?php }?>						
								</tbody>										
							</table>
	    				</div>

	    				<div class="col-md-12" style="text-align:center">
							<nav aria-label="Page navigation">
						        <ul class="pagination">
						            <?php echo $this->pagination->create_links(); ?>
						        </ul>
				            </nav>
					    </div>
		    		</div>
		    	</div>
			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript" src="<?=base_url(); ?>js/typeahead.js"></script>
		<script  type="text/javascript" language="javascript">
			//https://stackoverflow.com/questions/28445829/working-on-dynamically-added-input-elements-with-typeahead
			//autocompletar tags
			var array_tags = new Array(),
			codigosT = new Array(),
			array_entidad = <?php echo json_encode($entidad); ?>;

			//eliminar el tag en el front
			$('body').on('click','.deleteTag', function() {
				var id_t = $(this).attr('id'),
				value_tag = $('input.'+id_t).val(),	
				result = array_tags.filter(tag => tag != value_tag);
				array_tags=result;
				$('.'+id_t).remove();
			});
			//borrar los datos en el front
			$('body').on('click','.borrar', function() {
				if ($(this).parent().siblings().siblings().children('[name=name_parametro]').attr('data-obligatorio')=='0') {
					$(this).parent().siblings().siblings().children('[name=name_parametro]').val('');
				}
				//elimina todos los tags y los libera
				$(this).parent().siblings().children('.divtags').children('div').each(function(){
		    		var id_t = $(this).attr('id'),
		    		value_tag = $(this).html(),	
					result = array_tags.filter(tag => tag != value_tag);
					array_tags=result;
					$('.'+id_t).remove();
		    	});
			})
			//eliminar en db - boton eliminar
			$('body').on('click','.eliminar', function() {
				var id = $(this).parent().siblings().siblings().children('[name=name_parametro]').attr('data-id');
				var aa = $(this).parent().parent();
				//deleteTag
				var btnnames = {
                    ok : "Aceptar",
                    cancel : "Cancelar"
                }
                confirmar("<?=$this->lang->line('confirm_parametro')?>",function(){
                   	ajax('indicadores/eliminarTag',
	                {   
	                	id: id
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	aa.remove();
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
	            },false,btnnames);
			});

			$('body').on('click','.guardarTag', function() {
				var id_parametro = $(this).parent().siblings().siblings().children('[name=name_parametro]').attr('data-id'),
				obligatorio_parametro = $(this).parent().siblings().siblings().children('[name=name_parametro]').attr('data-obligatorio'),
				tags_select = new Array(),
				bandera = 0,
				name_generico = $(this).parent().siblings().siblings().siblings().children('[name=name_generico]').val(),
				name_parametro = $(this).parent().siblings().siblings().children('[name=name_parametro]').val(),
				eso=$(this);
				if (name_parametro == '') {
					bandera = 1;
					mensaje("<?=$this->lang->line('parametro_bad_1')?>");
				}
				$(this).parent().siblings().children('.divtags').children('input').each(function(){
		    		tags_select.push($(this).val());
		    	});
		    	var aa = $(this).parent().children('.eliminar'),
		    	ab = $(this).parent().siblings().children('.divtags').children('.tagspan'),
		    	cc = $(this).closest('tr').find('[name=name_parametro]');
		    	if (tags_select.length==0) {
		    		bandera = 1;
					mensaje("<?=$this->lang->line('parametro_bad_1')?>");
		    	}
		    	var name_entidad;
		    	for (var i = 0; i < array_entidad.length; i++) {
		    		if (array_entidad[i]._id.$oid == '<?php echo $id_entidad;?>') {
		    			name_entidad = array_entidad[i].name;
		    		}
		    	}

				if (bandera==0) {
					ajax('indicadores/guardarTag',
	                {   
	                	id: id_parametro,
	                	name: name_parametro,
	                	tags_select: tags_select,
	                	obligatorio: obligatorio_parametro,
	                	id_entidad: '<?php echo $id_entidad;?>',
	                	name_entidad: name_entidad,
                		name_generico: name_generico
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	aa.removeClass('hidden');
	                    	ab.removeClass('not_active');
	                    	ab.removeClass('pulse');
	                    	eso.removeClass('pulse');
	                    	cc.attr('data-id', data.id);
	                    	mensaje("<?=$this->lang->line('mensaje_ok_parametros')?>");                                 
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
				}
			});
			
			$('.tags_auto').typeahead({
				hint: true,
				highlight: true,
				minLength: 1,
			},{
				limit: 12,
				async: true,
				source: function(query, processSync, processAsync){
					return $.ajax({
						url: "<?=base_url();?>indicadores/verColumna",
						type: 'POST',
						data:{
							tags_auto: query, //texto
							tags_seleccionados: array_tags,
							id_entidad: '<?php echo $id_entidad;?>'
						},
						dataType: 'JSON',
						success: function(data){
							$(document).ajaxStop(function(){
								$("body").removeClass("loading");
							});
							if (data.res === "ok") {
								var tags_auto = [];
								for (var i = 0; i < data.bar.length; i++) {
									tags_auto.push(data.bar[i].name);
									codigosT[data.bar[i].name]=data.bar[i]._id.oid;
								}
								return processAsync(tags_auto);		                    		                     
							}else{
								//let muestra5 = $('#muestra5');
								//muestra5.html(data.msj);		                 	
							}
						}
					});
				}
			});

			//seleccionan un sugerencia
			$('.tags_auto').bind('typeahead:select', function(ev, suggestion) {
				var str=suggestion;				
				$(this).closest('td').next().children('div').append('<div class="btn  tagspan tag'+codigosT[suggestion]+' deleteTag not_active pulse" data-toggle="bottom" title="Eliminar tag" id="tag'+codigosT[suggestion]+'">'+suggestion+'</div>');
				$(this).closest('td').next().children('div').append('<input class="tag'+codigosT[suggestion]+'" '.concat(' type="hidden" data-id="'+codigosT[suggestion]+'" name="tag" value="'+suggestion+'">'));
				$(this).closest('td').next().next().children('.guardarTag').addClass('pulse');
				array_tags.push(suggestion);
				$(this).typeahead('val','');
			});				
					
			//agrega una fila a la tabla
			var hijos=0;
			function addRow() {				
				$('.addNew').append('<tr><td><input class="form-control" placeholder="<?=$this->lang->line('config_parametro_4')?>" name="name_generico" data-id=""  data-obligatorio="0"></td><td><input class="form-control" type="" name="name_parametro" placeholder="<?=$this->lang->line('config_parametro_5')?>" data-obligatorio="0" data-id=""></td><td><div class="typeahead"><input type="text" class="form-control typeahead-input tags_auto" name="tags_auto[]" id="hijoTag'+hijos+'" placeholder="<?=$this->lang->line('config_parametro_8')?>"></div></td><td><div class="divtags"></div></td><td><button class="btn btn-eraser mx-05 borrar"><i class="fa fa-eraser"></i></button><button class="btn btn-danger mx-05 eliminar hidden"><i class="fa fa-trash"></i></button><button class="btn btn-green guardarTag mx-05"><i class="fa fa-save"></i></button></td></tr>');
				$('#hijoTag'+hijos).typeahead({
					hint: true,
					highlight: true,
					minLength: 1,
				},{
					limit: 12,
					async: true,
					source: function(query, processSync, processAsync){
						return $.ajax({
							url: "<?=base_url();?>indicadores/verColumna",
							type: 'POST',
							data:{
								tags_auto: query,
								tags_seleccionados: array_tags,
								id_entidad: '<?php echo $id_entidad;?>'
							},
							dataType: 'JSON',
							success: function(data){
								if (data.res === "ok") {
									var tags_auto = [];
									for (var i = 0; i < data.bar.length; i++) {
										tags_auto.push(data.bar[i].name);
										codigosT[data.bar[i].name]=data.bar[i]._id.oid;
									}
									return processAsync(tags_auto);		                    		                     
								}else{
									//let muestra5 = $('#muestra5');
									//muestra5.html(data.msj);		                 	
								}
							}
						});
					}
				});
				$('#hijoTag'+hijos).bind('typeahead:select', function(ev, suggestion) {
					var str=suggestion;
					$(this).typeahead('val','');
					$(this).closest('td').next().children('div').append('<div class="btn  tagspan tag'+codigosT[suggestion]+' deleteTag not_active pulse" data-toggle="bottom" title="<?=$this->lang->line('config_parametro_9')?>" id="tag'+codigosT[suggestion]+'">'+suggestion+'</div>');
					$(this).closest('td').next().children('div').append('<input class="tag'+codigosT[suggestion]+'" '.concat(' type="hidden" data-id="'+codigosT[suggestion]+'" name="tag" value="'+suggestion+'">'));
					$(this).closest('td').next().next().children('.guardarTag').addClass('pulse');
					array_tags.push(suggestion);
				});
				hijos++;
			}

			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

			$(document).ready(function(){

				var total = parseInt('<?php echo $columnas; ?>');
		    	if (total==0) {
					$('.box-btn').find('input, textarea, button, .btn').attr('disabled','true');
					$('.table').find('input, textarea, button, .btn').attr('disabled','true');
					mensaje("<?=$this->lang->line('parametro_bad_4')?>");
				}

				//agregar las columnas relacionadas al array_tags
				array_tags = <?=json_encode($tags_seleccionados);?>;
				/*var columnas_configuracion = <?php echo json_encode($columnas_configuracion); ?>;
				if (columnas_configuracion.length>0) {
					for (var i = 0; i < columnas_configuracion.length; i++) {
						if (columnas_configuracion[i].length>0) {
							Object.assign(array_tags, array_tags, columnas_configuracion[i]);
						}
					}
				}*/

			    //something is entered in search form
			    /*$('#system-search').keyup( function() {
			       	var that = this;
			        // affect all table rows on in systems table
			        var tableBody = $('.table-list-search tbody');
			        var tableRowsClass = $('.table-list-search tbody tr');
			        $('.search-sf').remove();
			        tableRowsClass.each( function(i, val) {
			            //Lower text for case insensitive
			            var rowText = $('.table-list-search tbody tr').eq(i).children().children('input').val();//$(val).text().toLowerCase();
			            var inputText = $(that).val().toLowerCase();
			            if (rowText!==undefined) {
			            	rowText = rowText.toLowerCase().toString();
				            if(inputText != '') {
				                $('.search-query-sf').remove();
				                tableBody.prepend('<tr class="search-query-sf"><td colspan="3"><strong>Búsqueda por: "'
				                    + $(that).val()
				                    + '"</strong></td></tr>');
				            }else {
				                $('.search-query-sf').remove();
				            }
			            	if( rowText.indexOf( inputText ) == -1 ) {
				                //hide rows
				                tableRowsClass.eq(i).hide();				                
				            } else {
				                $('.search-sf').remove();
				                tableRowsClass.eq(i).show();
				            }
			            }else{
			            	 tableRowsClass.eq(i).hide();
			            }				            
			        });
			        //all tr elements are hidden
			        if(tableRowsClass.children(':visible').length == 0) {
			        	document.getElementById('formSystem').submit();
			            //tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="3">Busqueda no encontrado</td></tr>');
			        }
			    });*/
			});
			
		</script>
	</body>
</html>