<?php $this->load->view("common/admin/head")?>
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
    	<div class="col-md-12 principal-padding" >

    		<div class="row">
				<div class="col-xs-12 col-md-6">
    				<h2>Ubicación geográfica de los pacientes</h2>
    			</div>
    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
					<div class="input-group">
		      			<div class="input-group-addon">
		      				<label class="mb-0">Entidad:</label>
	      				</div>
						<select class="form-control" name="searchentidad">
					    	<?php $ent = $this->session->userdata('searchentidad');  if(!$ent) $ent=""; ?>
					        <option value="" style="display: none;"> -Selecione uno- </option>
					        <?php foreach ($entidad as $r) { ?>
								<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
							<?php } ?>
					    </select>
				    </div>							
					<?php } ?>
					<div class="input-group ml-1">
		      			<div class="input-group-addon">
		      				<label class="mb-0">Estado:</label>
	      				</div>									
						<select class="form-control" id="filtro_estado">
							<?php $est=$this->session->userdata('searchestado'); if(!$est) $est=""; ?>
							<option value="" style="display: none;">- seleccione una opción-</option>
                  			<?php foreach ($colores as $s) { ?>
                     			<option value="<?=$s?>" <?=($s==$est)? 'selected':''; ?>><?=$s?></option>
                     		<?php } ?>
						</select>
					</div>
					<button type="button" class="btn btn-green ml-1" id="filtrar">FILTRAR</button>
    			</div>
			</div>

			<div class="row mt-20 mx-0" width="100%">
				<div class="col-xs-12 col-md-12 box-shadow">
					<div class="row" style="padding-bottom: 20px">
						<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 box-info" style="text-align: center;">
							 <span style="font-size: 40px" id="numAll"><?php echo (is_array($allPatient))?$allPatient[0]->total:'0';?> </span><br><span>Total Pacientes</span>
						</div>
						<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 box-info" style="text-align: center;">
		            		<span id="numPb" style="font-size: 40px"></span><br><span id="pb-estado"></span>
						</div>
					</div>
					<div class="row" style="padding-top: 20px">
						<div class="col-md-12 ubicacion no-padding">
							<div id="map"></div>
						</div>
					</div>
				</div>
				
			</div>

    </div>
			
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
    	<script async defer src=""></script>
		<script  type="text/javascript" language="javascript">	   
			
	        var map, heatmap;
	        var id_entidad = '<?php echo $id_entidad; ?>';
	        var rojo_intenso = [
				          'rgba(152, 4, 3, 0)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)',
				          'rgba(152, 4, 3, 1)'
				        ],
				rojo = [
						  'rgba(229, 51, 51, 0)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)',
				          'rgba(229, 51, 51, 1)'
						],
				amarilla =[
						  'rgba(255, 255, 0, 0)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)',
				          'rgba(255, 255, 0, 1)'
						],
				verde =[
						  'rgba(0, 128, 0, 0)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)',
				          'rgba(0, 128, 0, 1)'
						]

	        function initMap(p){
	        	if(p == undefined){
	                map = new google.maps.Map(document.getElementById('map'), {
			            zoom: 14, 
			            center: {lat: 7.121852, lng: -73.114753},
			            mapTypeControl: false,
			        });              
			        //getPoints()
	           	}else{		          
		          //getPoints(p)
	     		}
	        }

	        function getPoints(p) {
	           	if(p == undefined){
	                 return []
	           	}else{
	           		let seleccion = $("#filtro_estado option:selected").text();

	             	ajax('Pacientes/poblacionMapaDeCalor',
			        {   
			            estado: p,
			            entidad: id_entidad
			       	},
			        function(data){
			            if(data.res=="ok"){			            	
			            	var estado_='';
			            	
			            	document.getElementById('numAll').innerHTML=data.all;
			             	data1 = data.msj;
						   	console.log(data1.length);
						    if (p!='0') {
						    	console.log('total',data1[0]['total']);
						    	document.getElementById('numPb').innerHTML=data1[0]['total'];
						    	document.getElementById('pb-estado').innerHTML='Total Pacientes '+seleccion;
						    }
						    
						    for (var j = 0; j < data1.length; j++) {
						    	if (data1[j]['estado'] == '1') {
			            		estado_=rojo_intenso;
				            	}else if(data1[j]['estado'] == '2'){
				            		estado_=rojo;
				            	}else if(data1[j]['estado'] == '3') {
				            		estado_=amarilla;
				            	}else{
				            		estado_=verde;
				            	}
				            	var range = data1[j]['datos'].length;
				            	data2 = [];
				            	for (i = 0; i < range; i++) {
									data2[i]= new google.maps.LatLng(parseFloat(data1[j]['datos'][i].lat), parseFloat(data1[j]['datos'][i].lon));
							    }
							    var dir = new google.maps.LatLng(7.121852, -73.114753);
							    map = new google.maps.Map(document.getElementById('map'), {
						            zoom: 14, 
						            center: dir,
						            mapTypeControl: false,
						        });
	    						heatmap = new google.maps.visualization.HeatmapLayer({
						            data: data2,
						            dissipating: 1,
						            gradient: estado_,
							        radius:25
						        });
						        //console.log(data2);
						        heatmap.setMap(map);
						    }
							    					        
		                }else{
		                	document.getElementById('numAll').innerHTML=data.all;
		                    mensaje(data.msj)
		                    document.getElementById('numPb').innerHTML=''
		                    document.getElementById('pb-estado').innerHTML=''
		                }
		            },10000);
               	}
	        }

	        $('#filtrar').on('click',function(){
	        	var estado = $('#filtro_estado').val(),
	        	bandera = 0;	        	
	        	if (estado=='') {
	        		bandera=1;
	        		mensaje('Debe seleccionar el estado de pacientes.');
	        	}
	        	<?php if ($this->session->userdata('id_rol') == "1"){ ?>	        		
	        		if ($('[name=searchentidad]').val()=='0') {
	        			bandera=1;
	        			mensaje('Debe seleccionar una entidad.');
	        		}else{
	        			id_entidad=$('[name=searchentidad]').val();
	        		}
	        	<?php } ?>
	        	if (bandera==0) {
	        		initMap(estado);
	        	}	        		
	        });

		    $(document).ready(function(){
		    });
			
		</script>
	</body>
</html>