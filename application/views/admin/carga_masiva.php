	<?php $this->load->view("common/admin/head")?>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/dashboard.css">
</head>
<body>
	<?php $this->load->view("common/admin/nav")?>
	<div class="col-md-12  col-lg-12 no-padding" id="wrapper-principal">
		<div class="col-md-12 principal-padding">

			<div class="row">
    			<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('titulo_masiva')?></h2>
    			</div>
			</div>

			<div class="row  mt-20">
				<div class="col-xs-12 col-md-6">
					<div class="col-xs-12 col-md-12 box-shadow">
						<h4><?=$this->lang->line('subtitulo_masiva')?></h4>
						<form id="formpacientes" action="<?php echo base_url();?>batch/cargarPacientes" method="POST" enctype="multipart/form-data" onsubmit="return false" target="my_iframe_pacientes">
							<div class="col-md-12 no-padding">
								<?php if ($this->session->userdata('id_rol') != "5e3307a1fd61ee8807387411") { ?>
									<input type="hidden" name="id_entidad" value="<?=$this->session->userdata('id_entidad'); ?>">
								<?php }else{ ?>
									<div class="form-group">
										<label><?=$this->lang->line('entidad')?></label>
									    <select class="form-control" name="id_entidad">
									    	<?php $ent = $this->session->userdata('id_entidad'); if(!$ent) $ent=""; ?>
									        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									        <?php foreach ($entidad as $r) { ?>
												<option value="<?= $r->_id; ?>" <?= ($r->_id==$ent)? 'selected':''; ?>><?= $r->name ?></option>
											<?php } ?>
									    </select>										    
									</div>
								<?php } ?>
									
								
								<div class="form-group">
									<label><?=$this->lang->line('pacientes_masivo')?></label><br>
									<small><?=$this->lang->line('adjunte_excel')?></small>
									<input type="file" name="pacientes" id="pacientes" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style="font-size:14px">
								</div>
								<div class="col-md-12 text-center">
								<button class="btn btn-green" id="cargar_pacientes"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;<?=$this->lang->line('btn_cargar')?> </button>
								</div>
							</div>
						</form>
					</div>							
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="col-xs-12 col-md-12 box-shadow">
						<div class="col-xs-12 col-sm-6 col-md-4 box-title no-padding">
							<h4><?=$this->lang->line('recordatorio_masivo')?></h4>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-8 no-padding">
							<a href="https://s3.amazonaws.com/ipsvirtual/Formato_sms_pacientes.xlsx" class="btn btn-green pull-right"><i class="fa fa-file" aria-hidden="true"></i> &nbsp;<?=$this->lang->line('descargar_formato')?></a>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12 no-padding">
							<form id="formfile" action="<?= base_url();?>batch/cargarSmsPacientes" method="POST" enctype="multipart/form-data" onsubmit="return false" target="my_iframe">
								<?php if ($this->session->userdata('id_rol') != "5e3307a1fd61ee8807387411") { ?>
									<input type="hidden" name="id_entidad" value="<?=$this->session->userdata('id_entidad'); ?>">
								<?php }else{ ?>
									<div class="form-group">
										<label><?=$this->lang->line('entidad')?></label>
									    <select class="form-control" name="id_entidad">
									    	<?php $ent = $this->session->userdata('id_entidad'); if(!$ent) $ent=""; ?>
									        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									        <?php foreach ($entidad as $r) { ?>
												<option value="<?=$r->_id;?>" <?=($r->_id==$ent)? 'selected':''; ?>><?=$r->name ?></option>
											<?php } ?>
									    </select>
									</div>
								<?php } ?>
								<div class="form-group">
									<label><?=$this->lang->line('cargar_recordatorios')?></label><br>
									<small><?=$this->lang->line('cargar_archivo')?></small>
									<input type="file" name="file" id="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
								</div>
								<div class="col-md-12 text-center">
								<button class="btn btn-green" id="cargar_sms"><i class="fa fa-arrow-up" aria-hidden="true"></i>&nbsp;<?=$this->lang->line('btn_cargar')?> </button>
								</div>
							</form>
						</div>
					</div>							
				</div>
			</div>

		</div>
	</div>
	<iframe id='my_iframe_pacientes' name='my_iframe_pacientes' class="hidden" src="" style="width:100%; height:500px;"></iframe>
	<iframe id='my_iframe' name='my_iframe' class="hidden" src="" style="width:100%; height:500px;"></iframe>
	<!-- ==============================================
	SCRIPTS
	=============================================== -->	
	<?php $this->load->view("common/admin/extras")?>
	<?php $this->load->view("common/admin/js")?>
	<script  type="text/javascript" language="javascript">	   
		$('#cargar_pacientes').on('click',function(){
		 	bandera = "0";
		 	if($('#pacientes').val() == ""){
			 	bandera = "1";
			 	mensaje("<?=$this->lang->line('bad_cargar_archivo')?>");
		 	}
		 	if(bandera=="0"){
			 	document.getElementById('formpacientes').submit();
			 	$("body").addClass("loading");
		 	}
		});
		
		/*$('#pacientes').on('change',function(){
	  		var fileSize = this.files[0].size / 1024;
		  	if(fileSize > (1024*1.658)){
		    	mensaje("El archivo excede el tamaño máximo permitido de 1mb");                  
		    	bandera="1";
		    }
		});*/

		$('#cargar_sms').on('click',function(){
		 	bandera = "0";
		 	if($('#file').val() == ""){
			 	bandera = "1";
			 	mensaje("<?=$this->lang->line('bad_cargar_archivo_1')?>");
		 	}
		 	if(bandera=="0"){
			 	document.getElementById('formfile').submit();
			 	$("body").addClass("loading");
		 	}
		});

		$(window).on("message onmessage", function(e) {
    		var data1 = e.originalEvent.data;
    		datos = JSON.stringify(data1);    
      		if (datos!=='""') {
		        datos = JSON.parse(datos);
		        datos = JSON.parse(datos);
		        if(datos.res == "ok"){
		        	if (datos.tipo=='sms_pacientes') {
		        		ajax('batch/cronCargaNotificacionSms',
				        {},
				        function(data){
				            if(data.res=="ok"){
				            	$("body").removeClass("loading");
				            	$('#file').val('');
		  						$('#my_iframe').attr('src', '');
		  						return mensaje("<?=$this->lang->line('ok_cargar_archivo')?>");
				            }else{
				            	$("body").removeClass("loading");
				             	return mensaje(data.msj);
				            }
				        },10000);		        		
		        	}
		        	if (datos.tipo=='pacientes') {
		        		$("body").removeClass("loading");
		        		$('#pacientes').val('');
		        		$('#my_iframe_pacientes').attr('src', '');
		        		return mensaje("<?=$this->lang->line('ok_cargar_archivo_1')?>");
		        	}
		        }else{
		        	$("body").removeClass("loading");
		        	return mensaje(datos.msj);
		        }
      		}
    	});		
	</script>
</body>
</html>