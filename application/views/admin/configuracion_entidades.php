<?php $this->load->view("common/admin/head")?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/dashboard.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
					<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('config_entidad_1')?></h2>
	    			</div>
	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<a href="#" name="addEntidad" class="btn btn-green pull-right"><i class="fa fa-plus-square"  aria-hidden="true"></i> &nbsp;<?=$this->lang->line('nueva_entidad')?></a>
	    			</div>
				</div>

				<div class="row mt-20">
					<div class="col-xs-12 col-md-12">
						<div class="table-responsive table-color">
							<table class="table table-hover table-bordered text-center">
								<thead>
									<tr>
										<th><?=$this->lang->line('entidad')?></th>
										<th><?=$this->lang->line('acciones')?></th>
									</tr>
								</thead>
								<tbody>
									<?php if(count($entidades) > 0 && is_array($entidades)){
								foreach ($entidades as $u) { ?>
									<tr>
										<td><?php echo $u->name; ?></td>
										<td>
											<a href="#" class="editar btn btn-eraser" data-id="<?php echo $u->_id;?>" data-nombre="<?php echo $u->name; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="#" class="eliminar btn btn-danger" data-id="<?php echo $u->_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
										</td>
									</tr>
								<?php } }else{ ?>
									<tr>
										<td colspan="2"><p class="text-center"><?=$this->lang->line('bad_table_entidad')?></p></td>
									</tr>
								<?php } ?>
								</tbody>								
							</table>
						</div>

						<div class="col-md-12" style="text-align:center">
							<nav aria-label="Page navigation">
					            <ul class="pagination">
					                <?php echo $this->pagination->create_links(); ?>
					            </ul>
			                </nav>
			            </div>

					</div>						
						
				</div>

			</div>
		</div>
		<div class="modal fade" id="entidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	              	<div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('editar_entidad')?></h4>
	             	</div>
	                <div class="modal-body">
                    	<input type="hidden" name="id_entidad">
                        <div class="form-group">
                             <label><?=$this->lang->line('entidad')?></label>
                             <input type="text" name="entidad" class="form-control">
                        </div>                        
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
	                    <button type="button" class="btn mas btn-green" id="guardar"><?=$this->lang->line('btn_guardar')?></button> 
	                </div>
	            </div>
	          </div>
	        </div>
        </div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript">
			$('[name=addEntidad]').on('click', function () {
				$('[name=id_entidad]').val('');
				$('[name=entidad]').val('');				
	        	$('#entidad').modal('show');
			});  
			$('.editar').on('click', function () {
				$('[name=id_entidad]').val($(this).attr('data-id'));
				$('[name=entidad]').val($(this).attr('data-nombre'));				
	        	$('#entidad').modal('show');				
			});
			$('.eliminar').on('click', function () {
				var id = $(this).attr('data-id');
				var btnnames = {
                    ok : "Aceptar",
                    cancel : "Cancelar"
                }
                confirmar("<?=$this->lang->line('confirm_entidad')?>",function(){
                   	ajax('usuarios/deleteEntidad',
	                {   
	                	id: id
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	location.reload();                                    
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
	            },false,btnnames);
			});
			$('#guardar').on('click', function () {
				var bandera=0;
				if ($('[name=entidad]').val()=='') {
					bandera=1;
					mensaje("<?=$this->lang->line('entidad_bad_1')?>");
				}				
				if (bandera==0) {
					ajax('usuarios/saveEntidad',
	                {   
	                	id: $('[name=id_entidad]').val(),	                	
	                	nombre: $('[name=entidad]').val()
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	location.reload();                                    
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
				}
			});
		</script>
	</body>
</html>