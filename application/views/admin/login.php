  <?php $this->load->view('common/web/head'); ?>
	<!-- ==============================================
		Custom CSS
		=============================================== -->
	   <link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/login.css" >
   	</head>  
   	<body>
  <body>
    <?php $this->load->view('common/web/navbar_login'); ?>
   <div class="box-container login_">
      <div class="col-sm-12 col-md-8 backbox">
        <div class="col-xs-12 loginMsg">
          <div class="textcontent">
            <p class="title"><?=$this->lang->line('text_header1')?></p>
            <p><?=$this->lang->line('subtitulo_login')?></p>
            <button id="switch1" class="switch1 btn btn-border-light" data-toggle="tab" href="#tab-registro"><?=$this->lang->line('registrar_btn')?></button>
          </div>
        </div>
        <div class="col-xs-12 signupMsg visibility">
          <div class="textcontent">
            <p class="title"><?=$this->lang->line('subtitulo_registro')?></p>
            <p><?=$this->lang->line('subtitulo_2_registro')?></p>
            <button id="switch2" class="switch2 btn btn-border-light" data-toggle="tab" href="#tab-login"><?=$this->lang->line('btn_ingresar')?></button>
          </div>
        </div>
      </div>
      <div class="col-xs-10 col-sm-12 col-md-4 col-xs-offset-1 col-sm-offset-0 col-md-offset-0 frontbox tab-content">
        <div class="login tab-pane fade in active" id="tab-login">
          <h2 class="green"><?=$this->lang->line('bienvenido_login')?></h2>
          <div class="form-group inputbox">
            <input type="text" name="usuario" id="usuario" placeholder="<?=$this->lang->line('placeholder_email')?>">
            <input type="password" name="contrasena" id="contrasena" placeholder="<?=$this->lang->line('placeholder_password')?>">
          </div>
          <a href="#" class="txt-light"><?=$this->lang->line('olvido_password')?></a>
          <div class="col-xs-12">
            <button type="button" class="btn btn-green" id="btnlogin"><?=$this->lang->line('btn_entrar')?></button>
          </div>
        </div>
        <div class="signup tab-pane fade" id="tab-registro">
          <h2><?=$this->lang->line('titulo_registro')?></h2>
          <div class="inputbox">
            <!-- <input type="text" name="fullname" placeholder="  FULLNAME">
            <input type="text" name="email" placeholder="  EMAIL">
            <input type="password" name="password" placeholder="  PASSWORD"> -->
            <p class="label-black"><?=$this->lang->line('descripcion_registro')?></p>
          </div>
         <!--  <button type="button" class="btn btn-green">Enviar</button> -->
        </div>
      </div>
    </div>

	<!-- ==============================================
	SCRIPTS
	=============================================== -->	
	<?php $this->load->view("common/admin/extras")?>
	<?php $this->load->view("common/admin/js")?>		
	<script  type="text/javascript" language="javascript">
    var $loginMsg = $('.loginMsg'),
    $login = $('.login'),
    $signupMsg = $('.signupMsg'),
    $signup = $('.signup'),
    $frontbox = $('.frontbox');

    $('.switch1').on('click', function() {
      $loginMsg.toggleClass("visibility");
      $frontbox.addClass("moving");
      $signupMsg.toggleClass("visibility");
    })

    $('.switch2').on('click', function() {
      $loginMsg.toggleClass("visibility");
      $frontbox.removeClass("moving");
      $signupMsg.toggleClass("visibility");
    })

	  $("#btnlogin").on( "click", function() {
      ajax('usuarios/login',
      {   
        email: $("#usuario").val(),
        contrasena: $("#contrasena").val()
      },
      function(data){
        if(data.res=="ok"){
          window.location.href=base_url+"admin/dashboard";                                            
        }else{
          mensaje(data.msj);
        }
      },10000);
    });
	</script>
	</body>
</html>