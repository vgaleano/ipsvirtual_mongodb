<?php $this->load->view("common/admin/head")?>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/encuesta.css">
</head>
<body>
	<div class="container">
		<div class="col-md-8 col-md-offset-2" style="margin-top: 5%;background: #f9f9f9;border:2px solid rgb(231, 231, 231);padding: 30px">
			<?php if(count($encuesta) >0){ 
				$contador = 0;
				foreach ($encuesta as $e) { 
					$contador += 1; ?>
				<br>
				<p id="unica<?php echo $contador;?>" data-id="<?php echo $e->id;?>"><span style="color: #468FE4;font-size: 16px;font-weight: bold;padding-right: 5px"><?php echo $contador;?>.</span><?php echo $e->pregunta;?></p>
					<?php if($e->id_tipo_pregunta == '1'){
							foreach ($e->respuesta as $r) { ?>
								<input type="radio" name="unica<?php echo $contador;?>" value="<?php echo $r->id?>" class="unica"> <?php echo $r->respuesta?><br>
					<?php } }if($e->id_tipo_pregunta == '2'){ ?>
						 <textarea rows="4" cols="50" class="form-control" name="unica<?php echo $contador;?>"></textarea>
				    <?php }else{ } 
				}  
			} ?>
			<div class="col-md-6 col-md-offset-3" style="margin-top:12px">
				<button class="btn btn-block" style="background: #7EBE50;color: #fff" id="guardar">Guardar</button>
			</div>
		</div>
	</div>
<!-- ==============================================
SCRIPTS
=============================================== -->	
<?php $this->load->view("common/admin/extras")?>
<?php $this->load->view("common/admin/js")?>
<script type="text/javascript">
	$('#guardar').on('click',function(){
		let preguntas = <?php echo $num_preguntas; ?>;
		let resultados = [];
		for (let i = 1; i <= preguntas; i++){
			let temp = {};
			let pregunta = document.getElementById('unica'+i).getAttribute('data-id')
			temp.idpregunta = pregunta
			let respuestas = document.getElementsByName('unica'+i)[0]
			let tipo = respuestas.type
			if(tipo == 'radio'){
				resp = $('input[name="unica'+i+'"]:checked').val()
				text = ''
				if(resp == undefined || resp == ''){ return mensaje('Debe responder todas las preguntas.')}
			}else if(tipo == 'textarea'){
				text = $('textarea[name="unica'+i+'"]').val()
				resp = ''
				if(text == undefined || text == ''){ return mensaje('Debe responder todas las preguntas.')}
			}
			temp.idrespuesta = resp
			temp.texto = text
			resultados.push(temp)
		}
		ajax('pacientes/encuesta',
				    {   
				        id: '<?php echo $paciente[0]->id;?>',
				        encuesta:resultados
				    },
				    function(data){
				        if(data.res=="ok"){   
				        	window.location.href=base_url+"web/login";
				        	//setTimeout(function(){ location.reload(); }, 2000);                              
				        }else{
				            mensaje(data.msj);
				        }
				    },10000);
	});
		
</script>
</body>
</html>