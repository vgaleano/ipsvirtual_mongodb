<?php $this->load->view("common/admin/head")?>		
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/typeahead.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-xs-12 col-md-12 col-lg-12 no-padding" id="wrapper-principal">
			<div class="col-xs-12 col-md-12 principal-padding">

				<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('paciente_text_6')?></h2>
	    			</div>
	    		</div>

	    		<div class="row box-shadow mt-20 row_100">
	    			<div class="col-xs-12 col-md-12 no-padding">

		    			<div class="table-responsive table-color">
		    				<table class="table table-hover table-bordered text-center table-list-search">
								<thead>
									<tr>
										<th width="15%"><?=$this->lang->line('config_parametro_5')?></th>
										<th width="15%"><?=$this->lang->line('paciente_text_2')?></th>
										<th width="15%"><?=$this->lang->line('paciente_text_3')?></th>
										<th width="10%"><?=$this->lang->line('acciones')?></th>
									</tr>
								</thead>
								<tbody class="addNew">
									<?php if(count($columns_configuracion) > 0){ 
									foreach ($columns_configuracion as $k_p => $p) { 
										$total_columns = count($p['columns']);
									 ?>
									<tr>
										<td rowspan="<?=$total_columns?>">
											<input class="form-control" value="<?=$p['name']?>" readonly>
										</td>
										<?php foreach ($p['columns'] as $kc => $vc) { ?>
										<td>
											<input class="form-control" name="name_parametro" value="<?=$vc?>" readonly>
										</td>
										<td>
											<input class="form-control" type="" name="valor" value="<?=$paciente[0][$vc]?>" placeholder="<?=$this->lang->line('paciente_text_3')?>">
										</td>
										<td>
											<button type="button" class="btn btn-save guardar" data-toggle="tooltip" title="Guardar" id="save"><i class="fa fa-save"></i></button>
										</td>
										<?php if ($total_columns>1) { ?>
											</tr>
										<?php }}
									if ($total_columns==1) { ?>
									</tr>
									<?php }}} ?>
								</tbody>
							</table>
	    				</div>
		    		</div>

		    		<div class="col-xs-12 col-md-12" style="text-align:center">
						<nav aria-label="Page navigation">
					        <ul class="pagination">
					            <?=$this->pagination->create_links(); ?>
					        </ul>
			            </nav>
				    </div>
		    	</div>
			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript" src="<?=base_url(); ?>js/typeahead.js"></script>
		<script  type="text/javascript" language="javascript">

			$('body').on('click','.guardar', function() {
				var id_paciente = '<?=$id_paciente?>',
					array_datos = {};

				if ( $(this).closest('tr').find('[name=valor]').val() != '' ) {
					array_datos = { column: $(this).closest('tr').find('[name=name_parametro]').val(), valor: $(this).closest('tr').find('[name=valor]').val() }
					ajax('pacientes/editarPaciente',
	                {   
	                	id_paciente: id_paciente,
	                	info: array_datos
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	mensaje("Actualizado el campo.");                          
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
				}
			});
			
		</script>
	</body>
</html>