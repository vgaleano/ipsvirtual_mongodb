<?php $this->load->view("common/admin/head")?>		
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/typeahead.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-xs-12 col-md-12 col-lg-12 no-padding" id="wrapper-principal">
			<div class="col-xs-12 col-md-12 principal-padding">

				<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('paciente_text_1')?></h2>
	    			</div>
	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
							<form id="formEntidad" action="<?=base_url();?>admin/newPaciente" method="POST" onsubmit="return false">
								<div class="input-group">
					      			<div class="input-group-addon">
					      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
				      				</div>
									<select class="form-control" name="searchentidad">
								    	<?php $ent = $this->session->userdata('searchentidad'); if(!$ent) $ent=""; ?>
								        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
								        <?php foreach ($entidad as $r) { ?>
											<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
										<?php } ?>
								    </select>
							    </div>																		
							</form>
						<?php } ?>
						<button class="btn btn-green ml-1" onclick="addRow()"><i class="fa fa-plus"></i> <?=$this->lang->line('paciente_text_4')?></button>
					</div>
	    		</div>

	    		<div class="row box-shadow mt-20 row_100">
	    			<div class="col-xs-12 col-md-12 no-padding">

		    			<div class="table-responsive table-color">
		    				<table class="table table-hover table-bordered text-center table-list-search">
								<thead>
									<tr>
										<th width="15%"><?=$this->lang->line('config_parametro_5')?></th>
										<th width="15%"><?=$this->lang->line('paciente_text_2')?></th>
										<th width="15%"><?=$this->lang->line('paciente_text_3')?></th>
									</tr>
								</thead>
								<tbody class="addNew">
									<?php if(count($columns_configuracion) > 0){ 
									foreach ($columns_configuracion as $k_p => $p) { 
										$total_columns = count($p->columns);
									 ?>
									<tr>
										<td rowspan="<?=$total_columns?>">
											<input class="form-control" value="<?=$p->name?>" readonly>
										</td>
										<?php foreach ($p->columns as $kc => $vc) { ?>
										<td>
											<input class="form-control" name="name_parametro" value="<?=$vc?>" readonly>
										</td>
										<td>
											<input class="form-control" type="" name="valor" placeholder="<?=$this->lang->line('paciente_text_3')?>">
										</td>
										<?php if ($total_columns>1) { ?>
											</tr>
										<?php }}
									if ($total_columns==1) { ?>
									</tr>
									<?php }}} ?>
								</tbody>										
							</table>
	    				</div>

	    				<div class="col-xs-12 col-md-12">
	    					<div class="form-group">	    						
	    						<button type="button" class="btn btn-save pull-right" data-toggle="tooltip" title="Guardar" id="save"><i class="fa fa-save"></i> <?=$this->lang->line('btn_guardar')?></button>
	    						<a href="<?=base_url()?>admin/dasboard" class="btn btn-border-dark pull-right"><?=$this->lang->line('btn_cancelar')?></a>
	    					</div>
	    				</div>

	    				<div class="col-xs-12 col-md-12" style="text-align:center">
						<nav aria-label="Page navigation">
					        <ul class="pagination">
					            <?=$this->pagination->create_links(); ?>
					        </ul>
			            </nav>
				    </div>
		    		</div>
		    	</div>
			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript" src="<?=base_url(); ?>js/typeahead.js"></script>
		<script  type="text/javascript" language="javascript">


			$('body').on('click','#save', function() {
				var id_entidad = '<?=$id_entidad?>',
					array_datos = [];

				$('.addNew>tr').each(function () {
					var tmp = {};
					if ( $(this).find('[name=valor]').val() != '' ) {
						tmp = { column: $(this).find('[name=name_parametro]').val(), valor: $(this).find('[name=valor]').val() }
						array_datos.push(tmp)
					}
				})

				if (array_datos.length>0) {
					ajax('pacientes/guardarPaciente',
	                {   
	                	info: array_datos,
	                	id_entidad: id_entidad
	                },
	                function(data){
	                    if(data.res=="ok"){ 
	                    	setTimeout(function() {
					          	window.location = base_url+'admin/dashboard'; 
					      	}, 3000);                    
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
				}
			});
			
			
			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

			$(document).ready(function(){
				if ('<?=$id_entidad?>'=='') {
					$('.box-btn').find('input, textarea, button, .btn').attr('disabled','true');
					$('.table').find('input, textarea, button, .btn').attr('disabled','true');
					mensaje("<?=$this->lang->line('paciente_text_5')?>");
				}
			});
			
		</script>
	</body>
</html>