<?php $this->load->view("common/admin/head")?>
	<link rel="stylesheet" href="<?=base_url(); ?>css/admin/perfil.css">
	<link rel="stylesheet" href="<?=base_url(); ?>css/full_calendar.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/indicadores.css">
	<!-- <link rel="stylesheet" href="https://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
    <script src="https://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script> -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
	<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
	<style type="text/css">
		.label-success {
			border-radius: 50px;
		}
		.border-left {
			border-left: 1px solid #c1c1c2;
		}
		#map { width: 100%; height: 600px; }
        body { font: 16px/1.4 "Helvetica Neue", Arial, sans-serif; }
        .ghbtns { position: relative; top: 4px; margin-left: 5px; }
	</style>
	</head>
	<body class="perfil">
		<?php $this->load->view("common/admin/nav")?>
		<!-- <?php if(!empty($noMedico) > 0){ ?> 
		<div class="col-md-3" style="background-color: #ccc;position: fixed;top: 200px;right: 0;z-index: 2;" id="ventana_emergente">
			<h4>Datos no médicos.<button type="button" class="close" id="close_nomedico"><span aria-hidden="true" class="pull-right" style="cursor: pointer;" id="">&times;</span></button></h4>
			<p><?=$noMedico[0]->observacion?></p>
		</div>
		<?php }?> -->
		<div class="col-md-12 no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding no-padding">

				<div class="row mx-0" width="100%">
					<div class="col-xs-12 col-sm-12 col-md-12 px-0 info-perfil">
						<div class="img-perfil">
							<?php if(isset($name_columns['Sexo']) && ($paciente[0][$name_columns['Sexo'][0]] == "F" || $paciente[0][$name_columns['Sexo'][0]] == "f" || $paciente[0][$name_columns['Sexo'][0]] == '0') && isset($paciente[0][$name_columns['Sexo'][0]])){ ?>
							<img src="<?=base_url()?>img/mujer.png" class="img-responsive">
							<?php }else{ ?>
							<img src="<?=base_url()?>img/hombre.png" class="img-responsive">
							<?php } ?>
						</div>
						<div class="col-xs-12 col-md-12 box-img px-0">
							<div class="col-xs-12 col-md-12">
								<h1><?=$name_patient." ".$apellido_patient; ?> 
									<span><i class="fa fa-circle <?=(isset($paciente[0]['state_risk']))?$colores[$paciente[0]['state_risk']['name']]:'';?>" aria-hidden="true" style="font-size: 30px"></i>&nbsp;<i class="fa fa-smile-o" aria-hidden="true" style="font-size: 30px"></i> </span>
								</h1>
								<h5><?=(isset($name_columns['Municipio']) && isset($paciente[0][$name_columns['Municipio'][0]]))?$paciente[0][$name_columns['Municipio'][0]]:'';?> - <?=(isset($name_columns['Departamento Residencia']) && isset($paciente[0][$name_columns['Departamento Residencia'][0]]))?$paciente[0][$name_columns['Departamento Residencia'][0]]:'';?></h5>
								<h5>
									<button class="btn btn-blue btn-circle btn-xs llamada" data-telefono="<?=($telefono!='')?$telefono:''; ?>" data-patient="<?=$name_patient?>" <?=($telefono!='')?'':'disabled';?> data-id="<?=$id?>" data-id_entidad="<?=$id_entidad?>"><i class="fa fa-phone"></i></button>  
									<button class="btn btn-blue btn-circle btn-xs mensaje" data-telefono="<?=($telefono!='')?$telefono:''; ?>" data-patient="<?=$name_patient?>" <?=($telefono!='')?'':'disabled';?> data-id="<?=$id?>" data-id_entidad="<?=$id_entidad?>"><i class="fa fa-envelope"></i></button>
									<button class="btn btn-green btn-circle btn-xs whatsapp" data-telefono="<?=($telefono!='')?$telefono:''; ?>" data-patient="<?=$name_patient?>" <?=($telefono!='')?'':'disabled';?> data-id="<?=$id?>" data-id_entidad="<?=$id_entidad?>"><i class="fa fa-whatsapp"></i></button>
									<a class="state-risk ml-1" href="#modal-score" data-toggle="modal"><span class="label <?=(isset($paciente[0]['state_risk']))? $colores[$paciente[0]['state_risk']['name']]:''?>">Score: <?=(isset($paciente[0]['state_risk']))? $colores[$paciente[0]['state_risk']['name']]:''?></span></a>
								</h5>
							</div>
						</div>
						<div class="col-xs-12 col-md-12 box-info px-0">
							<div class="col-xs-12 col-md-12">
								<div class="col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('cedula')?></label><br>
										<span><?=(isset($name_columns['Número documento']) && isset($paciente[0][$name_columns['Número documento'][0]]))?$paciente[0][$name_columns['Número documento'][0]]:'---'; ?></span>
									</div>
								</div>
								<div class="col-md-3 border-left">
									<div class="form-group">
										<label><?=$this->lang->line('direccion')?></label><br>
										<span><?=(isset($name_columns['Dirección']))?$paciente[0][$name_columns['Dirección'][0]]:'---'; ?></span>
									</div>
								</div>
								<div class="col-md-3 border-left">
									<div class="form-group">
										<label><?=$this->lang->line('edad')?></label><br>
										<span><?=$edad?></span>
									</div>
								</div>
								<div class="col-md-3 border-left">
									<div class="form-group">
										<label><?=$this->lang->line('estado')?></label><br>
										<span class="<?=(isset($paciente[0]['state_risk']))?$colores[$paciente[0]['state_risk']['name']]:'';?>">
											<?php if ( isset($paciente[0]['state_risk']) && $paciente[0]['state_risk']['name'] == 'Rojo intenso' ) {
												echo $this->lang->line('name_estado_1');
											}elseif ( isset($paciente[0]['state_risk']) && $paciente[0]['state_risk']['name'] == 'Rojo' ) {
												echo $this->lang->line('name_estado_2');
											}elseif ( isset($paciente[0]['state_risk']) && $paciente[0]['state_risk']['name'] == 'Amarillo' ) {
												echo $this->lang->line('name_estado_3');
											}else{
												echo $this->lang->line('name_estado_4');
											} ?>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row px-30 mx-0 xs-px-0" width="100%">
					<div class="col-xs-12 col-sm-12 col-md-9 principal-box">
						<div class="col-xs-12 col-md-12 box-pacientes no-padding">
							<div>
								<!-- Navv tabs -->
								<ul class="nav nav-tabs" role="tablists">
									<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-clock-o" aria-hidden="true"></i> <?=$this->lang->line('historial')?></a></li>
								    <li role="presentation"><a href="#estadistica" aria-controls="estadistica" role="tab" data-toggle="tab"><i class="fa fa-line-chart" aria-hidden="true"></i> <?=$this->lang->line('estadisticas')?></a></li>
								    <li role="presentation"><a href="#historico" aria-controls="historico" role="tab" data-toggle="tab"><i class="fa fa-paper-plane" aria-hidden="true"></i> <?=$this->lang->line('historial_mensajes')?></a></li>
								</ul>

	<!--  Tabb panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="home">
			<div class="col-xs-12 col-md-12" style="padding: 20px;background: #fff">
				<div class="col-xs-12 col-md-12 no-padding box-title">
					<p class="title"><?=$this->lang->line('ultima_observacion')?> <span class="label label-primary pull-right bandera"><?=date('d-m-y');//date('d-m-Y',strtotime($historico_ultimo->fecha_ult_control))?></span>
					</p>
				</div>
				<div class="col-xs-12 col-md-12 box-content">
					<p><?=(isset($observacion) && count($observacion) && $observacion[0]->last!='') ? $observacion[0]->last : $this->lang->line('bad_observacion_text') ?></p>
				</div>
	    		<!-- <div class="col-md-12 no-padding box-title">
					<p class="title">ÚLTIMO CONTROL <span class="label label-primary pull-right bandera"><?=date('d-m-y');//date('d-m-Y',strtotime($historico_ultimo->fecha_ult_control))?></span>
					</p>
				</div> -->
				<div class="col-xs-12 col-md-12 no-padding box-title pt-2">
					<p class="title"><?=$this->lang->line('titulo_condiciones')?></p>
				</div>
	    		<div class="col-xs-12 col-md-12">
	    			<div class="col-md-12 col-xs-12">
						<p><?=$this->lang->line('titulo_trazadora')?></p>
					</div>
	    			<div class="col-xs-12 col-md-12 no-padding">
	    				<div class="col-md-12 col-xs-12 table-responsive">
	    					<div class="col-md-12 col-xs-12 panel-group table-responsive" id="accordion_condiciones" role="tablist" aria-multiselectable="true">
		    					<table class="table table-borderless">
		    						<thead>
		    							<tr>
			    							<th></th>
			    							<th class="col"><?=$this->lang->line('rta_1')?></th>
			    							<th class="col"><?=$this->lang->line('rta_2')?></th>
			    							<th class="col" style="text-align: center;"><?=$this->lang->line('observaciones')?></th>
			    							<th class="col-xs-2" style="text-align: center;"><?=$this->lang->line('year')?></th>
			    							<th></th>
			    						</tr>
		    						</thead>
		    						<tbody>
		    							<?php print_r($this->session->userdata('site_lang')); foreach ($condiciones_trazadoras as $k_ct => $v_tc) { ?>
		    							<tr>
			    							<td class="btn-plus">
			    								<a data-id="<?=$v_tc['name_imput']?>" data-_id="<?=$v_tc['_id']['oid']?>" href="#" class="bs-example-modal-sm1 btn-link"><i class="fa fa-plus" aria-hidden="true" data-toggle="tooltip" title="Agregar incidencia"></i></a>&nbsp;<?=$v_tc['name_'.$this->session->userdata('site_lang')]?>
			    							</td>
			    							<td>
			    								<input type="radio" value="si" name="<?=$v_tc['name_imput']?>">
			    							</td>
			    							<td>
			    								<input type="radio" value="no" name="<?=$v_tc['name_imput']?>">
			    							</td>
			    							<td>
			    								<textarea class="form-control" rows="1" style="resize: none;" id="<?=$v_tc['name_imput']?>_obs"></textarea>
			    							</td>
			    							<td>
			    								<input onkeypress="return onlyNumber(event);" id="<?=$v_tc['name_imput']?>_ano" class="form-control" maxlength="4">
			    							</td>
			    							<td class="btn-box">
			    								<?php if(isset($v_tc['data'])){ ?> 
			    								<a role="button" data-toggle="collapse" data-parent="#accordion_condiciones" href="#collapse<?=$v_tc['name_collapse']?>_condiciones" aria-expanded="false" aria-controls="collapse<?=$v_tc['name_collapse']?>_condiciones"><i class="fa fa-arrow-down" aria-hidden="true" data-toggle="tooltip" title="Ver historial"></i></a>
			    								<?php } ?>
			    							</td>
			    						</tr>
			    						<tr id="collapse<?=$v_tc['name_collapse']?>_condiciones" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$v_tc['name_collapse']?>">
											<td colspan="6">
												<div class="panel-body">
													<?php if( isset($v_tc['data']) ){ 
														$timestamp = (String)$v_tc['data']->registration_date;
                      									$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000) ?>
														<p style="font-weight: lighter;">
														<?=$v_tc['name_'.$this->session->userdata('site_lang')]?><br>
											        	<?=$this->lang->line('year')?>: <?=$v_tc['data']->year ?><br>
											        	<?=$this->lang->line('text_observacion')?>: <?=$v_tc['data']->observacion ?><br>
											        	<?=$this->lang->line('text_registro')?>: <?=$dateInLocal;?>
														</p>
													<?php } ?>
											    </div>
											</td>
										</tr>
		    							<?php } ?>
		    						</tbody>
		    					</table>
		    				</div>
	    				</div>
	    			</div>
	    		</div>
	    		<div class="col-xs-12 col-md-12">
	    			<div class="col-md-12 col-xs-12">
						<p><?=$this->lang->line('titulo_relevantes')?></p>
					</div>
	    			<div class="col-xs-12 col-md-12 no-padding">
	    				<div class="col-md-12 col-xs-12 panel-group table-responsive" id="accordion" role="tablist" aria-multiselectable="true">
	    					<table class="table table-borderless">
	    						<tr class="text-center"> 
	    							<th></th>
								    <th class="text-center" colspan="2"><i class="fa fa-user" aria-hidden="true"></i> <?=$this->lang->line('paciente')?></th>
								    <th class="text-center" colspan="2"><i class="fa fa-user-md" aria-hidden="true"></i> <?=$this->lang->line('doctor')?></th>
								    <th></th>
								    <th></th>
								</tr>
								<tr>
									<th></th>
		    						<th class="col text-center"><?=$this->lang->line('rta_1')?></th>
		    						<th class="col text-center"><?=$this->lang->line('rta_2')?></th>
		    						<th class="col text-center"><?=$this->lang->line('rta_1')?></th>
		    						<th class="col text-center"><?=$this->lang->line('rta_2')?></th>
		    						<th class="col-xs-2 text-center"><?=$this->lang->line('year')?></th>
		    						<th></th>
								</tr>
								<?php foreach ($antecedentes_relevantes as $k_ar => $v_ar) { ?>
								<tr>
									<th class="btn-plus">
										<a data-id="<?=$v_ar['name_imput']?>" data-_id="<?=$v_ar['_id']['oid']?>" href="#" class="bs-example-modal-1 btn-link" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i></a>&nbsp;<?=$v_ar['name_'.$this->session->userdata('site_lang')]?>
									</th>
		    						<td class="col text-center">
		    							<input type="radio" value="si" name="<?=$v_ar['name_imput']?>_pa" disabled>
		    						</td>
		    						<td class="col text-center">
		    							<input type="radio" value="no" name="<?=$v_ar['name_imput']?>_pa" disabled>
		    						</td>
		    						<td class="col text-center">
		    							<input type="radio" value="si" name="<?=$v_ar['name_imput']?>_doc">
		    						</td>
		    						<td class="col text-center">
		    							<input type="radio" value="no" name="<?=$v_ar['name_imput']?>_doc">
		    						</td>
		    						<td><input onkeypress="return onlyNumber(event);" id="<?=$v_ar['name_imput']?>_ano" class="form-control" maxlength="4">
		    						</td>
		    						<td class="btn-box">
		    							<?php if(isset($v_ar['data'])){ ?> 
		    							<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$v_ar['name_collapse']?>" aria-expanded="false" aria-controls="collapse<?=$v_ar['name_collapse']?>"><i class="fa fa-arrow-down" aria-hidden="true" data-toggle="tooltip" title="Ver historial"></i></a>
		    							<?php } ?>
		    						</td>
								</tr>
								<tr id="collapse<?=$v_ar['name_collapse']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?=$v_ar['name_collapse']?>">
									<td colspan="6">
										<div class="panel-body">
									        <p style="font-weight: lighter;">
									        	<?php if( isset($v_ar['data'])){ 
									        	$timestamp = (String)$v_ar['data']->registration_date;
                      							$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000) ?>
													<p style="font-weight: lighter;">
													<?=$v_ar['name_'.$this->session->userdata('site_lang')]?><br>
										        	<?=$this->lang->line('year')?>: <?=$v_ar['data']->year ?><br>
										        	<?=$this->lang->line('text_observacion')?>: <?=$v_ar['data']->respuesta_doctor ?><br>
										        	<?=$this->lang->line('text_registro')?>: <?=$dateInLocal;?>
													</p>
												<?php } ?>
									        </p>
									    </div>
									</td>
								</tr>
								<?php } ?>
	    					</table>
	    				</div>
	    				<div class="col-xs-12 col-md-12">
	    					<div class="form-group">
	    						<label><?=$this->lang->line('observacion_consulta')?>: </label>
	    						<textarea class="form-control" rows="5"  id="observaciones"></textarea>
	    						<br><br>
	    					</div>
	    				</div>
	    				<div class="col-xs-12 col-sm-12 col-sm-offset-0 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4 no-padding">
							<button type="button" class="btn btn-block" id="relevante_trazadora" style="background-color: #5cb85c;color:#fff" ><?=$this->lang->line('btn_guardar')?></button>
						</div>
	    			</div>
	    		</div>
	    		<div class="clearfix"></div>
	    		<!-- <div class="col-md-12" style="padding-top: 20px;padding-bottom: 20px;border-top: 2px solid #eee">
	    			<div class="col-md-12 col-xs-12">
	    				<p>ÚLTIMOS CONTROLES</p>
	    			</div>
	    			<div class="col-md-12">
	    				<div class="col-md-3">
	    					<p>Filtración glomerular</p>
	    				</div>
	    				<div class="col-md-4">
	    					<ul class="text-center">
							  <li>(Cockcroft-Gault)<br><?php //echo $historico_ultimo->filtracion_glomerular;?></li>
							</ul>  
	    				</div>
	    				<div class="col-md-4">
	    					<ul class="text-center">
							  <li>(MDRD)<br><?php //echo $historico_ultimo->filtracion_glomerular;?></li>
							</ul>
	    				</div>
	    			</div> -->
	    			<!-- <div class="col-xs-12 col-md-12">
	    				<br>
						<p style="text-decoration: underline;">DATOS CLÍNICOS Y PARACLINICOS</p>
	    				<div class="col-xs-12 -md-12 contenedorDatosPaciente"></div>
	    				<div class="col-xs-12 col-md-12">
	    					<button class="btn pull-left" onclick="paginationDatos(1)"><< Atras</button>
	    					<button class="btn pull-right" onclick="paginationDatos(2)">Siguiente >></button>
	    				</div>
	    				<div class="col-md-3"></div>
	    			</div> -->
	    		<!-- </div> -->
	    	</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="estadistica">
	    	<div class="col-xs-12 col-xs-12">
	    		<?php if (count($name_indicators)>0) {
	    			foreach ($name_indicators as $k_i => $v_i) { ?>
	    		<div class="col-md-4" style="padding: 0">
	    			<div class="form-group">
	    				<button class="btn btn-green btn-block" style="border-radius: 0;border-right: 2px solid #fff;" onclick="mostrarGrafica('grafica_<?=$k_i+1?>', '<?=(String)$v_i->_id?>')"><?=$v_i->information->name?></button>
	    			</div>
	    		</div>		
    			<?php }} ?>
	    	</div>
	    	<div class="col-xs-12 col-md-12 view-graphic" id="reseña">
	    		<div id="col-md-12" style="text-align: center;padding-bottom: 30px;">
	    			<h4><?=$this->lang->line('bad_estadisticas')?></h4>
	    		</div>
	    	</div>
	    	<div class="col-xs-12 col-md-12 view-graphic" id="box-graph">
	    	</div>
	    </div>
	    <div role="tabpanel" class="tab-pane" id="historico"></div>
	</div>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3 pt-15 hidden-xs">						
						<div class="col-xs-12 col-md-12 back-blue-light text-center">
							<?=$this->lang->line('controles_realizados')?>
						</div>
						<div class="col-xs-12 col-md-12 no-padding">
							<div class="col-xs-12 col-md-12 wrapper px-0">
								<div id="calendarContainer"></div>
  								<div id="organizerContainer" style=""></div>
							</div>
						</div>

						<div class="col-xs-12 col-md-12 mt-20 box-shadow">
							<h5 class="text-center"><?=$this->lang->line('ultimos_mensajes')?></h5>
							<br>
							<?php if (count($mensajes_enviados)>0) {
							foreach ($mensajes_enviados as $k_ms => $v_ms) { ?>
								<p><i class="fa fa-envelope"></i> <?=$v_ms->mensaje?></p>
							<?php }}else{ ?>
								<p><?=$this->lang->line('bad_ultimos_mensajes')?></p>
							<?php } ?>							
						</div>
					</div>
				</div>

				<div class="row mt-20 px-30 mx-0">
					<div class="col-xs-12 ajuste-box">
						<div class="col-xs-12 col-md-12" style="margin-bottom: 20px">
							<div id="map"></div>
						</div> 
					</div>
				</div>
			</div>

		</div>		

		<!-- CONDICION TRAZADORA -->
		<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="bs-example-modal-sm">
		  	<div class="modal-dialog modal-sm" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
       					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel"><?=$this->lang->line('nuevo_incidente')?></h4>
      				</div>
      				<div class="modal-body">
      					<input type="hidden" id="trazador_inc" class="trazadora">
      				 	<div class="form-group">
        					<label><?=$this->lang->line('fecha')?></label>
        					<input onkeypress="return onlyNumber(event);" id="fecha_incidencia_trazadora" class="form-control trazadora" maxlength="4">
        				</div>
       					<div class="form-group">
       						<label><?=$this->lang->line('text_observacion')?></label>
       						<textarea class="form-control trazadora" rows="4" id="obs_incidencia"></textarea>
       					</div>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-green" onclick="guardarIncidencia('trazadora')"><?=$this->lang->line('btn_guardar')?></button>
     			 	</div>
		    	</div>
		    </div>
		</div>
		<!-- ANTECEDENTES RELEVANTES -->
		<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="bs-example-modal-1">
		  	<div class="modal-dialog modal-sm" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
       					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel"><?=$this->lang->line('nuevo_incidente')?></h4>
      				</div>
      				<div class="modal-body">
      					<input type="hidden" id="condicion_inc" class="relevante">
      				 	<div class="form-group">
        					<label><?=$this->lang->line('fecha')?></label>
        					<input onkeypress="return onlyNumber(event);" id="fecha_incidencia_relevante" class="form-control relevante" maxlength="4">
        				</div>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-green" onclick="guardarIncidencia('relevante')"><?=$this->lang->line('btn_guardar')?></button>
     			 	</div>
		    	</div>
		    </div>
		</div>
		<!-- score -->
		<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modal-score">
		  	<div class="modal-dialog modal-md" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
       					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;<?=strtoupper($name_patient." ".$apellido_patient);?></h4>
      				</div>
      				<div class="container-fluid modal-body">
      					<div class="row">
      						<div class="col-xs-12 col-md-6 score-box">
      							<span><?=$this->lang->line('score')?></span><br>
	      				 		<p style="">
	      				 			<?=(isset($paciente[0]['state_risk']))?$paciente[0]['score_risk']:'---';?>
	      				 		</p>
	      				 	</div>
	      				 	<div class="col-xs-12 col-md-6">
      							<span><?=$this->lang->line('estado')?></span><br>
	      				 		<p style="">
	      				 			<i class="fa fa-circle <?=(isset($paciente[0]['state_risk']))?$colores[$paciente[0]['state_risk']['name']]:'';?>" aria-hidden="true"></i>	
	      				 		</p>
	      				 	</div>
	      				 	<div class="col-xs-12 col-md-6">
	      				 		<p><strong><?=$this->lang->line('detalle_puntaje')?></strong></p>
	      				 		<ul>
	      				 			<li><strong><?=$this->lang->line('fumador')?>:&nbsp;</strong><span><?=(isset($name_columns['Fumador']) && isset($paciente[0][$name_columns['Fumador'][0]]))?$paciente[0][$name_columns['Fumador'][0]]:''; ?></span></li>
	      				 			<li><strong><?=$this->lang->line('genero')?>:&nbsp;</strong><span><?=(isset($name_columns['Sexo']) && ($paciente[0][$name_columns['Sexo'][0]] == "F" || $paciente[0][$name_columns['Sexo'][0]] == "f" || $paciente[0][$name_columns['Sexo'][0]] == '0') && isset($paciente[0][$name_columns['Sexo'][0]])) ? 'Femenino':'Masculino';?></span></li>
	      				 			<li><strong><?=$this->lang->line('edad')?>:&nbsp;</strong><span><?=$edad; ?></span> Años</li>
	      				 			<li><strong><?=$this->lang->line('colesterol')?>:&nbsp;</strong><span><?=(isset($name_columns['Colesterol total (Último valor)']) && isset($paciente[0][$name_columns['Colesterol total (Último valor)'][0]]))?$paciente[0][$name_columns['Colesterol total (Último valor)'][0]]:''; ?></span></li>
	      				 			<li><strong><?=$this->lang->line('tension_arterial')?>:&nbsp;</strong><span><?=(isset($name_columns['Tensión Arterial Sistólica (Último valor)']) && isset($paciente[0][$name_columns['Tensión Arterial Sistólica (Último valor)'][0]]))?$paciente[0][$name_columns['Tensión Arterial Sistólica (Último valor)'][0]]:''; ?></span></li>
	      				 		</ul>
	      				 	</div>
	      				 	<div class="col-xs-12 col-md-6">
	      				 		<p><strong><?=$this->lang->line('detalle_estado')?></strong></p>
	      				 		<ul>
	      				 			<li><strong><?=$this->lang->line('filtracion')?>:&nbsp;</strong> <span><?=(isset($paciente['filtracion_glomerular']) && isset($paciente[0]['filtracion_glomerular']))?$paciente[0]['filtracion_glomerular']:'';?></span></li>
      				 				<li><strong><?=$this->lang->line('diabetes')?>:&nbsp;</strong><span><?=(isset($name_columns['Diabetes Mellitus']) && isset($paciente[0][$name_columns['Diabetes Mellitus'][0]]))?$paciente[0][$name_columns['Diabetes Mellitus'][0]]:''; ?></span></li>
	      				 		</ul>
	      				 	</div>
      					</div>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cerrar')?></button>
     			 	</div>
		    	</div>
		    </div>
		</div>
		<!-- DATOS RELEVANTES -->
		<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="datos_no_medicos">
		  	<div class="modal-dialog modal-sm" role="document">
		    	<div class="modal-content">
		      		<div class="modal-header">
       					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel"><?=$this->lang->line('datos_no_medicos')?></h4>
      				</div>
      				<div class="modal-body">
      					<input type="hidden" name="id_paciente_nomedico" id="id_paciente_nomedico" value="<?=$id?>">
       					<div class="form-group">
       						<label><?=$this->lang->line('text_observacion')?></label>
       						<textarea class="form-control" rows="4" id="dato_nomedico_obs" maxlength="400"></textarea>
       					</div>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-green" id="guardardato_nomedico"><?=$this->lang->line('btn_guardar')?></button>
     			 	</div>
		    	</div>
		    </div>
		</div>

		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script type="text/javascript" src="<?=base_url(); ?>js/full_calendar.js"></script>

		<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/core.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/charts.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/themes/animated.js"></script>		

		<script src="<?=base_url()?>js/leaflet-heat.js"></script>

		<script src="https://leaflet.github.io/Leaflet.markercluster/example/realworld.388.js"></script>
		
		<script  type="text/javascript" language="javascript">
			valor_superformulario = '<?=$id?>';
			var  copy_antecendentes_relevantes = <?=json_encode($copy_antecendentes_relevantes)?>,
			copy_condiciones_trazadoras = <?=json_encode($copy_condiciones_trazadoras)?>;

			var map = L.map('map').setView([7.109793, -73.105242], 16);
			//var map = L.map('map').setView([-37.82109, 175.2193], 16);
			var marker = L.marker([7.109793, -73.105242]).addTo(map);

			var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
			}).addTo(map);

		    $(document.body).on('click','.bs-example-modal-1',function(){
				//antecedentes relevantes
				let data = $(this).attr('data-id')
				$('#condicion_inc').val(data)
				$('#bs-example-modal-1').modal('show')
        	});

        	$(document.body).on('click','.bs-example-modal-sm1',function(){
        		//modal de condiciones trazadoras
				let data = $(this).attr('data-id')
				$('#trazador_inc').val(data)
				$('#bs-example-modal-sm').modal('show')
        		//window.location.href = base_url+'admin/mensajeria'	
        	});

        	//contador de caracteres
			$(document).on('keyup', 'textarea[name="mensaje_personalizado"]', function(e) {
			    // maxlength attribute does not in IE prior to IE10
			    var $this = $(this),
			        maxlength = $this.attr('maxlength'),
			        maxlengthint = parseInt(maxlength),
			        caracteresActuales = $this.val().length;
			  		caracteresRestantes = maxlengthint - caracteresActuales,
			        espanCaractRestantes = $('.CaracteresRestantes');
			  
				espanCaractRestantes.html(caracteresRestantes);
				if($('html').hasClass("ie9")) {
				    if(caracteresRestantes == -1) {
				       caracteresRestantes = 0;
				    }
				}
			    if(!!maxlength){
			        var text = $this.val();
			        if (text.length >= maxlength) {
			            $this.val(text.substring(0,maxlength));            
			            e.preventDefault();
			        }
			    }
			});

			$('#close_nomedico').on('click',function(){
				$('#ventana_emergente').css('display','none')
			})

			$('#guardardato_nomedico').on('click',function(){
		    	if($('#dato_nomedico_obs').val()==''){
		    		return mensaje('Debe agregar una observación.');
		    	}
		    	$('#datos_no_medicos').modal('hide');
		    	ajax('pacientes/pacienteDatoNoMedico',
			    {   
			        id: '<?=$id; ?>',
			        datonomedico:$('#dato_nomedico_obs').val()
			    },
			    function(data){
			        if(data.res=="ok"){
			        	$('#dato_nomedico_obs').val(' ');                                  
			        }else{
			        	$('#mensaje').modal('hide');
			            mensaje(data.msj);
			        }
			    },10000);
	    	});

			$('#relevante_trazadora').on('click',function(){
		    	var condiciones_trazadoras = [],
		    	antecedentes_relevantes=[];

		    	for (var i = 0; i < copy_condiciones_trazadoras.length; i++) {
		    		if ( $('[name='+copy_condiciones_trazadoras[i]['name_imput']+']').is(":checked")) {
		    			fila={},campo={};
			    		fila.condicion = copy_condiciones_trazadoras[i]['name_imput']
			    		campo.rta = $('input:radio[name='+copy_condiciones_trazadoras[i]['name_imput']+']:checked').val()
			    		campo.obs = $('#'+copy_condiciones_trazadoras[i]['name_imput']+'_obs').val()
			    		campo.ano = $('#'+copy_condiciones_trazadoras[i]['name_imput']+'_ano').val()
			    		campo.id = copy_condiciones_trazadoras[i]['_id']['oid']
			    		fila.valores = campo
			    		condiciones_trazadoras.push(fila)
		    		}
		    	}
		    	for (var i = 0; i < copy_antecendentes_relevantes.length; i++) {
		    		if ( $('[name='+copy_antecendentes_relevantes[i]['name_imput']+'_doc]').is(":checked")) {
		    			fila={},campo={};
			    		fila.condicion = copy_antecendentes_relevantes[i]['name_imput']
			    		campo.rta_pa = ''//$('[name='+copy_antecendentes_relevantes[i]['name_imput']+'_pa]:checked').val()
			    		campo.rta_doc = $('[name='+copy_antecendentes_relevantes[i]['name_imput']+'_doc]:checked').val()
			    		campo.ano = $('#'+copy_antecendentes_relevantes[i]['name_imput']+'_ano').val()
			    		campo.id = copy_antecendentes_relevantes[i]['_id']['oid']
			    		fila.valores = campo
			    		antecedentes_relevantes.push(fila)
		    		}
		    	}
		    	var temp = {condiciones_trazadoras: condiciones_trazadoras, antecedentes_relevantes: antecedentes_relevantes};
		    	console.log(temp);
		    	//temp = JSON.stringify(temp);
		    	console.log(temp);
		    	ajax('pacientes/pacienteTrazadoraRelevantes',
			    {   
			        id: '<?=$id; ?>',
			        datos: temp,
			        observaciones: $('#observaciones').val()
			    },
			    function(data){
			        if(data.res=="ok"){
			        	mensaje('Datos guardados con éxito.');
			        }else{
			        	$('#mensaje').modal('hide');
			            mensaje(data.msj);
			        }
			    },10000);
		    });
			//guardar antecedentes relevantes y condiciones trazadoras
			function guardarIncidencia(formulario){
				let temp = []
				fila={},campo={};
				fila.condicion = formulario

				if (formulario=='relevante') {
					if (!validYear($('#fecha_incidencia_relevante').val())) {
						return mensaje('Debe agregar una fecha valida.');
					}
					fila.condicion = $('#condicion_inc').val()
					campo.rta_pa = ''
		    		campo.rta_doc = 'si'
		    		campo.ano = $('#fecha_incidencia_relevante').val()
		    		campo.id = $('#condicion_inc').attr('data-_id')
				}else{
					if (!validYear($('#fecha_incidencia_trazadora').val())) {
						return mensaje('Debe agregar una fecha valida.');
					}
					fila.condicion = $('#trazador_inc').val()
		    		campo.rta = 'si'
		    		campo.obs = $('#obs_incidencia').val()
		    		campo.ano = $('#fecha_incidencia_trazadora').val()
		    		campo.id = $('#trazador_inc').attr('data-_id')
				}
				console.log(campo);
				fila.valores = campo
				temp.push(fila)
				console.log(temp);
				if(formulario == "relevante"){
				 	$('#bs-example-modal-1').modal('hide')
				}else{
				 	$('#bs-example-modal-sm').modal('hide')
				}
				ajax('pacientes/pacienteIncidencia',
			    {   
			        id: '<?=$id; ?>',
			        data:temp,
			        modo: formulario
			    },
			    function(data){
			        if(data.res=="ok"){
			        	mensaje('Datos guardados con éxito.');
			        	//$('#dato_nomedico_obs').val(' ');                                  
			        }else{
			        	//$('#mensaje').modal('hide');
			            mensaje(data.msj);
			        }
			    },10000);
			}

			function limpiar(){
				$('#id_paciente').val(' ');
				$('#nombre_paciente').val(' ');
				$('#mensaje_personalizado').val(' ');
			}

			// this method is called when chart is first inited as we listen for "rendered" event
			function zoomChart() {
				// different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
				chart.zoomToIndexes(chartData.length - 40, chartData.length - 1);
			}
			
			$("#historico").load("paginationMensajesByPatient/<?=$id?>/");
			$(document).on("click", "#pagination-digg1 a", function(e){
			   e.preventDefault();
			    var href = $(this).attr("href");
			    $("#historico").load(href);
		  	});
			$(document).on("click", "#btnMymodal", function(e){
			    $('#tlimpiar').val('');
	  			$("#historico").load("paginationMensajesByPatient/<?=$id?>/");
		  	});

		  	function mostrarGrafica(id_div, id_indicator){
				ajax('pacientes/pacienteDatosGrafica',
			    {   
			        id_indicator: id_indicator,
			        id_patient: '<?= $id ?>',
			        id_entity: '<?=$id_entidad;?>'
			    },
			    function(data){
			        if(data.res=="ok"){
			        	data_tabla_pacientes_ok_bad=[];
			            	posicion_tabla_pacientes_ok_bad=0;
			            	$('#box-graph').empty();
			            	var graficas = data.graficas;
			            	console.log(graficas)

			            	for (var i = 0; i < graficas.length; i++) {
			            		var name_box = 'chartdiv'+(i+1);
			            		var content='<div class="table-responsive m-2"><table class="table text-center">';
			            			content+='<thead><tr><th>Total registros</th><th>Total registros sin fechas</th><th>Total registros sin valor</th><th>Total registros ok</th></tr></thead>';
			            			content+='<tbody>';
			            				content+='<tr><td class="label-blue">'+graficas[i]['total_registros']+'</td><td class="label-red">'+graficas[i]['total_registros_sin_fecha']+'</td><td class="label-red">'+graficas[i]['total_registros_sin_otros']+'</td><td class="label-green">'+graficas[i]['total_registros_ok']+'</td></tr>'
			            			content+='</tbody>';
			            		content+='</table></div>';
			            		var content1='<div class="box table-responsive m-2" id="databody"></div>';
			            		$('#box-graph').append('<div class="col-xs-12 col-md-12 box-shadow" style="margin-bottom:2rem;"><div id="'+name_box+'" style="width: 100%; height: 500px;"></div><p class="m-2">'+graficas[i]['description']+'</p>'+content+content1+'</div>');
			            	}
		            		am4core.ready(function() {
								am4core.disposeAllCharts();
								// Themes begin
								am4core.useTheme(am4themes_animated);
								// Themes end

								for (var i = 0; i < graficas.length; i++) {
				            		var puntos = graficas[i]['puntos_grafica'],
				            		categorias = graficas[i]['label'],
				            		name_box = 'chartdiv'+(i+1),
				            		name_grafica = graficas[i]['name'];
				            	
									// Create chart instance
									var chart = am4core.create(name_box, am4charts.XYChart);

									chart.data = puntos;

									// Create category axis
									var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
									categoryAxis.dataFields.category = "fecha";
									categoryAxis.title.text = graficas[i]['titulo'];
									categoryAxis.renderer.grid.template.location = 0;
									categoryAxis.renderer.minGridDistance = 20;
									categoryAxis.renderer.cellStartLocation = 0.1;
									categoryAxis.renderer.cellEndLocation = 0.9;

									// Create value axis
									var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

									valueAxis.title.text = "Data";
									valueAxis.min = 0; 
									//valueAxis.max = 100;
									valueAxis.extraMin = 0.001;
									valueAxis.extraMax = 0.001; 
									valueAxis.renderer.minLabelPosition = 0.05;
									valueAxis.renderer.maxLabelPosition = 0.95;

									var j = 0;
									for (var categoria in categorias) {
										// Create series
										var add = j+1,
										name_series = 'series'+add,
										name_bullet = 'bullet'+add;

										name_series = chart.series.push(new am4charts.LineSeries());
										name_series.dataFields.valueY = categorias[categoria];
										name_series.dataFields.categoryX = "fecha";
										name_series.name = categorias[categoria];
										name_series.dataFields.value = categorias[categoria];
										name_series.strokeWidth = 2;
										name_series.tensionX = 0.77;
										name_series.minBulletDistance = 15;

										// Drop-shaped tooltips
										name_series.tooltip.background.cornerRadius = 20;
										name_series.tooltip.background.strokeOpacity = 0;
										name_series.tooltip.pointerOrientation = "vertical";
										name_series.tooltip.label.minWidth = 40;
										name_series.tooltip.label.minHeight = 40;
										name_series.tooltip.label.textAlign = "middle";
										name_series.tooltip.label.textValign = "middle";

										var name_bullet = name_series.bullets.push(new am4charts.Bullet());
										name_bullet.interactionsEnabled = false;
										name_bullet.tooltipText = "{name} (Total: {valueY})";
										var durationRectangle = name_bullet.createChild(am4core.Rectangle);
										name_bullet.horizontalCenter = "middle";
										name_bullet.verticalCenter = "middle";
										name_bullet.width = 7;
										name_bullet.height = 7;
										durationRectangle.width = 7;
										durationRectangle.height = 7;

										var bullet = name_series.bullets.push(new am4charts.CircleBullet());
										bullet.events.on("hit", function(ev) {											
											$('#'+name_box).closest('.box-shadow').children('#databody').empty();
											var patients_ok = ev.target.dataItem.dataContext
											console.log(patients_ok)
											data_tabla_pacientes_ok_bad[posicion_tabla_pacientes_ok_bad]= {name_chart: name_box, data_ok:patients_ok, categoria: ev.target.dataItem.categoryX};
											var table=createTable(patients_ok, ev.target.dataItem.categoryX, 'success');
									  		$('#'+name_box).closest('.box-shadow').children('#databody').append(table);
											posicion_tabla_pacientes_ok_bad++;
										});
										j++
									}

									// Add chart cursor
									chart.cursor = new am4charts.XYCursor();
									chart.cursor.behavior = "zoomY";
									// Add legend
									chart.legend = new am4charts.Legend();

									chart.scrollbarX = new am4core.Scrollbar();

									chart.responsive.rules.push({
									  	relevant: function(target) {
									    	// Code that determines if rule is currently relevant
									    	// and returns true or false
									    	return false;
									  	},
									  	state: function(target, stateId) {
									    	// Code that creates a new SpriteState object to be
									    	// applied to target if Rule is "relevant"
									    	return;
									  	}
									});

									// Add chart title
									var title = chart.titles.create();
									title.text = graficas[i]['name'];
									title.fontSize = 25;
									title.marginBottom = 30;

									var label = categoryAxis.renderer.labels.template;
									label.truncate = true;
									label.maxWidth = 200;
									label.tooltipText = "{category}";

									categoryAxis.events.on("sizechanged", function(ev) {
										var axis = ev.target;
									  	var cellWidth = axis.pixelWidth / (axis.endIndex - axis.startIndex);
									  	if (cellWidth < axis.renderer.labels.template.maxWidth) {
										    axis.renderer.labels.template.rotation = -45;
										    axis.renderer.labels.template.horizontalCenter = "right";
										    axis.renderer.labels.template.verticalCenter = "middle";
									  	}else {
										    axis.renderer.labels.template.rotation = 0;
										    axis.renderer.labels.template.horizontalCenter = "middle";
										    axis.renderer.labels.template.verticalCenter = "top";
									  	}
									});

									valueAxis.renderer.labels.template.adapter.add("text", function(text) {
									  return text;
									});

								}
							}); // end am4core.ready()
			        }else{
			        	$('#mensaje').modal('hide');
			            mensaje(data.msj);
			        }
			    },10000);				
			}

			function createTable(data, mes, modo){
	        	console.log(data)
			    var table = "<table class='table text-center'>",
			    	count_ = Object.keys(data).length;;
			    table += "<thead>";
			    	table += "<tr class='"+modo+"'><th class='text-center' colspan='"+(count_-1)+"'>"+mes+"</th></tr>";
			    	table += "<tr>";
			    	for (var column in data) {
			    		if (column=='fecha') {
		            		continue;
		            	}
			    		table +="<th class='text-center'>"+column+"</th>";
			    	}
			    		//table +="<th><i class='fa fa-phone'></i></th>";
			        table +="</tr>";
			    table +="</thead>";
			    table +="<tbody>";
			    	table += "<tr>";
	            for (var column in data) {
	            	if (column=='fecha') {
	            		continue;
	            	}
		    		table +="<td>"+data[column]+"</td>";
		    	}
		    		table +="</tr>";	
			    table += "</tbody>";
			    table += "</table>";

			    return table;
			};

			$('.btn-plus>.btn-link').on('click', function () {
				var name_condition = $(this).attr('data-id'),
					id_condition = $(this).attr('data-_id'),
					name_class = $(this).attr('class');
				if (name_class.indexOf('bs-example-modal-1')!==-1) {
					$('#condicion_inc').val(name_condition);
					$('#condicion_inc').attr('data-_id', id_condition);
				}else{
					$('#trazador_inc').val(name_condition);
					$('#trazador_inc').attr('data-_id', id_condition);
				}
			})

			function validYear(e) {
				var regex = new RegExp("^\\d{4}$")
				if (regex.test(e)) {
			        return true;
			    }
			    return false;
			}
		</script>
	</body>
</html>