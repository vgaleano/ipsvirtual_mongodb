<table class="table table-hover table-bordered datos-clinicos">
	<tr>
		<th></th>
		<?php 
		for ($i=0; $i <count($mes_clinicos) ; $i++) { ?>
			<th><?php echo strtoupper(strftime("%h <br> %Y", strtotime ( $mes_clinicos[$i] ))) ?></th>
		<?php } ?>
	</tr>
	<?php
	foreach ($datos_clinicos as $key => $dc) {
		$name_td=key($dc); ?>
		<tr>
			<td><?php echo ($name_td=='razon_anbuminuria') ? 'RAZÓN ANBUMINURIA / CREATINURIA' : strtoupper(str_replace('_', ' ', $name_td)) ; ?></td>
			<?php					    								
			foreach ($dc as $keyD => $dt) {
				for ($i=0; $i < count($dt); $i++) { 
					$col_total=count($dt[$i]);
					if ($col_total>0) {
						if ($col_total>1) {
							$width_col = 100/$col_total; ?>
							<td class="no-padding">
							<table width="100%">
								<tr>
						<?php }										    						
						for ($j=0; $j < $col_total; $j++) { 
							$clase=$advertencia=$valor='';
							$valor = $dt[$i][$j];
							if ($name_td=='tas' && (intval($dt[$i][$j]) < 90 || intval($dt[$i][$j]) > 140)) {
								$clase = 'alert-rojo';
								$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
							}
							if ($name_td=='tad' && (intval($dt[$i][$j]) < 60 || intval($dt[$i][$j]) > 90)) {
								$clase = 'alert-rojo';
								$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
							}
							if ($name_td=='imc' && (intval($dt[$i][$j]) < 19 || intval($dt[$i][$j]) > 25)) {
								$clase = 'alert-rojo';
								$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
							}
							if ($name_td=='peso_ideal') {
								$valor=round($dt[$i][$j],'0');
							}
							if ($name_td=='hemoglobina_glicosilada' && intval($dt[$i][$j]) > 7) {
								$clase = 'alert-rojo';
								$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
							}
							if ($name_td=='colesterol_total' && intval($dt[$i][$j]) < 200) {
								$clase = 'alert-rojo';
								$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
							}
							if ($name_td=='colesterol_ldl') {
								if (intval(round($dt[$i][$j]['ldl_calculado']),'0') > 100) {
									$clase = 'alert-rojo';
								}
								if (intval(round($dt[$i][$j]['ldl']),'0') > 100) {
									$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
								}
								$valor=round($dt[$i][$j]['ldl'],'0');
							}
							if ($name_td=='colesterol_hdl' && intval($dt[$i][$j]) < 45) {
								$clase = 'alert-rojo';
								$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
							}	
							if ($name_td=='trigliceridos' && intval($dt[$i][$j]) > 150) {
								$clase = 'alert-rojo';
								$advertencia = '<i class="fa fa-exclamation-circle pull-right" aria-hidden="true" data-toggle="tooltip" title="Fuera del limite"></i>';
							}			    												
							if ($col_total==1) { ?>
								<td class="<?php echo $clase; ?>" style="padding: 3px;"><?php echo $valor.$advertencia; ?></td>
							<?php }else{ ?>
								<td width="<?php echo $width_col ?>" class="<?php echo $clase; ?>" style="padding: 3px;"><?php echo $valor.$advertencia; ?></td>
							<?php } } 					    											 
						if ($col_total>1){ ?>
							</tr>
							</table>
						</td>
						<?php }	}else{ ?>
						<td></td>
					<?php } } } ?>
		</tr>
	<?php } ?>					    						
</table>
