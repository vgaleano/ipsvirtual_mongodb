<?php date_default_timezone_set('America/Bogota');
if(isset($msj) && count($msj) > 0 ){ 
foreach ($msj as $m) { 
	if (isset( $m->registration_date)) {
	 	$timestamp = (String) $m->registration_date;
    	$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000);
	}else{
		$dateInLocal = '';
	} 
	
	if ($m->channel->name == 'SMS'){
		$img = 'https://s3.amazonaws.com/ipsvirtual/sms.png';
	}else if($m->channel->name == 'Llamada'){
		$img = 'https://s3.amazonaws.com/ipsvirtual/llamada.png';
	}else if($m->channel->name == 'Whatsapp'){
		$img = 'https://s3.amazonaws.com/ipsvirtual/whatspp.png';
	}else{
		$img = $m->channel->name;
	}?> 
	<div class="col-md-12 items-box-mensajes" style="padding-top: 10px;padding-bottom: 10px;padding-left: 25px;border-bottom: 1px solid #eee">
		<div class="col-md-1 col-lg-1 no-padding" style="text-align: center;">
			<img src="<?php echo $img?>" class="img-circle"  width="30" height="30">
		</div>
		<div class="col-md-4 col-lg-4 description-msj" style="text-align: center;">
			<strong><?php echo $m->user->name?></strong>
		</div>
		<div class="col-md-4 col-lg-4" style="text-align: left;">
			<p style="font-weight: lighter;"><?php echo $m->mensaje;?></p>
		</div>
		<div class="col-md-3 col-lg-3">
			<strong><?=$dateInLocal?></strong>
		</div>
	</div>
<?php } } ?>

<div class="col-md-12" style="text-align:center">
	<nav aria-label="Page navigation" id="pagination-digg1">
        <ul class="pagination">
            <?=$this->pagination->create_links(); ?>
        </ul>
    </nav>
</div>