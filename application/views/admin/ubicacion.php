<?php $this->load->view("common/admin/head")?>
<link rel="stylesheet" href="<?=base_url();?>css/admin/leaflet.css" />
    <script src="<?=base_url();?>js/leaflet.js"></script>
    <style>
        #map { width: 800px; height: 600px; }
        body { font: 16px/1.4 "Helvetica Neue", Arial, sans-serif; }
        .ghbtns { position: relative; top: 4px; margin-left: 5px; }
        a { color: #0077ff; }
    </style>
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
    	<div class="col-md-12 principal-padding" >

    		<div class="row">
				<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('titulo_ubicacion')?></h2>
    			</div>
    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
					<div class="input-group">
		      			<div class="input-group-addon">
		      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
	      				</div>
						<select class="form-control" name="searchentidad">
					    	<?php $ent = $this->session->userdata('searchentidad');  if(!$ent) $ent=""; ?>
					        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
					        <?php foreach ($entidad as $r) { ?>
								<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
							<?php } ?>
					    </select>
				    </div>							
					<?php } ?>
					<div class="input-group ml-1">
		      			<div class="input-group-addon">
		      				<label class="mb-0"><?=$this->lang->line('estado')?>:</label>
	      				</div>									
						<select class="form-control" id="filtro_estado">
							<?php $est=$this->session->userdata('searchestado'); if(!$est) $est=""; ?>
							<option value="" style="display: none;"><?=$this->lang->line('select_one')?></option>
                  			<?php foreach ($colores as $s) { ?>
                     			<option value="<?=$s?>" <?=($s==$est)? 'selected':''; ?>><?php if ($s=='Rojo intenso') {
                     				echo $this->lang->line('name_estado_1');
                     			}elseif ($s=='Rojo') {
                     				echo $this->lang->line('name_estado_2');
                     			}elseif ($s=='Amarillo') {
                     				echo $this->lang->line('name_estado_3');
                     			}else{
                     				echo $this->lang->line('name_estado_4');
                     			} ?></option>
                     		<?php } ?>
						</select>
					</div>
					<button type="button" class="btn btn-green ml-1" id="filtrar"><?=$this->lang->line('btn_filtrar')?></button>
    			</div>
			</div>

			<div class="row mt-20 mx-0" width="100%">
				<div class="col-xs-12 col-md-12 box-shadow">
					<div class="row" style="padding-bottom: 20px">
						<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 box-info" style="text-align: center;">
							 <span style="font-size: 40px" id="numAll"><?php echo (is_array($allPatient))?$allPatient[0]->total:'0';?> </span><br><span><?=$this->lang->line('total_pacientes')?></span>
						</div>
						<div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-6 col-md-offset-0 box-info" style="text-align: center;">
		            		<span id="numPb" style="font-size: 40px"></span><br><span id="pb-estado"></span>
						</div>
					</div>
					<div class="row" style="padding-top: 20px">
						<div class="col-md-12 ubicacion no-padding">
							<div id="map"></div>
						</div>
					</div>
				</div>
				
			</div>

    </div>
			
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
    	<!-- <script async defer src=""></script> -->

    	<script src="<?=base_url()?>js/leaflet-heat.js"></script>

		<script src="<?=base_url()?>js/latitud_longitud.js"></script>
		<script src="<?=base_url()?>js/latitud_longitud_1.js"></script>
		<script src="<?=base_url()?>js/latitud_longitud2.js"></script>
		<script src="<?=base_url()?>js/latitud_longitud_3.js"></script>

		<script  type="text/javascript" language="javascript">	   
			var id_entidad = '<?php echo $id_entidad; ?>';
	        var map = L.map('map').setView([7.121852, -73.114753], 16);
	        var array_puntos = [addressPoints, addressPoints1, addressPoints2, addressPoints3]
	        var heat='';

			var tiles = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
			}).addTo(map);

			$('#filtrar').on('click',function(){
	        	var estado = $('#filtro_estado').val(),
	        	bandera = 0;	        	
	        	if (estado=='') {
	        		bandera=1;
	        		mensaje("<?=$this->lang->line('ubicacion_bad_1')?>");
	        	}
	        	<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){ ?>	        		
	        		if ($('[name=searchentidad]').val()=='0') {
	        			bandera=1;
	        			mensaje("<?=$this->lang->line('ubicacion_bad_2')?>");
	        		}else{
	        			id_entidad=$('[name=searchentidad]').val();
	        		}
	        	<?php } ?>
	        	if (bandera==0) {
	        		getPoints(estado);
	        	}	        		
	        });

	        function getPoints(p) {
	           	if(p == undefined){
	                 return []
	           	}else{
	           		let seleccion = $("#filtro_estado option:selected").text();

	             	ajax('Pacientes/poblacionMapaDeCalor',
			        {   
			            estado: p,
			            entidad: id_entidad
			       	},
			        function(data){
			            if(data.res=="ok"){			            	
			            	var estado_='';			            	
			            	document.getElementById('numAll').innerHTML=data.total_pacientes;
						    if (p!='0') {
						    	if (heat!='') {
						    		map.removeLayer(heat)
						    	}								   
						    	document.getElementById('numPb').innerHTML=data.total;
						    	document.getElementById('pb-estado').innerHTML='Total Pacientes '+seleccion;
						    	addressP = array_puntos[data.name];
						    	addressP = addressP.map(function (p) { return [p[0], p[1]]; });

								heat = L.heatLayer(addressP).addTo(map),
								    draw = true;
						    }							    					        
		                }else{
		                	document.getElementById('numAll').innerHTML=data.total_pacientes;
		                    document.getElementById('numPb').innerHTML=''
		                    document.getElementById('pb-estado').innerHTML=''
		                }
		            },10000);
               	}
	        }
			
		</script>
	</body>
</html>