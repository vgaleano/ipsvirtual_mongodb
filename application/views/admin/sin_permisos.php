<?php $this->load->view("common/admin/head")?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/dashboard.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">
				<div class="col-md-12 container-fluid" id="content-principal">

					<div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 gracias">
						<div class="col-md-4">
							<p align="center"><img class="img-responsive" src="<?php echo base_url();?>img/Virtual.png"></p>
						</div>
						<div class="col-md-8">
							<h3><?=$this->lang->line('dashboard_bad_6')?></h3>
						</div>
						<div class="col-md-10 col-md-offset-1" style="text-align:center">
							<a href="<?php echo base_url()?>admin/dashboard" class="btn boton btn-block" style="color:#fff!important"><?=$this->lang->line('inicio')?></a> 
						</div>
					</div>			
				</div>
			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
	</body>
</html>