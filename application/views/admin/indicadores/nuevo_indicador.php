<?php $this->load->view("common/admin/head")?>
<link href="<?php echo base_url(); ?>css/configuracion_indicadores.css" rel="stylesheet">
<style type="text/css">
	.px-4{
		padding: 0 2rem;
	}
	.font-bold{
		font-weight: 600;
	}
	#collapseInfo input.border-red {
		border-bottom: 1px solid red;
	}
	select.border-red, input.border-red {
		border: 1px solid red;
	}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/typeahead.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
    	<div class="col-md-12 principal-padding" >

    		<div class="row">
    			<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('new_indicador_text_1')?> <a href="<?php echo base_url(); ?>admin/configuracionIndicadores" class="btn btn-border-dark"><i class="fa fa-arrow-left"></i></a></h2>
    			</div>
    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
						<form id="formEntidad" action="<?php print base_url();?>admin/nuevoIndicador" method="POST" onsubmit="return false">
							<div class="input-group">
				      			<div class="input-group-addon">
				      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
			      				</div>
								<select class="form-control" name="searchentidad">
							    	<?php $ent = $this->session->userdata('searchentidad'); if(!$ent) $ent=""; ?>
							        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
							        <?php foreach ($entidad as $r) { ?>
										<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
									<?php } ?>
							    </select>
						    </div>																		
						</form>
					<?php } ?>
				</div>
			</div>

			<div class="row box-shadow content_padre  mt-20">
				<div class="col-xs-12 col-md-12" id="contentAccordion">
				</div>	
			</div>
			
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
		<script  type="text/javascript" language="javascript" src="<?php print base_url(); ?>js/typeahead.js"></script>
		<script  type="text/javascript" language="javascript">

			var box_parametro = 1, //caja de grafica
			box_grupo = 1, //caja de grupo control
			box_categoria = 1, //cara de categoria
			heading = 1,
			codigosT = new Array(), //array de columnas variables
			array_periodicidad = <?php echo json_encode($periodicidad); ?>,
			array_tipo_indicadores = <?php echo json_encode($tipo_indicadores); ?>,
			array_estandar = <?php echo json_encode($estandar); ?>,
			box_estandar=0,
			columnsT = new Array();
			//console.log(array_periodicidad);

			//add row PADRE - OK
			function addRow() {
				var content='<div class="col-xs-12 col-md-12 card">';
					    content+='<div class="col-xs-12 col-md-12 card-header">';
					      	content+='<div class="col-xs-12 col-md-12 mt-1">';
					      		content+='<div class="panel-group pl-15" id="accordionGlobal">';
					      			//content informacion del evento
					      			content+='<div class="panel panel-default">';
					      				content+='<div class="panel-heading">';
								            content+='<h4 class="panel-title">';
								                content+='<a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseInfo"><?=$this->lang->line('indicador_text_2')?></a>';
								            content+='</h4>';
								       	content+='</div>';
								       	content+='<div id="collapseInfo" class="panel-collapse collapse collapseInfo">';

						      				content+='<div class="form-group">';
						      					content+='<label><?=$this->lang->line('titulo')?></label>'
								        		content+='<input class="form-control input-border " placeholder="<?=$this->lang->line('indicador_text_3')?>" name="titulo">';
								        	content+='</div>';
								        	content+='<div class="col-xs-12 col-md-4 pl-0">';
								      			content+='<div class="form-group">';
									      			content+='<label><?=$this->lang->line('periodicidad')?></label>';
									      			content+='<select class="form-control " name="periodicidad">';
									      				for (var i = 0; i < array_periodicidad.length; i++) {						      					
									      					content+='<option value="'+array_periodicidad[i]._id.$oid+'">'+array_periodicidad[i].name+'</option>';
									      				}
									      			content+='</select>';
									      		content+='</div>';
								      		content+='</div>';
								      		content+='<div class="clearfix"></div>';
								        	content+='<div class="form-group">';
								        		content+='<label><?=$this->lang->line('descripcion')?></label>'
								      			content+='<textarea class="form-control " placeholder="<?=$this->lang->line('indicador_text_4')?>" name="descripcion"></textarea>';
								      		content+='</div>';
								      		content+='<div class="clearfix"></div>';								      		
						      			content+='</div>';
					      			content+='</div>';
					      			//content estandar globales - riesgos eventos
					      			content+='<div class="panel panel-default">';
					      				content+='<div class="panel-heading">';
								            content+='<h4 class="panel-title input_btn">';
								                content+='<a data-toggle="collapse" data-parent="#accordionEstandar" href="#collapseEstandar"><?=$this->lang->line('indicador_text_5')?></a>';
								                content+='<button class="btn btn-green btn-xs addEstandar" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_5')?>"><i class="fa fa-plus"></i></button>';
								            content+='</h4>';
								       	content+='</div>';
								        content+='<div id="collapseEstandar" class="panel-collapse collapse collapseEstandar">';
								            content+='<div class="panel-body">';
									            content+='<table class="table">';
							      					content+='<tbody class="padre" id="content_estandares">';
							      					//agregar los estandares globales
							      					content+='</tbody>';
							      				content+='</table>';
						      				content+='</div>';
						      			content+='</div>';
					      			content+='</div>'
					      			//content parametros globales, se tienen en cuenta para el numerador y denominador
								    content+='<div class="panel panel-default">';								    	
								       	content+='<div class="panel-heading">';
								            content+='<h4 class="panel-title input_btn">';
								                content+='<a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseGlobal"><?=$this->lang->line('indicador_text_6')?></a>';
								                content+='<button class="btn btn-green btn-xs addParametros" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_6')?>"><i class="fa fa-plus"></i></button>';
								            content+='</h4>';
								       	content+='</div>';
								        content+='<div id="collapseGlobal" class="panel-collapse collapse collapseGlobal">';
								            content+='<div class="panel-body">';
									            content+='<table class="table">';
							      					content+='<tbody class="padre" id="content_parametros">';
							      					//agregar los parametros globales
							      					content+='</tbody>';
							      				content+='</table>';
						      				content+='</div>';
						      			content+='</div>';						      			
						      		content+='</div>';
						      		//content datos
						      		content+='<div class="panel panel-default">';
						      			content+='<div class="panel-heading">';
								            content+='<h4 class="panel-title input_btn">';
								                content+='<a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseDatos">Agregar reglas </a>';
								                content+='<button class="btn btn-green pull-right addCategorias btn-xs" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_8')?>"><i class="fa fa-plus"></i></button>';
								            content+='</h4>';
								       	content+='</div>';
								        content+='<div id="collapseDatos" class="panel-collapse collapse collapseDatos">';
								        	content+='<div class="panel-body">';
								        		//content categorias
								        		content+='<div class="panel-group" id="accordionCategorias">';
								        		content+='</div>';

								        	content+='</div>';
								        content+='</div>';
						      		content+='</div>';
						      		//content clasificación globales
								    content+='<div class="panel panel-default" data-id="collapseClasificacion">';
								       	content+='<div class="panel-heading">';
								            content+='<h4 class="panel-title input_btn">';
								                content+='<a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseClasificacion"><?=$this->lang->line('indicador_text_9')?></a>';
								                content+='<button class="btn btn-green btn-xs addGrupos" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_6')?>"><i class="fa fa-plus"></i></button>';
								            content+='</h4>';
								       	content+='</div>';
								        content+='<div id="collapseClasificacion" class="panel-collapse collapse collapseClasificacion">';
								            content+='<div class="panel-body">';
									            content+='<table class="table">';
							      					content+='<tbody class="panel-group accordionGrupos" id="accordionGruposClasficacion">';
							      					//agregar las clasificaciones globales
							      					content+='</tbody>';
							      				content+='</table>';
						      				content+='</div>';
						      			content+='</div>';						      			
						      		content+='</div>';
						      	content+='</div>';
						      	content+='<div class="col-xs-12 col-md-12 pr-0">';
									content+='<button type="button" class="btn btn-save guardar_padre pull-right" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_10')?>" data-modo="guardar"><i class="fa fa-save"></i></button>';
									content+='<button type="button" class="btn btn-eraser guardar_padre pull-right" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_11')?>" data-modo="borrador"><i class="fa fa-save"></i> <?=$this->lang->line('borrador')?></button>';
								content+='</div>';
						    content+='</div>';
						content+='</div>';
								
					content+='</div>';						

				$('#contentAccordion').append(content);
				//parametros estandares
				$('#contentAccordion>.card:not(.activeCard)').find('.panel .addEstandar').click();
				//parametros globales
				$('#contentAccordion>.card').find('.panel .addParametros').click();
				//parametros clasificaciones
				$('#contentAccordion>.card').find('.panel .addGrupos').click();
				//parametros en datos
				$('#contentAccordion>.card').find('.addCategorias').click();
			}

			//add estandares
			$('body').on('click', '.addEstandar', function () {
				if (box_estandar<array_tipo_indicadores.length) {
					var content='';
					content+='<tr data-class="rowEstandar'+box_estandar+'">';					
						content+='<td rowspan="3">';
							content+='<select class="form-control tipo_indicador" id="rowEstandar'+box_estandar+'" name="tipo_indicador" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_12')?>">';
		                		content+='<option value="" style="display:none;"><?=$this->lang->line('select')?></option>';
		                		for (var ij = 0; ij < array_tipo_indicadores.length; ij++) {
		                			content+='<option value="'+array_tipo_indicadores[ij]._id.$oid+'">'+array_tipo_indicadores[ij].name+'</option>';                			
		                		}
		                	content+='</select>'
						content+='</td>';

						for (var i = 0; i < array_estandar.length; i++) {
							
							if (i!=0) {
								content+='<tr data-class="rowEstandar'+box_estandar+'">';
							}
					
						content+='<td>'+array_estandar[i].name+'<input type="hidden" name="estandar" value="'+array_estandar[i]._id.$oid+'" data-name="'+array_estandar[i].name+'"></td>';
						content+='<td class="text-center">';
							content+='<select class="form-control btn_operador" name="tipo_operador">';
								content+='<option value="">Tipo operador</option>';
								content+='<option value="="> = </option>';
								content+='<option value="!="> != </option>';
								content+='<option value=">"> > </option>';
								content+='<option value="<"> < </option>';
								content+='<option value=">="> >= </option>';
								content+='<option value="<="> <= </option>';
								content+='<option value="rango"> A-B </option>';
							content+='</select>';
						content+='</td>';
						content+='<td class="text-center">';
							content+='<div class="input_">';
								content+='<input class="form-control " type="text" name="valor_parametro">';
							content+='</div>';
							content+='<div class="entre_ hidden">';
								content+='<div class="form-group">';
									content+='<div class="input-group">';
								      	content+='<input type="text" class="form-control" name="valor1" placeholder="<?=$this->lang->line('indicador_text_13')?>">';
								      	content+='<div class="input-group-addon">';
								      		content+='<select name="operador1">';
								      			content+='<option value=">"> > A</option>';
								      			content+='<option value="<"> < A</option>';
								      			content+='<option value="<="> <= A</option>';
								      			content+='<option value=">="> >= A</option>';
								      		content+='</select>';
								      	content+='</div>';
								    content+='</div>';
								content+='</div>';
								content+='<div class="form-group">';
								    content+='<div class="input-group">';
								      	content+='<input type="text" class="form-control" name="valor2" placeholder="<?=$this->lang->line('indicador_text_15')?>">';
								      	content+='<div class="input-group-addon">';
								      		content+='<select name="operador2">';
								      			content+='<option value=">"> > A</option>';
								      			content+='<option value="<"> < A</option>';
								      			content+='<option value="<="> <= A</option>';
								      			content+='<option value=">="> >= A</option>';
								      		content+='</select>';
								      	content+='</div>';
								    content+='</div>';
								content+='</div>';
							content+='</div>';
						content+='</td>';
						if (i==0) {
							content+='<td rowspan="3" class="text-center">';
							content+='<button type="button" class="btn btn-trash delete_estandar" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_22')?>"><i class="fa fa-trash"></i></button>';
							content+='</td>';
						}
						content+='</tr>';									
					}
					$(this).closest('#accordionGlobal').find('#content_estandares').append(content);
					box_estandar = box_estandar+1;
				}					
			});

			//add categoria - OK
			$('body').on('click', '.addCategorias', function () {
				var content='<div class="panel panel-default" data-id="collapseGrupos">';
						content+='<div class="panel-heading input_btn">';
			               	content+='<a data-toggle="collapse" data-parent="#accordionCategorias" href="#collapseGrupos'+box_categoria+'">';
			               		content+=' <input class="form-control categoria" name="categoria" placeholder="<?=$this->lang->line('indicador_text_16')?>">';
							content+='</a>';
			                content+='<div><button class="btn btn-green addGrupos btn-xs ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_23')?>"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-trash delete_categorias btn-xs ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_24')?>"><i class="fa fa-trash"></i></button></div>';							
				       	content+='</div>';
				       	content+='<div id="collapseGrupos'+box_categoria+'" class="panel-collapse collapse collapseGrupos">';
							content+='<div class="panel-body">';
								//content grupos
								content+='<div class="panel-group accordionGrupos" id="accordionGrupos">';
								content+='</div>';

							content+='</div>';
						content+='</div>';
					content+='</div>';
				
				$(this).closest('#accordionGlobal').find('#accordionCategorias').append(content);
				$(this).closest('.panel').find('#accordionCategorias').children('.panel').last().find('.addGrupos').click();
				box_categoria++;
			});

			//add grupos control - OK
			$('body').on('click', '.addGrupos', function () {
				var id_ = $(this).closest('.panel').attr('data-id'),
				name_label_grupos = (id_=='collapseGrupos')?'Nombre del label':'Nombre de la clasificación',
				content='<div class="panel panel-default">';				
						content+='<div class="panel-heading">';
							content+='<h4 class="panel-title input_btn">';
				                content+='<a data-toggle="collapse" data-parent="#accordionGrupos" href="#collapseParametros'+box_parametro+'">';
				                	content+=' <input class="form-control name_grupo" name="name_grupo" placeholder="'+name_label_grupos+'">';
				                content+='</a>';
				                content+='<div class="d-flex justify-content-center align-items-center">';
				                if (id_!='collapseGrupos') {
				                	content+='<select class="form-control tipo_regla" name="tipo_regla" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_17')?>">';
				                		content+='<option value="y" >Y</option>';
				                		content+='<option value="y/o">Y/O</option>';
				                	content+='</select>';
				                }
				                	content+='<select class="form-control tipo_indicador" name="tipo_indicador" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_12')?>">';
				                		content+='<option value="" style="display:none;"><?=$this->lang->line('select')?></option>';
				                		for (var i = 0; i < array_tipo_indicadores.length; i++) {
				                			content+='<option value="'+array_tipo_indicadores[i]._id.$oid+'" ';
				                			if (i==0) {
				                				content+=' selected ';
				                			}				                			
				                			content+='>'+array_tipo_indicadores[i].name+'</option>';
				                		}
				                	content+='</select>';
				                	content+='<button class="btn btn-green addParametros btn-xs ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_18')?>"><i class="fa fa-plus"></i></button>';
				                	content+='<button type="button" class="btn btn-xs btn-trash delete_grupo ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_19')?>"><i class="fa fa-trash"></i></button>';
				                content+='</div>';
				            content+='</h4>';
						content+='</div>';
						content+='<div id="collapseParametros'+box_parametro+'" class="panel-collapse collapse collapseParametros">';
				        	content+='<div class="panel-body">';
				        		//content parametros
				        		content+='<div class="table-responsive">';
				        			content+='<table class="table">';
				        				content+='<tbody>';																
		  								content+='</tbody>';
				        			content+='</table>';
				        		content+='</div>';
				        	content+='</div>';
				        content+='</div>';

					content+='</div>';

				$(this).closest('.panel').find('.accordionGrupos').append(content);
				$(this).closest('.panel').find('.addParametros').last().click();
				box_parametro++;
			});

			//add parametros - OK
			var hijos=0;
			$('body').on('click', '.addParametros', function () {
				//console.log('entra add parametros ')
				var id_ = $(this).closest('.panel-group').attr('id'),
				content='<tr>';
				if (id_ == 'accordionGrupos') {
					content+='<td class="text-center">';
						content+='<select class="form-control" name="tipo_formula">';
							content+='<option value="numerador">Numerador</option>';
							content+='<option value="denominador">Denominador</option>'
					content+='</td>';
				}
				
					content+='<td class="text-center">';
						if (id_ != 'accordionGruposClasficacion') {
						content+='<div class="input-group">';
						
			      			content+='<div class="input-group-addon param_">';
			      				content+='<input class="form-check-input" type="checkbox" name="tipo_fecha" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_20')?>">';
		      				content+='</div>';
		      			
							content+='<input type="text" class="form-control tags_auto " name="tags_auto" placeholder="<?=$this->lang->line('indicador_text_14')?>" id="hijoTag'+hijos+'" data-id="">';
					    content+='</div>';
					    }else{
					    	content+='<input type="text" class="form-control tags_auto " name="tags_auto" placeholder="<?=$this->lang->line('indicador_text_14')?>" id="hijoTag'+hijos+'" data-id="">';
					    }
					content+='</td>';
				
					content+='<td class="text-center">';
						content+='<select class="form-control btn_operador" name="tipo_operador">';
							content+='<option value="">Tipo operador</option>';
							content+='<option value="="> = </option>';
							content+='<option value="!="> != </option>';
							content+='<option value=">"> > </option>';
							content+='<option value="<"> < </option>';
							content+='<option value=">="> >= </option>';
							content+='<option value="<="> <= </option>';
							content+='<option value="rango"> A-B </option>';
						content+='</select>';
					content+='</td>';
					content+='<td class="text-center">';
						content+='<div class="input_">';
							content+='<input class="form-control " type="text" name="valor_parametro" list="valor_parametroA'+hijos+'">';
						    content+='<datalist id="valor_parametroA'+hijos+'">';
						    content+='</datalist>';
						content+='</div>';
						content+='<div class="entre_ hidden">';
							content+='<div class="form-group">';
								content+='<div class="input-group">';
							      	content+='<input type="text" class="form-control" name="valor1" placeholder="<?=$this->lang->line('indicador_text_13')?>" list="valor_parametroB'+hijos+'">';
							      	content+='<datalist id="valor_parametroB'+hijos+'">';
						    		content+='</datalist>';
							      	content+='<div class="input-group-addon">';
							      		content+='<select name="operador1">';
							      			content+='<option value=">"> > A</option>';
							      			content+='<option value="<"> < A</option>';
							      			content+='<option value="<="> <= A</option>';
							      			content+='<option value=">="> >= A</option>';
							      		content+='</select>';
							      	content+='</div>';
							    content+='</div>';
							content+='</div>';
							content+='<div class="form-group">';
							    content+='<div class="input-group">';
							      	content+='<input type="text" class="form-control" name="valor2" placeholder="<?=$this->lang->line('indicador_text_15')?>" list="valor_parametroB'+hijos+'">';
							      	content+='<datalist id="valor_parametroB'+hijos+'">';
						    		content+='</datalist>';
							      	content+='<div class="input-group-addon">';
							      		content+='<select name="operador2">';
							      			content+='<option value=">"> > A</option>';
							      			content+='<option value="<"> < A</option>';
							      			content+='<option value="<="> <= A</option>';
							      			content+='<option value=">="> >= A</option>';
							      		content+='</select>';
							      	content+='</div>';
							    content+='</div>';
							content+='</div>';
						content+='</div>';
					content+='</td>';
					content+='<td class="text-center">';
						content+='<button type="button" class="btn btn-trash delete_parametro" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_21')?>"><i class="fa fa-trash"></i></button>';
					content+='</td></tr>';
				$(this).closest('.panel').find('tbody').append(content);
			
				$('#hijoTag'+hijos).typeahead({
					hint: true,
					highlight: true,
					minLength: 1,
				},{
					limit: 12,
					async: true,
					source: function(query, processSync, processAsync){
						return $.ajax({
							url: "<?php print base_url(); ?>indicadores/verColumnaConfiguracion",
							type: 'POST',
							data:{
								tags_auto: query,
								id_entidad: '<?php echo $id_entidad; ?>'
							},
							dataType: 'JSON',
							success: function(data){
								$(document).ajaxStop(function(){
									$("body").removeClass("loading");
								});
								if (data.res === "ok") {
									var tags_auto = [];
									for (var i = 0; i < data.bar.length; i++) {
										tags_auto.push(data.bar[i].name);
										codigosT[data.bar[i].name]=data.bar[i]._id.$oid;
										columnsT[data.bar[i].name]=data.bar[i].columns;
									}
									return processAsync(tags_auto);		                    		                     
								}else{
									let muestra5 = $('#muestra5');
									muestra5.html(data.msj);		                 	
								}
							}
						});
					}
				});
				$('#hijoTag'+hijos).bind('typeahead:select', function(ev, suggestion) {					
					var str=suggestion,
					aa = $(this).closest('td').next().next().children('.entre_').children().children().children('datalist'),
					bb = $(this).closest('td').next().next().children('.input_').children('datalist');
					aa.children('option').remove();
					bb.children('option').remove();
					//trae los distinct valores del parametro seleccionado
					ajax('indicadores/getValorColumnaVariables',
	                {   
	                	id_entidad: '<?php echo $id_entidad; ?>',
	                	columns: columnsT[suggestion]
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	aa.append('<option value="NULL"></option>');
	                    	for (var i = 0; i < data.bar.length; i++) {
	                    	    aa.append('<option value="'+data.bar[i]._id+'"></option>');
	                    	    bb.append('<option value="'+data.bar[i]._id+'"></option>');
	                    	}                                   
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);                
					$(this).attr('data-id',codigosT[suggestion]);
				});
				hijos++;
			});

			//change operador input por select - OK
			$('body').on('change', '.btn_operador', function () {
				$(this).closest('td').next().children().addClass('hidden');
				if ($(this).val()=='rango') {
					$(this).closest('td').next().children('.entre_').removeClass('hidden');
				}else{
					$(this).closest('td').next().children('.input_').removeClass('hidden');
				}
			});

			//######## ELIMINAR - OK

			//eliminar estandar - front OK
			$('body').on('click', '.delete_estandar', function () {
				$(this).closest('tr').next().remove();	
				$(this).closest('tr').next().remove();	
				$(this).closest('tr').remove();	
				box_estandar= box_estandar-1;
				box_estandar=(box_estandar<0)?0:box_estandar;
				//console.log(box_estandar);
			})
			//eliminar categorias  - front OK
			$('body').on('click', '.delete_categorias', function () {
				$(this).closest('.panel').remove();				
			});

			//eliminar grupo control - front OK
			$('body').on('click', '.delete_grupo', function () {
				$(this).closest('.panel').remove();
			});

			//eliminar parametro - front OK
			$('body').on('click','.delete_parametro', function() {
				$(this).parent().parent().remove();
			});

			//####### END ELIMINAR

			//####### ADD BORDER BLUE

			//agrega el borde azul y gris a la card y mueve los actions buttons del padre
			$('body').on('click', '.card:not(.activeCard)', function () {
				$('.div_btn').removeClass('hidden');
				$('.card.activeCard .btn-div').addClass('hidden');
				$('.card.activeCard .grafica>div.activeGrafica').removeClass('activeGrafica');
				$('.card').removeClass('activeCard');
				var position = $(this).addClass('activeCard').position();
				$(this).find('.btn-div').removeClass('hidden');				
				$('.div_btn').animate({"top":position.top+"px"},1000,"linear");
				console.log(position);
			});

			//###### END BORDER BLUE
		   
		   	//remove border red when is bad
		   	$('body').on('change', '[name=titulo].border-red, .collapseEstandar select.border-red, #collapseClasificacion input.border-red, #collapseClasificacion select.border-red, [name=searchentidad].border-red', function () {
		   		$(this).removeClass('border-red');
		   	});

		    //guardar el padre con sus hijos gráficas
			$('body').on('click', '.guardar_padre', function () {	
				var bandera = 0,
				modo_guardar = $(this).attr('data-modo');		
				/* 
				tendrá la estructura: [informacion][parametros_globales][datos]
					informacion: [titulo][periodicidad][descripcion][estandar];
						estandar: [#][tipo_indicador][tipo_riesgo][tipo_operadorr][valor][operador1][valor1][operador2][valor2];
					parametros_globales: [#][tipo_fecha][id_parametro][tipo_operador][valor][operador1][valor1][operador2][valor2];
					clasificacion: [#][nombre_clasificacion][tipo_regla][tipo_operador][tipo_indicador]
						parametros: [#][id_parametro][tipo_operador][valor][operador1][valor1][operador2][valor2]
					datos: [#][name_categoria][grupos]
						grupos: [#][name_grupo][parametros][tipo_indicador]
							parametros: [#][tipo_formula (numerador/denominador)][tipo_fecha][id_parametro][tipo_operador][valor][operador1][valor1][operador2][valor2]
				*/
				var info = [];
				if ($('[name=titulo]').val()=='') {
					bandera=1;
					$('[name=titulo]').addClass('border-red');
					$('[name=titulo]').closest('.panel').children('.panel-heading').find('a').trigger('click');
					return mensaje('Debe agregar un título al indicador');
				}
				//informacion "Agregar estándar Globales" 
				var tmp_estandar = [],
				msj_estandar = '';
				$('#content_estandares>tr').each(function () {
					var name_class = $(this).attr('data-class'),
					tipo_indicador = $('#'+name_class).val(),
					tipo_operador = $(this).find('[name=tipo_operador]').val(),
					valor = $(this).find('[name=valor_parametro]').val(),
					operador1 = $(this).find('[name=operador1]').val(),
					operador2 = $(this).find('[name=operador2]').val(),
					valor1 = $(this).find('[name=valor1]').val(),
					valor2 = $(this).find('[name=valor2]').val(),
					ban = 0;
					if (modo_guardar=="guardar") {
						if (tipo_indicador=='') {
							ban = 1;
							$('#'+name_class).addClass('border-red');
							$(this).closest('.panel').children('.panel-heading').find('a').trigger('click');
							msj_estandar=('Debe seleccionar el tipo de indicador para el estándar global');
							return false;
						}
						if (tipo_operador=='') {
							ban = 1;
							$(this).find('[name=tipo_operador]').addClass('border-red');
							$(this).closest('.panel').children('.panel-heading').find('a').trigger('click');
							msj_estandar=('Debe seleccionar un operador para el estándar global');
							return false;
						}else{
							if (tipo_operador!='rango' && valor=='') {
								$(this).find('[name=valor_parametro]').addClass('border-red');
							}
							if (tipo_operador=='rango') {
								if (valor1=='') {
									$(this).find('[name=valor1]').addClass('border-red');
								}
								if (valor2=='') {
									$(this).find('[name=valor2]').addClass('border-red');
								}								
							}
							if ( (tipo_operador!='rango' && valor=='') || (tipo_operador=='rango' && (valor1=='' || valor2=='')) ) {
								ban = 1;
								$(this).closest('.panel').children('.panel-heading').find('a').trigger('click');
								msj_estandar=('Debe agregar un valor para el estándar global');
								return false;
							}
						}
					}						
					if (bandera==0 && ban==0) {
						var tmp={
							type_indicator: { 'id': tipo_indicador, 'name': $('#'+name_class).children("option:selected").html()},
							type_standar: {'id': $(this).find('[name=estandar]').val(), 'name': $(this).find('[name=estandar]').attr('data-name')},
							operator: tipo_operador,
							value: valor,
							operator1: (tipo_operador=='rango')?operador1:'',
							value1: valor1,
							operator2: (tipo_operador=='rango')?operador2:'',
							value2: valor2
						};
						tmp_estandar.push(tmp);
					}						
				});

				if (msj_estandar!='') {
					return mensaje(msj_estandar);
				}

				//informacion del evento
				var general = {
					name: $('[name=titulo]').val(),
					periodicity: {'id': $('[name=periodicidad]').val(), 'name': $('[name=periodicidad]').children("option:selected").html()},
					description: $('[name=descripcion]').val()
				};

				var tmp_global = [];
				//informacion de " Agregar parámetros Globales"
				$('#collapseGlobal tr').each(function () {
					var global_ = getDatosParametros($(this), 'global', modo_guardar, columnsT);
					if (global_['bandera']==0) {
						tmp_global.push(global_.tmp);
					}
				});

				//información "Agregar clasificación Globales"
				var tmp_clasificacion=[]
				msj_clasificacion='';
				$('#accordionGruposClasficacion>.panel').each(function () {
					var tmp_parametros=[],
					ban = 0;
					if ($(this).find('[name=name_grupo]').val()=='') {
						/*bandera=1;
						msj_clasificacion='Debe agregar un nombre a la clasificación';
						$(this).find('[name=name_grupo]').addClass('border-red');
						return false;*/
					}

					$(this).find('.collapseParametros tr').each(function () {
						var parametros_ = getDatosParametros($(this), 'global', modo_guardar, columnsT);
						if (parametros_['bandera']!=0) {
							/*bandera=1;
							msj_clasificacion=parametros_['msj'];
							$(parametros_['input']).addClass('border-red');
							ban++;
							return false;*/
						}else{
							if (parametros_['tmp'].length>0) {

							}
							tmp_parametros.push(parametros_['tmp']);
						}							
					});
					if (ban==0) {
						var grupo = {
							name_grupo: $(this).find('.name_grupo').val(),
							datos: tmp_parametros,
							type_indicator: {'id': $(this).find('.tipo_indicador').val(), 'name': $(this).find('.tipo_indicador').children("option:selected").html()},
							type_ruler: $(this).find('.tipo_regla').val()
						}
						tmp_clasificacion.push(grupo);
					}
				})

				if (msj_clasificacion!='') {
					mensaje(msj_clasificacion);
				}

				var tmp_categorias = [],
				msj_categorias='';
				//informacion de datos
				$('#accordionCategorias>.panel').each(function () {					
					var tmp_grupos = [],
					ban = 0;
					if ($(this).find('[name=categoria]').val()=='') {
						bandera=1;
						msj_categorias='Debe agregar un nombre a la categoría';
						return false;
					}
					$(this).find('#accordionGrupos .panel').each(function () {						
						var tmp_parametros = [],
						tipo_indicador = $(this).find('[name=tipo_indicador]').val();

						if ($(this).find('.name_grupo').val()=='') {
							bandera=1;
							msj_categorias='Debe agregar un nombre del label';
							return false;
						}

						if (tipo_indicador=='' && modo_guardar=="guardar"){
							bandera=1;
							ban++;
							msj_categorias='Debe seleccionar un tipo de indicador, por defecto es Normal';
							return false;
						}

						$(this).find('.collapseParametros tr').each(function () {
							var parametros_ = getDatosParametros($(this), $(this).find('[name=tipo_formula]').val(), modo_guardar, columnsT);
							if (parametros_['bandera']!=0) {
								bandera=1;
								msj_categorias=parametros_['msj'];
								ban++;
								$(parametros_['input']).addClass('border-red');
								return false;
							}else{
								tmp_parametros.push(parametros_['tmp']);
							}							
						});
						if (ban==0) {
							var grupo = {
								name_grupo: $(this).find('.name_grupo').val(),
								datos: tmp_parametros,
								type_indicator: {'id': tipo_indicador, 'name': $(this).find('[name=tipo_indicador]').children("option:selected").html()},
							}
							tmp_grupos.push(grupo);
						}							
					});
					if (ban==0) {
						var cat_ = {
							name_categoria: $(this).find('[name=categoria]').val(),
							grupos: tmp_grupos
						}
						tmp_categorias.push(cat_);
					}						
				});
				if (msj_categorias!='') {
					mensaje(msj_categorias);
				}
				var tmp_g = {
					information: general, //"Agregar información del evento"
					global_standar: tmp_estandar, //"Agregar estándar Globales"
					global_parameters: tmp_global, //"Agregar  parámetros Globales"
					global_ranking: tmp_clasificacion, //"Agregar clasificación Globales"
					datos: tmp_categorias //"Agregar datos"
				}

				info.push(tmp_g);
				if (info.length==0) {
					bandera = 1;
				}				
				if ($('[name=searchentidad]').val()=='') {
					bandera = 1;
					$('[name=searchentidad]').addClass('border-red');
					mensaje('Debe seleccionar la entidad');
				}
				if (bandera == 0) {
					var id_entidad = $('[name=searchentidad]').val();
					ajax('indicadores/guardarIndicadorPadre',
	                {   
	                	id: '',
	                	info: info,
	                	id_entidad: id_entidad,
	                	modo_guardar: (modo_guardar=='guardar')?1:0
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	mensaje(data.msj);
	                    	window.location = base_url+'admin/configuracionIndicadores';
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
				}
			});
			//objeto, de donde viene, el modo de guardar (son filas de los parámetros)
			function getDatosParametros(e, f, modo, columns=false) {
				var rta = [],
				object_tags_auto = e.find('[name=tags_auto]'),
				parametro = object_tags_auto.attr('data-id'),
				valor = valor1 = operador1 = valor2 = operador2 = '',
				tmp_={};

				var columns_ = columns[object_tags_auto.val()];
				console.log(columns_);

				rta['msj'] = '';
				rta['bandera']=0;
				rta['tmp']={};
				rta['input']='';

				if (modo=='guardar') {
					if (parametro==undefined || parametro=='') {						
						if (f!='denominador' || f=='global') {
							rta['msj'] = 'Debe seleccionar un parámetro';
							rta['bandera']=1;
							rta['input']=object_tags_auto;
							return rta;
						}					
					}else{
						//si el parametro no es tipo fecha y no ha seleccionado el tipo_operador -> bad				
						if (e.find('[name=tipo_fecha]').is(":checked") != true && (e.find('[name=tipo_operador]').val()=='Tipo operador' || e.find('[name=tipo_operador]').val()=='')) {
							rta['msj'] = 'Debe seleccionar un operador';
							rta['bandera']=1;
							rta['input']=e.find('[name=tipo_operador]');
							return rta;
						}else{
							//tipo_operador es >, =, >=, <, <= o !=
							if (e.find('[name=tipo_operador]').val()!='rango') {
								valor = e.find('[name=valor_parametro]').val();
							}else{
								//tipo_operador es rango
								if (e.find('[name=valor1]').val()=='') {
									rta['msj'] = 'Debe agregar un valor';
									rta['bandera']=1;
									rta['input']=e.find('[name=valor1]');
									return rta;
								}else{
									valor1 = e.find('[name=valor1]').val();
									operador1 = e.find('[name=operador1]').val();
								}
								if (e.find('[name=valor2]').val()=='') {
									rta['msj'] = 'Debe agregar un valor';
									rta['bandera']=1;
									rta['input']=e.find('[name=valor2]');
									return rta;
								}else{
									valor2 = e.find('[name=valor2]').val();
									operador2 = e.find('[name=operador2]').val();
								}
							}							
						}
						tmp_={
							type_formula: f,
							type_fecha: e.find('[name=tipo_fecha]').is(":checked"),
							parameter: {'id': parametro, 'name': object_tags_auto.val()},
							operator: e.find('[name=tipo_operador]').val(),
							value: valor,
							operator1: operador1,
							value1: valor1,
							operator2: operador2,
							value2: valor2,
							colums: columns_
						}
						rta['tmp']=tmp_;
						return rta;
					}	
				}else{
					tmp_={
						type_formula: f,
						state_fecha: e.find('[name=tipo_fecha]').is(":checked"),
						parameter: {'id': (parametro!='')?parametro:'', 'name': (parametro!='')?object_tags_auto.val():''},
						operator: e.find('[name=tipo_operador]').val(),
						value: e.find('[name=valor_parametro]').val(),
						operator1: e.find('[name=operador1]').val(),
						value1: e.find('[name=valor1]').val(),
						operator2: e.find('[name=operador2]').val(),
						value2: e.find('[name=valor2]').val(),
						colums: columns_
					}
					rta['tmp']=tmp_;					
					return rta;
				}
			}

			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

			$(document).ready(function(){
				addRow();
				var id_entidad = '<?php echo $id_entidad; ?>';
				if (id_entidad=='') {
					$('.content_padre').find('input, textarea, button, select').attr('disabled','disabled');
					mensaje('Debe seleccionar una entidad para continuar con la configuración de indicadores.');
				}
		    });
		</script>
	</body>
</html>