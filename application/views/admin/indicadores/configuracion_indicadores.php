<?php $this->load->view("common/admin/head")?>
<style type="text/css">
	.px-4{
		padding: 0 2rem;
	}
	.font-bold{
		font-weight: 600;
	}
	
	.dashboard.red{
		color: white;
		background: red;
		border-color: red;
	}
	.dashboard:not(.red){
		color: white;
		background: green;
		border-color: green;
	}
</style>
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
    	<div class="col-md-12 principal-padding">
    		<div class="row">
    			<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('config_indicador_1')?></h2>
    			</div>
    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
						<form id="formEntidad" action="<?php print base_url();?>admin/configuracionIndicadores" method="POST" onsubmit="return false">
							<div class="input-group">
				      			<div class="input-group-addon">
				      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
			      				</div>
								<select class="form-control" name="searchentidad">
							    	<?php $ent = $this->session->userdata('searchentidad');  if(!$ent) $ent=""; ?>
							        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
							        <?php foreach ($entidad as $r) { ?>
										<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
									<?php } ?>
							    </select>
						    </div>																		
						</form>
					<?php } ?>					
					<a href="<?php echo base_url(); ?>admin/configuracionGeneralIndicadores" class="btn btn-negro ml-1" ><i class="fa fa-cogs"></i> <?=$this->lang->line('config_indicador_1')?></a>
					<a href="<?php echo base_url(); ?>admin/nuevoIndicador" class="btn btn-green ml-1" ><i class="fa fa-plus"></i> <?=$this->lang->line('config_indicador_3')?></a>
					
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-12 col-md-12">

	    			<div class="table-responsive table-color">
	    				<table class="table table-hover table-bordered text-center">
	    					<thead>
	    						<tr>
									<th width="20%"><?=$this->lang->line('indicador')?></th>
									<th width="40%"><?=$this->lang->line('descripcion')?></th>
									<th><?=$this->lang->line('dashboard')?></th>
									<th><?=$this->lang->line('publicacion')?></th>
									<th><?=$this->lang->line('fecha_creacion')?></th>
									<th><?=$this->lang->line('acciones')?></th>
								</tr>
	    					</thead>
	    					<tbody>
	    						<?php if (isset($indicadores)) {
									if(count($indicadores) > 0  && is_array($indicadores)){
								foreach ($indicadores as $b) { 
									$timestamp = (String)$b->registration_date;
                      				$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000);
								?>
								<tr>
									<td><?= $b->information->name; ?></td>
									<td><?=$b->information->description; ?></td>
									<td>
										<select class="form-control dashboard <?=($b->state_dashboard == '0' ? 'red': ''); ?>" name="dashboard" data-id="<?php echo $b->_id; ?>">
											<option value="0" <?=($b->state_dashboard == '0' ? 'selected': ''); ?>><?=$this->lang->line('inactivo')?></option>
											<option value="1" <?=($b->state_dashboard == '1' ? 'selected': ''); ?>><?=$this->lang->line('activo')?></option>
										</select>
									</td>
									<td>
										<?=($b->state == '0' ? $this->lang->line('borrador'): $this->lang->line('publicado')); ?>
									</td>
									<td><?=$dateInLocal; ?></td>
									<td>
										<!-- <a href="<?=base_url(); ?>admin/getIndicador/<?=$b->id;?>" class="btn btn-blue <?=($b->publicar == '0' ? 'hidden': ''); ?>" data-toggle="tooltip" title="Ver Indicador"><i class="fa fa-eye" aria-hidden="true"></i></a> -->
										<a href="<?=base_url(); ?>admin/editarIndicador/<?=$b->_id; ?>" class="editar btn btn-green" data-toggle="tooltip" title="<?=$this->lang->line('editar_indicador')?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
										<a href="#" class="eliminar a_delete btn btn-danger" data-id="<?=$b->_id; ?>" data-toggle="tooltip" title="<?=$this->lang->line('eliminar_indicador')?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php } 
								}else{ ?>
								<tr>
									<td colspan="6"><p class="text-center"><?=$this->lang->line('bad_table_indicadores')?></p></td>
								</tr>
								<?php } 
								}else{ ?>
								<tr>
									<td colspan="6"><p class="text-center"><?=$this->lang->line('bad_table_indicadores')?></p></td>
								</tr>
								<?php } ?>
	    					</tbody>
						</table>
	    			</div>

	    			<div class="col-md-12" style="text-align:center">
						<nav aria-label="Page navigation">
					        <ul class="pagination">
					            <?=$this->pagination->create_links(); ?>
					        </ul>
			            </nav>
				    </div>

	    		</div>
    		</div>
	    		
    	</div>
			
	</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript">
			//cambia el estado dashboard de la grafica (activo o inactivo)
			$('body').on('change','.dashboard', function () {
				$(this).removeClass('red');
				ajax('indicadores/guardarDashboardGrafica',
                {   
                	id: $(this).attr('data-id'),
                	estado: $(this).val()
                },
                function(data){
                    if(data.res=="ok"){
                    	if ($(this).val()=='0') {
                    		$(this).addClass('red');
                    	}
                    	mensaje(data.msj);
                    }else{
                     	mensaje(data.msj);
                    }
                },10000);
			});

			//eliminar la grafica
			$('body').on('click','.eliminar', function () {
				var id = $(this).attr('data-id')
				var btnnames = {
                    ok : "Aceptar",
                    cancel : "Cancelar"
                }
                confirmar("<?=$this->lang->line('confirm_indicadores')?>",function(){
                   	ajax('indicadores/deleteGrafica',
	                {   
	                	id: id
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	location.reload();                                    
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
	            },false,btnnames);
			});

			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

		    /*$(document).ready(function () {
		    	var total = parseInt('<?php echo $config_variables; ?>');
		    	if (total==0) {
					$('.box-btn').find('input, textarea, button, .btn').attr('disabled','true');
					mensaje('Debe configurar los parametros del excel.');
				}
		    }) */
		</script>
	</body>
</html>