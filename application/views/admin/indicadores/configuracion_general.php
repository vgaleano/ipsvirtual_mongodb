<?php $this->load->view("common/admin/head")?>
	<style type="text/css">
		.px-4{
			padding: 0 2rem;
		}
		.font-bold{
			font-weight: 600;
		}
		.row{
			margin: 0px;
		}
	</style>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/typeahead.css">
</head>
<body>
	<?php $this->load->view("common/admin/nav")?>
	<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
    	<div class="col-md-12 principal-padding" >

    		<div class="row">
    			<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('config_general_1')?> <a href="<?php echo base_url(); ?>admin/configuracionIndicadores" class="btn btn-border-dark mb-0" ><i class="fa fa-arrow-left"></i></a></h2>
    			</div>
    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
						<form id="formEntidad" action="<?php print base_url();?>admin/configuracionGeneralIndicadores" method="POST" onsubmit="return false">
							<div class="input-group">
				      			<div class="input-group-addon">
				      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
			      				</div>
								<select class="form-control" name="searchentidad">
							    	<?php $ent = $this->session->userdata('searchentidad'); print_r($ent); if(!$ent) $ent=""; ?>
							        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
							        <?php foreach ($entidad as $r) { ?>
										<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
									<?php } ?>
							    </select>
						    </div>																		
						</form>
					<?php } ?>	
				</div>
    		</div>

    		<div class="row mt-20">
    			<div class="col-xs-12 col-md-12">
    				<div class="row content_padre">

	    				<div class="col-xs-12 col-xs-offset-0 col-md-4 col-md-offset-1 box-shadow">
	    					<div class="table-responsive">
	    						<table class="table table-hover table-bordered mb-0" data-tipo='tipo_indicador'>
	    							<thead>
	    								<tr>
	    									<th class="text-center" colspan="2"><?=$this->lang->line('config_general_2')?> <button class="btn btn-green btn-xs addRow"><i class="fa fa-plus"></i></button></th>
	    								</tr>
	    							</thead>
	    							<tbody>
	    								<?php if (count($tipo_indicadores)>0) {
	    								foreach ($tipo_indicadores as $k_ti => $v_ti) { ?>
	    								 	<tr>
	    										<td><input class="form-control tipo_indicador" type="" name="tipo_indicador" value="<?=$v_ti->name; ?>" <?=($v_ti->required=='1')?'readonly':''?>></td>
	    										<td class="text-center">
    											<?php if ($v_ti->required!='1') { ?>
	    											<a href="#" class="eliminar btn btn-danger mx-05" data-id="<?=$v_ti->_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
	    											<a href="#" class="save btn btn-green mx-05" data-id="<?=$v_ti->_id; ?>"><i class="fa fa-save"></i></a>
	    										<?php } ?>
	    										</td>
	    									</tr>
	    								<?php } } ?>	    								
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>
	    				<!--
	    				<div class="col-xs-12 col-xs-offset-0 col-md-4 col-md-offset-2 box-shadow">
	    					<div class="table-responsive">
	    						<table class="table table-hover table-bordered mb-0" data-tipo='tipo_periodicidad'>
	    							<thead>
	    								<tr>
	    									<th class="text-center" colspan="2">Tipo de periodicidad <button class="btn btn-green btn-xs addRow"><i class="fa fa-plus"></i></button></th>
	    								</tr>
	    							</thead>
	    							<tbody>
	    								<?php if (count($tipo_periodicidad)>0) {
	    								foreach ($tipo_periodicidad as $k_ti => $v_ti) { ?>
	    								 	<tr>
	    										<td><input class="form-control tipo_periodicidad" type="" name="tipo_periodicidad" value="<?php echo $v_ti->tipo_periodicidad; ?>" <?=($v_ti->obligatorio=='1')?'readonly':''?>></td>
	    										<td class="text-center">
    											<?php if ($v_ti->obligatorio!='1') { ?>
	    											<a href="#" class="eliminar btn btn-danger mx-05" data-id="<?php echo $v_ti->id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
	    											<a href="#" class="save btn btn-green mx-05" data-id="<?php echo $v_ti->id; ?>"><i class="fa fa-save"></i></a>
	    										<?php } ?>
	    										</td>
	    									</tr>
	    								<?php } } ?>	    								
	    							</tbody>
	    						</table>
	    					</div>
	    				</div> -->

	    				<div class="col-xs-12 col-md-12 box-shadow mt-20">
	    					<div class="table-responsive table-color">
	    						<table class="table table-hover table-bordered text-center mb-0" data-tipo='tipo_filtro'>
	    							<thead>
	    								<tr class="none-background">
	    									<th colspan="4"><?=$this->lang->line('config_general_3')?> </th>
	    								</tr>
	    								<tr>
	    									<th width="20%" class="text-center"><?=$this->lang->line('config_general_4')?></th>
	    									<th width="30%" class="text-center"><?=$this->lang->line('config_general_5')?></th>
	    									<th width="40%" class="text-center"><?=$this->lang->line('config_general_6')?></th>
	    									<th width="10%" class="text-center"><?=$this->lang->line('acciones')?></th>
	    								</tr> 
	    							</thead>
	    							<tbody id="tipo_filtro">	    								
	    								 								
	    							</tbody>
	    						</table>
	    					</div>
	    				</div>

	    			</div>
    			</div>
    		</div>

	    </div>
	</div>
	<!-- ==============================================
	SCRIPTS
	=============================================== -->	
	<?php $this->load->view("common/admin/extras")?>
	<?php $this->load->view("common/admin/js")?>
	<script  type="text/javascript" language="javascript" src="<?php print base_url(); ?>js/typeahead.js"></script>
	<script type="text/javascript">
		var array_tags = new Array();
		var codigosT = new Array();

		//eliminar el tag en el front
		$('body').on('click','.deleteTag', function() {
			var id_t = $(this).attr('id'),
			value_tag = $('input.'+id_t).val(),	
			result = array_tags.filter(tag => tag != value_tag);
			array_tags=result;
			$('.'+id_t).remove();
		});

		//addRow
		$('body').on('click', '.addRow', function () {
			var tipo = $(this).closest('table').attr('data-tipo');
			$(this).closest('table').children('tbody').append('<tr><td><input class="form-control '+tipo+'" type="" name="'+tipo+'"></td><td class="text-center"><a href="#" class="eliminar btn btn-danger mx-05" data-id=""><i class="fa fa-trash-o" aria-hidden="true"></i></a><a href="#" class="save btn btn-green mx-05" data-id=""><i class="fa fa-save"></i></a></td></tr>');
		});

		//guardar
		$('body').on('click', '.save', function () {
			var eso = $(this),
			tipo_tabla = eso.closest('table').attr('data-tipo'),
			tags_select = new Array()
			bandera = 0,
			value="",
			id_config = (tipo_tabla=='tipo_filtro')?eso.attr('data-id'):"",
			id=(tipo_tabla=='tipo_filtro')?eso.closest('tr').children('td').children('[name=tipo_filtro]').val():eso.attr('data-id');

			if (tipo_tabla=='tipo_filtro') {
				value = new Array();
				if (eso.parent().siblings().children('.divtags').children('input').length==0) {
					bandera=1;
					mensaje('Debe seleccionar parámetro ya preestablecido');
				}else{
					eso.parent().siblings().children('.divtags').children('input').each(function(){
			    		//value.push($(this).attr('data-id'));
			    		value.push($(this).val());
			    	});
				}
			}else{
				//tipo de indicadores, tipo de periodicidad
				value=eso.closest('tr').children('td').find('[name='+tipo_tabla+']').val();
			}

			if (bandera==0) {
				ajax('indicadores/saveConfiguracionGeneral',
	            {   
	            	id: id, //id_tabla o id_tipo_filtro
	            	tipo_tabla: tipo_tabla,
	            	value: value,
	            	id_entidad: '<?=$id_entidad; ?>',
	            	id_config: id_config //new - update
	            },
	            function(data){
	                if(data.res=="ok"){
	                	var ids = (data.tipo_tabla=='tipo_filtro')?data.id:data.id.$oid;
	                	if (data.tipo_tabla=='tipo_filtro') {
	                		//quita el not_active
		                	eso.parent().prev().children('.divtags').children('.tagspan.not_active').removeClass('not_active pulse');
		                	eso.removeClass('pulse');
	                	}		                	
	                	//add ids
	                	eso.attr('data-id',ids);
	                	eso.prev().attr('data-id',ids);
	                }else{
	                 	mensaje(data.msj);
	                }
	            },10000);
			}
				
		});

		//delete row
		$('body').on('click', '.eliminar', function () {
			var eso = $(this),
			bandera = 0,
			tipo_tabla = eso.closest('table').attr('data-tipo');
			if (eso.attr('data-id')!='') {
				var id = eso.attr('data-id');
				var btnnames = {
                    ok : "Aceptar",
                    cancel : "Cancelar"
                }
                confirmar("<?=$this->lang->line('confirm_general')?>",function(){
                   	ajax('indicadores/deleteConfiguracionGeneral',
	                {   
	                	id: id,
	                	tipo_tabla: tipo_tabla,
	                	id_entidad: '<?=$id_entidad; ?>'
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	if (tipo_tabla=='tipo_filtro') {					
								//elimina todos los tags y los libera
								eso.parent().siblings().children('.divtags').children('div').each(function(){
						    		var id_t = $(this).attr('id'),
						    		value_tag = $(this).html(),	
									result = array_tags.filter(tag => tag != value_tag);
									array_tags=result;
						    	});
						    	eso.parent().siblings().children('.divtags').empty();
						    	//quita los id's de los botones
						    	eso.attr('data-id','');
								eso.siblings().attr('data-id','');
								//quita el valor del typeahead
								eso.closest('tr').find('[name=columna_variable]').val('');
							}else{
								console.log('delete tr')
								eso.closest('tr').remove();
							}
	                    }else{
	                     	bandera=1;
	                     	mensaje(data.msj);
	                    }
	                },10000);
	            },false,btnnames);
			}else{
				if (tipo_tabla=='tipo_filtro') {					
					//elimina todos los tags y los libera
					eso.parent().siblings().children('.divtags').children('div').each(function(){
			    		var id_t = $(this).attr('id'),
			    		value_tag = $(this).html(),	
						result = array_tags.filter(tag => tag != value_tag);
						array_tags=result;
			    	});
			    	eso.parent().siblings().children('.divtags').empty();
			    	//quita los id's de los botones
			    	eso.attr('data-id','');
					eso.siblings().attr('data-id','');
					//quita el valor del typeahead
					eso.closest('tr').find('[name=columna_variable]').val('');
				}else{
					eso.closest('tr').remove();
				}
			}
		});

		//change de entidad
		$('[name=searchentidad]').on('change', function () {
	    	$('[name=searchentidad]').val($(this).val());
	    	document.getElementById('formEntidad').submit();
	    });

		$(document).ready(function(){
			var id_entidad = '<?php echo $id_entidad; ?>';
			if (id_entidad=='') {
				$('.content_padre').find('input, textarea, button, select').attr('disabled','disabled');
				mensaje("<?=$this->lang->line('general_bad_1')?>");
			}

			var tipo_filtro = <?php echo json_encode($tipo_filtro); ?>;
			var hijos = 1;
			for (var i = 0; i < tipo_filtro.length; i++) {	
				if (tipo_filtro[i].mode=='0') {
					var content = '';
					var id_camp = tipo_filtro[i]._id.$oid;
					content+='<tr>';
						content+='<td>'+tipo_filtro[i].name;
							content+='<input type="hidden" name="tipo_filtro" value="'+id_camp+'">';
						content+='</td>';
						content+='<td>';
							content+='<input class="form-control" name="columna_variable" id="hijos'+hijos+'">';
						content+='</td>';
						content+='<td>';
							content+='<div class="divtags">';

							if (tipo_filtro[i].columns.length>0) {
								var activas = tipo_filtro[i].columns;
								for (var j = 0; j < activas.length; j++) {
									content+='<div class="btn tagspan tag'+j+'_ deleteTag" data-toggle="bottom" title="<?=$this->lang->line("config_parametro_9")?>" id="tag'+j+'_">'+activas[j]+'</div>';
									content+='<input class="tag'+j+'_" type="hidden" data-id="'+activas[j]+'" name="tag" value="'+activas[j]+'">';
								}
							}

							content+='</div>'
						content+='</td>';
						content+='<td class="text-center">';
							content+='<a href="#" class="eliminar btn btn-danger mx-05" data-id="'+id_camp+'"><i class="fa fa-trash-o" aria-hidden="true"></i></a><a href="#" class="save btn btn-green mx-05" data-id="'+id_camp+'"><i class="fa fa-save"></i></a>';
						content+='</td>';
					content+='</tr>';

					$('#tipo_filtro').append(content);

					$('#hijos'+hijos).typeahead({
						hint: true,
						highlight: true,
						minLength: 1,
					},{
						limit: 12,
						async: true,
						source: function(query, processSync, processAsync){
							return $.ajax({
								url: "<?=base_url(); ?>indicadores/verColumna",
								type: 'POST',
								data:{
									tags_auto: query,
									id_entidad: '<?php echo $id_entidad; ?>',
									tags_seleccionados: array_tags
								},
								dataType: 'JSON',
								success: function(data){
									$(document).ajaxStop(function(){
										$("body").removeClass("loading");
									});
									if (data.res === "ok") {
										var tags_auto = [];
										for (var i = 0; i < data.bar.length; i++) {
											tags_auto.push(data.bar[i].name);
											codigosT[data.bar[i].name]=data.bar[i]._id.oid;
										}
										return processAsync(tags_auto);		                    		                     
									}else{
										//muestra5.html(data.msj);		                 	
									}
								}
							});
						}
					});
					$('#hijos'+hijos).bind('typeahead:select', function(ev, suggestion) {
						var str=suggestion;
						$(this).typeahead('val','');
						$(this).closest('td').next().children('div').append('<div class="btn tagspan tag'+codigosT[suggestion]+' deleteTag not_active pulse" data-toggle="bottom" title="<?=$this->lang->line("config_parametro_9")?>" id="tag'+codigosT[suggestion]+'">'+suggestion+'</div>');
						$(this).closest('td').next().children('div').append('<input class="tag'+codigosT[suggestion]+'" '.concat(' type="hidden" data-id="'+codigosT[suggestion]+'" name="tag" value="'+suggestion+'">'));
						array_tags.push(suggestion);
						//poner de color el boton borrar o guardar
						//$(this).closest('td').next().next().children('.eliminar').css('background','#c12e2a');
						$(this).closest('td').next().next().children('.save').addClass('pulse');//css('background','green');
					});
					hijos=hijos+1;
				}					
			}
			
	    });

		    

	</script>
</body>
</html>