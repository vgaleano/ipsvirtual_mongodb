	<?php $this->load->view("common/admin/head")?>
	<link href="<?php echo base_url(); ?>css/chart/Chart.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/indicadores.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.datetimepicker.css">
	<style type="text/css">
		.box-shadow{
			margin: 1rem 0;
		}
		.m-2 {
			margin: 2rem;
		}
		.label-red {
			color: red;
		}
		.label-green {
			color: green;
		}
		.label-blue {
			color: #00afe5!important;
		}
	</style>
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
	    	<div class="col-md-12 principal-padding" >

	    		<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('indicadores_text_1')?> </h2>
	    			</div>
	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>				    	
						<form id="formEntidad" action="<?=base_url();?>admin/indicadores" method="POST" onsubmit="return false">
							<div class="input-group">
				      			<div class="input-group-addon">
				      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
			      				</div>
								<select class="form-control" name="searchentidad">
							    	<?php $ent = $this->session->userdata('searchentidad'); if(!$ent) $ent=""; ?>
							        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
							        <?php foreach ($entidad as $r) { ?>
										<option value="<?=$r->_id; ?>" <?=($r->_id==$ent)? 'selected':''; ?>><?=$r->name ?></option>
									<?php } ?>
							    </select>
						    </div>																		
						</form>
						<?php } ?>
	    			</div>
				</div>

				<div class="row mt-20">
					<div class="col-xs-12 col-md-12">
						<div class="col-xs-12 col-md-12 box-shadow">
							<form id="algo" onsubmit="return false">
								<?php if ($this->session->userdata('id_rol') != "1"){ ?>
									<input type="hidden" name="searchentidad" value="<?php echo $this->session->userdata('id_entidad'); ?>">
								<?php } ?>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('desde')?></label>
										<input class="form-control  fecha" type="" name="fecha_in">
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('hasta')?></label>
										<input class="form-control fecha" type="" name="fecha_out">
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('sede')?></label>
										<select class="form-control" name="sede">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($sede)>0) { 
											foreach ($sede as $k_se => $v_se) { ?>
											 	<option value="<?=$v_se->_id; ?>"><?=$v_se->_id; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('profesional')?></label>
										<select class="form-control" name="profesional">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($profesional)>0) { 
											foreach ($profesional as $k_g => $v_g) { ?>
											 	<option value="<?=$v_g->_id; ?>"><?=$v_g->_id; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('especialidad')?></label>
										<select class="form-control" name="especialidad">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($especialidad)>0) { 
											foreach ($especialidad as $k_e => $v_e) { ?>
											 	<option value="<?=$v_e->_id; ?>"><?=$v_e->_id; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('eventos')?></label>
										<select class="form-control" name="eventos">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($eventos)>0) { 
											foreach ($eventos as $k_ev => $v_ev) { ?>
											 	<option value="<?=$v_ev->_id; ?>"><?=$v_ev->information->name; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('estandar')?></label>
										<select class="form-control" name="estandar">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($estandar)>0) { 
											foreach ($estandar as $k_es => $v_es) { ?>
											 	<option value="<?=$v_es->_id; ?>"><?=$v_es->name; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('diagnostico')?></label>
										<select class="form-control" name="diagnostico">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($diagnostico)>0) { 
											foreach ($diagnostico as $k_d => $v_d) { ?>
											 	<option value="<?=$v_d->name; ?>"><?=$v_d->name; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('genero')?></label>
										<select class="form-control" name="genero">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($genero)>0) { 
											foreach ($genero as $k_ge => $v_ge) { ?>
											 	<option value="<?=$v_ge->_id; ?>"><?=$v_ge->_id; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('edad')?></label>
										<select class="form-control" name="edad">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($edad)>0) { 
											foreach ($edad as $k_ed => $v_ed) { if ($v_ed->name=='Todos') {
												continue;
											} ?>
											 	<option value="<?=$v_ed->_id; ?>"><?=$v_ed->name; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('discapacidad')?></label>
										<select class="form-control" name="discapacidad">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($discapacidad)>0) { 
											foreach ($discapacidad as $k_di => $v_di) { ?>
											 	<option value="<?=$v_di->_id; ?>"><?=$v_di->_id; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('procedencia')?></label>
										<select class="form-control" name="Procedencia">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($procedencia)>0) { 
											foreach ($procedencia as $k_g => $v_p) { ?>
											 	<option value="<?=$v_p->_id; ?>"><?=$v_p->_id; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-md-3">
									<div class="form-group">
										<label><?=$this->lang->line('area')?></label>
										<select class="form-control" name="area">
											<option value=""><?=$this->lang->line('todos')?></option>
											<?php if (count($area)>0) { 
											foreach ($area as $k_a => $v_a) { ?>
											 	<option value="<?=$v_a->_id; ?>"><?=$v_a->_id; ?></option>
											<?php } } ?>											
										</select>
									</div>
								</div>	
								<div class="col-xs-12 col-md-3 pull-right  text-right">
									<div class="form-group">
										<button id="filtrar" class="btn btn-save"><?=$this->lang->line('btn_filtrar')?></button>
									</div>
								</div>																			
							</form>
						</div>						
					</div>
				</div>

				<div class="row mt-20 content_graficas"></div>

	    	</div>
	    </div>
	    <a class="hidden" href="" id="report1"></a>
	    <!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script type="text/javascript" src="<?php echo base_url();?>js/jquery.datetimepicker.full.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/core.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/charts.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/themes/animated.js"></script>
		<script src="//www.amcharts.com/lib/4/themes/dataviz.js"></script>

		<script  type="text/javascript" language="javascript">
		
			var ids_eventos = <?php echo json_encode($ids_eventos); ?>,
				data_tabla_pacientes_ok_bad = [],
				posicion_tabla_pacientes_ok_bad=0,
				chart_array=[],
				array_graficas = [];


			$('body').on('click', '#filtrar', function () {
				var bandera = 0;
				if ($('[name=searchentidad]').val()=='') {
					bandera = 1;
					return mensaje("<?=$this->lang->line('indicadores_bad_1')?>");
				}

				if (validate_fechaMayorQue($("[name=fecha_in]").val(),$("[name=fecha_out]").val())===0) {
					bandera = 1;
					return mensaje("<?=$this->lang->line('indicadores_bad_2')?>");
				}

				if (bandera==0) {
					ajax('indicadores/getKpis',
			        {   
			           	entidad: $('[name=searchentidad]').val(),
			            fecha_in: $("[name=fecha_in]").val(),
			            fecha_out: $("[name=fecha_out]").val(),
			            sede: $("[name=sede]").val(),
			            profesional: $("[name=profesional]").val(),
			            especialidad: $("[name=especialidad]").val(),
			            eventos: ($("[name=eventos]").val() == '')?ids_eventos:[$("[name=eventos]").val()],
			            estandar: $("[name=estandar]").val(),
			            diagnostico: $("[name=diagnostico]").val(),
			            genero: $("[name=genero]").val(),
			            edad: $("[name=edad]").val(),
			            discapacidad: $("[name=discapacidad]").val(),
			            procedencia: $("[name=procedencia]").val(),
			            area: $("[name=area]").val()
			        },
			        function(data){
			            if(data.res=="ok"){
			            	data_tabla_pacientes_ok_bad=[];
			            	posicion_tabla_pacientes_ok_bad=0;
			            	array_graficas=[];
			            	$('.content_graficas').empty();
			            	var graficas = data.graficas;
			            	array_graficas=graficas;
			            	console.log(graficas)

			            	for (var i = 0; i < graficas.length; i++) {
			            		var name_box = 'chartdiv'+(i+1);
			            		var content='<div class="table-responsive m-2"><table class="table text-center">';
			            			content+='<thead><tr><th><?=$this->lang->line("total_registros")?></th><th><?=$this->lang->line("total_registros_sin_fecha")?></th><th><?=$this->lang->line("total_registros_otros")?></th><th><?=$this->lang->line("total_registros_ok")?></th></tr></thead>';
			            			content+='<tbody>';
			            				content+='<tr><td class="label-blue">'+graficas[i]['total_registros']+'</td><td class="label-red">'+graficas[i]['total_registros_sin_fecha']+'</td><td class="label-red">'+graficas[i]['total_registros_sin_otros']+'</td><td class="label-green">'+graficas[i]['total_registros_ok']+'</td></tr>'
			            			content+='</tbody>';
			            		content+='</table></div>';
			            		var content1='<div class="box table-responsive m-2" id="databody"></div>';
			            		var content2='<div class="box table-responsive m-2" id="databody_bad"></div>';
			            		$('.content_graficas').append('<div class="col-xs-12 col-md-12 box-shadow"><button class="btn btn-save" type="button" onclick="savePDF('+i+',\''+graficas[i]['name']+'\', \''+name_box+'\');"><i class="fa fa-download"></i> <i class="fa fa-area-chart"></i> <?=$this->lang->line("descargar_grafica")?></button> <button class="btn btn-save" type="button" onclick="descargarAllExcel('+i+',\''+graficas[i]['name']+'\');"><i class="fa fa-download"></i> <i class="fa fa-file"></i> <?=$this->lang->line("descargar_sabana")?></button><div id="'+name_box+'" style="width: 100%; height: 500px;"></div><p class="m-2">'+graficas[i]['description']+'</p>'+content+content1+content2+'</div>');
			            	}
		            		am4core.ready(function() {
								am4core.disposeAllCharts();
								// Themes begin
								am4core.useTheme(am4themes_animated);
								// Themes end

								for (var i = 0; i < graficas.length; i++) {
				            		var puntos = graficas[i]['puntos_grafica'],
				            		categorias = graficas[i]['label'],
				            		name_box = 'chartdiv'+(i+1),
				            		info_standar = graficas[i]['info_standard'],
				            		name_grafica = graficas[i]['name'];
				            	
									// Create chart instance
									chart = am4core.create(name_box, am4charts.XYChart);

									chart.data = puntos;

									// Create category axis
									var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
									categoryAxis.dataFields.category = "label";
									categoryAxis.title.text = graficas[i]['titulo'];
									categoryAxis.renderer.grid.template.location = 0;
									categoryAxis.renderer.minGridDistance = 20;
									categoryAxis.renderer.cellStartLocation = 0.1;
									categoryAxis.renderer.cellEndLocation = 0.9;

									// Create value axis
									var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
									valueAxis.title.text = "Porcentajes (%)";
									valueAxis.min = 0; 
									valueAxis.max = 100;

									var j = 0;
									for (var categoria in categorias) {
										// Create series
										var add = j+1,
										name_series = 'series'+add,
										name_bullet = 'bullet'+add,
										column_template ='columnTemplate'+add;
										name_series = chart.series.push(new am4charts.ColumnSeries());
										name_series.dataFields.valueY = categorias[categoria]['name'];
										name_series.dataFields.categoryX = "label";
										name_series.name = categorias[categoria]['full_name'];
										name_series.dataFields.value = categorias[categoria]['full_name'];
										name_series.columns.template.tooltipText = "{name} (Total: {num_"+categorias[categoria]['num']+"}/{denominador_"+categorias[categoria]['num']+"}) \n Valor: {valueY}%";
										name_series.stacked = categorias[categoria]['stacked'];
										name_series.columns.template.width = am4core.percent(95);
										name_series.columns.template.fill = am4core.color(categorias[categoria]['color']); // green fill
										name_series.cursorTooltipEnabled = false;

										var name_bullet = name_series.bullets.push(new am4charts.LabelBullet());
										name_bullet.interactionsEnabled = false;
										name_bullet.label.text = "{valueY}%";
										name_bullet.label.fill = am4core.color("#ffffff");
										name_bullet.locationY = 0.5;										

										name_series.tooltip.getFillFromObject = false;
										name_series.tooltip.background.fill = am4core.color("#fff");
										name_series.tooltip.label.fill = am4core.color("#00");

										name_series.columns.template.events.on("hit", function(ev) {
											var name_box = ev.target.dataItem.dataContext.name_chart;
											$('#'+name_box).closest('.box-shadow').children('#databody').empty();
											$('#'+name_box).closest('.box-shadow').children('#databody_bad').empty();
											var patients_ok = ev.target.dataItem.dataContext.data_ok,
											patients_bad = ev.target.dataItem.dataContext.data_bad;
											if ( patients_ok.length>0 || patients_bad.length>0 ) {
												$('#'+name_box).closest('.box-shadow').children('#databody').append('<div class="row"><div class="col-xs-12 col-md-12"><button class="btn btn-save btn-block" onclick="exportTablaProcesadoOkBad('+posicion_tabla_pacientes_ok_bad+')"><i class="fa fa-download"></i> <i class="fa fa-file"></i> <?=$this->lang->line("exportar_excel")?></button></div></div>');
												data_tabla_pacientes_ok_bad[posicion_tabla_pacientes_ok_bad]= {name_chart: name_box, data_ok:patients_ok, data_bad: patients_bad, categoria: ev.target.dataItem.categoryX};
												posicion_tabla_pacientes_ok_bad++;
											}
										  	if ( patients_ok.length>0 ) {
										  		var table=createTable(patients_ok, ev.target.dataItem.categoryX, 'success');
										  		$('#'+name_box).closest('.box-shadow').children('#databody').append(table);
										  	}
										  	if ( patients_bad.length>0 ) {
									  			var table=createTable( patients_bad, ev.target.dataItem.categoryX, 'danger' );
   												$('#'+name_box).closest( '.box-shadow').children('#databody_bad').append(table );
										  	}
										});
										j++
									}

									// Add chart cursor
									chart.cursor = new am4charts.XYCursor();
									chart.cursor.behavior = "zoomY";
									// Add legend
									chart.legend = new am4charts.Legend();

									chart.scrollbarX = new am4core.Scrollbar();

									chart.responsive.rules.push({
									  	relevant: function(target) {
									    	// Code that determines if rule is currently relevant
									    	// and returns true or false
									    	return false;
									  	},
									  	state: function(target, stateId) {
									    	// Code that creates a new SpriteState object to be
									    	// applied to target if Rule is "relevant"
									    	return;
									  	}
									});

									// Add chart title
									var title = chart.titles.create();
									title.text = graficas[i]['name'];
									title.fontSize = 25;
									title.marginBottom = 30;

									var label = categoryAxis.renderer.labels.template;
									label.truncate = true;
									label.maxWidth = 200;
									label.tooltipText = "{category}";

									categoryAxis.events.on("sizechanged", function(ev) {
										var axis = ev.target;
									  	var cellWidth = axis.pixelWidth / (axis.endIndex - axis.startIndex);
									  	if (cellWidth < axis.renderer.labels.template.maxWidth) {
										    axis.renderer.labels.template.rotation = -45;
										    axis.renderer.labels.template.horizontalCenter = "right";
										    axis.renderer.labels.template.verticalCenter = "middle";
									  	}else {
										    axis.renderer.labels.template.rotation = 0;
										    axis.renderer.labels.template.horizontalCenter = "middle";
										    axis.renderer.labels.template.verticalCenter = "top";
									  	}
									});

									var valueAxis1 = chart.yAxes.push(new am4charts.ValueAxis());
									valueAxis1.renderer.opposite = true;
									valueAxis1.tooltip.disabled = true;
									valueAxis1.min = 0; 
									valueAxis1.max = 100;
									valueAxis1.renderer.labels.template.fillOpacity = 0;
									valueAxis1.renderer.grid.template.strokeOpacity = 0;
									valueAxis1.cursorTooltipEnabled = false;

									var color_range = '#FFFB00',
									color_alto = '#00FF00',
									color_bajo = '#FF0000';
									for (var j = 0; j<info_standar.length; j++) {
										//color  range
										var name_range = 'range'+add,
										axisRange = 'axisRange'+add,
										name_range = valueAxis.axisRanges.create(),
										axisRange = valueAxis1.axisRanges.create();

										if (info_standar[j].operator=='<' || info_standar[j].operator=='<=') {
											//name_range.value = 0;
											axisRange.endValue = parseInt(info_standar[j].value);
											axisRange.value = 0;
											//name_range.axisFill.fill = am4core.color(color_bajo);
											name_range.value = parseInt(info_standar[j].value);
											name_range.grid.stroke = am4core.color(color_bajo);
											name_range.grid.strokeWidth = 2;
											name_range.grid.strokeOpacity = 1;
										}
										if (info_standar[j].operator=='>' || info_standar[j].operator=='>=') {
											//name_range.value = parseInt(info_standar[j].value);
											//name_range.endValue = 100;
											axisRange.endValue = 100;
											axisRange.value = parseInt(info_standar[j].value);
											//name_range.axisFill.fill = am4core.color(color_alto);											
										}
										if (info_standar[j].operator=='rango') {
											//name_range.value = parseInt(info_standar[j].value1);
											//name_range.endValue = parseInt(info_standar[j].value2);
											axisRange.value = parseInt(info_standar[j].value1);
											axisRange.endValue = parseInt(info_standar[j].value2);
											//name_range.axisFill.fill = am4core.color(color_range);
											name_range.value = parseInt(info_standar[j].value2);
											name_range.grid.stroke = am4core.color(color_range);
											name_range.grid.strokeWidth = 2;
											name_range.grid.strokeOpacity = 1;
										}
										//name_range.axisFill.fillOpacity = 0.2;
										//name_range.grid.strokeOpacity = 0;

										if (info_standar[j].type_standar.name=='Riesgo Bajo') {
											axisRange.label.text = '<?=$this->lang->line("bajo")?>';
										}else if (info_standar[j].type_standar.name=='Riesgo Medio') {
											axisRange.label.text = '<?=$this->lang->line("medio")?>';
										}else{
											axisRange.label.text = '<?=$this->lang->line("alto")?>';
										}
										axisRange.grid.strokeOpacity = 0.1;
										axisRange.label.align = "left";
										axisRange.label.verticalCenter = "middle";
										axisRange.label.fillOpacity = 0.8;						
									}
									valueAxis.renderer.labels.template.adapter.add("text", function(text) {
									  return text + "%";
									});

									chart_array[i]=chart;
								}
							}); // end am4core.ready()			            									
			            }else{			            	
			             	mensaje(data.msj);
			            }
			        },120000000);
				}
			});

			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

		    $(document).ready(function () {

		    	$('body').on('focus',".fecha", function(){
					$(this).datetimepicker({
		    			format:'Y-m-d',
		    			timepicker:false,
					 	autoclose: true
					});
				});

				//validar fechas_fin sea mayor a fecha inicio
				jQuery(function(){
					jQuery('[name=fecha_in]').datetimepicker({
						format:'Y-m-d',
						onShow:function( ct ){
							this.setOptions({
								minDate:jQuery('[name=fecha_in]').val()?jQuery('[name=fecha_in]').val():false
							})
						},
						timepicker:false
					});
					jQuery('[name=fecha_out]').datetimepicker({
						format:'Y-m-d',
						onShow:function( ct ){
					    	this.setOptions({
								maxDate:jQuery('[name=fecha_in]').val()?jQuery('[name=fecha_in]').val():false
							})
						},
						timepicker:false
					});
				});

				jQuery.datetimepicker.setLocale('es');
		    })

		    function validate_fechaMayorQue(fechaInicial,fechaFinal)
	        {
	        	//dd/mm/yyyy
	            valuesStart=fechaInicial.split("-");
	            valuesEnd=fechaFinal.split("-");
	 
	            // Verificamos que la fecha no sea posterior a la actual
	            //var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]);
	            //var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]);
	            var dateStart=new Date(valuesStart[0],(valuesStart[1]-1),valuesStart[2]);
	            var dateEnd=new Date(valuesEnd[0],(valuesEnd[1]-1),valuesEnd[2]);
	            if(dateStart>=dateEnd)
	            {
	                return 0;
	            }
	            return 1;
	        }

	        function createTable(data, mes, modo){
	        	//console.log(data)
	        	var numm = (modo=='danger')?1:2
	        	var count = Object.keys(data[0]).length - numm;
			    var table = "<table class='table text-center'>";
			    table += "<thead>";
			    	table += "<tr class='"+modo+"'><th class='text-center' colspan='"+count+"'>"+mes+"</th></tr>";
			    	table += "<tr>";
			    	for (var column in data[0]) {
			    		if (column=='fecha' || column=='_id' || column=='Teléfono') {
		            		continue;
		            	}
			    		table +="<th>"+column+"</th>";
			    	}
			    		table +="<th><i class='fa fa-phone'></i></th>";
			        table +="</tr>";
			    table +="</thead>";
			    table +="<tbody>";
			    for (var i = 0; i < data.length; i++) {
			    	table += "<tr>";
			    	var telefono = '',
			    	nombre = '',
			    	_id = '';
			            for (var column in data[i]) {
			            	if (column=='_id') {
			            		_id=data[i][column];
			            	}
			            	if (column=='Teléfono') {
			            		telefono=data[i][column];
			            	}
			            	if (column=='fecha' || column=='_id' || column=='Teléfono') {
			            		continue;
			            	}			            	
			            	if (column=='Nombre') {
			            		nombre=data[i][column];
			            	}
				    		table +="<td>"+data[i][column]+"</td>";
				    	}
				    	var diss = (telefono=='')?'disabled':'';
				    	table +='<td><button class="btn btn-blue btn-circle btn-xs llamada" data-telefono="'+telefono+'" data-patient="'+nombre+'" '+diss+' data-id="'+_id+'" data-id_entidad="<?=$id_entidad?>"><i class="fa fa-phone"></i></button><button class="btn btn-blue btn-circle btn-xs mensaje" data-telefono="'+telefono+'" data-patient="'+nombre+'" '+diss+' data-id="'+_id+'" data-id_entidad="<?=$id_entidad?>"><i class="fa fa-envelope"></i></button></td>';
			        table +="</tr>";
			    }				
			    table += "</tbody>";
			    table += "</table>";

			    return table;
			};

			//exportar tabla procesada
			function exportTablaProcesadoOkBad(posicion) {
				var data = JSON.stringify(data_tabla_pacientes_ok_bad[posicion]);
				ajax('indicadores/exportTablasPacientesProcesadaBienMal',
                {
                	info: data
                },
                function(data){
                    if(data.res=="ok"){
                    	var urlF = data.url;                      
                    	console.log(base_url+urlF);
                    	window.open(base_url+urlF); 
                    }else{
                     	mensaje(data.msj);
                    }
                },10000);
			}
			//https://www.amcharts.com/docs/v4/tutorials/generating-multi-content-pdf-export/
			function savePDF(e, name, id) {
			  
			  	Promise.all([
				    chart_array[e].exporting.pdfmake,
				    chart_array[e].exporting.getImage("png")
			  	]).then(function(res) { 
			    
				    var pdfMake = res[0];
				    
				    // pdfmake is ready
				    // Create document template
				    var doc = {
				      	pageSize: "A4",
				      	pageOrientation: "portrait",
				      	pageMargins: [30, 30, 30, 30],
				      	content: []
				    };
				    			    
				    doc.content.push({
				      	image: res[1],
				      	width: 530
				    });

				    var descripcion = $('#'+id).siblings('p').html();
				    descripcion = descripcion.split('<br>');
				    for (var i = 0; i < descripcion.length; i++) {
				    	var textarea = descripcion[i].replace("&lt;", "<").replace("&gt;", ">");
				    	doc.content.push({
					      	text: textarea,
					      	fontSize: 10,
					      	margin: [0, 0, 0, 15]
					    });
				    }

					pdfMake.createPdf(doc).download(name+".pdf");
			    
			  	});
			}

			function descargarAllExcel(posicion, name) {
				var data = JSON.stringify(array_graficas[posicion]['puntos_grafica']);
				ajax('indicadores/exportAllDataTabla',
                {
                	info: data,
                	name: name,
                	id_entidad: '<?=$id_entidad?>'
                },
                function(data){
                    if(data.res=="ok"){
                    	var urlF = data.url;                      
                    	console.log(base_url+urlF);
                    	window.open(base_url+urlF); 
                    }else{
                     	mensaje(data.msj);
                    }
                },10000);
			}
				
		</script>
	</body>
</html>