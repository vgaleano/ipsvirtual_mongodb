<?php $this->load->view("common/admin/head")?>
<link href="<?php echo base_url(); ?>css/configuracion_indicadores.css" rel="stylesheet">
<style type="text/css">
	.px-4{
		padding: 0 2rem;
	}
	.font-bold{
		font-weight: 600;
	}
	#collapseInfo input.border-red {
		border-bottom: 1px solid red;
	}
	select.border-red, input.border-red {
		border: 1px solid red;
	}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/typeahead.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
    	<div class="col-md-12 principal-padding" >

    		<div class="row">
    			<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('indicador_text_1')?> <a href="<?php echo base_url(); ?>admin/configuracionIndicadores" class="btn btn-border-dark"><i class="fa fa-arrow-left"></i></a></h2>
    			</div>
    		</div>

			<div class="row box-shadow content_padre mt-20">

				<div class="col-xs-12 col-md-12" id="contentAccordion">
<div class="col-xs-12 col-md-12 card">
    <div class="col-xs-12 col-md-12 card-header">
      	<div class="col-xs-12 col-md-12 mt-1">
      		<div class="panel-group pl-15" id="accordionGlobal">
      			<div class="panel panel-default">
      				<div class="panel-heading">      					
			            <h4 class="panel-title">
			                <a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseInfo"><?=$this->lang->line('indicador_text_2')?></a>
			            </h4>
			       	</div>
			       	<div id="collapseInfo" class="panel-collapse collapse collapseInfo">
			       		<input value="<?=$informacion->_id; ?>" type="hidden" name="id_indicador">
	      				<div class="form-group">
	      					<label><?=$this->lang->line('titulo')?></label>
			        		<input class="form-control input-border " placeholder="<?=$this->lang->line('indicador_text_3')?>" name="titulo" value="<?=$informacion->information->name; ?>">
			        	</div>
			        	<div class="col-xs-12 col-md-4 pl-0">
			      			<div class="form-group">
				      			<label><?=$this->lang->line('periodicidad')?></label>
				      			<select class="form-control " name="periodicidad">
				      				<?php foreach ($periodicidad as $k_ap => $v_ap) { ?>
				      					<option value="<?=$v_ap->_id; ?>" <?=( $informacion->information->periodicity->id == $v_ap->_id)?'selected':''; ?>><?=$v_ap->name;?></option>
				      				<?php } ?>
				      			</select>
				      		</div>
			      		</div>
			      		<div class="clearfix"></div>
			        	<div class="form-group">
			        		<label><?=$this->lang->line('descripcion')?></label>
			      			<textarea class="form-control " placeholder="<?=$this->lang->line('indicador_text_4')?>" name="descripcion"><?php echo $informacion->information->description; ?></textarea>
			      		</div>
			      		<div class="clearfix"></div>
	      			</div>
      			</div>
      			<div class="panel panel-default">
      				<div class="panel-heading">
			            <h4 class="panel-title input_btn">
			                <a data-toggle="collapse" data-parent="#accordionEstandar" href="#collapseEstandar"><?=$this->lang->line('indicador_text_5')?></a>
			                <button class="btn btn-green btn-xs addEstandar" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_5')?>"><i class="fa fa-plus"></i></button>
			            </h4>
			       	</div>
			        <div id="collapseEstandar" class="panel-collapse collapse collapseEstandar">
			            <div class="panel-body">
				            <table class="table">
		      					<tbody class="padre" id="content_estandares"></tbody>
		      				</table>
	      				</div>
	      			</div>
      			</div>
			    <div class="panel panel-default">								    	
			       	<div class="panel-heading">
			            <h4 class="panel-title input_btn">
			                <a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseGlobal"><?=$this->lang->line('indicador_text_6')?></a>
			                <button class="btn btn-green btn-xs addParametros" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_6')?>"><i class="fa fa-plus"></i></button>
			            </h4>
			       	</div>
			        <div id="collapseGlobal" class="panel-collapse collapse collapseGlobal">
			            <div class="panel-body">
				            <table class="table">
		      					<tbody class="padre" id="content_parametros">

		      					</tbody>
		      				</table>
	      				</div>
	      			</div>						      			
	      		</div>
	      		<div class="panel panel-default">
	      			<div class="panel-heading">
			            <h4 class="panel-title input_btn">
			                <a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseDatos"><?=$this->lang->line('indicador_text_7')?> </a>
			                <button class="btn btn-green pull-right addCategorias btn-xs" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_8')?>"><i class="fa fa-plus"></i></button>
			            </h4>
			       	</div>
			        <div id="collapseDatos" class="panel-collapse collapse collapseDatos">
			        	<div class="panel-body">
			        		<div class="panel-group" id="accordionCategorias">
			        		</div>

			        	</div>
			        </div>
	      		</div>
	      		<div class="panel panel-default" data-id="collapseClasificacion">
			       	<div class="panel-heading">
			            <h4 class="panel-title input_btn">
			                <a data-toggle="collapse" data-parent="#accordionGlobal" href="#collapseClasificacion"><?=$this->lang->line('indicador_text_9')?></a>
			                <button class="btn btn-green btn-xs addGrupos" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_6')?>"><i class="fa fa-plus"></i></button>
			            </h4>
			       	</div>
			        <div id="collapseClasificacion" class="panel-collapse collapse collapseClasificacion">
			            <div class="panel-body">
				            <table class="table">
		      					<tbody class="panel-group accordionGrupos" id="accordionGruposClasficacion">
		      					
		      					</tbody>
		      				</table>
	      				</div>
	      			</div>						      			
	      		</div>
	      	</div>
	      	<div class="col-xs-12 col-md-12 pr-0">
				<button type="button" class="btn btn-save guardar_padre pull-right" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_10')?>" data-modo="guardar"><i class="fa fa-save"></i></button>
				<button type="button" class="btn btn-eraser guardar_padre pull-right" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_11')?>" data-modo="borrador"><i class="fa fa-save"></i> <?=$this->lang->line('borrador')?></button>
			</div>
	    </div>
	</div>			
</div>
				</div>

			</div>

    	</div>
			
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript" src="<?php print base_url(); ?>js/typeahead.js"></script>
		<script  type="text/javascript" language="javascript">

			var box_parametro = 1, //caja de grafica
			box_grupo = 1, //caja de grupo control
			box_categoria = 1, //cara de categoria
			heading = 1,
			codigosT = new Array(), //array de columnas variables
			info_indicador = <?php echo json_encode($informacion); ?>,
			array_periodicidad = <?php echo json_encode($periodicidad); ?>,
			array_tipo_indicadores = <?php echo json_encode($tipo_indicadores); ?>,
			array_globales = info_indicador.global_parameters,
			array_datos = info_indicador.datos,
			array_data_estandar = info_indicador.global_standar,
			array_estandar = <?php echo json_encode($estandar); ?>,
			array_clasificacion = info_indicador.global_ranking,
			init = 0,
			pos_globales = 0,
			pos_datos = 0,
			pos_grupos = 0,
			pos_parametros=0,
			box_estandar=0,
			pos_estandar=0,
			pos_clasificacion=0;
			var result = [],
			columnsT = new Array();

			//add row PADRE - OK
			function addRow() {

				//parametros globales
				init=1;
				if (array_globales.length>0) {
					for (var i = 0; i < array_globales.length; i++) {
						columnsT[array_globales[i].parameter.name]=array_globales[i].colums;
						pos_globales = i;
						$('#contentAccordion>.card').find('.panel .addParametros').click();
					}
				}else{
					$('#contentAccordion>.card').find('.panel .addParametros').click();
				}
					
				init=2;
				//parametros en datos
				if (array_datos.length>0) {
					for (var i = 0; i < array_datos.length; i++) {
						pos_datos=i;
						$('#contentAccordion>.card').find('.addCategorias').click();
					}
				}else{
					$('#contentAccordion>.card').find('.addCategorias').click();
				}
					
				init=3
				//parametros estandares
				if (Object.keys(array_data_estandar).length>0) {
					var map = new Map();
					for (const item of array_data_estandar) {
						if(!map.has(item.type_indicator.name)) {
							map.set(item.type_indicator.name, true);
							result.push({name: item.type_indicator.name, id: item.type_indicator.id});
						}
					}
					for (var i in result) { 
						pos_estandar=i;
						$('#contentAccordion>.card').find('.panel .addEstandar').click();
					}
				}else{
					$('#contentAccordion>.card').find('.panel .addEstandar').click();
				}

				init=4
				//parametros clasificacion
				if (array_clasificacion!==undefined && Object.keys(array_clasificacion).length>0) {					
					for (var i in array_clasificacion) { 
						pos_clasificacion=i;
						$('#collapseClasificacion').prev('.panel-heading').find('.addGrupos').click();
					}
				}else{
					$('#collapseClasificacion').prev('.panel-heading').find('.addGrupos').click();
				}						
				
				init=0;
			}

			//add estandares
			$('body').on('click', '.addEstandar', function () {
				if (box_estandar<array_tipo_indicadores.length) {
					var content='';
					var select_tipo_indicador='';
					content+='<tr data-class="rowEstandar'+box_estandar+'">';					
						content+='<td rowspan="3">';
							content+='<select class="form-control tipo_indicador" id="rowEstandar'+box_estandar+'" name="tipo_indicador" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_12')?>">';
		                		content+='<option value="" style="display:none;"><?=$this->lang->line("select")?></option>';
		                		for (var ij = 0; ij < array_tipo_indicadores.length; ij++) {
		                			content+='<option value="'+array_tipo_indicadores[ij]._id.$oid+'"';
		                			if (init==3 && result[pos_estandar].id==array_tipo_indicadores[ij]._id.$oid) {
				               			content+=' selected ';
				               		}
		                			content+='>'+array_tipo_indicadores[ij].name+'</option>';                			
		                		}
		                	content+='</select>'
						content+='</td>';

						var id_camp = result[pos_estandar].id;
						var data_estandar = array_data_estandar.filter(tT => tT.type_indicator.id == id_camp);
						console.log('data_estandar', data_estandar)

						for (var i = 0; i < array_estandar.length; i++) {
							var id_camp = array_estandar[i]._id.$oid,
							data_ = [],
							hide1 = '',
							hide2 = 'hidden';
							if (init==3 ) { //&& pos_estandar!=0
								data_ = data_estandar.filter(tT => tT.type_standar.id == id_camp);
								hide1 = (data_[0].operator=='rango')?'hidden':'',
								hide2 = (data_[0].operator=='rango')?'':'hidden';
							}

							if (i!=0) {
								content+='<tr data-class="rowEstandar'+box_estandar+'">';
							}
					
						content+='<td>'+array_estandar[i].name+'<input type="hidden" name="estandar" value="'+array_estandar[i]._id.$oid+'" data-name="'+array_estandar[i].name+'"></td>';
						content+='<td class="text-center">';
							content+='<select class="form-control btn_operador" name="tipo_operador">';
								content+='<option value="">Tipo operador</option>';
								content+='<option value="=" ';
								content+=(init==3 && data_.length>0 && data_[0].operator=='=')?'selected':'';
								content+=' > = </option>';
								content+='<option  value="!=" ';
								content+=(init==3 && data_.length>0 && data_[0].operator=='!=')?'selected':'';
								content+=' > != </option>';
								content+='<option value=">" ';
								content+=(init==3 && data_.length>0 && data_[0].operator=='>')?'selected':'';
								content+=' > > </option>';
								content+='<option value="<" ';
								content+=(init==3 && data_.length>0 && data_[0].operator=='<')?'selected':'';
								content+=' > < </option>';
								content+='<option value=">=" ';
								content+=(init==3 && data_.length>0 && data_[0].operator=='>=')?'selected':'';
								content+=' > >= </option>';
								content+='<option value="<=" ';
								content+=(init==3 && data_.length>0 && data_[0].operator=='<=')?'selected':'';
								content+=' > <= </option>';
								content+='<option value="rango" ';
								content+=(init==3 && data_.length>0 && data_[0].operator=='rango')?'selected':'';
								content+=' > A-B </option>';
							content+='</select>';
						content+='</td>';
						content+='<td class="text-center">';
							content+='<div class="input_ '+hide1+'">';
								content+='<input class="form-control " type="text" name="valor_parametro" ';
								content+=(init==3 && data_.length>0 )?' value="'+data_[0].value+'"':'';
								content+=' >';
							content+='</div>';
							content+='<div class="entre_ '+hide2+'">';
								content+='<div class="form-group">';
									content+='<div class="input-group">';
								      	content+='<input type="text" class="form-control" name="valor1" placeholder="<?=$this->lang->line('indicador_text_13')?>" ';
								      	content+=(init==3 && data_.length>0 )?' value="'+data_[0].value1+'"':'';
								      	content+=' >';
								      	content+='<div class="input-group-addon">';
								      		content+='<select name="operador1">';
								      			content+='<option value=">" ';
								      			content+=(init==3 && data_.length>0 &&  data_[0].operator1=='>')?'selected':'';
								      			content+=' > > A</option>';
								      			content+='<option value="<" ';
								      			content+=(init==3 && data_.length>0 && data_[0].operator1=='<')?'selected':'';
								      			content+=' > < A</option>';
								      			content+='<option value="<=" ';
								      			content+=(init==3 && data_.length>0 && data_[0].operator1=='<=')?'selected':'';
								      			content+=' > <= A</option>';
								      			content+='<option value=">=" ';
								      			content+=(init==3 && data_.length>0 && data_[0].operator1=='>=')?'selected':'';
								      			content+=' > >= A</option>';
								      		content+='</select>';
								      	content+='</div>';
								    content+='</div>';
								content+='</div>';
								content+='<div class="form-group">';
								    content+='<div class="input-group">';
								      	content+='<input type="text" class="form-control" name="valor2" placeholder="<?=$this->lang->line('indicador_text_15')?>" ';
								      	content+=(init==3 && data_.length>0)?' value="'+data_[0].value2+'"':'';
								      	content+=' >';
								      	content+='<div class="input-group-addon">';
								      		content+='<select name="operador2">';
								      			content+='<option value=">" ';
								      			content+=(init==3 && data_.length>0 && data_[0].operator2=='>')?'selected':'';
								      			content+=' > > A</option>';
								      			content+='<option value="<" ';
								      			content+=(init==3 && data_.length>0 && data_[0].operator2=='<')?'selected':'';
								      			content+=' > < A</option>';
								      			content+='<option value="<=" ';
								      			content+=(init==3 && data_.length>0 && data_[0].operator2=='<=')?'selected':'';
								      			content+=' > <= A</option>';
								      			content+='<option value=">=" ';
								      			content+=(init==3 && data_.length>0 && data_[0].operator2=='>=')?'selected':'';
								      			content+=' > >= A</option>';
								      		content+='</select>';
								      	content+='</div>';
								    content+='</div>';
								content+='</div>';
							content+='</div>';
						content+='</td>';
						if (i==0) {
							content+='<td rowspan="3" class="text-center">';
							content+='<button type="button" class="btn btn-trash delete_estandar" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_22')?>"><i class="fa fa-trash"></i></button>';
							content+='</td>';
						}
						content+='</tr>';									
					}
					$(this).closest('#accordionGlobal').find('#content_estandares').append(content);
					box_estandar = box_estandar+1;
				}					
			});

			//add categoria - OK
			$('body').on('click', '.addCategorias', function () {
				var content='<div class="panel panel-default" data-id="collapseGrupos">';
						content+='<div class="panel-heading input_btn">';
			               	content+='<a data-toggle="collapse" data-parent="#accordionCategorias" href="#collapseGrupos'+box_categoria+'">';
			               		content+='<input class="form-control categoria" name="categoria" placeholder="<?=$this->lang->line('indicador_text_16')?>" ';
			               		if (init==2 && array_datos.length>0) {
			               			content+=' value="'+array_datos[pos_datos].name_categoria+'" ';
			               		}
							content+='></a>';
			                content+='<div><button class="btn btn-green addGrupos btn-xs ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_23')?>"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-trash delete_categorias btn-xs ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_24')?>"><i class="fa fa-trash"></i></button></div>';							
				       	content+='</div>';
				       	content+='<div id="collapseGrupos'+box_categoria+'" class="panel-collapse collapse collapseGrupos">';
							content+='<div class="panel-body">';
								//content grupos
								content+='<div class="panel-group accordionGrupos" id="accordionGrupos">';
								content+='</div>';

							content+='</div>';
						content+='</div>';
					content+='</div>';
				
				$(this).closest('#accordionGlobal').find('#accordionCategorias').append(content);
				
				if (init==2) {
					pos_grupos=0;
					if (array_datos.length>0 && array_datos[pos_datos].grupos.length>0) {
						for (var i = 0; i < array_datos[pos_datos].grupos.length; i++) {
							pos_grupos=i;
							$(this).closest('.panel').find('#accordionCategorias').children('.panel').last().find('.addGrupos').click();
						}
					}else{
						$(this).closest('.panel').find('#accordionCategorias').children('.panel').last().find('.addGrupos').click();
					}					
				}else{
					$(this).closest('.panel').find('#accordionCategorias').children('.panel').last().find('.addGrupos').click();
				}
				
				box_categoria++;
			});

			//add grupos control - OK
			$('body').on('click', '.addGrupos', function () {
				var id_ = $(this).closest('.panel').attr('data-id'),
				name_label_grupos = (id_=='collapseGrupos')?'Nombre del label':'Nombre de la clasificación',
					content='<div class="panel panel-default">';
						content+='<div class="panel-heading">';
							content+='<h4 class="panel-title input_btn">';
				                content+='<a data-toggle="collapse" data-parent="#accordionGrupos" href="#collapseParametros'+box_parametro+'">';
				                	content+=' <input class="form-control name_grupo" name="name_grupo" placeholder="'+name_label_grupos+'"';
				                	if (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0) {
				                		content+=' value="'+array_datos[pos_datos].grupos[pos_grupos].name_grupo+'"';
				                	}
				                	if (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0) {
				                		content+=' value="'+array_clasificacion[pos_clasificacion].name_grupo+'"';
				                	}
				                content+='></a>';
				                content+='<div class="d-flex justify-content-center align-items-center">';
				                if (id_!='collapseGrupos') {
				                	content+='<select class="form-control tipo_regla" name="tipo_regla" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_17')?>">';
				                		content+='<option value="y"';
				                		if (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].type_ruler=='y') {
				                			content+=' selected ';
				                		}
				                		content+='>Y</option>';
				                		content+='<option value="y/o"';
				                		if (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].type_ruler=='y/o') {
				                			content+=' selected ';
				                		}
				                		content+='>Y/O</option>';
				                	content+='</select>';
				                }
				                	content+='<select class="form-control tipo_indicador" name="tipo_indicador" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_12')?>">';
				                		content+='<option value="" style="display:none;"><?=$this->lang->line("select")?></option>';
				                		for (var i = 0; i < array_tipo_indicadores.length; i++) {
				                			content+='<option value="'+array_tipo_indicadores[i]._id.$oid+'" ';
				                			if ( (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_tipo_indicadores[i]._id.$oid==array_datos[pos_datos].grupos[pos_grupos].type_indicator.id) || (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].type_indicator.id==array_tipo_indicadores[i]._id.$oid) ) {
				                				content+=' selected ';
				                			}else if(i==0){
				                				content+=' selected ';
				                			}
				                			content+='>'+array_tipo_indicadores[i].name+'</option>';
				                		}
				                	content+='</select>'
				                	content+='<button class="btn btn-green addParametros btn-xs ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_18')?>"><i class="fa fa-plus"></i></button>';
				                	content+='<button type="button" class="btn btn-xs btn-trash delete_grupo ml-2" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_19')?>"><i class="fa fa-trash"></i></button>';
				                content+='</div>';
				            content+='</h4>';
						content+='</div>';
						content+='<div id="collapseParametros'+box_parametro+'" class="panel-collapse collapse collapseParametros">';
				        	content+='<div class="panel-body">';
				        		//content parametros
				        		content+='<div class="table-responsive">';
				        			content+='<table class="table">';
				        				content+='<tbody>';																
		  								content+='</tbody>';
				        			content+='</table>';
				        		content+='</div>';
				        	content+='</div>';
				        content+='</div>';

					content+='</div>';

				$(this).closest('.panel').find('.accordionGrupos').append(content);
				if (init==2) {
					pos_parametros=0;
					if (array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0) {
						for (var i = 0; i < array_datos[pos_datos].grupos[pos_grupos].datos.length; i++) {
							columnsT[array_datos[pos_datos].grupos[pos_grupos].datos[i].parameter.name]=array_datos[pos_datos].grupos[pos_grupos].datos[i].colums;
							pos_parametros=i;
							$(this).closest('.panel').find('.addParametros').last().click();
						}
					}else{
						$(this).closest('.panel').find('.addParametros').last().click();
					}											
				}else if (init==4) { //clasificacion
					pos_parametros=0;
					if (array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0) {
						for (var i = 0; i < array_clasificacion[pos_clasificacion].datos.length; i++) {
							columnsT[array_clasificacion[pos_clasificacion].datos[i].parameter.name]=array_clasificacion[pos_clasificacion].datos[i].colums;
							pos_parametros=i;
							$(this).closest('.panel').find('.addParametros').last().click();
						}
					}else{
						$(this).closest('.panel').find('.addParametros').last().click();
					}		 
				}else{
					$(this).closest('.panel').find('.addParametros').last().click();
				}				
				box_parametro++;
			});

			//add parametros - OK
			var hijos=0;
			$('body').on('click', '.addParametros', function () {

				var id_ = $(this).closest('.panel-group').attr('id'),
				content='<tr>';
				if (id_ == 'accordionGrupos') {
					content+='<td class="text-center">';
						content+='<select class="form-control" name="tipo_formula">';
							content+='<option value="numerador" ';
							content+= (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].type_formula == 'numerador')?'selected':'';
							content+='>Numerador</option>';
							content+='<option value="denominador" ';
							content+= (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].type_formula == 'denominador')?'selected':'';
							content+='>Denominador</option>'
					content+='</td>';
				}
					content+='<td class="text-center">';
						if (id_ != 'accordionGruposClasficacion') {
						content+='<div class="input-group">';
			      			content+='<div class="input-group-addon param_">';
			      				content+='<input class="form-check-input" type="checkbox" name="tipo_fecha" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_20')?>" ';
			      				if (init==1 && array_globales.length>0) {
									content+=(array_globales[pos_globales].type_fecha=='true')?'checked':'';
								}
								if (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0) {
									content+=(array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].type_fecha=='true')?'checked':'';
								}
			      				content+='>';
		      				content+='</div>';
							content+='<input type="text" class="form-control tags_auto " name="tags_auto" placeholder="<?=$this->lang->line('indicador_text_14')?>" id="hijoTag'+hijos+'" ';
								if (init==1 && array_globales.length>0) {
									content+=' value="'+array_globales[pos_globales].parameter.name+'" data-id="'+array_globales[pos_globales].parameter.id+'" ';
								}
								if (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0) {
									content+=' value="'+array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].parameter.name+'" data-id="'+array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].parameter.id+'"';
								}
							content+='>';
					    content+='</div>';
					    }else{
					    	content+='<input type="text" class="form-control tags_auto " name="tags_auto" placeholder="<?=$this->lang->line('indicador_text_14')?>" id="hijoTag'+hijos+'" ';
					    	if (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0) {
					    		content+=' value="'+array_clasificacion[pos_clasificacion].datos[pos_parametros].parameter.name+'" data-id="'+array_clasificacion[pos_clasificacion].datos[pos_parametros].parameter.id+'"';
					    	}else{
					    		content+'data-id=""';
					    	}					    	
					    	content+='>';
					    }
					content+='</td>';
					content+='<td class="text-center">';
						content+='<select class="form-control btn_operador" name="tipo_operador">';						
							content+='<option value="">Tipo operador</option>';
							content+='<option value="=" ';
							content+= ( (init==1 && array_globales.length>0 && array_globales[pos_globales].operator == '=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator=='=') || (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='=') )?'selected':'';
							content+='> = </option>';
							content+='<option  value="!=" ';
							content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == '!=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator=='!=') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='!=') )?'selected':'';
							content+= '> != </option>';
							content+='<option value=">" ';
							content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == '>') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator=='>') || (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='>') )?'selected':'';
							content+= '> > </option>';
							content+='<option value="<" ';
							content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == '<') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator=='<') || (init==4 && array_clasificacion!==undefined &&  array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='<') )?'selected':'';
							content+= '> < </option>';
							content+='<option value=">=" ';
							content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == '>=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator=='>=') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='>=') )?'selected':'';
							content+= '> >= </option>';
							content+='<option value="<=" ';
							content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == '<=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator=='<=') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='<=') )?'selected':'';
							content+= '> <= </option>';
							content+='<option value="rango" ';
							content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == 'rango') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator=='rango') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='rango') )?'selected':'';
							content+='> A-B </option>';
						content+='</select>';
					content+='</td>';

					var hide1 = ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == 'rango') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator == 'rango') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='rango') )?'hidden':'',
					hide2 = ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator == 'rango') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator == 'rango') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator=='rango'))?'':'hidden';

					content+='<td class="text-center">';
						content+='<div class="input_ '+hide1+'">';
							content+='<input class="form-control " type="text" name="valor_parametro" list="valor_parametroA'+hijos+'" ';
							if (init==1 && array_globales.length>0) {
								content+= ' value="'+array_globales[pos_globales].value+'" ';
							}
							if (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0) {
								content+= 'value="'+array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].value+'" ';
							}	
							if (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0) {
								content+= 'value="'+array_clasificacion[pos_clasificacion].datos[pos_parametros].value+'" ';
							}							
						    content+='><datalist id="valor_parametroA'+hijos+'">';
						    content+='</datalist>';
						content+='</div>';
						content+='<div class="entre_ '+hide2+'">';
							content+='<div class="form-group">';
								content+='<div class="input-group">';
							      	content+='<input type="text" class="form-control" name="valor1" placeholder="<?=$this->lang->line('indicador_text_13')?>" list="valor_parametroB'+hijos+'" ';
							      	if (init==1 && array_globales.length>0) {
										content+= ' value="'+array_globales[pos_globales].value1+'" ';
									}
									if (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0) {
										content+= ' value="'+array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].value1+'" ';
									}	
									if (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0) {
										content+= ' value="'+array_clasificacion[pos_clasificacion].datos[pos_parametros].value1+'" ';
									}	
							      	content+='><datalist id="valor_parametroB'+hijos+'">';
						    		content+='</datalist>';
							      	content+='<div class="input-group-addon">';
							      		content+='<select name="operador1">';
							      			content+='<option value=">" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator1 == '>') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator1 == '>') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator1 == '>') )?'selected':'';
							      			content+='> > A</option>';
							      			content+='<option value="<" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator1 == '<') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator1 == '<') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator1 == '<') )?'selected':'';
							      			content+='> < A</option>';
							      			content+='<option value="<=" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator1 == '<=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator1 == '<=') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator1 == '<=') )?'selected':'';
							      			content+='> <= A</option>';
							      			content+='<option value=">=" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator1 == '>=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator1 == '>=') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator1 == '>=') )?'selected':'';
							      			content+='> >= A</option>';
							      		content+='</select>';
							      	content+='</div>';
							    content+='</div>';
							content+='</div>';
							content+='<div class="form-group">';
							    content+='<div class="input-group">';
							      	content+='<input type="text" class="form-control" name="valor2" placeholder="<?=$this->lang->line('indicador_text_15')?>" list="valor_parametroB'+hijos+'"';
							      	if (init==1 && array_globales.length>0) {
										content+= ' value="'+array_globales[pos_globales].value2+'" ';
									}
									if (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0) {
										content+= ' value="'+array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].value2+'" ';
									}
									if (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0) {
										content+= ' value="'+array_clasificacion[pos_clasificacion].datos[pos_parametros].value2+'" ';
									}
							      	content+='><datalist id="valor_parametroB'+hijos+'">';
						    		content+='</datalist>';
							      	content+='<div class="input-group-addon">';
							      		content+='<select name="operador2">';
							      			content+='<option value=">" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator2 == '>') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator2 == '>') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator2 == '>') )?'selected':'';
							      			content+='> > A</option>';
							      			content+='<option value="<" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator2 == '<') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator2 == '<') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator2 == '<') )?'selected':'';
							      			content+='> < A</option>';
							      			content+='<option value="<=" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator2 == '<=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator2 == '<=') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator2 == '<=') )?'selected':'';
							      			content+='> <= A</option>';
							      			content+='<option value=">=" ';
							      			content+= ((init==1 && array_globales.length>0 && array_globales[pos_globales].operator2 == '>=') || (init==2 && array_datos.length>0 && array_datos[pos_datos].grupos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos.length>0 && array_datos[pos_datos].grupos[pos_grupos].datos[pos_parametros].operator2 == '>=') || (init==4 && array_clasificacion!==undefined && array_clasificacion.length>0 && array_clasificacion[pos_clasificacion].datos.length>0 && array_clasificacion[pos_clasificacion].datos[pos_parametros].operator2 == '>=') )?'selected':'';
							      			content+='> >= A</option>';
							      		content+='</select>';
							      	content+='</div>';
							    content+='</div>';
							content+='</div>';
						content+='</div>';
					content+='</td>';
					content+='<td class="text-center">';
						content+='<button type="button" class="btn btn-trash delete_parametro" data-toggle="tooltip" title="<?=$this->lang->line('indicador_text_21')?>"><i class="fa fa-trash"></i></button>';
					content+='</td></tr>';
				$(this).closest('.panel').find('tbody').append(content);
			
				$('#hijoTag'+hijos).typeahead({
					hint: true,
					highlight: true,
					minLength: 1,
				},{
					limit: 12,
					async: true,
					source: function async (query, processSync, processAsync){
						return $.ajax({
							url: "<?php print base_url(); ?>indicadores/verColumnaConfiguracion",
							type: 'POST',
							data:{
								tags_auto: query,
								id_entidad: '<?php echo $id_entidad; ?>'
							},
							dataType: 'JSON',
							success: function(data){
								if (data.res === "ok") {
									var tags_auto = [];
									for (var i = 0; i < data.bar.length; i++) {
										tags_auto.push(data.bar[i].name);
										codigosT[data.bar[i].name]=data.bar[i]._id.$oid;
										columnsT[data.bar[i].name]=data.bar[i].columns;
									}
									return processAsync(tags_auto);		                    		                     
								}else{
									let muestra5 = $('#muestra5');
									muestra5.html(data.msj);		                 	
								}
							}
						});
					}
				});
				$('#hijoTag'+hijos).bind('typeahead:select', function(ev, suggestion) {					
					var str=suggestion,
					aa = $(this).closest('td').next().next().children('.entre_').children().children().children('datalist'),
					bb = $(this).closest('td').next().next().children('.input_').children('datalist');
					aa.children('option').remove();
					bb.children('option').remove();
					//trae los distinct valores del parametro seleccionado
					ajax('indicadores/getValorColumnaVariables',
	                {   
	                	id_entidad: '<?php echo $id_entidad; ?>',
	                	columns: columnsT[suggestion]
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	aa.append('<option value="NULL"></option>');
	                    	for (var i = 0; i < data.bar.length; i++) {
	                    	    aa.append('<option value="'+data.bar[i]._id+'"></option>');
	                    	    bb.append('<option value="'+data.bar[i]._id+'"></option>');
	                    	}                                   
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);                
					$(this).attr('data-id',codigosT[suggestion]);
				});
				hijos++;
			});

			//change operador input por select - OK
			$('body').on('change', '.btn_operador', function () {
				$(this).closest('td').next().children().addClass('hidden');
				if ($(this).val()=='rango') {
					$(this).closest('td').next().children('.entre_').removeClass('hidden');
				}else{
					$(this).closest('td').next().children('.input_').removeClass('hidden');
				}
			});

			//######## ELIMINAR - OK

			//eliminar categorias  - front OK
			$('body').on('click', '.delete_categorias', function () {
				$(this).closest('.panel').remove();				
			});

			//eliminar grupo control - front OK
			$('body').on('click', '.delete_grupo', function () {
				$(this).closest('.panel').remove();
			});

			//eliminar parametro - front OK
			$('body').on('click','.delete_parametro', function() {
				$(this).parent().parent().remove();
			});

			//change operador input por select - OK
			$('body').on('click', '.btn_operador', function () {
				$(this).closest('td').next().children().addClass('hidden');
				if ($(this).val()=='rango') {
					$(this).closest('td').next().children('.entre_').removeClass('hidden');
				}else{
					$(this).closest('td').next().children('.input_').removeClass('hidden');
				}
			});


			//####### END ELIMINAR

			//####### ADD BORDER BLUE

			//agrega el borde azul y gris a la card y mueve los actions buttons del padre
			$('body').on('click', '.card:not(.activeCard)', function () {
				$('.div_btn').removeClass('hidden');
				$('.card.activeCard .btn-div').addClass('hidden');
				$('.card.activeCard .grafica>div.activeGrafica').removeClass('activeGrafica');
				$('.card').removeClass('activeCard');
				var position = $(this).addClass('activeCard').position();
				$(this).find('.btn-div').removeClass('hidden');				
				$('.div_btn').animate({"top":position.top+"px"},1000,"linear");
			});

			//###### END BORDER BLUE

			//remove border red when is bad
		   	$('body').on('change', '[name=titulo].border-red, .collapseEstandar select.border-red, #collapseClasificacion input.border-red, #collapseClasificacion select.border-red, [name=searchentidad].border-red', function () {
		   		$(this).removeClass('border-red');
		   	});		   

		    //guardar el padre con sus hijos gráficas
			$('body').on('click', '.guardar_padre', function () {	
				var bandera = 0,
				modo_guardar = $(this).attr('data-modo');		
				/*
				solo se puede guardar un padre a la vez
				tendrá la estructura: [informacion][parametros_globales][datos]
					informacion: [titulo][periodicidad][descripcion][estandar];
						estandar: [#][tipo_indicador][tipo_riesgo][tipo_operadorr][valor][operador1][valor1][operador2][valor2];
					parametros_globales: [#][tipo_fecha][id_parametro][tipo_operador][valor][operador1][valor1][operador2][valor2];
					datos: [#][name_categoria][grupos]
						grupos: [#][name_grupo][parametros]
							parametros: [#][tipo_formula (numerador/denominador)][tipo_fecha][id_parametro][tipo_operador][valor][operador1][valor1][operador2][valor2]
				*/
				var info = [];
				if ($('[name=titulo]').val()=='') {
					bandera=1;
					$('[name=titulo]').addClass('border-red');
					$('[name=titulo]').closest('.panel').children('.panel-heading').find('a').trigger('click');
					return mensaje('Debe agregar un título al indicador');
				}
				//informacion estandar
				var tmp_estandar = [],
				msj_estandar = '';
				$('#content_estandares>tr').each(function () {
					var ban=0,
					name_class = $(this).attr('data-class'),
					tipo_indicador = $('#'+name_class).val(),
					tipo_operador = $(this).find('[name=tipo_operador]').val(),
					valor = $(this).find('[name=valor_parametro]').val(),
					operador1 = $(this).find('[name=operador1]').val(),
					operador2 = $(this).find('[name=operador2]').val(),
					valor1 = $(this).find('[name=valor1]').val(),
					valor2 = $(this).find('[name=valor2]').val();
					if (modo_guardar=="guardar") {
						if (tipo_indicador=='') {
							ban = 1;
							$('#'+name_class).addClass('border-red');
							$(this).closest('.panel').children('.panel-heading').find('a').trigger('click');
							msj_estandar=('Debe seleccionar el tipo de indicador para el estándar global');
							return false;
						}
						if (tipo_operador=='') {
							ban = 1;
							$(this).find('[name=tipo_operador]').addClass('border-red');
							$(this).closest('.panel').children('.panel-heading').find('a').trigger('click');
							msj_estandar=('Debe seleccionar un operador para el estándar global');
							return false;
						}else{
							if (tipo_operador!='rango' && valor=='') {
								$(this).find('[name=valor_parametro]').addClass('border-red');
							}
							if (tipo_operador=='rango') {
								if (valor1=='') {
									$(this).find('[name=valor1]').addClass('border-red');
								}
								if (valor2=='') {
									$(this).find('[name=valor2]').addClass('border-red');
								}								
							}
							if ( (tipo_operador!='rango' && valor=='') || (tipo_operador=='rango' && (valor1=='' || valor2=='')) ) {
								ban = 1;
								$(this).closest('.panel').children('.panel-heading').find('a').trigger('click');
								msj_estandar=('Debe agregar un valor para el estándar global');
								return false;
							}
						}
					}						
					if (bandera==0 && ban ==0) {
						var tmp={
							type_indicator: { 'id': tipo_indicador, 'name': $('#'+name_class).children("option:selected").html()},
							type_standar: {'id': $(this).find('[name=estandar]').val(), 'name': $(this).find('[name=estandar]').attr('data-name')},
							operator: tipo_operador,
							value: valor,
							operator1: (tipo_operador=='rango')?operador1:'',
							value1: valor1,
							operator2: (tipo_operador=='rango')?operador2:'',
							value2: valor2
						};
						tmp_estandar.push(tmp);
					}						
				});

				if (msj_estandar!='') {
					return mensaje(msj_estandar);
				}

				//informacion del evento
				var general = {
					name: $('[name=titulo]').val(),
					periodicity: {'id': $('[name=periodicidad]').val(), 'name': $('[name=periodicidad]').children("option:selected").html()},
					description: $('[name=descripcion]').val()
				};			

				var tmp_global = [];
				//informacion de " Agregar parámetros Globales"
				$('#collapseGlobal tr').each(function () {
					var global_ = getDatosParametros($(this), 'global', modo_guardar, columnsT);
					if (global_['bandera']==0) {
						tmp_global.push(global_.tmp);
					}
				});

				//información "Agregar clasificación Globales"
				var tmp_clasificacion=[]
				msj_clasificacion='';
				$('#accordionGruposClasficacion>.panel').each(function () {
					var tmp_parametros=[],
					ban = 0;
					if ($(this).find('[name=name_grupo]').val()=='') {
						/*bandera=1;
						msj_clasificacion='Debe agregar un nombre a la clasificación';
						$(this).find('[name=name_grupo]').addClass('border-red');
						return false;*/
					}

					$(this).find('.collapseParametros tr').each(function () {
						var parametros_ = getDatosParametros($(this), 'global', modo_guardar, columnsT);
						if (parametros_['bandera']!=0) {
							/*bandera=1;
							msj_clasificacion=parametros_['msj'];
							$(parametros_['input']).addClass('border-red');
							ban++;
							return false;*/
						}else{
							if (parametros_['tmp'].length>0) {

							}
							tmp_parametros.push(parametros_['tmp']);
						}							
					});
					if (ban==0) {
						var grupo = {
							name_grupo: $(this).find('.name_grupo').val(),
							datos: tmp_parametros,
							type_indicator: {'id': $(this).find('.tipo_indicador').val(), 'name': $(this).find('.tipo_indicador').children("option:selected").html()},
							type_ruler: $(this).find('.tipo_regla').val()
						}
						tmp_clasificacion.push(grupo);
					}
				})

				if (msj_clasificacion!='') {
					mensaje(msj_clasificacion);
				}

				//información "Agregar clasificación Globales"
				var tmp_categorias = [],
				msj_categorias='';
				$('#accordionCategorias>.panel').each(function () {					
					var tmp_grupos = [],
					ban = 0;
					if ($(this).find('[name=categoria]').val()=='') {
						bandera=1;
						msj_categorias='Debe agregar un nombre a la categoría';
						return false;
					}
					$(this).find('#accordionGrupos .panel').each(function () {						
						var tmp_parametros = [],
						tipo_indicador = $(this).find('[name=tipo_indicador]').val();

						if ($(this).find('.name_grupo').val()=='') {
							bandera=1;
							msj_categorias='Debe agregar un nombre del label';
							return false;
						}

						if (tipo_indicador=='' && modo_guardar=="guardar"){
							bandera=1;
							ban++;
							msj_categorias='Debe seleccionar un tipo de indicador, por defecto es Normal';
							return false;
						}

						$(this).find('.collapseParametros tr').each(function () {
							var parametros_ = getDatosParametros($(this), $(this).find('[name=tipo_formula]').val(), modo_guardar, columnsT);
							if (parametros_['bandera']!=0) {
								bandera=1;
								mensaje(parametros_['msj']);
								ban++;
								$(parametros_['input']).addClass('border-red');
								return false;
							}else{
								tmp_parametros.push(parametros_['tmp']);
							}							
						});
						if (ban==0) {
							var grupo = {
								name_grupo: $(this).find('.name_grupo').val(),
								datos: tmp_parametros,
								type_indicator: {'id': tipo_indicador, 'name': $(this).find('[name=tipo_indicador]').children("option:selected").html()},
							}
							tmp_grupos.push(grupo);
						}							
					});
					if (ban==0) {
						var cat_ = {
							name_categoria: $(this).find('[name=categoria]').val(),
							grupos: tmp_grupos
						}
						tmp_categorias.push(cat_);
					}						
				});
				if (msj_categorias!='') {
					mensaje(msj_categorias);
				}
				var tmp_g = {
					information: general, //"Agregar información del evento"
					global_standar: tmp_estandar, //"Agregar estándar Globales"
					global_parameters: tmp_global, //"Agregar  parámetros Globales"
					global_ranking: tmp_clasificacion, //"Agregar clasificación Globales"
					datos: tmp_categorias //"Agregar datos"
				}

				info.push(tmp_g);
				if (info.length==0) {
					bandera = 1;
				}
				if ($('[name=searchentidad]').val()=='') {
					bandera = 1;
					$('[name=searchentidad]').addClass('border-red');
					mensaje('Debe seleccionar la entidad');
				}
				if (bandera == 0) {
					console.log(info);
					var id_entidad = $('[name=searchentidad]').val();
					ajax('indicadores/guardarIndicadorPadre',
	                {   
	                	id: $('[name=id_indicador]').val(),
	                	info: info,
	                	id_entidad: id_entidad,
	                	modo_guardar: (modo_guardar=='guardar')?1:0
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	mensaje(data.msj);
	                    	window.location = base_url+'admin/configuracionIndicadores';
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
				}
			});
			//objeto, de donde viene, el modo de guardar (son filas de los parámetros)
			function getDatosParametros(e, f, modo, columns=false) {
				var rta = [],
				object_tags_auto = e.find('[name=tags_auto]'),
				parametro = object_tags_auto.attr('data-id'),
				valor = valor1 = operador1 = valor2 = operador2 = '',
				tmp_={};

				var columns_ = columns[object_tags_auto.val()];
				console.log(columns_);

				rta['msj'] = '';
				rta['bandera']=0;
				rta['tmp']={};
				rta['input']='';

				if (modo=='guardar') {
					if (parametro==undefined || parametro=='') {						
						if (f!='denominador' || f=='global') {
							rta['msj'] = 'Debe seleccionar un parámetro';
							rta['bandera']=1;
							rta['input']=object_tags_auto;
							return rta;
						}					
					}else{
						//si el parametro no es tipo fecha y no ha seleccionado el tipo_operador -> bad				
						if (e.find('[name=tipo_fecha]').is(":checked") != true && (e.find('[name=tipo_operador]').val()=='Tipo operador' || e.find('[name=tipo_operador]').val()=='')) {
							rta['msj'] = 'Debe seleccionar un operador';
							rta['bandera']=1;
							rta['input']=e.find('[name=tipo_operador]');
							return rta;
						}else{
							//tipo_operador es >, =, >=, <, <= o !=
							if (e.find('[name=tipo_operador]').val()!='rango') {
								valor = e.find('[name=valor_parametro]').val();
							}else{
								//tipo_operador es rango
								if (e.find('[name=valor1]').val()=='') {
									rta['msj'] = 'Debe agregar un valor';
									rta['bandera']=1;
									rta['input']=e.find('[name=valor1]');
									return rta;
								}else{
									valor1 = e.find('[name=valor1]').val();
									operador1 = e.find('[name=operador1]').val();
								}
								if (e.find('[name=valor2]').val()=='') {
									rta['msj'] = 'Debe agregar un valor';
									rta['bandera']=1;
									rta['input']=e.find('[name=valor2]');
									return rta;
								}else{
									valor2 = e.find('[name=valor2]').val();
									operador2 = e.find('[name=operador2]').val();
								}
							}							
						}
						tmp_={
							type_formula: f,
							type_fecha: e.find('[name=tipo_fecha]').is(":checked"),
							parameter: {'id': parametro, 'name': object_tags_auto.val()},
							operator: e.find('[name=tipo_operador]').val(),
							value: valor,
							operator1: operador1,
							value1: valor1,
							operator2: operador2,
							value2: valor2,
							colums: columns_
						}
						rta['tmp']=tmp_;
						return rta;
					}	
				}else{
					tmp_={
						type_formula: f,
						state_fecha: e.find('[name=tipo_fecha]').is(":checked"),
						parameter: {'id': (parametro!='')?parametro:'', 'name': (parametro!='')?object_tags_auto.val():''},
						operator: e.find('[name=tipo_operador]').val(),
						value: e.find('[name=valor_parametro]').val(),
						operator1: e.find('[name=operador1]').val(),
						value1: e.find('[name=valor1]').val(),
						operator2: e.find('[name=operador2]').val(),
						value2: e.find('[name=valor2]').val(),
						colums: columns_
					}
					rta['tmp']=tmp_;					
					return rta;
				}
			}

			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

			$(document).ready(function(){
				addRow();
		    });
		</script>
	</body>
</html>