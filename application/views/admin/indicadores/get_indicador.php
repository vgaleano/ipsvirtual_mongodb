<?php $this->load->view("common/admin/head")?>
	<style type="text/css">
		.px-4{
			padding: 0 2rem;
		}
		.font-bold{
			font-weight: 600;
		}
	</style>
</head>
<body>
	<?php $this->load->view("common/admin/nav")?>
	<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
    	<div class="col-md-12 principal-padding" >
		    <div class="col-md-12 container-fluid"  id="content-principal">

		    	<a href="<?php echo base_url(); ?>admin/configuracionIndicadores" class="btn btn-border-dark btn-regresar" ><i class="fa fa-arrow-left"></i></a>
							
		    	<div class="col-xs-12 col-md-12 box">
	    			<div class="col-xs-12 col-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">
	    				<div class="row">
	    					<div class="col-xs-12 col-md-12">
	    						<h4><?php echo $informacion[0]->titulo; ?></h4>
	    					</div>
	    					<div class="col-xs-12 col-md-12">	    						
	    						<div id="chartdiv" style="width: 100%; height: 500px;"></div>	    						
	    					</div>
	    					<div class="col-xs-12 col-md-12">
	    						<p><?php echo $informacion[0]->descripcion; ?></p>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>
	<!-- ==============================================
	SCRIPTS
	=============================================== -->	
	<?php $this->load->view("common/admin/extras")?>
	<?php $this->load->view("common/admin/js")?>
	<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/core.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/charts.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>libs/amcharts4/themes/animated.js"></script>
	<script type="text/javascript">
		
		$(document).ready(function () {

			ajax('servicios/getKpisUnique',
	        {   
	           	entidad: '<?php echo $id_entidad; ?>',
	            eventos: '<?php echo $id; ?>'
	        },
	        function(data){
	            if(data.res=="ok"){
	            	
	            	var puntos = data.puntos;
	            	var categorias = data.categorias;
	            	console.log('puntos',data.puntos);
	            	console.log('categorias',categorias);

					am4core.ready(function() {
						am4core.disposeAllCharts();
						// Themes begin
						am4core.useTheme(am4themes_animated);
						// Themes end

						// Create chart instance
						var chart = am4core.create("chartdiv", am4charts.XYChart);

						chart.data = puntos;

						// Create category axis
						var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
						categoryAxis.dataFields.category = "label";
						//categoryAxis.renderer.opposite = true;

						// Create value axis
						var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
						//valueAxis.renderer.inversed = true;
						valueAxis.title.text = "Porcentajes [%]";
						valueAxis.renderer.minLabelPosition = 0.01;
						valueAxis.min = 0; 
						valueAxis.max = 100; 

						var i = 0;
						for (var categoria in categorias) {
							// Create series
							var add = i+1;
							var name_series = 'series'+add;
							var name_series = chart.series.push(new am4charts.LineSeries());
							name_series.dataFields.valueY = categorias[categoria]['name'];
							name_series.dataFields.categoryX = "label";
							name_series.name = categorias[categoria]['name'];
							name_series.strokeWidth = 3;
							name_series.bullets.push(new am4charts.CircleBullet());
							name_series.tooltipText = "{name} ({num_"+categorias[categoria]['num']+"}) \n {categoryX}: {valueY}%";
							name_series.legendSettings.valueText = "{valueY}";
							name_series.visible  = false;
							i= i+1;
						}

						// Add chart cursor
						chart.cursor = new am4charts.XYCursor();
						chart.cursor.behavior = "zoomY";

						// Add legend
						chart.legend = new am4charts.Legend();
					}); // end am4core.ready()
	            }else{			            	
	             	mensaje(data.msj);
	            }
	        },3000000);

		})

	</script>
</body>
</html>