<?php $this->load->view("common/admin/head")?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/dashboard.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('titulo_recordatorio')?></h2>
	    			</div>
	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
							<form id="formEntidad" action="<?php print base_url();?>admin/pacienteMensajeria" method="POST" onsubmit="return false">
								<div class="input-group">
					      			<div class="input-group-addon">
					      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
				      				</div>
									<select class="form-control" name="searchentidad">
								    	<?php $ent = $this->session->userdata('searchentidad1'); if(!$ent) $ent=""; ?>
								        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
								        <?php foreach ($entidad as $r) { ?>
											<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
										<?php } ?>
								    </select>
							    </div>																		
							</form>
						<?php } ?>
					</div>
				</div>

				<div class="row  mt-20">
					<div class="col-xs-12 col-md-12">
						<div class="table-responsive table-color">
							<table class="table table-hover table-bordered text-center">
								<thead>
									<tr>
										<th><?=$this->lang->line('numero_documento')?></th>
										<th><?=$this->lang->line('nombre')?></th>
										<th><?=$this->lang->line('telefono')?></th>
										<th><?=$this->lang->line('fecha_cita')?></th>
										<th><?=$this->lang->line('fecha_envio')?></th>
										<th><?=$this->lang->line('estado_envio')?></th>
									</tr>
								</thead>
								<tbody>
								<?php if(isset($recordatorios) && count($recordatorios) > 0){
								foreach ($recordatorios as $r) { ?>
									<tr>
										<td><?php echo $r->numero_documento; ?></td>
										<td><?php echo $r->nombre; ?></td>
										<td><?php echo $r->telefono; ?></td>
										<td><?php echo $r->fecha_cita; ?></td>
										<td><?php $fecha_envio = ($r->fecha_envio_p1!="0000-00-00") ? $r->fecha_envio_p1 : $r->fecha_envio_p2;
										echo ($fecha_envio!='0000-00-00' && $fecha_envio!='') ?  date('d-m-Y',strtotime($fecha_envio)): '----' ; ?></td>
										<td><?php echo ($r->estado_envio=='1') ? $this->lang->line('activo') : $this->lang->line('desactivado') ; ?></td>
									</tr>
								<?php } }else{ ?>
									<tr>
										<td colspan="6"><p class="text-center"><?=$this->lang->line('bad_recordatorio_table')?></p></td>
									</tr>
								<?php } ?>
								</tbody>								
							</table>
						</div>

						<div class="col-md-12" style="text-align:center">
							<nav aria-label="Page navigation">
					            <ul class="pagination">
					                <?php echo $this->pagination->create_links(); ?>
					            </ul>
			                </nav>
			            </div>

					</div>
				</div>

			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript">	   

			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });
		</script>
	</body>
</html>