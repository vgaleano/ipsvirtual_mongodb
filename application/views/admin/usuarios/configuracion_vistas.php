<?php $this->load->view("common/admin/head")?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/dashboard.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/bootstrap-switch.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('config_vista_1')?> <a href="<?php echo base_url(); ?>admin/configuracionRoles" class="btn btn-border-dark ml-1" ><i class="fa fa-arrow-left"></i></a></h2>
	    			</div>
	    		</div>

	    		<div class="row  mt-20">
	    			<div class="col-xs-12 col-md-12">
	    				<div class="table-responsive table-color">
							<table class="table tabla table-striped text-center">
							    <thead>
								    <tr>
								        <th class="text-center"><?=$this->lang->line('vista')?></th>
								        <th class="text-center"><?=$this->lang->line('permiso')?></th>
								    </tr>
							    </thead>
							    <tbody>
									<?php if(!empty($vistas)){
									if(is_array($vistas) && count($vistas)) {
										foreach ($vistas as $so) {?>
									<tr>
										<td><?=$so->name_view ?></td>
										<td ><input type="checkbox" class="estado" id="estado" data-rol="<?=$id_rol?>" data-id="<?=$so->_id; ?>" name="my-checkbox" <?php if(in_array($so->_id, $permisos)){ echo 'checked'; } ?>></td>	
									</tr>
									 <?php }}}else{?>
									 <tr>
									 	<td colspan="5"><?=$this->lang->line('bad_table_vista')?></td>
									 </tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<div class="col-md-12" style="text-align:center">
							<nav aria-label="Page navigation">
					            <ul class="pagination">
					                <?php echo $this->pagination->create_links(); ?>
					            </ul>
			                </nav>
			            </div>
	    			</div>
	    		</div>
				
			</div>
		</div>
		<!-- <div class="modal fade" id="usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	              	<div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user fa-2x" aria-hidden="true" style="color:#468FE4"></i> Editar Usuario</h4>
	             	</div>
	                <div class="modal-body">
                    	<input type="hidden" name="id_rol">
                    	<div class="form-group">
                             <label>Rol</label>
                             <input type="text" name="nombre" class="form-control">
                        </div>
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-border-dark" data-dismiss="modal">Cancelar</button>
	                    <button type="button" class="btn mas btn-green" id="guardar">Guardar</button> 
	                </div>
	            </div>
	          </div>
	        </div>
        </div> ->
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script type="text/javascript" src="<?php echo base_url();?>js/bootstrap/bootstrap-switch.js"></script>
		<script  type="text/javascript" language="javascript">
			$("[name='my-checkbox']").bootstrapSwitch();
			 function cambiarestado(id,rol,estado){
	      	 	ajax('usuarios/permisoVista',
	            {   
	            	vista: id,
	            	rol:rol,
	            	estado:estado
	            },
	            function(data){
	                if(data.res=="ok"){
	                	//location.reload();   
	                	mensaje(data.msj);                                      
	                }else{
	                 	mensaje(data.msj);
	                }
	            },10000);
        	}

        	$(document).ready(function() {
		    	$("[name='my-checkbox']").on('switchChange.bootstrapSwitch', function(event, state) {
				  	var id = $(this).attr('data-id');
				  	var rol = $(this).attr('data-rol');
				  	if( $(this).prop('checked')) {
				  		var estado=1;
				  	}else{
				  		var estado=0;
				  	}
				 	//alert(estado);
				  	cambiarestado(id,rol,estado);
				});

		    });

		</script>
	</body>
</html>