<?php $this->load->view("common/admin/head")?>
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/admin/dashboard.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
					<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('config_user_1')?></h2>
	    			</div>
	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<a href="<?php echo base_url(); ?>admin/configuracionRoles" class="btn btn-negro ml-1"><i class="fa fa-cogs"  aria-hidden="true"></i> &nbsp;<?=$this->lang->line('config_user_2')?></a>
	    				<a href="#" name="addUsuario" class="btn btn-green ml-1"><i class="fa fa-plus-square"  aria-hidden="true"></i> &nbsp;<?=$this->lang->line('config_user_3')?></a>
	    			</div>
				</div>

				<div class="row mt-20">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="table-responsive table-color">
							<table class="table table-hover table-bordered text-center">
								<thead>
									<tr>
										<th><?=$this->lang->line('correo')?></th>
										<!-- <th>Contraseña</th> -->
										<th><?=$this->lang->line('rol')?></th>
										<th><?=$this->lang->line('entidad')?></th>
										<th><?=$this->lang->line('acciones')?></th>
									</tr>
								</thead>
								<tbody>
									<?php if(count($usuarios) > 0 ){
								foreach ($usuarios as $u) { ?>
									<tr>
										<td><?php echo $u->email; ?></td>
										<!-- <td><?php echo $u->password; ?></td> -->
										<td><?php echo $u->rol->name; ?></td>
										<td><?php echo (isset($u->entity->name))?$u->entity->name:'' ?></td>
										<td>
											<a href="#" class="editar btn btn-eraser" data-id="<?php echo $u->_id;?>" data-correo="<?php echo $u->email; ?>" data-contrasena="<?php echo $u->password; ?>" data-id_rol="<?php echo $u->rol->id; ?>" data-id_entidad="<?php echo (isset($u->entity->id))?$u->entity->id:''; ?>" data-nombre="<?php echo $u->name ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											<?php if ($u->rol->id!='5e3307a1fd61ee8807387411') { ?>
												<a href="#" class="eliminar btn btn-danger" data-id="<?php echo $u->_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
											<?php } ?>											
										</td>
									</tr>
								<?php } }else{ ?>
									<tr>
										<td colspan="5"><p class="text-center"><?=$this->lang->line('bad_table_user')?></p></td>
									</tr>
								<?php } ?>
								</tbody>								
							</table>
						</div>
						<div class="col-md-12" style="text-align:center">
							<nav aria-label="Page navigation">
					            <ul class="pagination">
					                <?php echo $this->pagination->create_links(); ?>
					            </ul>
			                </nav>
			            </div>
					</div>
				</div>
				
			</div>
		</div>
		<div class="modal fade" id="usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
	        <div class="modal-dialog" role="document">
	            <div class="modal-content">
	              	<div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('editar_usuario')?></h4>
	             	</div>
	                <div class="modal-body">
	                	<form id="addUser">
	                		<input type="hidden" name="id_usuario">
	                    	<div class="form-group">
	                             <label><?=$this->lang->line('nombre')?></label>
	                             <input type="text" name="nombre" class="form-control" placeholder="<?=$this->lang->line('nombre')?>">
	                        </div>
	                        <div class="form-group">
	                             <label><?=$this->lang->line('placeholder_email')?></label>
	                             <input type="text" name="correo" class="form-control" placeholder="<?=$this->lang->line('placeholder_email')?>">
	                        </div>
	                        <div class="form-group">
	                             <label><?=$this->lang->line('placeholder_password')?></label>
	                             <input type="text" name="contrasena" class="form-control" placeholder="<?=$this->lang->line('placeholder_password')?>">
	                        </div>
	                        <div class="form-group">	
								<label><?=$this->lang->line('rol')?></label>
								<select class="form-control" name="rol">
									<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									<?php foreach ($rol as $r) { ?>
										<option value="<?php echo $r->_id; ?>"><?php echo $r->name ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">	
								<label><?=$this->lang->line('entidad')?></label>
								<select class="form-control" name="entidad">
									<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									<?php foreach ($entidad as $r) { ?>
										<option value="<?php echo $r->_id; ?>"><?php echo $r->name ?></option>
									<?php } ?>
								</select>
							</div>
	                	</form>	                    	
	                </div>
	                <div class="modal-footer">
	                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
	                    <button type="button" class="btn mas btn-green" id="guardar"><?=$this->lang->line('btn_guardar')?></button> 
	                </div>
	            </div>
	          </div>
	        </div>
        </div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript">
			$('[name=addUsuario]').on('click', function () {
				$('#addUser')[0].reset();
				$('[name=rol]').attr('disabled', false);
				$('[name=rol] option').eq(1).css('display','none');
	        	$('#usuario').modal('show');
			});  
			$('.editar').on('click', function () {
				$('[name=id_usuario]').val($(this).attr('data-id'));
				$('[name=nombre]').val($(this).attr('data-nombre'));
				$('[name=correo]').val($(this).attr('data-correo'));
				$('[name=contrasena]').val($(this).attr('data-contrasena'));
				$('[name=rol]').val($(this).attr('data-id_rol'));
				if ($(this).attr('data-id_rol')=='1') { //super admin
					$('[name=rol] option').eq(1).css('disabled','block');
					$('[name=rol]').attr('disabled', true);
				}else{
					$('[name=rol] option').eq(1).css('display','none');
					$('[name=rol]').attr('disabled', false);
				}
				$('[name=entidad').val($(this).attr('data-id_entidad'));
	        	$('#usuario').modal('show');				
			});
			$('.eliminar').on('click', function () {
				var id = $(this).attr('data-id')
				var btnnames = {
                    ok : "Aceptar",
                    cancel : "Cancelar"
                }
                confirmar("<?=$this->lang->line('confirm_user')?>",function(){
                   	ajax('usuarios/f',
	                {   
	                	id: id
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	location.reload();                                    
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
	            },false,btnnames);
			});
			$('#guardar').on('click', function () {
				var bandera=0;
				if ($('[name=nombre]').val()=='') {
					bandera=1;
					mensaje("<?=$this->lang->line('user_bad_1')?>");
				}
				if ($('[name=correo]').val()=='') {					
					bandera=1;
					mensaje("<?=$this->lang->line('user_bad_2')?>");
				}else{
					if(validarCorreo($('[name=correo]').val()) == false){
						bandera = 1;
						mensaje("<?=$this->lang->line('user_bad_3')?>");				
					}
				}
				if ($('[name=contrasena]').val()=='') {
					bandera=1;
					mensaje("<?=$this->lang->line('user_bad_4')?>");
				}
				if ($('[name=rol]').val()=='') {
					bandera=1;
					mensaje('Debe agregar un rol para el usuario');
				}
				if ($('[name=rol]').val()!='1') { //super admin
					if ($('[name=entidad]').val()=='') {
						bandera=1;
						mensaje("<?=$this->lang->line('user_bad_5')?>");
					}
				}
				if (bandera==0) {
					ajax('usuarios/saveUsuario',
	                {   
	                	id: $('[name=id_usuario]').val(),
	                	correo: $('[name=correo]').val(),
	                	contrasena: $('[name=contrasena]').val(),
	                	id_rol: $('[name=rol]').val(),
	                	name_rol: $('[name=rol]').children("option:selected").html(),
	                	id_entidad: $('[name=entidad]').val(),
	                	name_entidad: $('[name=entidad]').children("option:selected").html(),
	                	nombre: $('[name=nombre]').val()
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	location.reload();                                    
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
				}
			});
			$(document).ready(function () {
				var entidad = <?php echo json_encode($entidad); ?>;
				if (entidad.length==0) {
					mensaje('Debe agregar una entidad');
				}
				var roles = <?php echo json_encode($rol); ?>;
				if (roles.length==0) {
					mensaje("<?=$this->lang->line('user_bad_6')?>");
				}
			})
		</script>
	</body>
</html>