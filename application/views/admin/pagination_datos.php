<table class="table table-hover table-bordered datos-clinicos">
	<thead>
		<tr>
			<th><?=$this->lang->line('nombre')?></th>	
			<th><?=$this->lang->line('numero_documento')?></th>	
			<th><?=$this->lang->line('entidad')?></th>		
		</tr>
	</thead>
	<tbody>
		<?php foreach ($entidades as $key => $dc) { ?>
			<tr>
				<td><?php echo $dc->nombre." ".$dc->apellido ?></td>
				<td><?php echo $dc->numero_documento ?></td>
				<td>
					<?php 
					foreach ($dc->entidades as $key => $et) {
						echo $et->entidad."<br>";
					}
					?>
				</td>
			</tr>
		<?php } ?>
	</tbody>							    						
</table>
<div class="col-12" style="font-size: 13px;">
    <ul class="pagination" id="pagination-digg11">
        <?php echo $this->pagination->create_links(); ?>
    </ul>
</div>
