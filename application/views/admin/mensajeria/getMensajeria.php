<?php $this->load->view("common/admin/head")?>
<style type="text/css">
	span{
		font-weight: lighter;
	}
	.label-blue {
		color: #00afe5
	}
	.label_ {
		    display: inline-block;
	    padding: 6px 12px;
	    margin-bottom: 0;
	    font-size: 14px;
	    font-weight: 400;
	    line-height: 1.42857143;
	    text-align: center;
	    white-space: nowrap;
	    vertical-align: middle;
	    color: white;
	    border-radius: 5px;
	}
	.label_.ok{		
	    background: green;
	}
	.label_:not(.ok){		
	    background: red;
	}
</style>
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
					<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('titulo_ver_mensajeria')?> <?=$campana[0]->name;?> <a href="<?php echo base_url();?>admin/mensajeria" class="btn btn-border-dark"><i class="fa fa-arrow-left"></i></a></h2>
	    			</div>
				</div>

				<div class="row ">
					<div class="col-xs-12 col-xs-offset-0 col-md-8 col-md-offset-2 box-shadow">
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<label><?=$this->lang->line('titulo')?>:</label><br>
								<span><?=$campana[0]->name;?></span>
							</div>
							<div class="form-group">
								<label><?=$this->lang->line('periodicidad')?>:</label><br>
								<span><?=($campana[0]->type_campaign->name=='Mensual')?$this->lang->line('mensual'):$this->lang->line('fecha');?></span>
							</div>
							<div class="form-group">
								<label><i class="fa fa-calendar label-blue"></i> <?=$this->lang->line('dia_campana')?>:</label><br>
								<?php if($campana[0]->fecha == ""){ ?>
									<span><?=$campana[0]->dia;?></span>
								<?php }else{?>
								<span><?=$campana[0]->fecha;?></span>
								 <?php } ?>
							</div>
							<div class="form-group">
								<label><i class="fa fa-envelope-open label-blue"></i> <?=$this->lang->line('mensajeria_text_1')?>:</label><br>
								<?php if($campana[0]->message_banck->id == ''){ ?>
									<span><?php echo $campana[0]->text;?></span>
								<?php }else{ ?>
								<span><?=$campana[0]->message_banck->name;?></span>
								 <?php } ?>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<div class="form-group">
								<label><?=$this->lang->line('estado')?>:</label><br>
								<?=($campana[0]->state_message == 1 ? '<span class="label_ ok">'.$this->lang->line('activo').'</span>': '<span class="label_">'.$this->lang->line('inactivo').'</span>');?>								
							</div>
							<div class="form-group">
								<label><?=$this->lang->line('canal')?>:</label><br>
								<span><?php $type_ = ($campana[0]->channel->name=='SMS') ? 'envelope' : 'phone';
								echo '<i class="fa fa-'.$type_.' label-blue"></i> '.$campana[0]->channel->name;?></span>
							</div>
							<div class="form-group">
								<label><i class="fa fa-list-alt label-blue"></i> <?=$this->lang->line('mensajeria_text_2')?>:</label>
								<ul>
								<?php foreach ($campana[0]->data_clinical as $d) {  ?>
									<li><?=$d->name;?></li>
								<?php } ?>
								</ul>
							</div>
						</div>
				</div>
				</div>

			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript">
		    $(document).ready(function(){
		    });
			
		</script>
	</body>
</html>