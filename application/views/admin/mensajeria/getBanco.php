	<?php $this->load->view("common/admin/head")?>
</head>
<body>
	<?php $this->load->view("common/admin/nav")?>
	<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
		<div class="col-md-12 principal-padding">

			<div class="row">
				<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('banco')?> <?=$mensaje[0]->name;?> <a href="<?=base_url();?>admin/bancoMensajes" class="btn btn-border-dark"><i class="fa fa-arrow-left"></i></a></h2>
    			</div>
    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
					
					<a href="#mensaje_banco" class="btn btn-green pull-right" data-toggle="modal"><i class="fa fa-plus-square" aria-hidden="true"></i> &nbsp;<?=$this->lang->line('new_sms')?></a>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-md-12">
    				<div class="table-responsive table-color">
						<table class="table table-hover table-bordered text-center">
							<thead>
								<tr>
									<th class="text-center" style="width: 75%"><?=$this->lang->line('mensaje')?></th>
									<th class="text-center"><?=$this->lang->line('canal')?></th>
									<th><?=$this->lang->line('acciones')?></th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($mensaje[0]->message) > 0){ 
									foreach ($mensaje[0]->message as $m) { ?>
									<tr>
										<td><?=$m->text?></td>
										<td><?=($m->channel->name=='Llamada') ? $this->lang->line('llamada') : $this->lang->line('sms'); ?></td>
										<td>
											<a href="#" class="editar btn btn-green" data-id="<?=$mensaje[0]->_id;?>" data-mensaje="<?=$m->text; ?>" data-id_channel="<?=$m->channel->id?>" data-name_channel="<?=$m->channel->name?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
											<a href="#" class="eliminar btn btn-danger" data-id="<?=$mensaje[0]->_id;?>" data-mensaje="<?=$m->text; ?>" data-id_channel="<?=$m->channel->id?>" data-name_channel="<?=$m->channel->name?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
										</td>
									</tr>
									<?php } } else { ?>
									 <tr>
										<td colspan="4"><?=$this->lang->line('bad_table_banco')?></td>
									</tr>
									<?php } ?> 
							</tbody>									
						</table>
					</div>
    			</div>
    		</div>
		</div>
	</div>
	<div class="modal fade" id="mensaje_banco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('new_sms')?></h4>
             	</div>
                <div class="modal-body">
                	<input type="hidden" name="id_banco" id="id_banco" value="<?=$mensaje[0]->_id;?>">
                	<div class="form-group">
                		<label><?=$this->lang->line('canal')?></label>
                		<select class="form-control" name="canal">
                			<?php foreach ($canales as $k_c => $v_c) { ?>
                				<option value="<?=$v_c->_id?>"><?=($v_c->name=='Llamada') ? $this->lang->line('llamada') : $this->lang->line('sms'); ?></option>
                			<?php } ?>
                		</select>
                	</div>
                   	<div class="form-group" id="box-mensajes">
						<label><?=$this->lang->line('mensaje')?></label>
						<textarea rows="4" cols="50" class="form-control" name="msj_bancomsj" maxlength="490" id="msj_bancomsj"></textarea>
					</div>
					<div class="form-group" style="text-align: right;" id="box-mensajes-contador">
		                <span><?=$this->lang->line('res_sms')?> </span><span class="CaracteresRestantes">490</span> 
		            </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
                    <button type="button" class="btn mas btn-green" id="guardar_banco"><?=$this->lang->line('btn_guardar')?></button> 
                </div>
            </div>
          </div>
        </div>
    </div>

    <div class="modal fade" id="editar_mensaje_banco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('editar_mensaje')?></h4>
             	</div>
                <div class="modal-body">
                	<input type="hidden" name="id_banco_editar" id="id_banco_editar" value="<?=$mensaje[0]->_id;?>">
                	<div class="form-group">
                		<label><?=$this->lang->line('canal')?></label>
                		<select class="form-control" name="canal_editar">
                			<?php foreach ($canales as $k_c => $v_c) { ?>
                				<option value="<?=$v_c->_id?>"><?=($v_c->name=='Llamada') ? $this->lang->line('llamada') : $this->lang->line('sms'); ?></option>
                			<?php } ?>
                		</select>
                	</div>
                   	<div class="form-group" id="box-mensajes">
						<label><?=$this->lang->line('mensaje')?></label>
						<textarea rows="4" cols="50" class="form-control" name="editar_msj_bancomsj" maxlength="490" id="editar_msj_bancomsj"></textarea>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
                    <button type="button" class="btn mas btn-green" id="guardar_editar_banco"><?=$this->lang->line('btn_guardar')?></button> 
                </div>
            </div>
          </div>
        </div>
    </div>
	<!-- ==============================================
	SCRIPTS
	=============================================== -->	
	<?php $this->load->view("common/admin/extras")?>
	<?php $this->load->view("common/admin/js")?>
	<script  type="text/javascript" language="javascript">
		$(document).on('keyup', 'textarea[name="msj_bancomsj"]', function(e) {
		    // maxlength attribute does not in IE prior to IE10
		    var $this = $(this),
	        maxlength = $this.attr('maxlength'),
	        maxlengthint = parseInt(maxlength),
	        caracteresActuales = $this.val().length,
  			caracteresRestantes = maxlengthint - caracteresActuales,
	        espanCaractRestantes = $('.CaracteresRestantes');
		  
			espanCaractRestantes.html(caracteresRestantes);
			if($('html').hasClass("ie9")) {
			    if(caracteresRestantes == -1) {
			       caracteresRestantes = 0;
			    }
			}
		    if(!!maxlength){
		        var text = $this.val();
		        if (text.length >= maxlength) {
		            $this.val(text.substring(0,maxlength));            
		            e.preventDefault();
		        }
		    }
		});
		$('#guardar_banco').on('click',function(){
        	if($('#msj_bancomsj').val() == ""){
        		bandera=1
        		mensaje("<?=$this->lang->line('bad_mensaje')?>")
        	}else{
        		ajax('mensajeria/newMensajeBanco',
		        {   
		            id: $('#id_banco').val(),
		            mensaje: $('#msj_bancomsj').val(),
		            id_channel: $('[name=canal]').val(),
		            name_channel: $('[name=canal]').children("option:selected").html()
		       	},
		        function(data){
		            if(data.res=="ok"){
	            		location.reload()     
	                }else{
	                    mensaje(data.msj)
	                }
	            },10000);
        	}
        })
        $('#guardar_editar_banco').on('click',function(){
        	if($('#editar_msj_bancomsj').val() == ""){
        		bandera=1
        		mensaje("<?=$this->lang->line('bad_mensaje')?>")
        	}else{
				ajax('mensajeria/editMensajeBanco',
		        {   
		        	id: $('#id_banco_editar').val(),
		            mensaje: $('#editar_msj_bancomsj').val(),
		            id_channel: $('[name=canal_editar]').val(),
		            name_channel: $('[name=canal_editar]').children("option:selected").html(),
		            mensaje_old: mensaje_old,
		            id_channel_old: id_channel_old,
		            name_channel_old: name_channel_old
		       	},
		        function(data){
	            	if(data.res=="ok"){
	            		location.reload()     
	                }else{
	                    mensaje(data.msj)
	                }
	            },10000);
        	}
        })
        $(document.body).on('click','.eliminar',function(){
	        var registro1 = $(this).attr('data-id'),
	        mensaje = $(this).attr('data-mensaje'),
        	id_channel = $(this).attr('data-id_channel'),
        	name_channel = $(this).attr('data-name_channel');
           var btnnames = {
                ok : "<?=$this->lang->line('btn_aceptar')?>",
                cancel : "<?=$this->lang->line('btn_cancelar')?>"
            }
            confirmar("<?=$this->lang->line('confirm_banco_mensaje')?>",function(){
               	ajax('mensajeria/deleteMensajeBanco',
                {   
                	id: registro1,
                	mensaje: mensaje,
		            id_channel: id_channel,
		            name_channel: name_channel
                },
                function(data){
                    if(data.res=="ok"){
                    	location.reload();                                    
                    }else{
                     	mensaje(data.msj);
                    }
                },10000);
            },false,btnnames);
        });
        $(document.body).on('click','.editar',function(){
        	$('#id_banco_editar').val($(this).attr('data-id'));
        	$('#editar_msj_bancomsj').val($(this).attr('data-mensaje'));
        	$('[name=canal_editar]').val($(this).attr('data-id_channel'))
        	mensaje_old = $(this).attr('data-mensaje')
        	id_channel_old = $(this).attr('data-id_channel')
        	name_channel_old = $(this).attr('data-name_channel')
        	$('#editar_mensaje_banco').modal('show')
        });		
	</script>
</body>
</html>