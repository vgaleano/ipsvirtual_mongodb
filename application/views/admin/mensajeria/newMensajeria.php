<?php $this->load->view("common/admin/head")?>
<link rel="stylesheet" href="<?=base_url(); ?>css/plugins/bootstrap-tagsinput.css">
<link rel="stylesheet" href="<?=base_url(); ?>css/jquery.datetimepicker.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('titulo_nueva_campana')?> <a href="<?=base_url();?>/admin/mensajeria" class="btn btn-border-dark ml-1"><i class="fa fa-arrow-left"></i></a></h2>
	    			</div>

	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
						<form id="formEntidad" action="<?php print base_url();?>admin/newMensajeria" method="POST" onsubmit="return false">
							<div class="input-group">
				      			<div class="input-group-addon">
				      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
			      				</div>
								<select class="form-control" name="searchentidad">
							    	<?php $ent = $this->session->userdata('searchentidad'); if(!$ent) $ent=""; ?>
							        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
							        <?php foreach ($entidad as $r) { ?>
										<option value="<?php echo $r->_id; ?>" <?php echo ($r->_id==$ent)? 'selected':''; ?>><?php echo $r->name ?></option>
									<?php } ?>
							    </select>
						    </div>																		
						</form>
					<?php } ?>
					</div>
				</div>	

				<div class="row  mt-20" id="form_new">
	    			<div class="col-xs-12 col-md-12">
	    				<div class="col-xs-12 col-md-12 box-shadow">
	    					<div class="col-md-6">
								<div class="form-group">
									<label><?=$this->lang->line('name_titulo_campana')?></label>
									<input type="text" name="titulo" class="form-control" id="titulo">
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('dia_campana')?><small style="display: none;" id="small-fecha"><?=$this->lang->line('descripcion_dia_campana')?></small><small style="display: none;" id="small-num"><?=$this->lang->line('campana_text_1')?></small></label>
									<input type="text" name="dia" id="fecha-num" maxlength="2" class="form-control" onkeypress="return onlyNumber(event);">
									<input type="text" name="fecha" id="fecha-fecha" class="fecha form-control" style="display: none;">
								</div>
								<div class="form-group">	
									<label><?=$this->lang->line('estado')?></label>
									<select class="form-control" id="estado_cam">
									<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									  <option value="1"><?=$this->lang->line('activo')?></option>
									  <option value="0"><?=$this->lang->line('inactivo')?></option>
									</select>
								</div>								
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><?=$this->lang->line('periodicidad')?></label>
									<select class="form-control" id="tipo_campana">
									  <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									  <?php foreach ($tipoCampanas as $t) { ?>
									  		<option value="<?=$t ?>"><?=($t=='Mensual')?$this->lang->line('mensual'):$this->lang->line('fecha'); ?></option>
									   <?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('canal')?></label>
									<select class="form-control" id="canal">
									  <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									  <?php foreach ($canal as $c) { 
								  		if ($c->_id=='5e55d4ee377e601bf443d23e') {
									   		continue;
									   	} ?>
									  		<option value="<?=$c->_id ?>"><?=($c->name=='Llamada') ? $this->lang->line('llamada') : $this->lang->line('sms'); ?></option>
									   <?php } ?>
									</select>
								</div>
								
							</div>
							<div class="col-md-12">
								<div class="form-check">
									<input type="checkbox" class="form-check-input" id="op_banco_msj">
									<label class="form-check-label"><?=$this->lang->line('campana_text_2')?></label>
								</div>
								<div class="form-group" id="box-banco-msj" style="display: none;">	
									<label><?=$this->lang->line('campana_text_3')?></label>
									<select class="form-control" id="banco">
										<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
										<?php foreach ($banco as $b) { ?>
										<option value="<?=$b->id ?>"><?=$b->titulo ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('campana_text_4')?></label>
									<select class="form-control" id="datos_clinicos">
										<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									    <?php foreach ($datosClinicos as $d) { ?>
										<option value="<?=$d->_id ?>"><?=$d->name ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('campana_text_5')?></label><br>
									<select multiple data-role="tagsinput" id="tags" name="tags">
									</select>
								</div>
								<div class="form-group" id="box-mensajes">
									<label><?=$this->lang->line('mensaje')?></label>
									<textarea rows="4" cols="50" class="form-control" name="mensaje_campana_msj" maxlength="160" id="mensaje_campana_msj"></textarea>
								</div>
								<div class="col-md-6 pull-right" style="text-align: right;" id="box-mensajes-contador">
				                    <span><?=$this->lang->line('campana_text_6')?> </span><span class="CaracteresRestantes">0</span> 
				                 </div>
								<div class="col-md-6 col-md-offset-3">
									<div class="col-md-6">
										<button type="button" class="btn btn-block btn-border-dark btn-cancelar"><?=$this->lang->line('btn_cancelar')?></button>
									</div>
									<div class="col-md-6">
										<button type="button" class="btn btn-block btn-green" id="btn-guardar"><?=$this->lang->line('btn_guardar')?></button>
									</div>
								</div>		
							</div>
	    				</div>
	    					
					</div>	
				</div>
			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script type="text/javascript" src="<?=base_url(); ?>js/plugins/bootstrap-tagsinput.js"></script>
		<script type="text/javascript" src="<?=base_url();?>js/jquery.datetimepicker.full.min.js"></script>
		<script  type="text/javascript" language="javascript">	
			$(document.body).on('click','.btn-cancelar',function(){
        		window.location.href = base_url+'admin/mensajeria'	
        	});

			$('body').on('focus',".fecha", function(){
				var dateToday = new Date(); 
			    $(this).datetimepicker({
        			format:'d-m-Y',
        			timepicker:false,
        			autoclose: true,
        			minDate: dateToday
        		});
			});

			function remove(t){
				//remove tags and come back the option to the select
				var id = t.getAttribute('data-id'),
				text = t.getAttribute('data-text'),
				contenedor = document.getElementById('datos_clinicos'),
				option= document.createElement("option");
				option.text = text
				option.value = id
				contenedor.add(option)
				t.parentNode.remove()
			}  

			$('#tipo_campana').on('change',function(){
				if($(this).val() == 1){
					$('#fecha-num').css('display','block')
					$('#small-num').css('display','block')
					$('#fecha-fecha').css('display','none')
					$('#small-fecha').css('display','none')
				}else{
					$('#fecha-num').css('display','none')
					$('#small-num').css('display','none')
					$('#fecha-fecha').css('display','block')
					$('#small-fecha').css('display','block')
				}
			});
			
			$('#op_banco_msj').on('change',function(){
				if(document.getElementById("op_banco_msj").checked == true){
					ajax('mensajeria/getBancoMensajes',
			        {   
			            id_entidad: '<?=$id_entidad?>'
			       	},
			        function(data){
			            if(data.res=="ok"){
			            	$('#banco').empty();
			            	var content = '';
			            	content+= '<option value="" style="display: none;"> '+"<?=$this->lang->line('select_one')?>"+' </option>'
			            	for (var i = 0; i < data.data.length; i++) {
			            		content+= '<option value="'+data.data[i]._id.$oid+'">'+data.data[i].name+'</option>';
			            	}
			            	$('#banco').append(content);
			            	$('#box-banco-msj').css('display','block');
							$('#box-mensajes').css('display','none');
							$('#box-mensajes-contador').css('display','none');
		                }else{
		                    mensaje(data.msj)
		                }
		            },10000);						
				}else{
					$('#banco').empty();
					$('#box-banco-msj').css('display','none');
					$('#box-mensajes').css('display','block');
					$('#box-mensajes-contador').css('display','block');
				}
			});

			$('#datos_clinicos').on('change',function(){
				//crear tagsinput
				var selection = $("option:selected",this).text(),
				val = $(this).val(),
				contenedor = document.getElementById('tags'),
				option= document.createElement("option");
				option.text = selection
				option.value = val
				contenedor.add(option)
				$( ".bootstrap-tagsinput > input").css('display','none')
				$( ".bootstrap-tagsinput").append('<span class="tag label label-info">'+selection+'<span class="op_condicion" data-role="remove" onclick="remove(this)" data-id="'+val+'" data-text="'+selection+'"></span></span>');
				$('option[value="'+$(this).val()+'"]',this).remove()
			});

			$(document).on('keyup', 'textarea[name="mensaje_campana_msj"]', function(e) {
			    // maxlength attribute does not in IE prior to IE10
			    var $this = $(this),
		        maxlength = $this.attr('maxlength'),
		        maxlengthint = parseInt(maxlength),
		        caracteresActuales = $this.val().length,
	  			caracteresRestantes = caracteresActuales,
		        espanCaractRestantes = $('.CaracteresRestantes');
			  
				espanCaractRestantes.html(caracteresRestantes);
				if($('html').hasClass("ie9")) {
				    if(caracteresRestantes == -1) {
				       caracteresRestantes = 0;
				    }
				}
			    if(!!maxlength){
			        var text = $this.val();
			        if (text.length >= maxlength) {
			            $this.val(text.substring(0,maxlength));            
			            e.preventDefault();
			        }
			    }
			});
			$('#btn-guardar').on('click',function(){
				var bandera =0,
				condiciones = [],
				id_entidad='<?=$id_entidad; ?>';
				if($('#titulo').val() == ""){
					bandera =1
					mensaje("<?=$this->lang->line('campana_bad_1')?>")
				}
				if($('#tipo_campana').val() == ""){
					bandera =1
					mensaje("<?=$this->lang->line('campana_bad_2')?>")
				}else{
					if($('#tipo_campana').val() == 1){
						if($('#fecha-num').val() != ""){
							if($('#fecha-num').val() > 31){
								bandera =1
							 	mensaje("<?=$this->lang->line('campana_bad_3')?>")
							}
						}else{
							bandera =1
							mensaje("<?=$this->lang->line('campana_bad_4')?>")
						}
					}else{
						if($('#fecha-fecha').val() == ""){
							bandera =1
							mensaje("<?=$this->lang->line('campana_bad_4')?>")
						}
					}
				}
				
				if($('#estado_cam').val() == ""){
					bandera =1
					mensaje("<?=$this->lang->line('campana_bad_5')?>")
				}
				if($('#canal').val() == ""){
					bandera =1
					mensaje("<?=$this->lang->line('campana_bad_6')?>")
				}
				<?php if ($this->session->userdata('id_rol') == "1") { ?>
					if ($('[name=id_entidad]').val()=='') {
						bandera=1;
						mensaje("<?=$this->lang->line('campana_bad_7')?>");
					}else{
						id_entidad=$('[name=id_entidad]').val();
					}
				<?php } ?>
				if($('.op_condicion').length == 0){
					bandera =1
					mensaje("<?=$this->lang->line('campana_bad_8')?>")
				}else{
					$('.op_condicion').each(function(){ 
						condiciones.push({id: $(this).attr('data-id'), name: $(this).attr('data-text')});	
					})
				}
				if($("#op_banco_msj").prop('checked') == true){
					if($('#banco').val() == ""){
						bandera =1
						mensaje("<?=$this->lang->line('campana_bad_9')?>")
					}
				}else{
					if($('#mensaje_campana_msj').val() == ""){
						bandera =1
						mensaje("<?=$this->lang->line('campana_bad_10')?>")
					}
				}
				if(bandera == 0){
					ajax('mensajeria/newCampanaMensajeria',
			        {   
			            titulo: $('#titulo').val(),
			            tipo_campana: $('#tipo_campana').val(),
			            fechanum: $('#fecha-num').val(),
			            fechafecha: $('#fecha-fecha').val(),
			            id_canal: $('#canal').val(),
			            name_canal: $('#canal').children("option:selected").html(),
			            estado:$('#estado_cam').val(),
			            id_banco: $('#banco').val(),
			            name_banco: ($('#banco').val()=='')?'':$('#banco').children("option:selected").html(),
			            condiciones: condiciones,			            
			            mensaje:$('#mensaje_campana_msj').val(),			            
			            id_entidad: id_entidad
			       	},
			        function(data){
			            if(data.res=="ok"){
			            	mensaje(data.msj)
			            	setTimeout(function(){ window.location.href = base_url+'admin/mensajeria'  }, 2000);      
		                }else{
		                    mensaje(data.msj)
		                }
		            },10000);
				}
			});

			$('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

		    $(document).ready(function(){
		    	var id_entidad = '<?php echo $id_entidad; ?>';
				if (id_entidad=='') {
					$('#form_new').find('input, textarea, button, select').attr('disabled','disabled');
					mensaje("<?=$this->lang->line('campana_bad_11')?>");
				}
		    });
			
		</script>
	</body>
</html>