<?php $this->load->view("common/admin/head")?>
<style type="text/css">
	.modal-footer .btn+.btn {
    margin-bottom: 20px;
}
</style>
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">

				<div class="row">
	    			<div class="col-xs-12 col-md-6">
	    				<h2><?=$this->lang->line('titulo_campana')?></h2>
	    			</div>

	    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
	    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") {  ?>
							<form id="formEntidad" action="<?php print base_url();?>admin/mensajeria" method="POST" onsubmit="return false">
								<div class="input-group">
					      			<div class="input-group-addon">
					      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
				      				</div>
									<select class="form-control" name="searchentidad">
								    	<?php $ent = $this->session->userdata('searchentidad2'); if(!$ent) $ent=""; ?>
								        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
								        <?php foreach ($entidad as $r) { ?>
											<option value="<?=$r->_id; ?>" <?=($r->_id==$ent)? 'selected':''; ?>><?=$r->name ?></option>
										<?php } ?>
								    </select>
							    </div>																		
							</form>
						<?php } ?>
						<a href="<?=base_url()?>admin/newMensajeria" class="btn btn-green ml-1"><i class="fa fa-plus-square" aria-hidden="true"></i> &nbsp;<?=$this->lang->line('nueva_campana')?></a>
					</div>
				</div>	

				<div class="row mt-20">
					<div class="col-xs-12 col-md-12">
						<div class="table-responsive table-color">
							<table class="table table-hover table-bordered text-center">
								<thead>
									<tr>
										<th><?=$this->lang->line('nombre_campana')?></th>
										<th><?=$this->lang->line('estado')?></th>
										<th><?=$this->lang->line('canal')?></th>
										<th><?=$this->lang->line('periodicidad')?></th>
										<th><?=$this->lang->line('fecha_creacion')?></th>
										<th><?=$this->lang->line('acciones')?></th>
									</tr>
								</thead>
								<tbody>
								<?php if(count($campanas) > 0){
								foreach ($campanas as $c) {
								$timestamp = (String)$c->registration_date;
                          		$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000); ?>
								<tr>
									<td><?=$c->name; ?></td>
									<td><?=($c->state_message == '1' ? $this->lang->line('activo') : $this->lang->line('desactivado')); ?></td>
									<td>
										<?=($c->channel->name=='Llamada') ? $this->lang->line('llamada') : $this->lang->line('sms'); ?>
									</td>
									<td><?=($c->type_campaign->name=='Mensual')?$this->lang->line('mensual'):$this->lang->line('fecha'); ?></td>
									<td><?=$dateInLocal; ?></td>
									<td>
										<a class="btn btn-blue" href="<?=base_url()?>admin/getMensajeria/<?=$c->_id; ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<!-- <a class="btn btn-eraser" href="<?=base_url()?>admin/editMensajeria"><i class="fa fa-pencil" aria-hidden="true"></i></a> -->
										<a href="#" class="eliminar btn btn-danger" data-id="<?=$c->_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</td>
								</tr>
										<?php } ?>
								<?php }else{ ?>
								<tr>
									<td colspan="6"><p class="text-center"><?=$this->lang->line('bad_mensajeria_tabla')?></p></td>
								</tr>
								<?php } ?>
								</tbody>
								
							</table>
						</div>
					</div>
				</div>			
			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript">	

			$(document.body).on('click','.eliminar',function(){
		        var registro1 = $(this).attr('data-id');
	           var btnnames = {
                    ok : "Aceptar",
                    cancel : "Cancelar"
                }
                confirmar("<?=$this->lang->line('confirm_mensajeria')?>",function(){
                   	ajax('mensajeria/deleteMensajeria',
	                {   
	                	id: registro1
	                },
	                function(data){
	                    if(data.res=="ok"){
	                    	location.reload();                                    
	                    }else{
	                     	mensaje(data.msj);
	                    }
	                },10000);
	            },false,btnnames);
	        });

		    $(document).ready(function(){  
		    });

		    $('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });
			
		</script>
	</body>
</html>