<?php $this->load->view("common/admin/head")?>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/plugins/bootstrap-tagsinput.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.datetimepicker.css">
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
			<div class="col-md-12 principal-padding">
				<div class="col-md-12 container-fluid" id="content-principal">
					<div class="col-xs-12 col-sm-12 col-md-12 box" id="newMsj">
						<div class="col-xs-12 col-sm-12 col-md-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1">
							<div class="col-md-12 box-title">
								<h4><?=$this->lang->line('titulo_editar_campana')?></h4>
							</div>
							<div class="col-md-12">
								<div class="form-group">
									<label><?=$this->lang->line('name_titulo_campana')?></label>
									<input type="text" name="titulo" class="form-control" id="titulo" value="<?php echo  $campana->titulo;?>">
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('name_titulo_campana')?></label>
									<select class="form-control" id="tipo_campana">
									  <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									  <?php foreach ($tipoCampanas as $t) { ?>
									  		<option value="<?php echo $t->id ?>" <?php echo ($campana->id_tipo_campana ==  $t->id ? 'selected':'');?> ><?=($t->tipo=='Mensual')?$this->lang->line('mensual'):$this->lang->line('fecha'); ?></option>
									   <?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('dia_campana')?></label>
									<input type="text" name="fecha" id="fecha-fecha" class="form-control" value="<?php echo ($campana->fecha == "" ? $campana->dia:$campana->fecha)?>" disabled>
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('canal')?></label>
									<select class="form-control" id="canal">
									  <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									  <?php foreach ($canal as $c) { 
									  	if ($c->_id=='5e55d4ee377e601bf443d23e') {
							   				continue;
							   			} ?>
									  		<option value="<?php echo $c->id ?>" <?php echo ($campana->id_canal ==  $c->id ? 'selected':'');?> ><<?=($c->name=='Llamada') ? $this->lang->line('llamada') : $this->lang->line('sms'); ?></option>
									   <?php } ?>
									</select>
								</div>
								<div class="form-group">	
									<label><?=$this->lang->line('estado')?></label>
									<select class="form-control" id="estado_cam">
									<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									  <option value="1" <?php echo ($campana->estado ==  1 ? 'selected':'');?>><?=$this->lang->line('activo')?></option>
									  <option value="0" <?php echo ($campana->estado ==  0 ? 'selected':'');?>><?=$this->lang->line('inactivo')?></option>
									</select>
								</div>
								<div class="form-group">
									<div class="form-check">
									    <input type="checkbox" class="form-check-input" id="op_banco_msj">
									    <label class="form-check-label"><?=$this->lang->line('campana_text_2')?></label>
									</div>
								</div>
								<div class="form-group" id="box-banco-msj" style="display: none;">	
									<label><?=$this->lang->line('campana_text_3')?></label>
									<select class="form-control" id="banco">
									<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									  <?php foreach ($banco as $b) { ?>
									  		<option value="<?php echo $b->id ?>"><?php echo $b->titulo ?></option>
									   <?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('campana_text_4')?></label>
									<select class="form-control" id="datos_clinicos">
									<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
									   <?php foreach ($datosClinicos as $d) { ?>
									  		<option value="<?php echo $d->id ?>"><?php echo $d->dato_clinico ?></option>
									   <?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label><?=$this->lang->line('campana_text_5')?></label><br>
									<select multiple data-role="tagsinput" id="tags" name="tags">
									</select>
								</div>
								<div class="form-group" id="box-mensajes">
									<label><?=$this->lang->line('mensaje')?></label>
									<textarea rows="4" cols="50" class="form-control" name="mensaje_campana_msj" maxlength="490" id="mensaje_campana_msj"></textarea>
								</div>
								<div class="col-md-6 pull-right" style="text-align: right;" id="box-mensajes-contador">
		                            <span><?=$this->lang->line('res_sms')?> </span><span class="CaracteresRestantes">490</span> 
		                        </div>
								<div class="col-md-6 col-md-offset-3">
									<div class="col-md-6">
										<button type="button" class="btn btn-block btn-border-dark btn-cancelar"><?=$this->lang->line('btn_cancelar')?></button>
									</div>
									<div class="col-md-6">
										<button type="button" class="btn btn-block btn-green" id="btn-guardar"><?=$this->lang->line('btn_guardar')?></button>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
		<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script type="text/javascript" src="<?php echo base_url(); ?>js/plugins/bootstrap-tagsinput.js"></script>
		<script type="text/javascript" src="<?php echo base_url();?>js/jquery.datetimepicker.full.min.js"></script>
		<script  type="text/javascript" language="javascript">	
			$(document.body).on('click','.btn-cancelar',function(){
        		window.location.href = base_url+'admin/mensajeria'	
        	});
			$('body').on('focus',".fecha", function(){
				var dateToday = new Date(); 
				    $(this).datetimepicker({
		        			format:'d-m-Y',
		        			timepicker:false,
		        			autoclose: true,
		        			minDate: dateToday
		        		});
				});
			function remove(t){
				//remove tags and come back the option to the select
				let id = t.getAttribute('data-id')
				let text = t.getAttribute('data-text')
				let contenedor = document.getElementById('datos_clinicos')
				let option= document.createElement("option")
				option.text = text
				option.value = id
				contenedor.add(option)
				t.parentNode.remove()
			}  
			$('#tipo_campana').on('change',function(){
				let val = $(this).val()
				if(val == 1){
					$('#fecha-num').css('display','block')
					$('#small-num').css('display','block')
					$('#fecha-fecha').css('display','none')
					$('#small-fecha').css('display','none')
				}else{
					$('#fecha-num').css('display','none')
					$('#small-num').css('display','none')
					$('#fecha-fecha').css('display','block')
					$('#small-fecha').css('display','block')
				}
			});
			
			$('#op_banco_msj').on('change',function(){
				if(document.getElementById("op_banco_msj").checked == true){
					$('#box-banco-msj').css('display','block');
					$('#box-mensajes').css('display','none');
					$('#box-mensajes-contador').css('display','none');
				}else{
					$('#box-banco-msj').css('display','none');
					$('#box-mensajes').css('display','block');
					$('#box-mensajes-contador').css('display','block');
				}
			});
			$('#datos_clinicos').on('change',function(){
				//crear tagsinput
				let selection = $("option:selected",this).text()
				let val = $(this).val()
				//<option value="Amsterdam">Amsterdam</option>
				let contenedor = document.getElementById('tags')
				let option= document.createElement("option")
				option.text = selection
				option.value = val
				contenedor.add(option)
				//<span class="tag label label-info">Washington<span data-role="remove"></span></span>
				$( ".bootstrap-tagsinput > input").css('display','none')
				$( ".bootstrap-tagsinput").append('<span class="tag label label-info">'+selection+'<span class="op_condicion" data-role="remove" onclick="remove(this)" data-id="'+val+'" data-text="'+selection+'"></span></span>');
				$('option[value="'+$(this).val()+'"]',this).remove()
			});
			$(document).on('keyup', 'textarea[name="mensaje_campana_msj"]', function(e) {
			    // maxlength attribute does not in IE prior to IE10
			    var $this = $(this),
			        maxlength = $this.attr('maxlength'),
			        maxlengthint = parseInt(maxlength),
			        caracteresActuales = $this.val().length;
			  			caracteresRestantes = maxlengthint - caracteresActuales,
			        espanCaractRestantes = $('.CaracteresRestantes');
			  
				espanCaractRestantes.html(caracteresRestantes);
				if($('html').hasClass("ie9")) {
				    if(caracteresRestantes == -1) {
				       caracteresRestantes = 0;
				    }
				}
			    if(!!maxlength){
			        var text = $this.val();
			        if (text.length >= maxlength) {
			            $this.val(text.substring(0,maxlength));            
			            e.preventDefault();
			        }
			    }
			});
			$('#btn-guardar').on('click',function(){
				let bandera =0
				let condiciones = []
				if($('#titulo').val() == ""){
					bandera =1
					mensaje('Debe agregar un titulo.')
				}
				if($('#tipo_campana').val() == ""){
					bandera =1
					mensaje('Debe agregar tipo de campaña.')
				}else{
					if($('#tipo_campana').val() == 1){
						if($('#fecha-num').val() != ""){
							if($('#fecha-num').val() > 31){
								bandera =1
							 	mensaje("<?=$this->lang->line('campana_bad_3')?>")
							}
						}else{
							bandera =1
							mensaje("<?=$this->lang->line('campana_bad_4')?>")
						}
					}else{
						if($('#fecha-fecha').val() == ""){
							bandera =1
							mensaje("<?=$this->lang->line('campana_bad_5')?>")
						}
					}
				}
				
				if($('#estado_cam').val() == ""){
					bandera =1
					mensaje("<?=$this->lang->line('campana_bad_6')?>")
				}
				if($('#canal').val() == ""){
					bandera =1
					mensaje("<?=$this->lang->line('editar_campana_bad_1')?>")
				}
				if($('.op_condicion').length == 0){
					bandera =1
					mensaje("<?=$this->lang->line('campana_bad_8')?>")
				}else{
					$('.op_condicion').each(function(){ 
						condiciones.push($(this).attr('data-id'))	
					})
				}
				if($("#op_banco_msj").prop('checked') == true){
					if($('#banco').val() == ""){
						bandera =1
						mensaje("<?=$this->lang->line('campana_bad_9')?>")
					}
				}else{
					if($('#mensaje_campana_msj').val() == ""){
						bandera =1
						mensaje("<?=$this->lang->line('campana_bad_10')?>")
					}
				}
				if(bandera == 0){
					ajax('mensajeria/newCampanaMensajeria',
				        {   
				            titulo: $('#titulo').val(),
				            tipo_campana: $('#tipo_campana').val(),
				            canal: $('#canal').val(),
				            condiciones: condiciones,
				            banco: $('#banco').val(),
				            mensaje:$('#mensaje_campana_msj').val(),
				            estado:$('#estado_cam').val(),
				            fechanum: $('#fecha-num').val(),
				            fechafecha: $('#fecha-fecha').val()
				       	},
				        function(data){
				            if(data.res=="ok"){
				            	mensaje(data.msj)
				            	setTimeout(function(){ window.location.href = base_url+'admin/mensajeria'  }, 2000);      
				                }else{
				                    mensaje(data.msj)
				                }
				            },10000);
				}
			});
		    $(document).ready(function(){
		    });
			
		</script>
	</body>
</html>