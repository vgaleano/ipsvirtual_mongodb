	<?php $this->load->view("common/admin/head")?>
</head>
<body>
	<?php $this->load->view("common/admin/nav")?>
	<div class="col-md-12 col-lg-12  no-padding" id="wrapper-principal">
		<div class="col-md-12 principal-padding">

			<div class="row">
				<div class="col-xs-12 col-md-6">
    				<h2><?=$this->lang->line('titulo_banco')?></h2>
    			</div>
    			<div class="col-xs-12 col-md-6 mt-20 box-btn">
    				<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
					<form id="formEntidad" action="<?php print base_url();?>admin/bancoMensajes" method="POST" onsubmit="return false">
						<div class="input-group">
			      			<div class="input-group-addon">
			      				<label class="mb-0"><?=$this->lang->line('entidad')?>:</label>
		      				</div>
							<select class="form-control" name="searchentidad">
						    	<?php $ent = $this->session->userdata('searchentidad4');  if(!$ent) $ent=""; ?>
						        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
						        <?php foreach ($entidad as $r) { ?>
									<option value="<?=$r->_id; ?>" <?=($r->_id==$ent)? 'selected':''; ?>><?=$r->name ?></option>
								<?php } ?>
						    </select>
					    </div>									
					</form>
					<?php } ?>
    				<a href="#banco" class="btn btn-green ml-1" data-toggle="modal"><i class="fa fa-plus-square" aria-hidden="true"></i> &nbsp;<?=$this->lang->line('nuevo_banco')?></a>
    			</div>
    		</div>

    		<div class="row mt-20">
    			<div class="col-xs-12 col-md-12">
    				<div class="table-responsive table-color">
						<table class="table table-hover table-bordered text-center">
							<thead>
								<tr>
									<th><?=$this->lang->line('banco')?></th>
									<th><?=$this->lang->line('estado')?></th>
									<th><?=$this->lang->line('fecha_creacion')?></th>
									<th><?=$this->lang->line('acciones')?></th>
								</tr>
							</thead>
							<tbody>
								<?php  date_default_timezone_set('America/Bogota');
								if(count($banco) > 0  && is_array($banco)){
								foreach ($banco as $b) {
								$timestamp = (String)$b->registration_date;
                          		$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000); ?>
								<tr>
									<td><?=$b->name; ?></td>
									<td><?=($b->state_message == '1' ? $this->lang->line('activo'): $this->lang->line('inactivo')); ?></td>
									<td><?=$dateInLocal; ?></td>
									<td>
										<a class="btn btn-blue" href="<?=base_url()?>admin/getbancoMensajes/<?= $b->_id;?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="#" class="editar btn btn-green" data-id="<?=$b->_id;?>" data-titulo="<?=$b->name; ?>" data-estado="<?=$b->state_message; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
										<a href="#" class="eliminar btn btn-danger" data-id="<?=$b->_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php }}else{ ?>
								<tr>
									<td colspan="4"><p class="text-center"><?=$this->lang->line('bad_banco_tabla')?></p></td>
								</tr>
								<?php } ?>
							</tbody>									
						</table>
					</div>
    			</div>
    		</div>
		</div>
	</div>
	<div class="modal fade" id="banco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('nuevo_banco')?></h4>
             	</div>
                <div class="modal-body">
                    <div class="form-group">
                         <label><?=$this->lang->line('titulo')?></label>
                         <input type="text" name="titulo" id="titulo" class="form-control">
                    </div>
                    <div class="form-group">	
						<label><?=$this->lang->line('estado')?></label>
						<select class="form-control" id="estado">
							<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
							<option value="1"><?=$this->lang->line('activo')?></option>
						    <option value="0"><?=$this->lang->line('inactivo')?></option>
						</select>
					</div>
					<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
					<div class="form-group">
						<label><?=$this->lang->line('entidad')?></label>
					    <select class="form-control" name="id_entidad">								    	
					        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
					        <?php foreach ($entidad as $r) { ?>
								<option value="<?=$r->_id; ?>"><?=$r->name ?></option>
							<?php } ?>
					    </select>
					</div>
					<?php } ?>
            	</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
                    <button type="button" class="btn mas btn-green" id="guardar_banco"><?=$this->lang->line('btn_guardar')?></button> 
                </div>
            </div>
          </div>
        </div>
    </div>
    <div class="modal fade" id="editar_banco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
              	<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-archive fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('editar_banco')?></h4>
             	</div>
                <div class="modal-body">
                    <form action="" id="form" method="POST" onsubmit="return false">
                    	<input type="hidden" name="id_banco" id="id_banco">
                        <div class="form-group">
                             <label><?=$this->lang->line('titulo')?></label>
                             <input type="text" name="titulo" id="titulo_editar" class="form-control">
                        </div>
                        <div class="form-group">	
							<label><?=$this->lang->line('estado')?></label>
							<select class="form-control" id="estado_editar">
								<option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
								<option value="1"><?=$this->lang->line('activo')?></option>
							    <option value="0"><?=$this->lang->line('inactivo')?></option>
							</select>
						</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-border-dark" data-dismiss="modal"><?=$this->lang->line('btn_cancelar')?></button>
                    <button type="button" class="btn mas btn-green" id="guardar_editar_banco"><?=$this->lang->line('btn_guardar')?></button> 
                </div>
                    </form>
            </div>
          </div>
        </div>
    </div>
	<!-- ==============================================
	SCRIPTS
	=============================================== -->	
	<?php $this->load->view("common/admin/extras")?>
	<?php $this->load->view("common/admin/js")?>
	<script  type="text/javascript" language="javascript">	

		$(document.body).on('click','.eliminar',function(){
	        var registro1 = $(this).attr('data-id');
           	var btnnames = {
                ok : "<?=$this->lang->line('btn_aceptar')?>",
                cancel : "<?=$this->lang->line('btn_cancelar')?>"
            }
            confirmar("<?=$this->lang->line('confirm_banco')?>",function(){
               	ajax('mensajeria/deleteBanco',
                {   
                	id: registro1
                },
                function(data){
                    if(data.res=="ok"){
                    	location.reload();                                    
                    }else{
                     	mensaje(data.msj);
                    }
                },10000);
            },false,btnnames);
        });

        $('#guardar_banco').on('click',function(){
        	var bandera=0,
        	id_entidad='<?=$id_entidad; ?>';
        	if($('#titulo').val() == ""){
        		bandera=1
        		mensaje("<?=$this->lang->line('banco_bad_1')?>")
        	}
        	if($('#estado').val() == ""){
        		bandera=1
        		mensaje("<?=$this->lang->line('banco_bad_2')?>")
        	}
        	<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
				if ($('[name=id_entidad]').val()=='') {
					bandera=1;
					mensaje("<?=$this->lang->line('banco_bad_3')?>");
				}else{
					id_entidad=$('[name=id_entidad]').val();
				}
			<?php } ?>
        	if(bandera==0){
        		ajax('mensajeria/newBanco',
		        {   
		            titulo: $('#titulo').val(),
		            estado:$('#estado').val(),
		            id_entidad: id_entidad
		       	},
		        function(data){
		            if(data.res=="ok"){
		            	mensaje(data.msj)
	            		location.reload()     
	                }else{
	                    mensaje(data.msj)
	                }
	            },10000);
        	}
        });

        $(document.body).on('click','.editar',function(){
        	document.getElementById('id_banco').value=$(this).attr('data-id')
        	document.getElementById('titulo_editar').value= $(this).attr('data-titulo') 
        	var estado = $(this).attr('data-estado') 
        	$('#estado_editar option[value="'+estado+'"]').prop('selected', true)
        	$('#editar_banco').modal('show')
        });

        $('#guardar_editar_banco').on('click',function(){
        	var bandera=0
        	if($('#titulo_editar').val() == ""){
        		bandera=1
        		mensaje("<?=$this->lang->line('banco_bad_1')?>")
        	}
        	if($('#estado_editar').val() == ""){
        		bandera=1
        		mensaje("<?=$this->lang->line('banco_bad_2')?>")
        	}
        	if(bandera==0){
        		$('#editar_banco').modal('hide')
        		ajax('mensajeria/editBanco',
		        {   
		        	id: $('#id_banco').val(),
		            titulo: $('#titulo_editar').val(),
		            estado: $('#estado_editar').val()
		       	},
		        function(data){
		            if(data.res=="ok"){
		            	location.reload()     
	                }else{
	                    mensaje(data.msj)
	                }
	            },10000);
        	}
        })
		
		 $('[name=searchentidad]').on('change', function () {
	    	$('[name=searchentidad]').val($(this).val());
	    	document.getElementById('formEntidad').submit();
	    });

	</script>
</body>
</html>