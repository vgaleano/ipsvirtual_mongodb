<?php $this->load->view("common/admin/head")?>
		<link rel="stylesheet" href="<?=base_url(); ?>css/admin/dashboard.css">
		<style type="text/css">
			.pt-6 {
				padding-top: 6px!important;
			}
		</style>
	</head>
	<body>
		<?php $this->load->view("common/admin/nav")?>
		<div class="clearfix"></div>
		<div class="col-xs-12 col-md-12 col-lg-12" id="wrapper-principal">
			<div class="hidden-xs col-md-12 box-contador">
				<div class="col-sm-3 col-md-3 line-default d-flex flex-column">
					<span class="numero"><?=$total;?></span>
					<small><?=$this->lang->line('total_pacientes')?></small>
				</div>
				<div class="col-sm-9 col-md-9">
					<div class="col-sm-3 col-md-3 line-redred d-flex flex-column">
						<span class="numero"><?=$RojoIntenso;?></span>
						<small><?=$this->lang->line('name_estado_1')?></small>
					</div>
					<div class="col-xs-3 col-sm-3 col-md-3 line-red d-flex flex-column">
						<span class="numero"><?=$Rojo;?></span>
						<small><?=$this->lang->line('name_estado_2')?></small>
					</div>
	    			<div class="col-xs-3 col-sm-3 col-md-3 line-yellow d-flex flex-column">
	    				<span class="numero"><?=$Amarillo;?></span>
	    				<small><?=$this->lang->line('name_estado_3')?></small>
	    			</div>
	    			<div class="col-xs-3 col-sm-3 col-md-3 line-green d-flex flex-column">
	    				<span class="numero"><?=$Verde;?></span>
	    				<small><?=$this->lang->line('name_estado_4')?></small>
	    			</div>
				</div>
			</div>
			<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
			<div class="col-xs-12 visible-sm visible-xs principal-padding" style="margin-top: 15px;">
				<div class="col-xs-12 box-mensajes ">
					<form id="formEntidad" action="<?php print base_url();?>admin/dashboard" method="POST" onsubmit="return false">
						<div  class="col-xs-12 col-sm-6 xs-px-0">
							<label><?=$this->lang->line('paciente_duplicado')?></label>
					    	<p><?=(count($duplicados)>0)?count($duplicados).' <button type="button" class="btn mas btn-green ver_duplicados" data-toggle="modal" data-target="#modalDuplicados">Ver</button>':'0'; ?></p>
						</div>
						<div class="col-xs-12 col-sm-6 xs-px-0">
							<label><?=$this->lang->line('entidad')?></label>
						    <select class="form-control" name="searchentidad">
						    	<?php $ent = $this->session->userdata('searchentidad'); if(!$ent) $ent=""; ?>
						        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
						        <?php foreach ($entidad as $r) { ?>
									<option value="<?=$r->_id; ?>" <?=($r->_id==$ent)? 'selected':''; ?>><?=$r->name ?></option>
								<?php } ?>
						    </select>
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix visible-xs visible-sm"></div>
			<?php } ?>
			<div class="col-xs-12 col-md-10 principal-padding" id="caja-principal">
				<div class="col-xs-12 col-md-12 box-pacientes">
					<div class="container-fluid col-xs-12 col-md-12 filtro xs-px-0">
						<div class="hidden-xs d-flex justify-content-between">
							<form class="d-flex justify-content-center" id="formbuscar" action="<?php print base_url();?>admin/dashboard" method="POST">
								<input type="hidden" id="tbusqueda" name="tbusqueda" value="btn">
								<input type="text" name="filtro" placeholder="<?=$this->lang->line('search_dashboard')?>">
								<button type="submit" class="btn buscar" id="filtrar"><i class="fa fa-search" aria-hidden="true"></i></button>
							</form>
							<div class="text-right no-padding pull-right hidden">
								<button class="btn-circle-azul" type="button" data-toggle="modal" data-target="#voz_general" data-toggle="tooltip" title="Llamada masiva"><i class="fa fa-phone" aria-hidden="true"></i></button>
								<button class="btn-circle-azul" type="button" data-toggle="modal" data-target="#mensaje_general" data-toggle="tooltip" title="Mensaje masivo"><i class="fa fa-comments-o" aria-hidden="true"></i></button>
								
							</div>
							<div class="toggle text-center no-padding d-flex justify-content-between align-items-center">
								<a class="btn btn-green" href="<?=base_url();?>admin/newPaciente" title="Crear paciente" style="margin-right: 2rem;"><i class="fa fa-plus" aria-hidden="true"></i></a>
								<p><small>View as: </small>
									<a class="view grid on" href="#" aria-label="Grid"><i class="fa fa-th" aria-hidden="true"></i></a>
									<a class="view list" href="#" aria-label="List"><i class="fa fa-bars" aria-hidden="true"></i></a>
								</p>
							</div>
						</div>
						<div class="block-xs hidden-sm  hidden-md  hidden-lg">
							<form class="d-flex justify-content-center" id="formbuscar" action="<?php print base_url();?>admin/dashboard" method="POST">
								<input type="hidden" id="tbusqueda" name="tbusqueda" value="btn">
								<input type="text" name="filtro" placeholder="<?=$this->lang->line('search_dashboard')?>">
								<button type="submit" class="btn buscar" id="filtrar"><i class="fa fa-search" aria-hidden="true"></i></button>
							</form>
							<br>
							<div class="toggle text-center no-padding d-flex justify-content-between align-items-center">
								<a class="btn btn-green" href="<?=base_url();?>admin/newPaciente" title="Crear paciente" style="margin-right: 2rem;"><i class="fa fa-plus" aria-hidden="true"></i></a>
								<p><small>View as: </small>
									<a class="view grid on" href="#" aria-label="Grid"><i class="fa fa-th" aria-hidden="true"></i></a>
									<a class="view list" href="#" aria-label="List"><i class="fa fa-bars" aria-hidden="true"></i></a>
								</p>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12 items-pacientes">
						<ul class="view-as grid">
							<?php if(count($pacientes) > 0){ 
							foreach ($pacientes as $p) { 
								$timestamp = (String) $p['registration_date']['milliseconds'];
                  				$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000);
                  				$telefono = '';
                  				if (isset($name_columns['Teléfono'])) {
                  					for ($ip=0; $ip<count($name_columns['Teléfono']); $ip++) {
	                  					if (isset($p[$name_columns['Teléfono'][$ip]]) && strlen($p[$name_columns['Teléfono'][$ip]])==10 && (substr($p[$name_columns['Teléfono'][$ip]], 0, 1)=="3" || substr($p[$name_columns['Teléfono'][$ip]], 0, 1)==3 )) {
	                  						$telefono=$p[$name_columns['Teléfono'][$ip]];
	                  						break;
	                  					}
	                  				}
                  				}          				
                  				$name_patient = '';
                  				if (isset($name_columns['Nombre'])) {
                  					for ($ip=0; $ip<count($name_columns['Nombre']); $ip++) {
                  						if (isset($p[$name_columns['Nombre'][$ip]])) {
                  							$name_patient.=" ".$p[$name_columns['Nombre'][$ip]];
                  						}	                  					
	                  				}
                  				}
	                  				
                  				$apellido_patient = '';
                  				if (isset($name_columns['Apellido'])) {
                  					for ($ip=0; $ip<count($name_columns['Apellido']); $ip++) {
	                  					if (isset($p[$name_columns['Apellido'][$ip]])) {
                  							$apellido_patient.=" ".$p[$name_columns['Apellido'][$ip]];
                  						}
	                  				}
                  				}?>
							<li>
							<div class="paciente-card">
								<div class="box-lista">
									<?php if(date('Y-m-d') == $dateInLocal){ ?> <span class="label new-indicator">Nuevo</span><?php } ?>
									<p class="box-state-indicator d-flex justify-content-between pt-6">
										<a href="<?=base_url()?>admin/editarPaciente/<?=$p['_id']['oid']?>" class="btn btn-green"><i class="fa fa-pencil"></i></a>
										<i class="fa fa-circle <?=(isset($p['state_risk']))?$colores[$p['state_risk']['name']]:'';?>" aria-hidden="true"></i>
									</p> 
									<a style="text-decoration: none;" onclick="redireccionar('<?=$p['_id']['oid']?>')">
										<p align="center" class="img-as">
											<?php if(isset($name_columns['Sexo']) && isset($p[$name_columns['Sexo'][0]]) && ($p[$name_columns['Sexo'][0]] == "F" || $p[$name_columns['Sexo'][0]] == "f" || $p[$name_columns['Sexo'][0]] == '0' )){ ?>
												<img src="<?=base_url()?>img/mujer.png" class="img-circle"  width="100" height="100">
											<?php }else{ ?>
												<img src="<?=base_url()?>img/hombre.png" class="img-circle"  width="100" height="100">
											<?php } ?>
										</p>
									</a>
									<p class="name">
										<a style="text-decoration: none; font-weight: bold;" onclick="redireccionar('<?=$p['_id']['oid']?>')"><?=$name_patient." ".$apellido_patient;?>		
										</a>
									</p>
									<p class="aditional-information"><?=(isset($name_columns['Municipio']) && isset($p[$name_columns['Municipio'][0]]))?$p[$name_columns['Municipio'][0]]:'';?> - <?=(isset($name_columns['Departamento Residencia']) && isset($p[$name_columns['Departamento Residencia'][0]]))?$p[$name_columns['Departamento Residencia'][0]]:'';?></p>
									<!-- <p class="aditional-information"><strong>Última consulta:</strong><?php echo $p->fecha_ult_control ?></p>-->
									<hr>
									<p class="description"><strong><?=(isset($name_columns['IPS']) && isset($p[$name_columns['IPS'][0]]) && $p[$name_columns['IPS'][0]]!='')?substr($p[$name_columns['IPS'][0]],0,31):'';?></strong></p>
								</div>
								<div class="row no-padding box-buttons">
									<div class="col-md-4 no-padding">
										<button type="button" class="btn llamada btn-block" data-telefono="<?=($telefono!='')?$telefono:''; ?>" data-patient="<?=$name_patient?>" <?=($telefono!='')?'':'disabled';?> data-id="<?=$p['_id']['oid']?>" data-id_entidad="<?=$id_entidad_?>"><?=$this->lang->line('llamar')?></button>
									</div>
									<div class="col-md-4 no-padding">
										<button type="button" class="btn mensaje btn-block" data-telefono="<?=($telefono!='')?$telefono:''; ?>" data-patient="<?=$name_patient?>" <?=($telefono!='')?'':'disabled';?> data-id="<?=$p['_id']['oid']?>" data-id_entidad="<?=$id_entidad_?>"><?=$this->lang->line('sms')?></button>
									</div>
									<div class="col-md-4 no-padding">
										<button type="button" class="btn whatsapp btn-block" data-telefono="<?=($telefono!='')?$telefono:''; ?>" data-patient="<?=$name_patient?>" <?=($telefono!='')?'':'disabled';?> data-id="<?=$p['_id']['oid']?>" data-id_entidad="<?=$id_entidad_?>"><?=$this->lang->line('WHATSAPP')?></button>
									</div>
								</div>
							</div>
							</li>
							<?php } }else{ ?>
								<li style="font-size:13px"><?=$this->lang->line('bad_search_dashboard')?></li>
							<?php } ?>
						</ul>
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12" style="text-align:center">
						<nav aria-label="Page navigation">
					        <ul class="pagination">
					            <?=$this->pagination->create_links(); ?>
					        </ul>
			            </nav>
				    </div>
				</div>
			</div>
			<?php if ($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411") { ?>
			<div class="col-md-2 hidden-sm hidden-xs" style="margin-top: 15px;padding-left: 0px;">
				<div class="col-xs-12 box-mensajes">
					<form id="formEntidad" action="<?php print base_url();?>admin/dashboard" method="POST" onsubmit="return false">
						<label><?=$this->lang->line('paciente_duplicado')?></label>
					    <p><?=(count($duplicados)>0)?count($duplicados).' <button type="button" class="btn mas btn-green ver_duplicados" data-toggle="modal" data-target="#modalDuplicados">Ver</button>':'0'; ?></p>
					    <br>
						<label><?=$this->lang->line('entidad')?></label>
					    <select class="form-control" name="searchentidad">
					    	<?php $ent = $this->session->userdata('searchentidad'); if(!$ent) $ent=""; ?>
					        <option value="" style="display: none;"> <?=$this->lang->line('select_one')?> </option>
					        <?php foreach ($entidad as $r) { ?>
								<option value="<?=$r->_id; ?>" <?=($r->_id==$ent)? 'selected':''; ?>><?=$r->name ?></option>
							<?php } ?>
					    </select>
					</form>
				</div>
			</div>
			<?php } ?>
		</div>
		<!-- MENSAJE GENERAL-->
		<div class="modal fade" id="mensaje_general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-comments-o fa-2x" aria-hidden="true" style="color:#468FE4"></i> <?=$this->lang->line('mensaje_masivo')?></h4>
				    </div>
			      	<div class="modal-body">
				        <form action="" id="form_general" method="POST" onsubmit="return false">
				        	<div class="form-group">
				        		<label><?=$this->lang->line('estado_paciente')?></label>
				        		<select class="form-control " id="tipo_paciente">
				        			<option value="" style="display: none;">-Seleccione uno-</option>
				        			<?php foreach ($estado as $e) { ?>
				        			<option value="<?=$e->id?>">
				        				<?=$e->estado?></option>
				        			<?php } ?>
				        		</select>
				        	</div>
				        	<div class="form-group">
				        		<input type="checkbox" name="encuesta" value="1" id="encuesta_msj"> Enviar encuesta
				        	</div>
				        	<div class="form-group" id="encuesta-box" style="display:none">
				        		<label>Encuesta</label>
				        		<select class="form-control " id="encuesta_opcion">
				        			<option value="" style="display: none;">-Seleccione uno-</option>
				        			<?php foreach ($encuesta as $e) { ?>
				        			<option value="<?=$e->id?>"><?=$e->encuesta?></option>
				        			<?php } ?>
				        		</select>
				        	</div>
							<div class="form-group">
						         <label>Mensaje</label>
						         <textarea class="form-control " rows="4" cols="50" id="mensaje_general_txt" name="mensaje_general_txt" maxlength="159"></textarea>
						    </div>
						    <div class="col-md-6 pull-right" style="text-align: right;">
	                            <span>Caracteres restantes </span><span class="CaracteresRestantesGnr">160</span> 
	                        </div>
						</form>
					</div>
					<div class="modal-footer">
					    <button type="button" class="btn limpiar btn-border-dark" data-dismiss="modal">Cancelar</button>
					    <button type="button" class="btn mas btn-green" id="enviar_general">Enviar</button> 
					</div>  
			    </div>
			</div>
		</div>
	
		<!-- VOZ GENERAL-->
		<div class="modal fade" id="voz_general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel" style="color:#47bbff"><i class="fa fa-phone fa-2x" aria-hidden="true" ></i> LLAMADA MASIVA</h4>
				    </div>
			      	<div class="modal-body">
				        <form action="" id="form_general" method="POST" onsubmit="return false">
				        	<div class="form-group">
				        		<label>Estado pacientes</label>
				        		<select class="form-control " id="tipo_paciente_general">
				        			<option value="" style="display: none;">-Seleccione uno-</option>
				        			<?php foreach ($estado as $e) { ?>
				        			<option value="<?=$e->id?>"><?=$e->estado?></option>
				        			<?php } ?>
				        		</select>
				        	</div>
							<div class="form-group">
						         <label>Mensaje</label>
						         <textarea rows="4" cols="50" id="mensaje_voz_general" class="form-control "></textarea>
						    </div>
						</form>
					</div>
					<div class="modal-footer">
					    <button type="button" class="btn limpiar btn-border-dark" data-dismiss="modal">Cancelar</button>
					    <button type="button" class="btn mas btn-green" id="enviar_voz_general">Enviar</button> 
					</div>
			    </div>
			</div>
		</div>

		<!-- Pacientes duplicados -->
		<div class="modal fade" id="modalDuplicados" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
				       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel" style="color:#47bbff">PACIENTES DUPLICADOS</h4>
				    </div>
			      	<div class="modal-body">
				        <div class="form-group">
					        <div class="col-xs-12 -md-12 contenedorPacientesDuplicados"></div>
					    </div>
					</div>
					<div class="modal-footer">					     
					</div>
			    </div>
			</div>
		</div>
		
			<!-- ==============================================
		SCRIPTS
		=============================================== -->	
		<?php $this->load->view("common/admin/extras")?>
		<?php $this->load->view("common/admin/js")?>
		<script  type="text/javascript" language="javascript">
			$(document).on('keyup', 'textarea[name="mensaje_personalizado"]', function(e) {
			    // maxlength attribute does not in IE prior to IE10
			    // http://stackoverflow.com/q/4717168/740639
			    var $this = $(this),
			        maxlength = $this.attr('maxlength'),
			        maxlengthint = parseInt(maxlength),
			        caracteresActuales = $this.val().length;
			  			caracteresRestantes = maxlengthint - caracteresActuales,
			        espanCaractRestantes = $('.CaracteresRestantes');
			  
				espanCaractRestantes.html(caracteresRestantes);
				if($('html').hasClass("ie9")) {
				    if(caracteresRestantes == -1) {
				       caracteresRestantes = 0;
				    }
				}
			    if(!!maxlength){
			        var text = $this.val();
			        if (text.length >= maxlength) {
			            $this.val(text.substring(0,maxlength));            
			            e.preventDefault();
			        }
			    }
			});

			$(document).on('keyup', 'textarea[name="mensaje_general_txt"]', function(e) {
			    // maxlength attribute does not in IE prior to IE10
			    // http://stackoverflow.com/q/4717168/740639
			    var $this = $(this),
			        maxlength = $this.attr('maxlength'),
			        maxlengthint = parseInt(maxlength),
			        caracteresActuales = $this.val().length;
			  			caracteresRestantes = maxlengthint - caracteresActuales,
			        espanCaractRestantes = $('.CaracteresRestantesGnr');
			  
				espanCaractRestantes.html(caracteresRestantes);
				if($('html').hasClass("ie9")) {
				    if(caracteresRestantes == -1) {
				       caracteresRestantes = 0;
				    }
				}
			    if(!!maxlength){
			        var text = $this.val();
			        if (text.length >= maxlength) {
			            $this.val(text.substring(0,maxlength));            
			            e.preventDefault();
			        }
			    }
			});
			
			
			var $gridButton = jQuery( 'a.view.grid' );
			var $listButton = jQuery( 'a.view.list' );
			var $items      = jQuery( 'ul.view-as' );
			var $imgs      = jQuery( 'ul.img-as' );
			var $listaBox  = jQuery( 'div.box-lista' );

			$listButton.on('click',function(){
				$gridButton.removeClass('on');
				$listButton.addClass('on');
				$items.removeClass('grid').addClass('list');
				$imgs.removeClass('ver').addClass('ocultar');
				$listaBox.addClass('list-box'); 
			});

			$gridButton.on('click',function(){
				$listButton.removeClass('on');
				$gridButton.addClass('on');
				$items.removeClass('list').addClass('grid');
				$imgs.removeClass('ocultar').addClass('ver');
				$listaBox.removeClass('list-box');
			});

			$('#encuesta_msj').on('change',function(){
				if(document.getElementById("encuesta_msj").checked == true){
					$('#encuesta-box').css('display','block');
				}else{
					$('#encuesta-box').css('display','none');
				}
			});

			function limpiar(){
				$('#id_paciente').val(' ');
				$('#id_paciente_voz').val(' ');
				$('#nombre_paciente').val(' ');
				$('#tipo_paciente').val('');
				$('#tipo_paciente_general').val('');
				$('#mensaje_personalizado').val(' ');
				$('#mensaje_general_txt').val(' ');
				$('#voz_personalizado').val(' ');
				$('#mensaje_voz_general').val(' ');
			}
			
		    
			$('#enviar_general').on('click',function(){
		    	if($('#mensaje_general_txt').val()==''){
		    		return mensaje("<?=$this->lang->line('bad_mensaje')?>");
		    	}
		    	if($('#tipo_paciente').val()==''){
		    		return mensaje("<?=$this->lang->line('dashboard_bad_1')?>");
		    	}
		    	if(document.getElementById("encuesta_msj").checked == true){
		    		if($('#encuesta_opcion').val()==''){
			    		return mensaje("<?=$this->lang->line('dashboard_bad_2')?>");
			    	}
		    	}
		    	$('#mensaje_general').modal('hide');
		    	ajax('mensajeria/general',
				    {   
				        grupo: $('#tipo_paciente').val(),
				        mensaje:$('#mensaje_general_txt').val(),
				        encuesta:$('#encuesta_opcion').val(),
				        tipo: 'sms'
				    },
				    function(data){
				        if(data.res=="ok"){
				        	mensaje("<?=$this->lang->line('dashboard_bad_3')?>");
				            limpiar();                                    
				        }else{
				        	$('#mensaje').modal('hide');
				            mensaje(data.msj);
				        }
				    },10000);
		    });
		    
		    $('#enviar_voz_general').on('click',function(){
		    	if($('#mensaje_voz_general').val()==''){
		    		return mensaje("<?=$this->lang->line('bad_mensaje')?>");
		    	}
		    	if($('#tipo_paciente_general').val()==''){
		    		return mensaje("<?=$this->lang->line('dashboard_bad_4')?>");
		    	}
		    	$('#voz_general').modal('hide');
		    	ajax('mensajeria/general',
				    {   
				        grupo: $('#tipo_paciente_general').val(),
				        mensaje:$('#mensaje_voz_general').val(),
				        tipo: 'llamada'
				    },
				    function(data){
				        if(data.res=="ok"){
				        	mensaje("<?=$this->lang->line('dashboard_bad_5')?>");
				            limpiar();                                    
				        }else{
				        	$('#mensaje').modal('hide');
				            mensaje(data.msj);
				        }
				    },10000);
		    });
		   
		    $(document).ready(function(){
		    });

		    $('[name=searchentidad]').on('change', function () {
		    	$('[name=searchentidad]').val($(this).val());
		    	document.getElementById('formEntidad').submit();
		    });

		    function redireccionar(id){
		    	console.log(id);
		    	$.redirect(base_url + 'admin/perfil', {'id': id});
		    }
		    $('.ver_duplicados').on('click', function () {
		    	console.log('entra')
		    	$(".contenedorPacientesDuplicados").load("paginationDuplicadosPacientes");		    	
		    });
		    $(document).on("click", "#pagination-digg11 a", function(e){
			    e.preventDefault();
			    var href = $(this).attr("href");
			    console.log(href);
			    $(".contenedorPacientesDuplicados").load(href);
		  	});
		</script>
	</body>
</html>