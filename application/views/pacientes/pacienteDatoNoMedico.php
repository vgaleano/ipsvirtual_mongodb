<?php
$id = trim($this->input->post('id', TRUE));
$datonomedico = trim($this->input->post('datonomedico', TRUE));
$data = array(
	'id_paciente'=>$id,
	'observacion'=>$datonomedico
);
$query = $this->db->insert('datos_no_medicos',$data);
if ($query){
   print json_encode(array("res"=>"ok"));
}else{
	print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente al momento de guardar, inténtalo de nuevo." ));
}