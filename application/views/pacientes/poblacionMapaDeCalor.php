<?php
/*$con=mysqli_connect('apps.ccsmckgi9ggi.us-east-1.rds.amazonaws.com','dgualdron','91532004ab','ips_virtual');
// Check connection
if (mysqli_connect_errno())
{
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
}*/
$estado=$this->input->post('estado',TRUE);
$id_entidad=$this->input->post('entidad', TRUE);


$this->db->where('id_entidad', $id_entidad);
$this->db->from('pacientes');
$all_pacients = $this->db->count_all_results();


if ($estado=='') {
	$estados=['0'];
}elseif ($estado=='0') {
	$estados = ['1','2','3','4'];
}else{
	$estados = [$estado];
}

$datos=[];
$ok=0;
for ($i=0; $i < count($estados); $i++) {
	$datos[$i]['estado']=$estados[$i];
	$datos[$i]['datos']=array();
	$datos[$i]['total']=0;
	$this->db->select('pacientes.latitud as lat, pacientes.longitud as lon');
	$this->db->join('pacientes_estados', 'pacientes_estados.numero_identificacion = pacientes.numero_documento', 'left');
	$this->db->where('pacientes.latitud!="" and pacientes.longitud!=""');
	if ($estados[$i]!='0') {
		$this->db->where('pacientes_estados.id_estado', $estados[$i]);
	}
	if ($id_entidad!='') {
		$this->db->where('pacientes_estados.id_entidad', $id_entidad);
	}
	$query = $this->db->get('pacientes');
	if ($query->num_rows() > 0){
		$query=$query->result();
		$datos[$i]['datos']=$query;
		$datos[$i]['total']=count($query);
		$ok++;
	}
}

if ($ok==0) {
	print json_encode(array("res"=>"bad", "msj"=>"No se han encontrado coincidencias.", "all"=>$all_pacients ));
}else{
	print json_encode(array("res"=>"ok","msj"=>$datos, "all"=>$all_pacients));
}