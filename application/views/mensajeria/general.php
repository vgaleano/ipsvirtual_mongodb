<?php 
$grupo =$this->input->post('grupo', TRUE);
$mensaje =$this->input->post('mensaje', TRUE);
//$pacientes = $this->db->get_where('pacientes',array('id_estado'=>$grupo,'activo'=>'1'));
$sql ='select pacientes.celular, estado.id FROM historico_datos_paciente join estado on historico_datos_paciente.id_estado = estado.id join pacientes on historico_datos_paciente.numero_identificacion = pacientes.numero_documento and estado.id=1 order by historico_datos_paciente.fecha_registro desc ';
$pacientes = $this->db->query($sql);
$tipo= $this->input->post('tipo', TRUE);
$encuesta=$this->input->post('encuesta',TRUE);
$id_usuario = $this->session->id;
if(!empty($pacientes->result())){
	$pacientes = $pacientes->result();
	foreach ($pacientes as $p){
        $celular = $p->celular;
        if($celular != NULL){
            $primer_numero = substr($celular, 0, 1);
            if($primer_numero == "3"){
                if($tipo == 'sms'){
                    //viene encuesta
                    if(!empty($encuesta)){
                        //shortened google
                        $url=''.$p->id.'/'.$encuesta;
                        $apiKey = '';
                        $postData = array('longUrl' => $url, 'key' => $apiKey);
                        $jsonData = json_encode($postData);
                        $curlObj = curl_init();
                        curl_setopt($curlObj, CURLOPT_URL, '');
                        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
                        curl_setopt($curlObj, CURLOPT_HEADER, 0);
                        curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
                        curl_setopt($curlObj, CURLOPT_POST, 1);
                        curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
                        $response = curl_exec($curlObj);
                        $json = json_decode($response);
                        curl_close($curlObj);
                        $short = $json->id;
                        //mensaje de texto
                        $mensaje = $mensaje.' '.$short;
                        $url = '';
                        $datos='user=&pass=%&dest='.$celular.'&text='.str_replace(' ','%20',$mensaje);
                        $ch = curl_init(); 
                        curl_setopt($ch, CURLOPT_URL, $url.$datos);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                        $output = curl_exec($ch);
                        curl_close($ch);  
                        $data = array(
                            'id_estado_pacientes'=>$grupo,
                            'mensaje'=>$mensaje,
                            'id_canal'=>'1',
                            'id_usuario'=>$id_usuario,
                            'encuesta'=>$encuesta
                            );
                    }else{
                        $url = '';
                        $datos='user=&pass=%&dest='.$celular.'&text='.str_replace(' ','%20',$mensaje);
                        $ch = curl_init(); 
                        curl_setopt($ch, CURLOPT_URL, $url.$datos);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                        $output = curl_exec($ch);
                        curl_close($ch);  
                        $data = array(
                            'id_estado_pacientes'=>$grupo,
                            'mensaje'=>$mensaje,
                            'id_usuario'=>$id_usuario,
                            'id_canal'=>'1'
                            );
                    }
                }else{
                    $url = '?';
                    $datos='user=&pass=%&dest='.$celular.'&text='.str_replace(' ','%20',$mensaje);
                    $ch = curl_init(); 
                    curl_setopt($ch, CURLOPT_URL, $url.$datos);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
                    $output = curl_exec($ch);
                    curl_close($ch);
                    $data = array(
                        'id_estado_pacientes'=>$grupo,
                        'mensaje'=>$mensaje,
                        'id_usuario'=>$id_usuario,
                        'id_canal'=>'2'
                        );
                }
            }
        }
    }
    $this->db->insert('log_general',$data); 
    print json_encode(array("res"=>"ok"));
}else{
	print json_encode(array("res"=>"bad","msj" =>"Se presentó un inconveniente encontrando pacientes con este estado, inténtelo más tarde."));
    	exit();
}