<?php

/*
Primero busca el mensaje que es para enviar el recordatorio en la tabla mensajes_banco
Luego se trae los registros que tengan en estado_envio 1 y en estado_contacto 1, sigue con el siguiente proceso:
- Se estandariza el formato de la fecha cita para el mensaje de recordatorio.
- Se valida si la fecha_envio_p1 y fecha_envio_p2 es igual 0000-00-00 para asignar la bandera en 1 así asignando en estado_envio en 0, que significa ya cumplio con los sms recordatorio p1 y recordatorio con recomendaciones p2.
- Cuando es recordatorio p1 se valida que sea la fecha hoy, si es así se revisa la unidad de periodicidad y depende de éste se suma la unidad a la fecha_envio_p1, si la fecha calculada es igual a la fecha cita entonces se actualiza la fecha_envio_p1 a 0000-00-00, si es lo contrario se deja con la fecha calculada y se envia el mensaje recordatorio.
- Cuando es recordatorio p2 se valida que sea la fecha hoy, si es así se revisa la unidad de periodicidad_anticipada, se envia un mensaje de recordatorio, luego se procede en particionar las recomendaciones por *, se envían aquellos que sean menor o igual a 160 caracteres.
*/

//date_default_timezone_set('America/Bogota');
setlocale (LC_TIME,'es_ES.UTF-8');
fecha_hoy=date('Y-m-d');
/*$fecha_hoy='2019-02-08';
echo "fecha hoy ".$fecha_hoy;
echo "<br><br>";*/

require_once '/var/www/html/ips_virtual/application/third_party/simplexlsx.class.php';
$usuario = "dgualdron";
$password = "91532004ab";
$servidor = "apps.ccsmckgi9ggi.us-east-1.rds.amazonaws.com";
$basededatos = "ips_virtual";
// creación de la conexión a la base de datos con mysql_connect()
$conexion = mysqli_connect($servidor, $usuario, $password) or die ("No se ha podido conectar al servidor de Base de datos");
$db = mysqli_select_db($conexion, $basededatos) or die ( "Upps! Pues va a ser que no se ha podido conectar a la base de datos" );
// establecer y realizar consulta. guardamos en variable.

//traer el mensaje de recordatorio
$mensaje_recordatorio;
$consulta = "SELECT mensaje FROM mensajes_banco where id_banco_mensaje='6';";
$resultado = mysqli_query($conexion, $consulta) or die ( "Algo ha ido mal en la consulta a la base de datos");
// Bucle while que recorre cada registro y muestra cada campo en la tabla.
while ($columna = mysqli_fetch_array($resultado)){
    $mensaje_recordatorio=$columna['mensaje'];
}

$consulta = "SELECT id, telefono, fecha_cita, hora_cita, periodicidad, periodicidad_anticipada, recomendacion, fecha_envio_p1, fecha_envio_p2 FROM paciente_mensajeria where estado_contacto=1 and estado_envio=1;";
$resultado = mysqli_query($conexion, $consulta) or die ( "Algo ha ido mal en la consulta a la base de datos");
// Bucle while que recorre cada registro y muestra cada campo en la tabla.
while ($columna = mysqli_fetch_array($resultado)){
    /*echo "<br><br> telefono: ".$columna['telefono'];*/
    
    $formato_fecha = strftime("%d de %B del %Y", strtotime($columna['fecha_cita']));
    $mensaje_recordatorioF=str_replace("#fecha", $formato_fecha, $mensaje_recordatorio);
    //se le agrega la hora cita al mensaje
    if ($columna['hora_cita']!='00:00:00') {
        $mensaje_recordatorioF=$mensaje_recordatorioF." a las ".date('H:i',strtotime($columna['hora_cita']));
    }
    /*echo "<br> mensaje_recordatorio: ".$mensaje_recordatorioF;*/

    $sql_p1='';    
    $bandera_p1=0;    
    if ($columna['fecha_envio_p1']=='0000-00-00') {
        $bandera_p1=1;
    }
    $columna['periodicidad']=strtolower($columna['periodicidad']);
    //si la fecha envio p1 es igual a hoy
    if ($columna['fecha_envio_p1']==$fecha_hoy) {
        /*echo "<br>envio mensaje recordatorio p1";*/

        //calcula la proxima notifacion sms
        $rta_p1=periodicidadRecordatorios(utf8_encode($columna['periodicidad']),$columna['fecha_envio_p1'],$columna['fecha_cita']);
        $sql_p1=' fecha_envio_p1="'.$rta_p1['sql'].'"';  
        $bandera_p1=$rta_p1['bandera'];
        
        //envia sms - recordatorio            
        envioSMS($columna['telefono'],$mensaje_recordatorioF);
        $sql22='INSERT INTO historico_paciente_mensajeria (telefono) VALUES ("'.$columna['telefono'].'");';
        sqlMayor($sql22,$conexion);
    }

    $sql_p2='';
    $bandera_p2=0;
    if ($columna['fecha_envio_p2']=='0000-00-00') {
        $bandera_p2=1;
    }

    //si la fecha envio p2 es igual a hoy
    /*echo "<br> fecha_envio_p2: ".$columna['fecha_envio_p2'];*/
    $columna['periodicidad_anticipada']=strtolower($columna['periodicidad_anticipada']);
    if ($columna['fecha_envio_p2']==$fecha_hoy) {
        /*echo "<br>envio mensaje recordatorio p2";*/

        //calcular la proxima fecha
        $rta_p2=periodicidadRecordatorios(utf8_encode($columna['periodicidad_anticipada']),$columna['fecha_envio_p2'],$columna['fecha_cita']);
        $sql_p2=' fecha_envio_p2="'.$rta_p2['sql'].'"';  
        $bandera_p2=$rta_p2['bandera'];

        //envia sms - recordadotorio
        envioSMS($columna['telefono'],$mensaje_recordatorioF);
        $sql22='INSERT INTO historico_paciente_mensajeria (telefono) VALUES ("'.$columna['telefono'].'");';
        sqlMayor($sql22,$conexion);

        //Particionar las recomendaciones
        $particion=explode("*", utf8_encode($recomendaciones));
        $mensaje=array();
        for ($i=0; $i < count($particion); $i++) {
            $particion[$i]=rtrim($particion[$i]);
            if ($particion[$i]!='') {
                if (strlen($particion[$i])<=160) {
                    //envia sms - recomendaciones
                    /*echo "<br>envio mensaje recomendaciones";*/
                    envioSMS($columna['telefono'],$particion[$i]);
                    $sql22='INSERT INTO historico_paciente_mensajeria (telefono) VALUES ("'.$columna['telefono'].'");';
                    sqlMayor($sql22,$conexion);
                }
            }                
        }
    }

    if ($sql_p2!='' || $sql_p1!='') {
        $sql_p3=($sql_p2!='' && $sql_p1!='') ? ' , ' : '';
        $sql_4=($bandera_p1==1 && $bandera_p2==1) ? ' , estado_envio="0"' : '';
        $sql1='UPDATE paciente_mensajeria set '.$sql_p1.$sql_p3.$sql_p2.$sql_4.' where id="'.$columna['id'].'";';
        sqlMayor($sql1,$conexion);
    }
    
}

function sqlMayor($sql,$conexion){
    //insertar los registros
    $runcheck = mysqli_query($conexion, $sql);
    if (!$runcheck) {
        //die('No se pudo hacer la carga masiva: '.mysql_error());
    }else{
        //print 'ok';
    }
}

function envioSMS($celular,$mensaje)
{
    $url = '';
    $datos='user=&pass=&dest='.$celular.'&text='.str_replace(' ','%20',$mensaje);
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url.$datos);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
    $output = curl_exec($ch);
    /*echo "<pre>";
    print_r($output);
    echo "</pre>";*/
    curl_close($ch);
}

function periodicidadRecordatorios($unidad,$fecha_envio,$fecha_cita)
{
    $rta=array();    
    $unidad=strtolower($unidad);
    $newDate='';
    $bandera=0;
    if (utf8_encode($unidad)=='cada semana') {
        $newDate = strtotime ( '+1 week' , strtotime ( $fecha_envio ) ) ;
        $newDate = date ( 'Y-m-d' , $newDate );
        //la fecha obtenida es igual a la fecha cita -> se setea la fecha
        if ($newDate==$fecha_cita) {
            $bandera=1;
            $newDate="";
        }       
    }
    if (utf8_encode($unidad)=='un día antes') {
        $bandera=1;
    }
    if ($unidad=='cada día') {
        $newDate = strtotime ( '+1 day' , strtotime ( $fecha_envio ) ) ;
        $newDate = date ( 'Y-m-d' , $newDate );
        //la fecha obtenida es igual a la fecha cita -> se setea la fecha
        if ($newDate==$fecha_cita) {
            $bandera=1;
            $newDate="";
        } 
    }   
    if ($unidad=='anualmente') {
        $newDate=date ( 'Y-m-d' ,strtotime ( '+1 year' , strtotime ( $fecha_envio ) ) );
        if ($newDate>$fecha_cita) {//la fecha calculada es menor a la fecha hoy se envia vacio
            $newDate="";
            $bandera=1;
        }
    }
    if ($unidad=='todos los días laborales') {
        $newDate=diasLaborales($fecha_envio,$fecha_cita);
        if ($newDate=='') {
            $bandera=1;
        }
    }
    //en personalizar suponiendo que venga una fecha especifica -> Personalizar: 20-02-2019
    if (strrpos($unidad,'personalizar')!==false) {
        $newDate="";
        $bandera=1;
    }
    //falta definir como será
    /*if (strrpos($unidad, 'cada mes el día')) {
        $cosas=explode(' ', $unidad);
        for ($i=0; $i < count($cosas); $i++) { 
            if (is_int($cosas[$i])==true){
                $date1=date('Y-m-d',strtotime(date('Y-m',strtotime($fecha_cita))));
            }
        }
    }*/
    //falta definir el formato
    /*if (strrpos($unidad, 'cada semana los días')) {
        $cosas=explode(' ', $unidad);
        for ($i=0; $i < count($cosas); $i++) { 
            if (is_int($cosas[$i])==true){
                $date1=date('Y-m-d',strtotime(date('Y-m',strtotime($fecha_cita))));
            }
        }
    }*/

    $rta['sql']=$newDate;//no se repite
    $rta['bandera']=$bandera;

    return $rta;
}

function diasLaborales($fecha_inicio, $fecha_fin) {
    $date_1 = date_create($fecha_inicio);
    $date_2 = date_create($fecha_fin);
    if ($date_1 > $date_2) return '';
    while ($date_1 <= $date_2) {
        date_add($date_1, date_interval_create_from_date_string('1 day'));
        $day_week = $date_1->format('w');
        if ($day_week > 0 && $day_week < 6) {
            return $date_1->format('Y-m-d');
            break;
        }        
    }
    return '';
}
    
mysqli_close( $conexion );