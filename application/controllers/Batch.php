<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('America/Bogota');
require_once APPPATH.'third_party/spout/src/Spout/Autoloader/autoload.php';
//require_once 'D:\Programas\xampp_7_3_12\htdocs\ips_virtual\application\third_party\spout\src\Spout\Autoloader\autoload.php';
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Batch extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('collection_model');
	}

	//subir el archivo excel de pacientes al proyecto
	public function cargarPacientes(){
		$_id=$this->input->post('id_entidad', TRUE);
		if($_FILES['pacientes']['tmp_name'] != ""){
			// obtenemos los datos del archivo 
		    $tipo = $_FILES["pacientes"]['type']; //vnd.openxmlformats-officedocument.spreadsheetml.sheet    vnd.ms-excel
		    //print_r($tipo);
		    $archivo = $_FILES["pacientes"]['name'];
		    $fecha=date('d-m-Y_Hi');
			if ($archivo != ""){
				if ($tipo == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
					// guardamos el archivo a la carpeta files
					$nombreModificado = str_replace(' ', '_', $archivo);
					$destino = "/var/www/html/ips_virtual/carga_masiva/".$fecha."_".$nombreModificado;//"D:\\Programas\\xampp_7_3_12\htdocs\\ips_virtual\\carga_masiva\\".$fecha."_".$nombreModificado;//
					if (copy($_FILES['pacientes']['tmp_name'],$destino)){
						chmod($destino, 0777);
						//chown($destino, 'ec2-user');
						$result = $this->collection_model->insert_collection('process', array('state'=>'1', 'url'=>$destino, 'type'=>'1', 'entity'=>array('id'=>$_id)));
						if ($result) {
							$r = @json_encode(array('res'=>'ok','tipo'=>'pacientes'));
						}else{
							$r = @json_encode(array('res'=>'bad', 'msj'=>'Lo sentimos, error insertando el documento.','tipo'=>'pacientes'));
						}
					}else{
						$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, error al subir el archivo.','tipo'=>'pacientes'));
					}
				}else{
					$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, error: solo se permite XSLT.','tipo'=>'pacientes'));
				}
			}else{
				$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, error al subir archivo.','tipo'=>'pacientes'));
			}
		}else{
			$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, el archivo que intenta cargar no corresponde con el formato.','tipo'=>'pacientes'));
		}
		print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
	}

	//subur el archivo excel de recorcatorios
	public function cargarSmsPacientes()
	{
		$_id=$this->input->post('id_entidad', TRUE);
		if($_FILES['file']['tmp_name'] != ""){
			// obtenemos los datos del archivo
		    $tipo = $_FILES["file"]['type']; //vnd.openxmlformats-officedocument.spreadsheetml.sheet    vnd.ms-excel
		    //print_r($tipo);
		    $archivo = $_FILES["file"]['name'];
		    $prefijo = substr(md5(uniqid(rand())),0,6);
		    $fecha=date('d-m-Y_Hi');
			if ($archivo != ""){
				if ($tipo == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
					// guardamos el archivo a la carpeta files
					$nombreModificado = str_replace(' ', '_', $archivo);
					$destino = "/var/www/html/ips_virtual/carga_masiva/".$fecha."_".$nombreModificado;
					if (copy($_FILES['file']['tmp_name'],$destino)){
						chmod($destino, 0777);
						//chown($destino, 'ec2-user');
						$result = $this->collection_model->insert_collection('process', array('state'=>'1', 'url'=>$destino, 'type'=>'2', 'entity'=>array('id'=>$_id)));
						if ($result) {
							$r = @json_encode(array('res'=>'ok','tipo'=>'sms_pacientes'));
						}else{
							$r = @json_encode(array('res'=>'bad', 'msj'=>'Lo sentimos, error insertando el documento.','tipo'=>'sms_pacientes'));
						}				
					}else{
						$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, error al subir el archivo.','tipo'=>'sms_pacientes'));
					}
				}else{
					$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, error: solo se permite XSLT.','tipo'=>'sms_pacientes'));
				}
			}else{
				$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, error al subir archivo.','tipo'=>'sms_pacientes'));
			}
		}else{
			$r = @json_encode(array('res'=>'bad', 'msj'=> 'Lo sentimos, el archivo que intenta cargar no corresponde con el formato.','tipo'=>'sms_pacientes'));
		}
		//print_r($_FILES['file']);
		print "<script>parent.postMessage( '".$r."' , '".base_url()."')</script>";
	}

	//cron carga paciente y recordatorios sms pacientes
	public function cronCargaMasiva(){
		//CRON CARGA DE PACIENTES procesos: tipo->1 (datos pacientes),  tipo->3 (controles pacientes)
		$fecha_registro = date('Y-m-d m:h:s');
		$procesos = $this->collection_model->get_collection('process', ['$and'=> [['state'=>'1'], ['type'=>'1']]], ['projection'=>['_id'=>'1', 'url'=>'1','type'=>'1', 'entity.id'=>'1'], 'limit'=>'1']);
		if (count($procesos)>0) {
			$_id = (String)$procesos[0]->_id;
			$filePath = $procesos[0]->url;
			$id_entidad = $procesos[0]->entity->id;
			$type_process = $procesos[0]->type;//tipo pacientes

			//abrir el xlsx
			try {
				//cambia el estado a desactivado el proceso
				$result = $this->collection_model->update_collection($_id, 'process', array('state'=>'0'));
				if ($result) {
					if ($type_process=='1') { //sabana de datos pacientes
						$columns_name = $this->collection_model->get_collection('columns_name', ['entity.id'=>$id_entidad],  ['projection'=>['columns'=>'1']]);
						$columns_name = objectToArray($columns_name);
						
						$reader = ReaderEntityFactory::createXLSXReader();
						$reader->setShouldFormatDates(true); //que la hoja tiene datos de formato fecha y hay que activarlo
						//abre el documento
						$reader->open($filePath);
						$i=0;
						//iterar por hoja del documento, suponiendo que solo sea la primera hoja en el archivo xlsx
						foreach ($reader->getSheetIterator() as $sheet) {
							$datos_clear_sql = [];
							$name_old = [];
							$datos_pacientes = [];
							//iteracion por fila de la hoja del documento
				        	foreach ($sheet->getRowIterator() as $k => $row) {
				        		//el primero es para obtener los nombres de las columnas	
				        		if ($k == 1) {
			        				$datos_clear_sql =datosClearSql($row->getCells(), $id_entidad);	
									$name_columns_excel=datosClearName($row->getCells());//elimina las columnas que estan vacios
									$name_news = array_diff($name_columns_excel, array_column($columns_name, 'name'));
									$name_news =datosClearSql($name_news, $id_entidad);//array(name=>valor)

									//actualizo los nombres de las columnas por id_entidad
									if (count($name_news)>0 && count($columns_name)>0) { //REVISAR
										$result_1 = $this->collection_model->update_push_collection(['entity.id'=>$id_entidad], 'columns_name', $name_news);
									}else{ //OK
										$result_1 = $this->collection_model->insert_many_collection('columns_name', $name_news);
									}

									$name_old = array_diff(array_column($columns_name, 'name'), $name_columns_excel);
								}else{
									//insertar los pacientes 
									$datos_paciente = datosClear($row->getCells(), array_column($datos_clear_sql, 'name'));								
									if (count($name_old)>0) {
										$datos_paciente_part1 = array_fill(0, count($name_old), '');
										$datos_paciente_part1 = array_fill_keys($name_old, $datos_paciente_part1);
										$datos_paciente=array_merge($datos_paciente, $datos_paciente_part1);
									}

									//agrega la entidad
									$datos_paciente = $datos_paciente+array('entity'=>array('id'=>$id_entidad));
									$datos_pacientes[] = $datos_paciente;
									/*if ($k==100) {
										break;
									}*/
								}
							}//foreach row

							if (count($datos_pacientes)>0) {
								$result_2 = $this->collection_model->insert_many_collection('patients', $datos_pacientes);
								//var_dump($result_2);
							}
							break; // no need to read more sheets										
						}//foreach  reader of sheet
						$reader->close();
					}
				}
			}catch(Exception $e){
				echo $e->getMessage();
				exit();
			}
		}
	}

	public function cronCargaNotificacionSms()
	{
		//CRON CARGA DE PACIENTES procesos: tipo->1 (datos pacientes),  tipo->3 (controles pacientes)
		$fecha_registro = date('Y-m-d m:h:s');
		$procesos = $this->collection_model->get_collection('process', ['$and'=> [['state'=>'1'], ['type'=>'2']]], ['projection'=>['_id'=>'1', 'url'=>'1','type'=>'1', 'entity.id'=>'1'], 'limit'=>'1']);
		if (count($procesos)>0) {
			$_id = (String)$procesos[0]->_id;
			$filePath = $procesos[0]->url;
			$id_entidad = $procesos[0]->entity->id;
			$type_process = $procesos[0]->type;//tipo pacientes

			//abrir el xlsx
			try {
				//cambia el estado a desactivado el proceso
				$result = $this->collection_model->update_collection($_id, 'process', array('state'=>'0'));
				if ($result) {
					if ($type_process=='2') { //sabana de datos pacientes

						/*Proceso:
						- Se revisa que cada columna no esté vacio
						- Cuando la columna es de tipo telefono, se busca el primero que cumpla: tenga 10 numeros, empiece en 3 y que sea un número (estado_contacto es 1), si no cumple el estado_contacto es 0
						- Cuando la columna es de tipo periodicidad, se calcula la diferencia en la unidad enviada entre la fecha de cargue y la fecha de cita, así obteniendo la primera fecha con la resta de la unidad para enviar el sms periodicamente
						- Cuando la columna es de tipo periodicidad (segunda), se calcula el día exacto con respecto a la unidad de la columna y la fecha cita
						- Estado de envío es 1 */
						$fecha_hoy=date('Y-m-d');

						$type_document = $this->collection_model->get_collection('type_document');
						$type_document = objectToArray($type_document);

						$reader = ReaderEntityFactory::createXLSXReader();
						$reader->setShouldFormatDates(true); //que la hoja tiene datos de formato fecha y hay que activarlo
						//abre el documento
						$reader->open($filePath);
						$i=0;
						//iterar por hoja del documento, suponiendo que solo sea la primera hoja en el archivo xlsx
						foreach ($reader->getSheetIterator() as $sheet) {
							//iteracion por fila de la hoja del documento
							/*estructura excel
								[0] => Fecha de la Cita
					            [1] => Paciente
					            [2] => Tipo de Identificación
					            [3] => Número de Identificación
					            [4] => Teléfono
					            [5] => Teléfono
					            [6] => Teléfono
					            [7] => periodicidad de recordatorio
					            [8] => periodicidad de recordatorio
					            [9] => Recomendaciones
							*/
					        $data_ = [];
				        	foreach ($sheet->getRowIterator() as $k => $row) {
				        		$a=[];
				        		if ($k!=1) {
				        			$e = datosClearWithNull($row->getCells());
				        			if (count($e)>0) {
										if ($e[0]!='' && $e[2]!='') {
											$a['nombre'] = $e[2]; //nombre paciente
											$a['tipo_documento'] = ($e[3]!='' && array_key_exists($e[3], $type_document)==true) ? $type_document[$e[3]] : '';
											$a['numero_documento'] = $e[4]; 
											$a['telefono'] = ''; //columna telefono
											$a['estado_contacto']="0";//no tiene numero telefonico valido
											$estado_contacto="0";
											if ($e[5]!='' || $e[6]!='' || $e[7]!='') {
												for ($j=5; $j < 8; $j++) {
													//validaciones telefono
													if (is_int($e[$j])==true && strlen($e[$j])==10 && substr($e[$j], 0, 1)=="3") {
														$a['telefono']=$e[$j];
														$a['estado_contacto']="1";
														$estado_contacto="1";
														break;
													}
												}
											}
											//columna Fecha de la Cita
											$fecha_cita=date('Y-m-d',strtotime($e[0]));
											$a['fecha_cita']=$fecha_cita;
											
											$a['hora_cita']=$e[1]; //columna hora de la Cita

											//columna periodicidad de recordatorio 1
											$newdate1='';
											if ($e[8]!='' && $estado_contacto=="1") {
												$a['periodicidad']=$e[8];
												//se calcula la primera fecha de la periodicidad 1: unidad, fecha cita, fecha hoy
												$newdate1=periodicidadRecordatorios($e[8], $fecha_cita, $fecha_hoy);
											}else{
												$a['periodicidad']='';
											}

											//columna periodicidad de recordatorio
											$newdate2='';
											if ($e[9]!='' && $estado_contacto=="1") {
												$a['periodicidad_anticipada']=$e[9];
												//se calcula la fecha anterior de la periodicidad 2: unidad, fecha cita, fecha hoy
												$newdate2=periodicidadRecordatorios($e[9], $fecha_cita, $fecha_hoy);
											}else{
												$a['periodicidad_anticipada']='';
											}
											
											$a['recomendacion']=$e[10]; //columna Recomendaciones

											$a['fecha_envio_p1']=$newdate1;
											$a['fecha_envio_p2']=$newdate2;
											
											$a['estado_envio']= (($newdate1=='' || $newdate1=='NULL') && ($newdate2=='' || $newdate2=='NULL')) ? '0' : '1' ;
											$a['entity']=['id'=>$id_entidad];
											$data_[] = $a;
										}
									}
				        		}
				        	}
			        		$result =  $this->collection_model->insert_many_collection('patients_sms', $data_);
			        		if ($result) {
			        			print json_encode(array("res"=>"ok"));
			        		}else{
			        			print json_encode(array("res"=>"bad", "msj"=>'Error al subir el archivo'));
			        		}
			        		break; // no need to read more sheets
						}//foreach  reader of sheet
						$reader->close();
					}
				}
			}catch(Exception $e){
				print json_encode(array("res"=>"bad", "msj"=>$e->getMessage()));
				//echo $e->getMessage();
				exit();
			}
		}
	}

	//obtiene latitud y longitud de la direccion del paciente por el API Google
	public function cronUpdateDireccionPacientes()
	{
		$this->load->view("batch/cron_update_direccion_pacientes");
	}
}

//celdas
function datosClearSql($e, $id_entidad){
	$a =array_map(function($cell) use ($id_entidad) {
		//limpia de caracteres y saltos de lineas
		$cell = str_replace(array("\r\n", "\n\r", "\r", "\n"), " ", $cell);		
		$cell = str_replace(array("'", '"', "|", "?", "¡", "¿", "!",), "", $cell);
		$cell = str_replace(array("."), "_", $cell);
		//$cell = utf8_decode($cell);
		return array( 'entity'=>['id'=>$id_entidad] , 'name'=>$cell);
  	}, $e);
  	return $a;
}

function datosClearName($e){
	$a = [];
	if (count($e)>0) {
		for ($i=0; $i < count($e); $i++) { 
			if (!empty($e[$i]) && $e[$i]!='' && $e[$i]!='NULL') {
				$cell = $e[$i];
				$cell = str_replace(array("\r\n", "\n\r", "\r", "\n"), " ", $cell);		
				$cell = str_replace(array("'", '"', "|", "?", "¡", "¿", "!"), "", $cell);
				$cell = str_replace(array("."), "_", $cell);
				$a[]=$cell;
			}
		}
	}
  return $a;
}

//celdas, nombres de  las columnas
function datosClear($e, $f){
	$a = [];
	if (count($e)==count($f)) {
		for ($i=0; $i < count($e); $i++) { 
			if (!empty($f[$i]) && $f[$i]!='' && $f[$i]!='NULL') {
				$cell = $e[$i];
				$cell = str_replace(array("\r\n", "\n\r", "\r", "\n"), " ", $cell);		
				$cell = str_replace(array("'", '"', "|", "?", "¡", "¿", "!"), "", $cell);
				//$cell = str_replace(array("."), "_", $cell);
				$a=$a+[$f[$i]=>$cell];
			}
		}
	}
		
  return $a;
}

function datosClearWithNull($e){
	$a =array_map(function($cell) {
		//limpia de caracteres y saltos de lineas
		$cell = str_replace(array("\r\n", "\n\r", "\r", "\n"), " ", $cell);		
		$cell = str_replace(array("'", '"', "|", "?", "¡", "¿", "!",), "", $cell);
		$cell = str_replace(array("."), "_", $cell);
		//$cell = utf8_decode($cell);
		return $cell;
  	}, $e);
  	return $a;
}
function remove_accent($str){
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str); 
}

function objectToArray ( $object ) {
	if(!is_object($object) && !is_array($object)) {
  	return $object;
	}  
	return array_map( 'objectToArray', (array) $object );
}

function periodicidadRecordatorios($unidad,$fecha_cita,$fecha_hoy)
{
	$newDate='NULL';//no se repite
	$unidad=strtolower($unidad);
	if ($unidad=='cada semana' || $unidad=='cada día') {
		$datetime1 = new DateTime($fecha_hoy);
		$datetime2 = new DateTime($fecha_cita);
		$interval = $datetime1->diff($datetime2);
		if ($unidad=='cada semana') {//cada semana
			$semanas = intval($interval->format('%a') / 7); //[semana]
			if($semanas!=0){
				$newDate = strtotime ( '-'.$semanas.' week' , strtotime ( $fecha_cita ) ) ;
				$newDate = date ( 'Y-m-d' , $newDate );
			}else{
				$newDate = '';
			}
		}else{//todos los días
			$dias= $interval->format('%a');
			$newDate = strtotime ( '-'.$dias.' day' , strtotime ( $fecha_cita ) ) ;
			$newDate = date ( 'Y-m-d' , $newDate );
		}
	}
	if ($unidad=='un día antes') {
		$newdate22 = strtotime ( '-1 day' , strtotime ( $fecha_cita ) ) ;
		$newDate = date ( 'Y-m-d' , $newdate22 );
		if (date ( 'Y-m-d' , $newdate22 )<$fecha_hoy) {
			$newDate='';
		}
	}
	if ($unidad=='todos los días laborales') {
		$newDate=diasLaborales($fecha_hoy,$fecha_cita);
	}
	if ($unidad=='anualmente') {
		$newDate=date ( 'Y-m-d' ,strtotime ( '-1 year' , strtotime ( $fecha_cita ) ) );
		if ($newDate<$fecha_hoy) {//la fecha calculada es menor a la fecha hoy se envia vacio
			$newDate='';
		}else{
			$newDate=$newDate;
		}
	}
	//en personalizar suponiendo que venga una fecha especifica -> Personalizar: 20-02-2019
	if (strrpos($unidad,'personalizar')!==false) {
		$texto=explode(' ',$unidad);
		if (count($texto)>1) {
			$fecha_p = $texto[1];
			//suponiendo que el formato sea d/m/Y
			if (strrpos($fecha_p,'/')!==false) {
				$componentes_fecha=explode('/', $fecha_p);
				$fecha_p=str_replace('/', '-', $fecha_p);
			}else{//el formato sea d-m-Y
				$componentes_fecha=explode('-', $fecha_p);
			}
			if (count($componentes_fecha)==3 && checkdate($componentes_fecha[1], $componentes_fecha[0], $componentes_fecha[2])) {
				$newDate=date('Y-m-d',strtotime($fecha_p));
			}else{
				$newDate='';
			}
		}else{
			$newDate='';
		}
	}
	//falta definir como será
	/*if (strrpos($unidad, 'cada mes el día')) {
		$cosas=explode(' ', $unidad);
		for ($i=0; $i < count($cosas); $i++) { 
			if (is_int($cosas[$i])==true){
				$date1=date('Y-m-d',strtotime(date('Y-m',strtotime($fecha_cita))));
			}
		}
	}*/
	//falta definir el formato
	/*if (strrpos($unidad, 'cada semana los días')) {
		$cosas=explode(' ', $unidad);
		for ($i=0; $i < count($cosas); $i++) { 
			if (is_int($cosas[$i])==true){
				$date1=date('Y-m-d',strtotime(date('Y-m',strtotime($fecha_cita))));
			}
		}
	}*/

	return $newDate;
}

function diasLaborales($fecha_inicio, $fecha_fin) {
	$date_1 = date_create($fecha_inicio);
	$date_2 = date_create($fecha_fin);
	if ($date_1 > $date_2) return '';
	while ($date_1 <= $date_2) {
		$day_week = $date_1->format('w');
		if ($day_week > 0 && $day_week < 6) {
			return $date_1->format('Y-m-d');
			break;
		}
		date_add($date_1, date_interval_create_from_date_string('1 day'));
	}
	return '';
}