<?php if ( ! defined('BASEPATH')) exit('Direct access allowed');
class LanguageSwitcher extends CI_Controller
{
   public function __construct() {
       parent::__construct();
   }
   function switchLang($language = "") {
       $language = ($language != "") ? $language : "spanish";
       $this->session->set_userdata('site_lang', $language);
       print json_encode(array('res'=>'ok', 'url'=>$_SERVER['HTTP_REFERER']));
       //redirect(base_url());//
   }
}