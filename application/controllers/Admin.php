<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	 
	private $type_campaign;

	function __construct() {
		parent::__construct();
		$this->load->model('administrador_model');
		$this->load->model('collection_model');
		$this->type_campaign = array('Mensual', 'Fecha');
	}

	public function index()
	{	
		if (is_null($this->session->userdata('site_lang'))) {
			$this->session->set_userdata('site_lang','spanish');
		}
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");
		redirect(base_url().'admin/dashboard');
	}	

	public function dashboard($offset=0){

		$algo = $this->collection_model->get_collection('entities', array('state'=>'1'));
		foreach ($algo as $document) {
			echo '<pre>';
		print_r($document);
		echo '</pre>';
		}
		exit();


		$login = $_SESSION;
		if(!$login) redirect(base_url()."web/login");

		$vista="5e33084efd61ee8807387414";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$busqueda=FALSE;
		if($this->input->post('tbusqueda', TRUE)!=''){
			$this->session->set_userdata('searchtbusqueda', "");
		}
		if(isset($_POST["filtro"])) {
			$busqueda=$this->input->post('filtro', TRUE);
			$this->session->set_userdata('searchtbusqueda', $busqueda);
		}else{
			if (($this->session->userdata('searchtbusqueda')!='')) {
				$busqueda = $this->session->userdata('searchtbusqueda');
			}
		}

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}

		//fecha_registro, telefonico, sexo, nombre, apellido, municipio, departamento, ips
		$data_paciente_projection = [];
		$data_name_columns_ = [];
		$data_name_columns = ['Nombre', 'Apellido', 'Teléfono', 'Municipio', 'Sexo', 'IPS', 'Departamento Residencia'];
		print_r($id_entidad);
		$name_columns_by_entity = $this->collection_model->get_collection('columns_configuration', ['$and'=>[['required'=>'1'], ['entity.id'=>$id_entidad]]], ['projection'=>['columns'=>'1', 'name'=>'1']]);
		echo '<pre>';
		print_r($name_columns_by_entity);
		echo '</pre>';
		if (count($name_columns_by_entity)>0) {
			foreach ($name_columns_by_entity as $k_ncbe => $v_ncbe) {
				if (count($v_ncbe->columns)>0 && in_array($v_ncbe->name, $data_name_columns)) {
					for ($j=0; $j < count($v_ncbe->columns); $j++) { 
						$data_paciente_projection = array_merge($data_paciente_projection, [$v_ncbe->columns[$j]=>'1']);
					}
					$data_name_columns_[$v_ncbe->name] = $v_ncbe->columns;
				}
			}
		}
		$data['id_entidad_']=$id_entidad;
		$data_paciente_projection = array_merge($data_paciente_projection, ['registration_date'=>'1', 'state_risk'=>'1']);
	    $filter= ($busqueda!==false) ? ['$and'=>[ ['entity.id'=>$id_entidad], ['name'=> new MongoDB\BSON\Regex( "$busqueda", 'i' )]] ] : ['entity.id'=>$id_entidad];	    
		$total = $this->collection_model->get_total('patients', $filter );
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/dashboard";
		$config['per_page'] = 12;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ';
		$config['prev_link'] = ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
	    $data['offset'] = $offset;
		$query = $this->collection_model->get_collection('patients', $filter, ['limit'=>$config['per_page'], 'skip'=>$offset, 'projection'=>$data_paciente_projection]);
		$query = objectToArray($query);
		$data['pacientes'] = $query;
		$filter_color=array_merge($filter, ['state_risk.name'=>'Rojo intenso']);
		$data['RojoIntenso'] = $this->collection_model->get_total('patients', $filter_color);
		$filter_color=array_merge($filter, ['state_risk.name'=>'Rojo']);
		$data['Rojo'] = $this->collection_model->get_total('patients', $filter_color);
		$filter_color=array_merge($filter, ['state_risk.name'=>'Amarillo']);
		$data['Amarillo'] = $this->collection_model->get_total('patients', $filter_color);
		$filter_color=array_merge($filter, ['state_risk.name'=>'Verde']);
		$data['Verde'] = $this->collection_model->get_total('patients', $filter_color);
		//buscar los duplicados de los pacientes
		$data['duplicados'] = array();
		$data['name_columns'] = $data_name_columns_;
		$data['total'] = $total;
		$data['colores'] = ['Rojo intenso'=>'rojo-intenso', 'Rojo'=>'rojo', 'Amarillo'=>'amarillo', 'Verde'=>'verde'];
		$data["titulo"] = "Dashboard";
		$data["active"] = "dashboard";
		$this->load->view("admin/dashboard",$data);
	}

	public function perfil(){		
		$_id=$_POST['id'];//numero_paciente
		$login = $this->session->logged_in;
		if(!$login || !$_id) redirect(base_url()."web/login");

		$vista = "5e33098efd61ee8807387415";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');


		$id_entidad = $this->collection_model->get_collection('patients', ['_id' => new MongoDB\BSON\ObjectId($_id)], ['projection'=>['entity'=>'1']]);
		$id_entidad = $id_entidad[0]->entity->id;

		$data_paciente_projection = [];
		$data_name_columns_ = [];
		$data_name_columns = ['Nombre', 'Apellido', 'Teléfono', 'Municipio', 'Sexo', 'IPS', 'Departamento Residencia', 'Número documento', 'Fecha de nacimiento', 'Dirección', 'Fumador', 'Colesterol total (Último valor)', 'Tensión Arterial Sistólica (Último valor)', 'Diabetes Mellitus'];
		$name_columns_by_entity = $this->collection_model->get_collection('columns_configuration', ['$and'=>[['required'=>'1'], ['entity.id'=>$id_entidad]]], ['projection'=>['columns'=>'1', 'name'=>'1']]);
		if (count($name_columns_by_entity)>0) {
			foreach ($name_columns_by_entity as $k_ncbe => $v_ncbe) {
				if (count($v_ncbe->columns)>0 && in_array($v_ncbe->name, $data_name_columns)) {
					for ($j=0; $j < count($v_ncbe->columns); $j++) { 
						$data_paciente_projection = array_merge($data_paciente_projection, [$v_ncbe->columns[$j]=>'1']);
					}
					$data_name_columns_[$v_ncbe->name] = $v_ncbe->columns;
				}
			}
		}
		$data_paciente_projection = array_merge($data_paciente_projection, ['registration_date'=>'1', 'state_risk'=>'1', 'score_risk'=>'1', 'filtracion_glomerular'=>'1']);
		$paciente = $this->collection_model->get_collection('patients', ['_id' => new MongoDB\BSON\ObjectId($_id)], ['projection'=>$data_paciente_projection]);
		$paciente=objectToArray($paciente);
		$data['paciente'] = $paciente;
		$data['name_columns'] = $data_name_columns_;
		$timestamp = (String) $paciente[0]['registration_date']['milliseconds'];
		$dateInLocal = date("Y-m-d H:m:s", (int)$timestamp/1000);
		$telefono = '';
		if (isset($data_name_columns_['Teléfono'])) {
			for ($ip=0; $ip<count($data_name_columns_['Teléfono']); $ip++) {
				if (isset($paciente[0][$data_name_columns_['Teléfono'][$ip]]) && strlen($paciente[0][$data_name_columns_['Teléfono'][$ip]])==10 && substr($paciente[0][$data_name_columns_['Teléfono'][$ip]], 0, 1)=="3") {
					$telefono=$paciente[0][$data_name_columns_['Teléfono'][$ip]];
					break;
				}
			}
		}
		$data['telefono']=$telefono;
		$name_patient = '';
		if (isset($data_name_columns_['Nombre'])) {
			for ($ip=0; $ip<count($data_name_columns_['Nombre']); $ip++) {
				if (isset($paciente[0][$data_name_columns_['Nombre'][$ip]])) {
					$name_patient.=" ".$paciente[0][$data_name_columns_['Nombre'][$ip]];
				}	                  					
			}
		}
		$data['name_patient'] = $name_patient;	
		$apellido_patient = '';
		if (isset($data_name_columns_['Apellido'])) {
			for ($ip=0; $ip<count($data_name_columns_['Apellido']); $ip++) {
				if (isset($paciente[0][$data_name_columns_['Apellido'][$ip]])) {
					$apellido_patient.=" ".$paciente[0][$data_name_columns_['Apellido'][$ip]];
				}
			}
		}
		$data['apellido_patient'] = $apellido_patient;
		$data['edad'] = (isset($data_name_columns_['Fecha de nacimiento']) && isset($paciente[0][$data_name_columns_['Fecha de nacimiento'][0]]) && $paciente[0][$data_name_columns_['Fecha de nacimiento'][0]]!='') ?(  (int) date('Y') - (int) date('Y', strtotime($paciente[0][$data_name_columns_['Fecha de nacimiento'][0]])) ) : '---';
		$data['colores'] = ['Rojo intenso'=>'rojo-intenso', 'Rojo'=>'rojo', 'Amarillo'=>'amarillo', 'Verde'=>'verde'];
		$data['id'] = $_id;
		$data['id_entidad']=$id_entidad;
		$mensajes_personalizados = $this->collection_model->get_collection('log_envios_personalizados', ['patient.id'=>$_id], ['limit'=>2, 'projection'=>['mensaje'=>'1']]);
		$data['mensajes_enviados']=$mensajes_personalizados;
		$condiciones_trazadoras = $this->collection_model->get_collection('condiciones_trazadoras');
		$condiciones_trazadoras=objectToArray($condiciones_trazadoras);
		$copy_condiciones_trazadoras=$condiciones_trazadoras;
		$ids_condiciones = array_column($condiciones_trazadoras, '_id');
		$ids_condiciones = array_column($ids_condiciones, 'oid');
		$pipeline = [
		    [ '$match' => ['patient.id' => $_id, 'condicion_trazadora.id'=>['$in'=>$ids_condiciones]] ],
		    [ '$project' => ['last'=>['$arrayElemAt'=>['$data',-1]], 'condicion_trazadora'=>1] ]
		];
		$historial_condiciones_patient = $this->collection_model->get_aggregate('historial_condiciones_trazadoras', $pipeline);
		if (count($historial_condiciones_patient)>0) {
			foreach ($historial_condiciones_patient as $k_1 => $v_1) {
				$id_camp=$v_1->condicion_trazadora->id;
				$activas=array_filter($ids_condiciones, function ($tT) use ($id_camp) 
				{
					if ($id_camp==$tT) {
						return $tT;
					}
				});
				if (count($activas)>0) {
					$posicion_activas=array_keys($activas);
					$condiciones_trazadoras[$posicion_activas[0]]['data'] = (array_key_exists(0, $v_1->last)) ? $v_1->last[0] : $v_1->last;
				}
			}
		}
		$data['condiciones_trazadoras']=$condiciones_trazadoras;
		$data['copy_condiciones_trazadoras']=$copy_condiciones_trazadoras;
		$antecedentes_relevantes = $this->collection_model->get_collection('antecedentes_relevantes');
		$antecedentes_relevantes=objectToArray($antecedentes_relevantes);
		$copy_antecendentes_relevantes = $antecedentes_relevantes;
		$ids_antecedentes = array_column($antecedentes_relevantes, '_id');
		$ids_antecedentes = array_column($ids_antecedentes, 'oid');		
		$pipeline = [
		    [ '$match' => ['patient.id' => $_id, 'antecedente_relevante.id'=>['$in'=>$ids_antecedentes]] ],
		    [ '$project' => ['last'=>['$arrayElemAt'=>['$data',-1]], 'antecedente_relevante'=>1] ]
		];
		$historial_antecendentes_patient = $this->collection_model->get_aggregate('historial_antecedentes_relevantes', $pipeline);
		if (count($historial_antecendentes_patient)>0) {
			foreach ($historial_antecendentes_patient as $k_1 => $v_1) {
				$id_camp=$v_1->antecedente_relevante->id;
				$activas=array_filter($ids_antecedentes, function ($tT) use ($id_camp) 
				{
					if ($id_camp==$tT) {
						return $tT;
					}
				});
				if (count($activas)>0) {
					$posicion_activas=array_keys($activas);
					$antecedentes_relevantes[$posicion_activas[0]]['data'] = (array_key_exists(0, $v_1->last)) ? $v_1->last[0] : $v_1->last;
				}					
			}
		}
		$data['antecedentes_relevantes']=$antecedentes_relevantes;
		$data['copy_antecendentes_relevantes']=$copy_antecendentes_relevantes;
		$data['name_indicators']=$this->collection_model->get_collection('indicators',['$and'=>[['state'=>'1'], ['entity.id'=>$id_entidad]]], ['limit'=>3, 'projection'=>['information'=>'1']]);
		$pipeline = [
		    [ '$match' => ['patient.id' => $_id] ],
		    [ '$project' => ['last'=>['$arrayElemAt'=>['$observaciones.observacion',-1]]] ]
		];
		$data['observacion'] = $this->collection_model->get_aggregate('historial_observaciones_patient', $pipeline);

		$data["titulo"] = "Perfil";
		$data["active"] = "dashboard";
		$this->load->view("admin/paciente/perfil",$data);
	}

	public function paginationMensajesByPatient($_id, $offset=0){

		$total = $this->collection_model->get_total('log_envios_personalizados', ['patient.id'=>$_id]);
	    $config['total_rows'] =$total;
		$config["base_url"] = base_url()."admin/paginationMensajesByPatient/$_id/";
		$config['per_page'] = 10;
		$config['uri_segment'] = '4';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ';
		$config['prev_link'] = ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
		$query=$this->collection_model->get_collection('log_envios_personalizados', ['patient.id'=>$_id], ['limit'=>$config['per_page'], 'skip'=>$offset, 'projection'=>['channel'=>'1', 'mensaje'=>'1', 'encuesta'=>'1', 'user'=>'1', 'registration_date'=>'1']]);
		$data["msj"] = $query;
		$this->load->view('admin/paciente/pagination_mensajes_by_patient',$data);		  
	}

	public function configuracionUsuarios($offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");
		
		$vista="5e330a9bfd61ee8807387420";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$total = $this->collection_model->get_total('users', array('state'=>'1'));

		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/configuracionUsuarios";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('users', array('state'=>'1'), array('limit'=>$config['per_page'], 'skip'=>$offset));
	    $data["usuarios"]=$query;
	    $data["rol"]=$this->collection_model->get_collection('roles', array('state'=>'1'));
	    $data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
	   
	    $data["active"]='configuracionUsuarios';
		$data["titulo"] = "Configuración usuarios";

		$this->load->view("admin/usuarios/configuracion_usuarios",$data);
	}

	public function configuracionRoles($offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");
		
		$vista="5e330a9bfd61ee8807387421";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');

		$total = $this->collection_model->get_total('roles', array('state'=>'1'));
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/configuracionRoles";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('roles', array('state'=>'1'), array('limit'=>$config['per_page'], 'skip'=>$offset));
	    $data["roles"]=$query;

	  	$data["active"]='configuracionUsuarios';
	  	$data["titulo"] = "Configuración roles";

		$this->load->view("admin/usuarios/configuracion_roles",$data);
	}

	public function verRol($_id,$offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");
		
		$vista = "5e330a9bfd61ee8807387429";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');

		$permisos = $this->collection_model->get_collection('roles', ['_id' => new MongoDB\BSON\ObjectId($_id)], ['projection' => ['views'=>1]]);
		if (count($permisos)>0) {
			$permisos= objectToArray($permisos[0]->views);
			$permisos = array_column($permisos, 'id');
		}
		$data["permisos"] = $permisos;

		$total = $this->collection_model->get_total('views');
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/verRol/".$_id;
		$config['per_page'] = 10;
		$config['uri_segment'] = '4';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('views', false, array('limit'=>$config['per_page'], 'skip'=>$offset));
	    $data["vistas"]=$query;
	    $data['id_rol']=$_id;

	    $data["titulo"] = "Configuración roles";
	    $data["active"]='configuracionUsuarios';

		$this->load->view("admin/usuarios/configuracion_vistas",$data);
	}

	public function configuracionEntidades($offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");
		
		$vista = "5e330a9bfd61ee8807387422";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');

		$total=$this->collection_model->get_total('entities', array('state'=>'1'));
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/configuracionEntidad";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('entities', array('state'=>'1'), array('limit'=>$config['per_page'], 'skip'=>$offset));
	    $data["entidades"]=$query;

	    $data["active"]='configuracionEntidades';
	    $data["titulo"] = "Configuración entidades";

		$this->load->view("admin/configuracion_entidades",$data);
	}

	public function cargaMasiva()
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee8807387417";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}

		$data["titulo"] = "Carga masiva";
		$data["active"] = "carga_masiva";

		$this->load->view("admin/carga_masiva",$data);
	}

	public function configuracionParametros($offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect("web/login");
		
		$vista = "5e330a9bfd61ee8807387423";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));	
		}
		$data['id_entidad'] = $id_entidad;

		$searchSystem = false;
		if(isset($_POST["searchSystem"])) {
			$searchSystem=$this->input->post('searchSystem', TRUE);
			$this->session->set_userdata('searchSystem', $searchSystem);
		}else{
			if (($this->session->userdata('searchSystem')!='')) {
				$searchSystem = $this->session->userdata('searchSystem');
			}
		}

		$filter= ($searchSystem!==false) ? ['$and'=>[ ['entity.id'=>$id_entidad], ['state'=>'1'], ['name'=> new MongoDB\BSON\Regex( "$searchSystem", 'i' )]] ] : ['$and'=> [ ['state'=>'1'], ['entity.id'=>$id_entidad] ]];
		$total = $this->collection_model->get_total('columns_configuration', $filter );
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/configuracionParametros";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('columns_configuration',$filter, array('limit'=>$config['per_page'], 'skip'=>$offset));
	    $data["columnas_configuracion"] = $query;
		
		$columnas = $this->collection_model->get_total('columns_name', array('entity.id'=>$id_entidad));
		$data['columnas']=$columnas;

		$tags_seleccionados = $this->collection_model->get_collection('columns_configuration', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1'], ['$nor'=>[["columns"=>['$size'=>0]]] ] ]], ['projection'=>['columns'=>'1']]);		
		$tags_seleccionados=objectToArray($tags_seleccionados);
		$tags_seleccionados = array_column($tags_seleccionados, 'columns');
		$tags_final = [];
		for ($i=0; $i < count($tags_seleccionados); $i++) { 
			for ($j=0; $j < count($tags_seleccionados[$i]); $j++) { 
				$tags_final[]=$tags_seleccionados[$i][$j];
			}
		}
		$data['tags_seleccionados'] = $tags_final;
		$data["titulo"]="Configuracion Parametros";
		$data["active"]='configuracionParametros';
		$this->load->view("admin/configuracion_parametros",$data);
	}

	//listadode indicadores configurados
	public function configuracionIndicadores($offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect("web/login");
		
		$vista = "5e330a9bfd61ee8807387424";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');

		$this->load->model('Pacientes_model');

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));	
		}
		$data['id_entidad'] = $id_entidad;

		$total = $this->collection_model->get_total('indicators',['$and'=>[['entity.id'=>$id_entidad], ['state'=>['$in'=>['1','0']]]]]);
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/configuracionIndicadores";
		$config['per_page'] = 3;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('indicators',['$and'=>[['entity.id'=>$id_entidad], ['state'=>['$in'=>['1','0']]]]], array('limit'=>$config['per_page'], 'skip'=>$offset), ['projection'=>['_id'=>'1', 'information.name'=>'1', 'information.description'=>'1', 'state'=>'1', 'state_dashboard'=>'1'], 'registration_date'=>'1']);
	    $data["indicadores"]=$query;

		$data["titulo"]="Indicadores";
		$data["active"]='configuracionIndicadores';
		$this->load->view("admin/indicadores/configuracion_indicadores",$data);
	}

	//configuracion de tipo indicadores, tipo de periodicidad y filtros de indicadores
	public function configuracionGeneralIndicadores()
	{
		$login = $this->session->logged_in;
		if(!$login) redirect("web/login");
		
		$vista = "5e330a9bfd61ee8807387428";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');	

		$login = $this->session->logged_in;
		if(!$login) redirect("web/login");
		if($this->session->userdata('id_rol') != "5e3307a1fd61ee8807387411") redirect("admin/dashboard");

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}

		$data['id_entidad'] = $id_entidad;
		//$data['tipo_periodicidad'] = $this->collection_model->get_collection('entities_periodicity', ['entity.id'=>$id_entidad]);
		$data['tipo_indicadores'] = $this->collection_model->get_collection('entities_indicators', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]], ['projection'=>['_id'=>'1', 'required'=>'1', 'name'=>'1']]);
		$data['tipo_filtro'] = $this->collection_model->get_collection('entities_filters', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]], ['projection'=>['_id'=>'1', 'name'=>'1', 'columns'=>'1', 'mode'=>'1']]);

		$data["titulo"]="Configuracion General";
		$data["active"]='configuracionIndicadores';
		$this->load->view("admin/indicadores/configuracion_general", $data);
	}

	public function nuevoIndicador()
	{
		$login = $this->session->logged_in;
		if(!$login) redirect("web/login");
		
		$vista = "5e330a9bfd61ee8807387425";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}
		$data['id_entidad'] = $id_entidad;

		$data['periodicidad'] = $this->collection_model->get_collection('entities_periodicity', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]]);//el ultimo año, ultimo semestre, hace 6 meses
		$data['tipo_indicadores'] = $this->collection_model->get_collection('entities_indicators', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]]);
		$data['estandar'] = $this->collection_model->get_collection('type_standar');

		$data["titulo"]="Nuevo Indicador";
		$data["active"]='configuracionIndicadores';
		$this->load->view("admin/indicadores/nuevo_indicador",$data);
	}

	public function editarIndicador($_id)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");
		
		$vista = "5e330a9bfd61ee8807387426";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');	
		$informacion = $this->collection_model->get_collection('indicators', ['_id' => new MongoDB\BSON\ObjectId($_id)]);
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$id_entidad=$informacion[0]->entity->id;
		}
		
		$data['informacion'] = $informacion[0];
		$data['id_entidad'] = $id_entidad;	

		$data['periodicidad'] = $this->collection_model->get_collection('entities_periodicity', ['entity.id'=>$id_entidad]);//el ultimo año, ultimo semestre, hace 6 meses
		$data['tipo_indicadores'] = $this->collection_model->get_collection('entities_indicators', ['entity.id'=>$id_entidad]);
		$data['estandar'] = $this->collection_model->get_collection('type_standar');
		
		$data["titulo"]="Editar Indicador";
		$data["active"]='configuracionIndicadores';
		$this->load->view("admin/indicadores/editar_indicador",$data);
	}

	public function indicadores()
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee880738741f";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}
		$data['id_entidad'] = $id_entidad;

		$eventos =  $this->collection_model->get_collection('indicators', ['$and'=>[['entity.id'=>$id_entidad],['state'=>'1']]], ['projection'=>['_id'=>'1', 'information'=>'1']]);
		$data['genero'] = $this->administrador_model->get_by_tipo_filtro('Género', $id_entidad);;
		$data['edad'] = $this->collection_model->get_collection('range_age');
		$data['sede'] = $this->administrador_model->get_by_tipo_filtro('Sede', $id_entidad);
		$data['profesional'] = $this->administrador_model->get_by_tipo_filtro('Profesional', $id_entidad);
		$data['especialista'] = $this->administrador_model->get_by_tipo_filtro('Especialista', $id_entidad);
		$data['eventos'] = $eventos;
		$data['estandar'] = $this->collection_model->get_collection('type_standar');
		$data['diagnostico'] = $this->administrador_model->get_by_tipo_filtro('Diagnóstico', $id_entidad);
		$data['discapacidad'] = $this->administrador_model->get_by_tipo_filtro('Discapacidad', $id_entidad);
		$data['procedencia'] = $this->administrador_model->get_by_tipo_filtro('Procedencia', $id_entidad);
		$data['area'] = $this->administrador_model->get_by_tipo_filtro('Área', $id_entidad);
		$ids_eventos = array_column(objectToArray($eventos), '_id');
		$data['ids_eventos']= array_column($ids_eventos, 'oid');

		$data["titulo"]="Indicadores";
		$data["active"] = "indicadores";
		$this->load->view("admin/indicadores/indicadores",$data);
	}

	public function pacienteMensajeria($offset=0){
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee880738741e";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad1', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad1')!='')) {
					$entidad_search = $this->session->userdata('searchentidad1');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}

		$total = $this->collection_model->get_total('patients_sms', ['entity.id'=>$id_entidad]);
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/pacienteMensajeria";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('patients_sms', ['entity.id'=>$id_entidad], ['limit'=>$config['per_page'], 'skip'=>$offset, 'projection'=>['numero_documento'=>'1', 'nombre'=>'1', 'telefono'=>'1', 'fecha_cita'=>'1', 'fecha_envio_p1'=>'1', 'fecha_envio_p2'=>'1', 'estado_envio'=>'1'], 'sort'=>['estado_envio'=>-1, 'fecha_cita'=>1]]);
	    $data["recordatorios"]=$query;

	    $data["titulo"] = "Recordatorios de pacientes";
	    $data["active"]='pacienteMensajeria';
		$this->load->view("admin/carga_paciente_mensajeria",$data);
	}

	public function bancoMensajes($offset=0){
		$login = $this->session->logged_in;
		if(!$login) redirect("web/login");

		$vista = "5e330a9bfd61ee880738741c";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect('admin/permisos');
		
		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad4', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad4')!='')) {
					$entidad_search = $this->session->userdata('searchentidad4');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}

		$total = $this->collection_model->get_total('message_banck', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]]);
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/bancoMensajes";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('message_banck', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]], ['limit'=>$config['per_page'], 'skip'=>$offset, 'projection'=>['name'=>'1', 'state_message'=>'1', 'registration_date'=>'1']]);
		$data["banco"] = $query;
		$data["id_entidad"]=$id_entidad;

		$data["titulo"] = "Banco de mensajes";
		$data["active"] = "bancoMensajes";
		$this->load->view("admin/mensajeria/bancoMensajes",$data);
	}

	public function getbancoMensajes($id){
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");
		if(!$id) redirect(base_url()."admin/bancoMensajes");

		$vista = "5e330a9bfd61ee880738741d";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$banco = $this->collection_model->get_total('message_banck', ['_id' => new MongoDB\BSON\ObjectId($id)]);
		if($banco == 0) redirect(base_url()."admin/bancoMensajes");

		$mensaje = $this->collection_model->get_collection('message_banck', ['_id' => new MongoDB\BSON\ObjectId($id)], ['projection'=>['name'=>'1', 'message'=>'1',  'entity'=>'1']]);		
		$data["mensaje"] = $mensaje;
		$data["canales"] = $this->collection_model->get_collection('type_channel');

		$data["titulo"] = "Banco de mensajes";
		$data["active"] = "bancoMensajes";
		$this->load->view("admin/mensajeria/getBanco",$data);
	}

	public function mensajeria($offset=0){
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee8807387418";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');
		
		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad2', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad2')!='')) {
					$entidad_search = $this->session->userdata('searchentidad2');
				}
			}			
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}

		$total = $this->collection_model->get_total('messaging_campaigns', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]]);
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/mensajeria";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    $query=$this->collection_model->get_collection('messaging_campaigns', ['$and'=>[['entity.id'=>$id_entidad], ['state'=>'1']]], ['limit'=>$config['per_page'], 'skip'=>$offset, 'projection'=>['name'=>'1', 'state_message'=>'1', 'registration_date'=>'1', 'channel'=>'1', 'type_campaign'=>'1']]);
		$data["campanas"] = $query;

		$data["id_entidad"] = $id_entidad;
		$data["titulo"] = "Mensajería";
		$data["active"] = "mensajeria";
		$this->load->view("admin/mensajeria/mensajeria",$data);
	}

	public function newMensajeria(){
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee8807387419";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));	
		}
		$data['id_entidad'] = $id_entidad;
		$data["canal"] = $this->collection_model->get_collection('type_channel');
		$data["datosClinicos"] = $this->collection_model->get_collection('data_clinical');
		$data["tipoCampanas"] = $this->type_campaign;

		$data["titulo"] = "Mensajería";
		$data["active"] = "mensajeria";
		$this->load->view("admin/mensajeria/newMensajeria",$data);
	}

	public function getMensajeria($id){
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee880738741a";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$campana = $this->collection_model->get_collection('messaging_campaigns', ['_id' => new MongoDB\BSON\ObjectId($id)]);
		$data["campana"] = $campana;

		$data["titulo"] = "Campaña";
		$data["active"] = "mensajeria";
		$this->load->view("admin/mensajeria/getMensajeria",$data);
	}

	public function ubicacion(){
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee880738741b";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		$data["id_entidad"] = $id_entidad;
		$data['colores'] = ['Rojo intenso', 'Rojo', 'Amarillo', 'Verde'];

		$total = $this->collection_model->get_total('patients', ['entity.id'=>$id_entidad] );
		$data["allPatient"] = $total;
		$data["titulo"] = "Ubicación";
		$data["active"] = "ubicacion";
		$this->load->view("admin/ubicacion",$data);
	}

	public function ubicacion1(){
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = "5e330a9bfd61ee880738741b";
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		$data["id_entidad"] = $id_entidad;
		$data['colores'] = ['Rojo intenso', 'Rojo', 'Amarillo', 'Verde'];

		$total = $this->collection_model->get_total('patients', ['entity.id'=>$id_entidad] );
		$data["allPatient"] = $total;
		$data["titulo"] = "Ubicación";
		$data["active"] = "ubicacion";
		$this->load->view("admin/ubicacion1",$data);
	}

	public function permisos(){
		$data["active"]='';
		$data["titulo"] = "Permiso Denegado";
		$this->load->view("admin/sin_permisos",$data);
	}

	public function newPaciente($offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = '5e61f6d2b363eb5cecfb8b5b';
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->session->userdata('id_entidad');
		if($this->session->userdata('id_rol') == "5e3307a1fd61ee8807387411"){
			$entidad_search = false;
			if(isset($_POST["searchentidad"])) {
				$entidad_search=$this->input->post('searchentidad', TRUE);
				$this->session->set_userdata('searchentidad', $entidad_search);
			}else{
				if (($this->session->userdata('searchentidad')!='')) {
					$entidad_search = $this->session->userdata('searchentidad');
				}
			}			
			$id_entidad=$entidad_search;
			$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		}
		$data['id_entidad'] = $id_entidad;
		$pipeline = [
		    [ '$match' => [ 'entity.id'=>$id_entidad, 'state'=>'1', '$expr' => [ '$gt' => [ ['$size'=> '$columns'] , 0]] ] ],
		    [ '$project' => ['name'=>1 ] ]
		];
		$total= $this->collection_model->get_aggregate('columns_configuration', $pipeline);
		$total = count($total);
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/newPaciente/";
		$config['per_page'] = 10;
		$config['uri_segment'] = '3';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    
	    $pipeline = [
		    [ '$match' => [ 'entity.id'=>$id_entidad, 'state'=>'1', '$expr' => [ '$gt' => [ ['$size'=> '$columns'] , 0]] ] ],
		    [ '$project' => ['name'=>1, 'columns'=>1] ],		     
		    [ '$skip' => (Int)$offset ],
		    [ '$limit' => $config['per_page'] ]
		];
		$data_projection = $this->collection_model->get_aggregate('columns_configuration', $pipeline);
		$data["columns_configuracion"] = $data_projection;

		$data["titulo"] = "Nuevo paciente";
		$data["active"] = "dashboard";
		$this->load->view("admin/paciente/nuevo_paciente",$data);
	}

	public function editarPaciente($_id, $offset=0)
	{
		$login = $this->session->logged_in;
		if(!$login) redirect(base_url()."web/login");

		$vista = '5e625de0471d8432706687ab';
		$permisos= $this->session->vistas;
		if (!in_array($vista, $permisos)) redirect(base_url().'admin/permisos');

		$id_entidad = $this->collection_model->get_collection('patients', ['_id' => new MongoDB\BSON\ObjectId($_id)], ['projection'=>['entity.id'=>'1']] );
		$id_entidad = $id_entidad[0]->entity->id;

		$pipeline = [
		    [ '$match' => [ 'entity.id'=>$id_entidad, 'state'=>'1', '$expr' => [ '$gt' => [ ['$size'=> '$columns'] , 0]] ] ],
		    [ '$project' => ['name'=>1 ] ]
		];
		$total= $this->collection_model->get_aggregate('columns_configuration', $pipeline);
		$total = count($total);
		$config['total_rows'] = $total;
		$config["base_url"] = base_url()."admin/editarPaciente/$_id/";
		$config['per_page'] = 10;
		$config['uri_segment'] = '4';
		$config['first_link'] = '<< '.$this->lang->line('ir_primero');
		$config['last_link'] = $this->lang->line('ultimo').' >';
		$config['next_link'] = ' '.$this->lang->line('siguiente').' ' . '&gt;';
		$config['prev_link'] = ' &lt;' . ' '.$this->lang->line('atras');
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	    $this->pagination->initialize($config);
	    $data['offset'] = $offset;
	    
	    $pipeline = [
		    [ '$match' => [ 'entity.id'=>$id_entidad, 'state'=>'1', '$expr' => [ '$gt' => [ ['$size'=> '$columns'] , 0]] ] ],
		    [ '$project' => ['name'=>1, 'columns'=>1] ],		     
		    [ '$skip' => (Int)$offset ],
		    [ '$limit' => $config['per_page'] ]
		];
		$data_projection = $this->collection_model->get_aggregate('columns_configuration', $pipeline);
	    $data_projection1=objectToArray($data_projection);
	    $name_projection = array_column($data_projection1, 'columns');
	    $name_projection_final = [];
	    foreach ($name_projection as $k1 => $v1) {
	    	foreach ($v1 as $k2 => $v2) {
	    		$name_projection_final[] = $v2;
	    	}
	    }
	    $name_projection = array_fill_keys($name_projection_final, '1');
	    $query=$this->collection_model->get_collection('patients', ['_id' => new MongoDB\BSON\ObjectId($_id)], ['projection'=>$name_projection]);
	    $query = objectToArray($query);
		$data["paciente"] = $query;
		$data["columns_configuracion"] = $data_projection1;
		$data["entidad"]=$this->collection_model->get_collection('entities', array('state'=>'1'));
		$data["id_paciente"]=$_id;
		$data["id_entidad"]=$id_entidad;

		$data["active"] = "dashboard";
		$data['titulo'] = 'Editar paciente';
		$this->load->view('admin/paciente/editar_paciente', $data);
	}
}

function objectToArray ( $object ) {
	if(!is_object($object) && !is_array($object)) {
  		return $object;
	}  
	return array_map( 'objectToArray', (array) $object );
}