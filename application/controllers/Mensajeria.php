<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mensajeria extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('collection_model');
	}

	//views/common/js.php - sms - llamada
	public function personalizado(){ 
		$_id=$this->input->post('id', TRUE); //id paciente
		$mensaje=$this->input->post('mensaje', TRUE);
		$tipo=$this->input->post('tipo', TRUE); //llamada - mensaje - encuesta
		$encuesta=$this->input->post('encuesta',TRUE);
		$id_entidad=$this->input->post('id_entidad', TRUE);
		$telefono=(int)$this->input->post('telefono', TRUE);
		$id_usuario=$this->session->id;

		$output = '';
		$id_channel='';
	    $name_channel='';

		if(is_null($telefono)){
	        print json_encode(array("res"=>"bad","msj" =>"Este paciente no tiene número de celular registrado."));
	        exit();
	    }else{
	        if(substr($telefono, 0, 1) != 3 && strlen($telefono)==10){
	            print json_encode(array("res"=>"bad","msj" =>"El número de celular registrado no es válido."));
	            exit();
	        }
	    }
	    if($tipo == 'sms'){
	        if(!empty($encuesta)){
	            //shortened google
	            $url=base_url().'admin/encuesta/'.$id.'/'.$encuesta;
	            $apiKey = '';
	            $postData = array('longUrl' => $url, 'key' => $apiKey);
	            $jsonData = json_encode($postData);
	            $curlObj = curl_init();
	            curl_setopt($curlObj, CURLOPT_URL, '');
	            curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
	            curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
	            curl_setopt($curlObj, CURLOPT_HEADER, 0);
	            curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
	            curl_setopt($curlObj, CURLOPT_POST, 1);
	            curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
	            $response = curl_exec($curlObj);
	            $json = json_decode($response);
	            curl_close($curlObj);
	            $short = $json->id;
	            //mensaje de texto
	            $mensaje = $mensaje.' '.$short;
	            $url = '';
	            $datos='user=&pass=&dest='.$telefono.'&text='.str_replace(' ','%20',$mensaje);
	            $ch = curl_init(); 
	            curl_setopt($ch, CURLOPT_URL, $url.$datos);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	            $output = curl_exec($ch);
	            curl_close($ch);
	            $id_channel='';
	            $name_channel='';
	        }else{
	            $url = '';
	            $datos='user=&pass=&dest='.$telefono.'&text='.str_replace(' ','%20',$mensaje);
	            $ch = curl_init(); 
	            curl_setopt($ch, CURLOPT_URL, $url.$datos);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	            $output = curl_exec($ch);
	            curl_close($ch); 
	            $id_channel='';
	            $name_channel='';
	        } 
	    } elseif ($tipo == 'llamada') {
	    	$id_channel='';
	    	$name_channel='';
	        $url = '';
	        $datos='user=&pass=&dest='.$telefono.'&text='.str_replace(' ','%20',$mensaje);
	        $ch = curl_init(); 
	        curl_setopt($ch, CURLOPT_URL, $url.$datos);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	        $output = curl_exec($ch);
	        curl_close($ch);
	    } else {
            $id_channel='';
	        $name_channel='';
        }

	    $result = $this->collection_model->insert_collection('log_envios_personalizados', ['patient'=>['id'=>$_id], 'channel'=>['id'=>$id_channel, 'name'=>$name_channel], 'mensaje'=>$mensaje, 'encuesta'=>$encuesta, 'user'=>['id'=>$this->session->_id, 'name'=>$this->session->nombre], 'rta'=>$output]);

	    if ($result) {
	    	 print json_encode(array("res"=>"ok"));
	    }else{
	    	 print json_encode(array("res"=>"bad", "msj"=>"Problemas para enviar el mensaje"));
	    }	   
	}

	//views/admin/dashboard.php
	public function general(){
		$this->load->view("mensajeria/general");
	}

	public function newCampanaMensajeria(){
		$titulo = $this->input->post('titulo');
		$tipo_campana = $this->input->post('tipo_campana');
		$id_canal = $this->input->post('id_canal');
		$name_canal = $this->input->post('name_canal');
		$condiciones = $this->input->post('condiciones');
		$id_banco = $this->input->post('id_banco');
		$name_banco = $this->input->post('namee_banco');
		$mensaje = $this->input->post('mensaje');
		$estado = $this->input->post('estado');
		$fechanum= $this->input->post('fechanum');
		$fechafecha= $this->input->post('fechafecha');
		$id_entidad= $this->input->post('id_entidad');

		if($mensaje != '' && $name_canal == 'SMS' && strlen($mensaje) > 161){
			print json_encode(array("res"=>"bad", "msj"=>"El mensaje excede en el limite de caracteres permitidos (max 160)." ));
			exit();
		}

		$query = $this->collection_model->insert_collection('messaging_campaigns', ['name'=>$titulo, 'state_message'=>$estado, 'type_campaign'=>['name'=>$tipo_campana], 'dia'=>$fechanum, 'fecha'=>$fechafecha, 'channel'=>['id'=>$id_canal, 'name'=>$name_canal], 'text'=>$mensaje, 'message_banck'=>['id'=>$id_banco, 'name'=>$name_banco], 'entity'=>['id'=>$id_entidad], 'user'=>['id'=>$this->session->userdata('_id'), 'name'=>$this->session->userdata('nombre')], 'state'=>'1', 'data_clinical'=>$condiciones]);
		if($query){
			print json_encode(array("res"=>"ok","msj"=>"Campaña creada con exito."));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente agregando la nueva campaña, inténtalo de nuevo." ));
			exit();
		}
	}

	public function deleteMensajeria(){
		$_id = $this->input->post('id');

		$result = $this->collection_model->update_collection($_id, 'messaging_campaigns', ['state'=>'2']);
		if($result){
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente al eliminar la campaña de mensajería." ));
		}
	}

	public function newBanco(){
		$titulo = $this->input->post('titulo');
		$estado = $this->input->post('estado');
		$id_entidad = $this->input->post('id_entidad');

		$query = $this->collection_model->insert_collection('message_banck', ['name'=>$titulo, 'state_message'=>$estado, 'entity'=>['id'=>$id_entidad], 'state'=>'1', 'user'=>['id'=>$this->session->userdata('_id'), 'name'=>$this->session->userdata('nombre')], 'message'=>array()]);
		if($query){
			print json_encode(array("res"=>"ok","msj"=>"Banco creado con éxito."));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente agregando el nuevo banco, inténtalo de nuevo." ));
		}
	}

	public function editBanco(){
		$_id = $this->input->post('id');
		$titulo = $this->input->post('titulo');
		$estado = $this->input->post('estado');
		
		$query = $this->collection_model->update_collection($_id, 'message_banck', ['name'=>$titulo, 'state_message'=>$estado, 'user'=>['id'=>$this->session->userdata('_id'), 'name'=>$this->session->userdata('nombre')]]);
		if($query){
			print json_encode(array("res"=>"ok","msj"=>"Banco actualizado con éxito."));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente actualizando el banco, inténtalo de nuevo." ));
		}
	}

	public function deleteBanco(){
		$_id = $this->input->post('id', TRUE);
		if($_id==""){
		   print json_encode(array("res"=>"bad","msj" =>"El id no viene"));
		   exit();
		}
		$query = $this->collection_model->update_collection($_id, 'message_banck', ['state'=>'2']); 
		if($query){
			print json_encode(array("res"=>"ok","msj"=>"Tus datos han sido eliminados"));
		}else{
			print json_encode(array("res"=>"bad", "msj" =>"Tus datos no han sido eliminados"));
		}
	}

	public function newMensajeBanco(){
		$_id = $this->input->post('id');
		$mensaje = $this->input->post('mensaje');
		$id_channel = $this->input->post('id_channel');
		$name_channel = $this->input->post('name_channel');

		$query = $this->collection_model->update_push_collection($_id, 'message_banck', array('message'=>(object)array('text'=>$mensaje, 'channel'=>['id'=>$id_channel, 'name'=>$name_channel])));
		if($query){
			print json_encode(array("res"=>"ok","msj"=>"Banco creado con éxito."));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente agregando el nuevo banco, inténtalo de nuevo." ));
		}
	}

	public function editMensajeBanco(){
		$_id = $this->input->post('id');
		$mensaje = $this->input->post('mensaje');
		$id_channel = $this->input->post('id_channel');
		$name_channel = $this->input->post('name_channel');

		$mensaje_old = $this->input->post('mensaje_old');
		$id_channel_old = $this->input->post('id_channel_old');
		$name_channel_old = $this->input->post('name_channel_old');

		$result = $this->collection_model->update_pull_collection($_id, 'message_banck', array('message'=>(object)array('text'=>$mensaje_old, 'channel'=>['id'=>$id_channel_old, 'name'=>$name_channel_old])));
		if ($result) {
			$query = $this->collection_model->update_push_collection($_id, 'message_banck', array('message'=>(object)array('text'=>$mensaje, 'channel'=>['id'=>$id_channel, 'name'=>$name_channel])));
			if($query){
				print json_encode(array("res"=>"ok","msj"=>"Banco creado con éxito."));
			}else{
				print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente agregando el nuevo banco, inténtalo de nuevo." ));
			}
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente agregando el nuevo banco, inténtalo de nuevo." ));
		}
	}

	public function deleteMensajeBanco(){
		$_id = $this->input->post('id');
		$mensaje = $this->input->post('mensaje');
		$id_channel = $this->input->post('id_channel');
		$name_channel = $this->input->post('name_channel');

		$result = $this->collection_model->update_pull_collection($_id, 'message_banck', array('message'=>(object)array('text'=>$mensaje, 'channel'=>['id'=>$id_channel, 'name'=>$name_channel])));
		if ($result) {
			print json_encode(array("res"=>"ok","msj"=>"Banco creado con éxito."));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"Se presento un inconveniente agregando el nuevo banco, inténtalo de nuevo." ));
		}
	}

	public function getBancoMensajes()
	{
		$id = $this->input->post('id_entidad');

		$result = $this->collection_model->get_collection('message_banck', ['$and'=>[['entity.id'=>$id], ['state'=>'1'], ['state_message'=>'1']]], ['projection'=>['name'=>'1']]);
		if ($result) {
			print json_encode(array('res'=>'ok', 'data'=>$result));
		}else{
			print json_encode(array('res'=>'bad', 'msj'=>'No hay mensajes configurados para la entidad seleccionada'));
		}		
	}

	public function cronSendPacienteMensajeria()
	{
		$this->load->view("mensajeria/cronSendPacienteMensajeria_update");
	}

	public function cronSendPacienteMensajeriaUpdate()
	{
		$this->load->view("mensajeria/cronSendPacienteMensajeria_update");
	}
}