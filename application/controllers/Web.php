<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function index()
	{	
		if (is_null($this->session->userdata('site_lang'))) {
			$this->session->set_userdata('site_lang','spanish');
		}
		$data['titulo'] = 'Index';
		$this->load->view("web/index");
	}

	public function login(){
		if (is_null($this->session->userdata('site_lang'))) {
			$this->session->set_userdata('site_lang','spanish');
		}
		$data["titulo"] = "Login";
		$this->load->view("admin/login", $data);
	}

	public function info() {
		phpinfo();
	}
}