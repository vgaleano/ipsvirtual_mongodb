<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicios extends CI_Controller {

	public function subestados(){
		$this->load->view("servicios/subestados");
	}
	public function reporteMensajeriaNotificaciones()
	{
		$this->load->view("servicios/reporteMensajeriaNotificaciones");
	}
	/*public function eliminarDuplicados()
	{
		$this->load->view("servicios/eliminarDuplicados");
	}*/

	public function datosPacientes()
	{
		$this->load->view("servicios/datos_pacientes");
	}
	//parametros
	public function verColumna()
	{
		$this->load->view("servicios/ver_columna");
	}
	public function guardarTag()
	{
		$this->load->view("servicios/guardar_columna");
	}
	public function eliminarTag()
	{
		$this->load->view("servicios/eliminar_columna");
	}
	public function verColumnaVariables()
	{
		$this->load->view("servicios/ver_columna_variables");
	}
	public function getValorColumnaVariables()
	{
		$this->load->view("servicios/get_valor_columna_variables");
	}
	//indicadores
	public function guardarIndicadorPadre()
	{
		$this->load->view("servicios/guardar_indicador_padre");
	}
	public function guardarDashboardGrafica()
	{
		$this->load->view("servicios/guardar_dashboard_grafica");
	}
	public function deleteGrafica()
	{
		$this->load->view("servicios/delete_grafica");
	}
	public function deleteConfiguracionGeneral()
	{
		$this->load->view("servicios/delete_configuracion_general");
	}
	public function saveConfiguracionGeneral()
	{
		$this->load->view("servicios/save_configuracion_general");
	}
	public function getKpis()
	{
		$this->load->view("servicios/get_kpis");
	}
	public function verColumnaVariablesUnicas()
	{
		$this->load->view("servicios/ver_columna_variables_unicas");
	}
	public function getPacientes()
	{
		$this->load->view("servicios/get_pacientes");
	}
	public function getPacientes1()
	{
		$this->load->view("servicios/get_pacientes1");
	}
	public function getKpisUnique()
	{
		$this->load->view("servicios/get_kpis_unique");
	}
	public function getKpis2()
	{
		$this->load->view("servicios/get_kpis2");
	}
	public function FunctionName() {
	
		$post = array(
		    'first_name' => 'Daniel',
		    'last_name' => 'Lorena',
		    'email' => 'Daniel6993@uis.com',
		    'name_university' => 'Universidad Industrial de Santander',
		    'id_university' => '5e14e830761b0000110014c0',
		    'password' => 'Pass12345678',
		);
		 
		$target_url= 'https://didacticweb.guarumo.com/api/newPlayer';
		// Prepare new cURL resource
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$target_url);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POST, count($post));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_VERBOSE,true);
		 
		// Submit the POST request
		$result = curl_exec($ch);
		print_r($result);
		// Close cURL session handle
		curl_close($ch);

	}
}