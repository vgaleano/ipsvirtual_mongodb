<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//require_once '/var/www/html/ips_virtual/application/third_party/spout/src/Spout/Autoloader/autoload.php';
require_once APPPATH.'third_party/spout/src/Spout/Autoloader/autoload.php';
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;
use Box\Spout\Writer\Common\Creator\Style\StyleBuilder;
use Box\Spout\Common\Entity\Style\CellAlignment;
use Box\Spout\Common\Entity\Style\Color;
class Indicadores extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('collection_model');
	}

	public function verColumna()
	{
		$texto = $this->input->post('tags_auto');
		$tags_agregadas = $this->input->post('tags_seleccionados');
		$id_entidad = $this->input->post('id_entidad');

		$filter = ['entity.id'=>$id_entidad];
		if (isset($tags_agregadas) && !is_null($tags_agregadas) && count($tags_agregadas)>0) {
			$activas=array_filter($tags_agregadas, function ($tT) use ($texto) 
			{
				if (strtolower($texto)==strtolower($tT)) {
					return $tT;
				}
			});
			if (count($activas)>0) {
				print json_encode(array("res"=>"bad", "msj" => 'El columna ya está agregado en la lista.' ));
				exit();
			};
		}
		if ($texto!='') {
			$filter=array_merge($filter, ['name'=> new MongoDB\BSON\Regex( "$texto", 'i' )]);
		}
		$result = $this->collection_model->get_collection('columns_name', (count($filter)>1) ? ['$and'=>[$filter]] : $filter, ['projection'=>['_id'=>'1', 'name'=>'1']]);
		if (!empty($result) && count($result)>0) {
			$result=objectToArray($result);
			$activas=(isset($tags_agregadas) && !is_null($tags_agregadas) && count($tags_agregadas)>0) ? array_diff(array_column($result, 'name'), $tags_agregadas) : array_column($result, 'name');
			$activas=array_values($activas);
			$activas_find = [];
			for ($i=0; $i < count($activas); $i++) { 
				$key_find = array_search($activas[$i], array_column($result, 'name'));
				$activas_find[]=$result[$key_find];
			}
			print json_encode(array("res"=>"ok" , "bar"=>$activas_find));
		}else{
			print json_encode(array("res"=>"bad", "msj" => "No existen registros en la tabla columnas." ));
		}
	}

	public function guardarTag()
	{		
		$_id = $this->input->post('id');//id de columna_variable
		$name = $this->input->post('name');//name de columna_variable
		$tags = $this->input->post('tags_select'); //array ids columnas
		$obligatorio = $this->input->post('obligatorio');
		$id_entidad = $this->input->post('id_entidad');
		$name_entidad = $this->input->post('name_entidad');
		$name_generico = $this->input->post('name_generico');

		if ($name==''){
			print json_encode(array('res'=>'bad','msj'=>'Debe agregar un identificador para las columnas relacionadas'));
			exit();
		}

		if (count($tags)==0){
			print json_encode(array('res'=>'bad','msj'=>'Debe seleccionar las columnas'));
			exit();
		}

		if ($_id==''){ //new
			$result = $this->collection_model->insert_collection('columns_configuration', array('name'=>$name, 'name_generico'=>$name_generico, 'required'=>$obligatorio, 'entity'=>array('id'=>$id_entidad, 'name'=>$name_entidad), 'columns'=>$tags, 'state'=>'1'));
			if ($result) {
				print json_encode(array('res'=>'ok'));
			}else{
				print json_encode(array('res'=>'bad','msj'=>'Problemas al crear las columnas relacionadas'));
			}
		}else{ //update		
			$result=$this->collection_model->update_collection($_id, 'columns_configuration', array('name'=>$name, 'name_generico'=>$name_generico, 'columns'=>$tags));
			if ($result) {
				print json_encode(array('res'=>'ok'));
			}else{
				print json_encode(array('res'=>'bad','msj'=>'Problemas al actualizar las columnas relacionadas'));
			}
		}
	}

	public function eliminarTag()
	{
		$_id = $this->input->post('id');//id de columna_variable
		$result=$this->collection_model->update_collection($_id, 'columns_configuration', array('state'=>'2'));
		if ($result) {
			print json_encode(array('res'=>'ok'));
		}else{
			print json_encode(array('res'=>'bad','msj'=>'Problemas al actualizar las columnas relacionadas'));
		}
	}

	public function saveConfiguracionGeneral()
	{
		$id=$this->input->post('id'); //id de otras tablas o el id del tipo_filtro
		$tipo_tabla=$this->input->post('tipo_tabla');
		$value = $this->input->post('value');
		$id_entidad = $this->input->post('id_entidad');
		$id_config = $this->input->post('id_config');
		$id_columna_variable = $this->input->post('id_columna_variable');

		if ($tipo_tabla!='tipo_filtro') {
			if ($id=='') {
				//new
				$result = ($tipo_tabla=='tipo_periodicidad') ? $this->collection_model->insert_collection('entities_periodicity', ['name'=>$value, 'required'=>'0', 'state'=>'1', 'entity'=>['id'=>$id_entidad]]) : $this->collection_model->insert_collection('entities_indicators', ['name'=>$value, 'required'=>'0', 'state'=>'1', 'entity'=>['id'=>$id_entidad]]);
				if ($result) {
					print json_encode(array('res'=>'ok', 'id'=>$result));
				}else{
					print json_encode(array('res'=>'bad', 'msj'=>'No se pudo hacer los cambios'));
				}		
			}else{
				//update
				$result = ($tipo_tabla=='tipo_periodicidad') ? $this->collection_model->update_collection($id, 'entities_periodicity', ['name'=>$value]) : $this->collection_model->update_collection($id, 'entities_indicators', ['name'=>$value]);
				if ($result) {
					print json_encode(array('res'=>'ok', 'id'=>''));
				}else{
					print json_encode(array('res'=>'bad', 'msj'=>'No se pudo hacer los  cambios'));
				}
			}
		}else{
			$result=$this->collection_model->update_collection($id, 'entities_filters', ['columns'=>$value]);
			if ($result) {
				print json_encode(array('res'=>'ok', 'id'=>$id, 'tipo_tabla'=>$tipo_tabla));
			}else{
				print json_encode(array('res'=>'bad', 'msj'=>'No se pudo guardar los  cambios'));
				exit();
			}
		}
	}

	public function deleteConfiguracionGeneral()
	{
		$id = $this->input->post('id');
		$tipo_tabla = $this->input->post('tipo_tabla');
		$id_entidad = $this->input->post('id_entidad');

		if ($tipo_tabla!='tipo_filtro') {			
			$result = ($tipo_tabla=='tipo_indicador') ? $this->collection_model->get_total('entities_indicators', ['$and'=>[['global_standar.type_indicator.id'=>$id], ['entity.id'=>$id_entidad]]]) : $this->collection_model->get_total('entities_periodicity', ['$and'=>[['global_standar.type_indicator.id'=>$id], ['entity.id'=>$id_entidad]]]);

			if ($result>0) {
				print json_encode(array('res'=>'bad','msj'=>'No se puede eliminar el dato porque es usado en graficas.'));
			}else{
				$result = ($tipo_tabla=='tipo_periodicidad') ? $this->collection_model->update_collection($id, 'entities_periodicity', ['state'=>'2']) : $this->collection_model->update_collection($id, 'entities_indicators', ['state'=>'2']);

				if ($result) {
					print json_encode(array('res'=>'ok'));
				}else{
					print json_encode(array('res'=>'bad', 'msj'=>'No se puede eliminar el dato'));
				}
			}			
		}else{
			//tipo_filtro
			$result = $this->collection_model->update_collection($id, 'entities_filters', ['columns'=>[]]);

			if ($result) {
				print json_encode(array('res'=>'ok'));
			}else{
				print json_encode(array('res'=>'bad', 'msj'=>'No se puede eliminar el dato'));
			}
		}
	}
	//no se de donde se utiliza
	public function verColumnaConfiguracion()
	{
		$texto = $this->input->post('tags_auto');
		$id_entidad = $this->input->post('id_entidad');

		$filter = ['entity.id'=>$id_entidad, 'state'=>'1'];
		if ($texto!='') {
			$filter=array_merge($filter, ['name'=> new MongoDB\BSON\Regex( "$texto", 'i' )]);
		}
		
		$result = $this->collection_model->get_collection('columns_configuration', ['$and'=>[$filter]], ['projection'=>['_id'=>'1', 'name'=>'1', 'columns'=>'1']]);

		if ($result){
		   print json_encode(array("res"=>"ok" , "bar"=>$result));
		}else{
			print json_encode(array("res"=>"bad", "msj" => "No existen registros en la tabla columnas." ));
		}
	}
	//no se de donde se utiliza
	public function getValorColumnaVariables()
	{
		$columns = $this->input->post('columns');
		$id_entidad = $this->input->post('id_entidad');
		$group = [];
		if (count($columns)>1) {
			for ($i=0; $i < count($columns); $i++) { 
				$group[] = array('$'.$columns[$i] => $columns[$i]);
			}
			$pipeline = [
			    [ '$match' => ['entity.id' => $id_entidad] ],
			    [ '$group' => ['_id' => $group] ]
			];
		}else{
			$pipeline = [
			    [ '$match' => ['entity.id' => $id_entidad] ],
			    [ '$group' => ['_id' => '$'.$columns[0]] ]
			];
		}	
		$result = $this->collection_model->get_aggregate('patients', $pipeline);
		
		if ($result){
		   	print json_encode(array("res"=>"ok" , "bar"=>$result));
		}else{
			print json_encode(array("res"=>"bad", "msj" => "No existen registros en la tabla columnas." ));
		}	
	}

	public function guardarIndicadorPadre()
	{
		$_id = $this->input->post('id');
		$info = $this->input->post('info');
		$id_entidad = $this->input->post('id_entidad');
		$modo_guardar = $this->input->post('modo_guardar');
		if ($_id=='') {
			$final = array_merge($info[0], array('state'=>$modo_guardar, 'state_dashboard'=>'1', 'entity'=>array('id'=>$id_entidad)));
			$result = $this->collection_model->insert_collection('indicators', $final);
		}else{
			$data = array_merge($info[0],['state'=>$modo_guardar]);
			$result = $this->collection_model->update_collection($_id, 'indicators', $data);
		}			

		if ($result) {
			print json_encode(array('res'=>'ok','msj'=>'Se guardo la información correctamente'));
		}else{
			print json_encode(array('res'=>'bad','msj'=>'No se pudo agregar la información general'));
		}
	}

	public function guardarDashboardGrafica()
	{
		$_id = $this->input->post('id');
		$estado = $this->input->post('estado');

		$result = $this->collection_model->update_collection($_id, 'indicators', ['state_dashboard'=>$estado]);

		if ($result) {
			$dashboard = ($estado=='0')?'Inactivo':'Activo';
			print json_encode(array('res'=>'ok', 'msj'=>"Actualizado el estado a $dashboard"));
		}else{
			print json_encode(array('res'=>'bad', 'msj'=>"No se pudo acctualir el estado de la gráfica"));
		}
	}

	public function deleteGrafica()
	{
		$_id = $this->input->post('id');

		$result = $this->collection_model->update_collection($_id, 'indicators', ['state'=>'2']);

		if ($result) {
			print json_encode(array('res'=>'ok', 'msj'=>"Eliminado el indicador"));
		}else{
			print json_encode(array('res'=>'bad', 'msj'=>"No se pudo acctualir el estado de la gráfica"));
		}
	}
	//areglar para rango, arreglar para cuando haya denominador, arreglar para que sea con filtros, arreglar para que traiga los datos del paciente
	public function getKpis()
	{
		$id_entidad=$this->input->post('entidad');
		$fecha_in=$this->input->post('fecha_in');
		$fecha_out=$this->input->post('fecha_out');
		$edad=$this->input->post('edad');
		$genero=$this->input->post('genero');
		$sede=$this->input->post('sede');
		$profesional=$this->input->post('profesional');
		$especialidad=$this->input->post('especialidad');
		$ids_eventos=$this->input->post('eventos');//envia uno o todos los enventos en un array()
		$estandar=$this->input->post('estandar');
		$diagnostico=$this->input->post('diagnostico');
		$discapacidad=$this->input->post('discapacidad');
		$procedencia=$this->input->post('procedencia');
		$area=$this->input->post('area');
		/*$id_entidad='5e338f93434c00008a006ab3';
		$fecha_in='';
		$fecha_out='';
		$edad='';
		$genero='0';
		$sede='';
		$profesional='';
		$especialidad='';
		$ids_eventos=array('5e38d14a0365000094001917');//array('5e38d14a0365000094001917', '5e3b99e51e5a0000be003dd0');
		$estandar='';
		$diagnostico='';
		$discapacidad='';
		$procedencia='';
		$area='Bucaramanga';*/
		$search = ['Género'=>$genero, 'Edad'=>$edad, 'Sede'=>$sede, 'Profesional'=>$profesional, 'Especialidad'=>$especialidad, 'Diagnóstico'=>$diagnostico, 'Discapacidad'=>$discapacidad, 'Procedencia'=>$procedencia, 'Área'=>$area];
		$color_rojo_intenso=array('#b41706', '#8e1a09', '#6a190a', '#c00000', '#c90000', '#d20000', '#db0c00', '#a60100', '#ae1100', '#b51c00', '#bd2500', '#c52d06', '#cd350e' );
		$color_rojo=array('#ff0000', '#f80000', '#e62e1b', '#df0000', '#ea0000', '#f40000', '#ff1d0b', '#ff2e16', '#ff3b1f', '#ee3a1f', '#ee3a1f', '#d11507', '#a51b0b');
		$color_azul=array('#00913f', '#00a135', '#00722e', '#008f39', '#008000', '#008f4c', '#317f43', '#308446', '#348043', '#007723', '#007f2a', '#008732', '#179740', '#259f48', '#31a84f', '#008f39', '#155e29', '#107631');
		$copy_rojo_intenso=$color_rojo_intenso;
		$copy_rojo=$color_rojo;
		$copy_azul=$color_azul;
		$first_semestre = array('1','2','3','4','5','6');
		if ($fecha_in=='') {
			$fecha_in = date('Y-m-d', strtotime('-1 year', strtotime(date('y-m-d')))); //opr default se tomo hace un año
			$fecha_out = date('Y-m-d'); //por defecto se toma la fecha de hoy
		}
		$data_paciente = [];
		$name_data_paciente_ini = ['Nombre', 'Apellido', 'Sexo', 'Teléfono', 'Fecha de nacimiento'];
		$name_data_paciente = [];
		if ($id_entidad=='') {
			print  json_encode(array("res"=>"bad", "Debe seleccionar la entidad"));
			exit();
		}else{
			$result_ = $this->collection_model->get_collection('columns_configuration', ['$and'=>[['state'=>'1'], ['entity.id'=>$id_entidad]]], ['projection'=>['name'=>'1', 'columns'=>'1']]);
			if (count($result_)>0) {
				foreach ($result_ as $k_r => $v_r) {
					if (count($v_r->columns)>0 && in_array($v_r->name, $name_data_paciente_ini)) {
						for ($j=0; $j < count($v_r->columns); $j++) { 
							$data_paciente = array_merge($data_paciente, [$v_r->columns[$j]=>'1']);
							$name_data_paciente[$v_r->name][]=$v_r->columns[$j];
						}
					}
				}
			}
		}		
		$filter_global_parameters1 = [];
		$key_edad = '';
		//Get columns filtros - SUPONIENDO QUE LA COLUMNAS SEA 1
		$info_filtros = $this->collection_model->get_collection('entities_filters', ['$and'=>[['state'=>'1'], ['entity.id'=>$id_entidad], ['mode'=>'0']]], ['projection'=>['name'=>'1', 'columns'=>'1']]);
		if (count($info_filtros)>0) {
			for ($i=0; $i < count($info_filtros); $i++) { 
				if (count($info_filtros[$i]->columns)>0 && $search[$info_filtros[$i]->name]!='') {
					if ($info_filtros[$i]->name=='Edad') {
						$key_edad = $info_filtros[$i]->columns[0];
						continue;
					}
					$filter_global_parameters1 = array_merge($filter_global_parameters1, [$info_filtros[$i]->columns[0]=>$search[$info_filtros[$i]->name]]);
					for ($j=0; $j < count($info_filtros[$i]->columns); $j++) { 
						$data_paciente = array_merge($data_paciente, [$info_filtros[$i]->columns[$j]=>'1']);
						$name_data_paciente[$info_filtros[$i]->name][]=$info_filtros[$i]->columns[$j];
					}
				}
			}
		}		
		$filter_range_edades = [];
		if ($edad!='') {
			$info_edades = $this->collection_model->get_collection('range_age', ['_id' => new MongoDB\BSON\ObjectId($edad)], ['projection'=>['out'=>'1', 'in'=>'1']]);
			if (count($info_edades)>0) {
				$ini_fecha = (int)date('Y') - $info_edades[0]->in;
				$fin_fecha = (int)date('Y') - $info_edades[0]->out;
				$filter_range_edades[$key_edad] = ['in'=>$fin_fecha.'-01-01', 'out'=>$ini_fecha.'-12-31'];
			}
		}
		$graficas = [];
		$num_grafica = 1;
		if ($ids_eventos!='') {
			foreach ($ids_eventos as $k_ie => $v_ie) {
				$get_indicador = [];
				//$total_registros_globales = 0;
				$indicador = $this->collection_model->get_collection('indicators', ['$and'=>[['_id' => new MongoDB\BSON\ObjectId($v_ie)], ['state'=>['$in'=>['1','0']]]]]);
				if (count($indicador)>0) {
					$filter_global_parameters = $filter_global_parameters1;
					$filter_global_parameters = array_merge($filter_global_parameters, ['entity.id'=>$id_entidad]);
					$get_indicador['name']=$indicador[0]->information->name;
					$words = explode(" ", remove_accent($get_indicador['name']));
					$acronym = "";
					foreach ($words as $w) {
					  $acronym .= $w[0];
					}
					$get_indicador['description']=str_replace(array("\r\n", "\n\r", "\r", "\n"), "<br />", $indicador[0]->information->description);
					
					//Global PARAMETROS - suponiendo que no sean fechas (FALTA AGREGAR LA PARTE EN DONDE SEAN FECHAS)
					$global_parameters = $indicador[0]->global_parameters;					
					if (count($global_parameters)>0) {
						for ($i=0; $i < count($global_parameters); $i++) { 
							//que no sea fecha y el operador es diferente a rango
							if ($global_parameters[$i]->type_fecha=='false' && $global_parameters[$i]->operator!='range') {
								if ($global_parameters[$i]->operator=='=') {
									for ($j=0; $j < count($global_parameters[$i]->colums); $j++) { 
										$filter_global_parameters = array_merge($filter_global_parameters, [$global_parameters[$i]->colums[$j]=>$global_parameters[$i]->value]);
									}								
								}
								if ($global_parameters[$i]->operator=='!=') {
									for ($j=0; $j < count($global_parameters[$i]->colums); $j++) { 
										$filter_global_parameters = array_merge($filter_global_parameters, [$global_parameters[$i]->colums[$j]=>['$ne'=>$global_parameters[$i]->value]]);
									}								
								}
							}
						}
					}		
					//printArray($filter_global_parameters);		
					//get informacion de parametros globales - TOTAL
					/*if (count($filter_global_parameters)>0) {
						$filter_global = (count($filter_global_parameters)>1) ? ['$and'=>[$filter_global_parameters]] : $filter_global_parameters;
						$total_global_parameter = $this->collection_model->get_total('patients', $filter_global);
					}*/
					//Global ESTANDAR - está por tipo de indicador - son porcentajes [%]
					$global_standar = $indicador[0]->global_standar;
					$info_global_standar = []; // [id_type_standar][operator][value][operator1][value1][operator2][value2]
					$info_name_type_indicator = []; //[#][id_type_standar]
					if (count($global_standar)>0) {
						//obtengo los tipo de indicadores unicos globales
						for ($i=0; $i < count($global_standar); $i++) { 
							if (!in_array($global_standar[$i]->type_indicator->name, $info_name_type_indicator)) {
								$info_name_type_indicator[]=$global_standar[$i]->type_indicator->name;
							}
						}						
						for ($i=0; $i < count($info_name_type_indicator); $i++) { 
							$campo=$info_name_type_indicator[$i];
							$activas=array_filter($global_standar, function ($tT) use ($campo, $estandar) 
							{
								if ( $tT->type_indicator->name==$campo && ( $estandar=='' || $tT->type_standar->id==$estandar) ) {
									return $tT;
								}
							});
							$activas=array_values($activas);
							$info_global_standar[$campo] = $activas;
						}						
					}
					/*echo "<pre>";
					print_r($info_name_type_indicator);
					echo "</pre>";
					exit();*/
					//Global RANKING - es la clasificacion - está por tipo de indicador son rangos de valores					
					$info_global_ranking = [];
					if (isset($indicador[0]->global_ranking)) {
						$global_ranking = $indicador[0]->global_ranking;
						if (count($global_ranking)>0) {
							for ($i=0; $i < count($info_name_type_indicator); $i++) { 
								$campo=$info_name_type_indicator[$i];
								$activas=array_filter($global_ranking, function ($tT) use ($campo) 
								{
									if ($tT->type_indicator->name==$campo) {
										return $tT;
									}
								});
								$activas=array_values($activas);
								$info_global_ranking[$campo] = $activas;
							}	
						}
						$info_global_ranking=objectToArray($info_global_ranking);
					}
						
					/*echo "<pre>";
					print_r($info_global_ranking);
					echo "</pre>";*/
					$global_datos = $indicador[0]->datos;
					$global_datos = objectToArray($global_datos);					
					if (count($global_datos)>0) {
						for ($i=0; $i < count($global_datos); $i++) { 	
							$name_categoria = $global_datos[$i]['name_categoria'];
							for ($ii=0; $ii < count($info_name_type_indicator); $ii++) {	
								/*if ($ii==0) {
									continue;
								}*/
								//echo "<br>".$info_name_type_indicator[$ii];
								$filter_data=[];
								$data=array('array_fechas'=>array(), 'array_normales'=>array(), 'array_sql'=>array('numerador'=>array(), 'denominador'=>array()), 'array_grupos'=>array());
								for ($k=0; $k < count($global_datos[$i]['grupos']); $k++) { 
									if ($global_datos[$i]['grupos'][$k]['type_indicator']['name']==$info_name_type_indicator[$ii]) {
										$array_fechas = [];
										$array_sql = [];
										$array_normal = [];
										$array_grupos = ['fechas'=>array(), 'normal'=>array()];
										$tmp1 = [];
										$tmp2 = [];
										for ($l=0; $l < count($global_datos[$i]['grupos'][$k]['datos']); $l++) {
											if ($global_datos[$i]['grupos'][$k]['datos'][$l]['type_fecha']=='true') {
												for ($m=0; $m < count($global_datos[$i]['grupos'][$k]['datos'][$l]['colums']); $m++) { 
													if ($global_datos[$i]['grupos'][$k]['datos'][$l]['operator'] == '') {
														$array_fechas = [$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m]=>['$gte'=>$fecha_in, '$lte'=>$fecha_out]];
														$tmp1[$global_datos[$i]['grupos'][$k]['datos'][$l]['type_formula']][$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m]] = ['operator'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['operator'], 'value'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['value'], 'operator1'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['operator1'], 'value1'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['value1'], 'operator2'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['operator2'], 'value2'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['value2'] ];
													}
													$array_sql = [$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m] => '1'];										
												}
												$data['array_fechas'][$global_datos[$i]['grupos'][$k]['datos'][$l]['type_formula']][]=$array_fechas;											
											}else{
												for ($m=0; $m < count($global_datos[$i]['grupos'][$k]['datos'][$l]['colums']); $m++) { 
													$array_sql = [$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m] => '1'];	
													$tmp2[$global_datos[$i]['grupos'][$k]['datos'][$l]['type_formula']][$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m]] = ['operator'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['operator'], 'value'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['value'], 'operator1'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['operator1'], 'value1'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['value1'], 'operator2'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['operator2'], 'value2'=>$global_datos[$i]['grupos'][$k]['datos'][$l]['value2'] ];
												}										
											}											
											$data['array_sql'][$global_datos[$i]['grupos'][$k]['datos'][$l]['type_formula']]=array_merge($data['array_sql'][$global_datos[$i]['grupos'][$k]['datos'][$l]['type_formula']], $array_sql);	
										}
										$array_grupos['fechas']=$tmp1;
										$array_grupos['normal']=$tmp2;
										$data['array_grupos'][]=$array_grupos;
									}
								}
								//printArray($data);
								//FALTA HACER LAS COSAS POR DENOMINADOR
								$filter_data[] = ['entity.id'=>$id_entidad];
								$array_standar = $info_global_standar[$info_name_type_indicator[$ii]];
								$ranking_tipo_indicador = [];
								if (isset($info_global_ranking[$info_name_type_indicator[$ii]])) {
									$ranking_tipo_indicador = $info_global_ranking[$info_name_type_indicator[$ii]];
								}
								//OBTIENE TODOS LOS PACIENTES
								$patients_by_fecha=[];
								//REVISAR BIEN CUANDO ES DENOMINADOR Y NUMERADOR
								//DENOMINADOR - EXISTE DENOMINADOR									
								if (isset($data['array_fechas']['denominador'])) {
									$filter_or = ['$or'=>$data['array_fechas']['denominador']];
									$filter_data[] = $filter_or;
									$filter_data[] = $filter_global_parameters;
									$projection = $data['array_sql']['denominador'];
									$projection = array_merge($projection, $data_paciente);
									$projection = (count($projection) >0) ? ['projection'=>$projection] : false;
									$filter_data = (count($filter_data)>1) ? ['$and'=>$filter_data] : $filter_data;
									$patients_by_fecha = $this->collection_model->get_collection('patients', $filter_data, $projection);
								}else{
									//NUMERADOR
									$filter_or = ['$or'=>$data['array_fechas']['numerador']];
									$filter_data[] = $filter_or;
									$filter_data[] = $filter_global_parameters;
									$projection = $data['array_sql']['numerador'];
									$projection = array_merge($projection, $data_paciente);
									$projection = (count($projection) >0) ? ['projection'=>$projection] : false;
									$filter_data = (count($filter_data)>1) ? ['$and'=>$filter_data] : $filter_data;
									$patients_by_fecha = $this->collection_model->get_collection('patients', $filter_data, $projection);
								}
								//printArray($patients_by_fecha);
								//exit();
								//verifica con las reglas creadas
								$array_grupos_reglas = $data['array_grupos'];
								$total_registros = 0;//
								$total_registros_sin_fecha = 0;
								//$total_registros_globales=count($patients_by_fecha) * count($array_grupos_reglas);
								/*echo "<br>TOTAL PACIENTES ".count($patients_by_fecha);
								echo "<br>TOTAL GRUPOS ".count($array_grupos_reglas);
								echo "<br>TOTAL de registros ".$total_registros;*/
								$reglas_by_grupos=[];
								//CLASIIFICACION
								if (count($info_global_ranking)>0 && count($ranking_tipo_indicador)>0) {
									$name_ranking = array_column($ranking_tipo_indicador, 'name_grupo');
									foreach ($ranking_tipo_indicador as $k_rti => $v_rti) {
										$datos_ranking = $v_rti['datos'];
										$tmp_ranking=[];
										$tmp_ranking['name'] = $v_rti['name_grupo'];
										$tmp_ranking['type_ruler'] = $v_rti['type_ruler'];
										$tmp_ranking['datos'] = [];
										//array_grupos_reglas tengo los nombre de las columnas agrupados por grupos
										for ($kl=0; $kl < count($array_grupos_reglas); $kl++) { 
											$array_keys_by_grupos = array_keys($array_grupos_reglas[$kl]['normal']['numerador']);
											$tmp_keys = [];
											for ($ih=0; $ih < count($array_keys_by_grupos); $ih++) {
												foreach ($datos_ranking as $k_dr => $v_dr) {
													$colums_ = $v_dr['colums'];//array()
													if (in_array($array_keys_by_grupos[$ih], $colums_)) {
														$tmp_keys[] = ['type_fecha'=>$v_dr['type_fecha'], 'operator'=>$v_dr['operator'], 'value'=>$v_dr['value'], 'operator1'=>$v_dr['operator1'], 'value1'=>$v_dr['value1'], 'operator2'=>$v_dr['operator2'], 'value2'=>$v_dr['value2'], 'colums'=>$colums_];
														break;
													}
												}													
											}
											$tmp_ranking['datos'][]=$tmp_keys;
										}
										$reglas_by_grupos[]=$tmp_ranking;
									}
								}
								/*echo "<br>Clasificacion";
								echo "<pre>";
								print_r($reglas_by_grupos);
								echo "</pre>";
								exit();*/
								//se filtra por edades
								if ($key_edad!='') {
									$data_edad = $filter_range_edades[$key_edad];
									$activas=array_filter($patients_by_fecha, function ($tT) use ($data_edad, $key_edad) 
									{
										if (($tT->$key_edad>$data_edad['in'] && $tT->$key_edad<$data_edad['out']) || $tT->$key_edad==$data_edad['in'] || $tT->$key_edad==$data_edad['out']) {
											return $tT;
										}
									});
									$activas=array_values($activas);
									$patients_by_fecha=$activas;
								}
								//NUMERADOR
								$data_ok = [];
								$data_bad = [];//array final									
								$info_punto = [];
								for ($j=0; $j < count($array_grupos_reglas); $j++) {
									$keys_fechas = array_keys($array_grupos_reglas[$j]['fechas']['numerador']);
									$keys_normal = array_keys($array_grupos_reglas[$j]['normal']['numerador']);
									$activas = [];
									$activas_bad = [];
									/*echo "<pre>";
									print_r($keys_fechas);
									echo "</pre>";*/
									for ($k=0; $k < count($keys_fechas); $k++) { 
										$campo = $keys_fechas[$k];
										//las que tienen algo en fechas
										$activas =  (count($activas)>0) ? $activas : $patients_by_fecha;
										$formula = $array_grupos_reglas[$j]['fechas']['numerador'][$campo];
										$activas=array_filter($activas, function ($tT) use ($campo, $formula, $fecha_in, $fecha_out) 
										{												
											if ($tT->$campo!='' && $tT->$campo!='NULL') {
												//REVISAR PARA FECHAS CUANDO SEA DIFERENTE DE VACIO O RANGO
												/*($formula['operator']=='='  && $tT->$campo==$formula['value']) || 
													($formula['operator']=='<'  && $tT->$campo< $formula['value']) || 
													($formula['operator']=='>'  && $tT->$campo> $formula['value']) || 
													($formula['operator']=='>=' && $tT->$campo>=$formula['value']) || 
													($formula['operator']=='<=' && $tT->$campo<=$formula['value']) ||
													($formula['operator']=='!=' && $tT->$campo!=$formula['value']) || 
												*/	
												if (($formula['operator']=='' && (($tT->$campo>$fecha_in && $tT->$campo<$fecha_out) || $tT->$campo==$fecha_in || $tT->$campo==$fecha_out )) ) {
													return $tT;
												}
												if ($formula['operator']!='range') {
													# code...
												}
											}
										});												
										$activas=array_values($activas);
										$total_registros = $total_registros + count($activas);
										//las que tienen vacio en fechas
										$activas_bad =  (count($activas_bad)>0) ? $activas_bad : $patients_by_fecha;
										$activas_bad = filterByFecha($activas_bad, $campo, '2');
										$total_registros_sin_fecha= $total_registros_sin_fecha + count($activas_bad);
										$total_registros = $total_registros + count($activas_bad);
									}
									$activas_ini = $activas;
									/*echo "<br>Filtro de fecha ok ".count($activas_ini)."<br>";
									echo "<br>Filtro de fecha bad ".count($activas_bad)."<br>";
									echo "<br>TOTAL REGISTROS  ".$total_registros."<br>";*/
									//evalua por regla
									if (count($activas)>0) {
										$activas = objectToArray($activas);
										$tmp_activas=[];
										$tmp_activas_bad=[];											
										for ($l=0; $l < count($activas); $l++) { 
											$tmp_ok=[];
											$tmp_bad=[];
											$bandera=false;
											for ($k=0; $k < count($keys_normal); $k++) { 
												$campo = $keys_normal[$k];
												$formula = $array_grupos_reglas[$j]['normal']['numerador'][$campo];
												//FALTA EVALUAR POR RANGO
												if ($formula['operator']!='range') {
													if (strtolower($formula['value'])!='null' && $formula['value']!='' && strtolower($formula['value'])!==null) {
														if ( (($formula['operator']=='=' && $activas[$l][$campo]==$formula['value']) || ($formula['operator']=='<' && $activas[$l][$campo]<$formula['value']) || ($formula['operator']=='>' && $activas[$l][$campo]>$formula['value']) || ($formula['operator']=='>=' && $activas[$l][$campo]>=$formula['value']) || ($formula['operator']=='<=' && $activas[$l][$campo]<=$formula['value']) || ($formula['operator']=='!=' && $activas[$l][$campo]!=$formula['value'])) && strtolower($activas[$l][$campo])!='null' && $activas[$l][$campo]!='' && strtolower($activas[$l][$campo])!==null ) {
															$tmp_ok = array_merge($tmp_ok, [$campo=>$activas[$l][$campo]]);
															$tmp_bad = array_merge($tmp_bad, [$campo=>$activas[$l][$campo]]);
														}else{
															$bandera=true;
															$tmp_bad = array_merge($tmp_bad, [$campo=>$activas[$l][$campo]]);
														}
													}else{
														if ( ($formula['operator']=='=' && $activas[$l][$campo]==$formula['value']) || ($formula['operator']=='!=' && $activas[$l][$campo]!=$formula['value'])) {
															$tmp_ok = array_merge($tmp_ok, [$campo=>$activas[$l][$campo]]);
															$tmp_bad = array_merge($tmp_bad, [$campo=>$activas[$l][$campo]]);
														}else{
															$bandera=true;
															$tmp_bad = array_merge($tmp_bad, [$campo=>$activas[$l][$campo]]);
														}
													}					
												}else{

												}													
											}
											if (count($tmp_ok)==count($keys_normal)) {
												$temp = ['fecha'=> date('Y-m',  strtotime($activas[$l][$keys_fechas[0]])) ];
												$texto=[];
												if (count($name_data_paciente['Nombre'])>0) {
													for ($im=0; $im < count($name_data_paciente['Nombre']); $im++) { 
														if (isset($activas[$l][$name_data_paciente['Nombre'][$im]])) {
															$texto[]=$activas[$l][$name_data_paciente['Nombre'][$im]];
														}															
													}
												}
												if (count($name_data_paciente['Apellido'])>0) {
													for ($im=0; $im < count($name_data_paciente['Apellido']); $im++) { 
														if (isset($activas[$l][$name_data_paciente['Apellido'][$im]])) {
															$texto[]=$activas[$l][$name_data_paciente['Apellido'][$im]];
														}
													}
												}
												$temp = array_merge($temp, ['Nombre'=>join(' ', $texto)]);
												if (count($name_data_paciente['Sexo'])>0 && isset($activas[$l][$name_data_paciente['Sexo'][0]])) {
													$temp = array_merge($temp, ['Sexo'=>$activas[$l][$name_data_paciente['Sexo'][0]]]);
												}
												$texto='';
												if (count($name_data_paciente['Teléfono'])>0) {
													for ($im=0; $im < count($name_data_paciente['Teléfono']); $im++) { 
														if (isset($activas[$l][$name_data_paciente['Teléfono'][$im]]) && strlen($activas[$l][$name_data_paciente['Teléfono'][$im]])==10 && substr($activas[$l][$name_data_paciente['Teléfono'][$im]], 0, 1)=="3") {
															$texto=$activas[$l][$name_data_paciente['Teléfono'][$im]];
															break;
														}
													}
												}
												$temp = array_merge($temp, ['Teléfono'=>$texto]);
												if (count($name_data_paciente['Fecha de nacimiento'])>0 && isset($activas[$l][$name_data_paciente['Fecha de nacimiento'][0]])) {
													$temp = array_merge($temp, ['Fecha de nacimiento'=>$activas[$l][$name_data_paciente['Fecha de nacimiento'][0]]]);
												}
												if (isset($name_data_paciente['Sede']) && count($name_data_paciente['Sede'])>0 && isset($name_data_paciente['Sede'][0])) {
													$temp = array_merge( $temp,  [ 'Sede' => $activas[$l][$name_data_paciente['Sede'][0]] ] );
												}
												if (isset($name_data_paciente['Profesional']) && count($name_data_paciente['Profesional'])>0 && isset($name_data_paciente['Profesional'][0])) {
													$temp = array_merge( $temp,  [ 'Profesional' => $activas[$l][$name_data_paciente['Profesional'][0]] ] );
												}
												if (isset($name_data_paciente['Estándar']) && count($name_data_paciente['Estándar'])>0 && isset($name_data_paciente['Estándar'][0])) {
													$temp = array_merge( $temp,  [ 'Estándar' => $activas[$l][$name_data_paciente['Estándar'][0]] ] );
												}
												if (isset($name_data_paciente['Especialidad']) && count($name_data_paciente['Especialidad'])>0 && isset($name_data_paciente['Especialidad'][0])) {
													$temp = array_merge( $temp,  [ 'Especialidad' => $activas[$l][$name_data_paciente['Especialidad'][0]] ] );
												}
												if (isset($name_data_paciente['Diagnóstico']) && count($name_data_paciente['Diagnóstico'])>0 && isset($name_data_paciente['Diagnóstico'][0])) {
													$temp = array_merge( $temp,  [ 'Diagnóstico' => $activas[$l][$name_data_paciente['Diagnóstico'][0]] ] );
												}
												if (isset($name_data_paciente['Discapacidad']) && count($name_data_paciente['Discapacidad'])>0 && isset($name_data_paciente['Discapacidad'][0])) {
													$temp = array_merge( $temp,  [ 'Discapacidad' => $activas[$l][$name_data_paciente['Discapacidad'][0]] ] );
												}
												if (isset($name_data_paciente['Procedencia']) && count($name_data_paciente['Procedencia'])>0 && isset($name_data_paciente['Procedencia'][0])) {
													$temp = array_merge( $temp,  [ 'Procedencia' => $activas[$l][$name_data_paciente['Procedencia'][0]] ] );
												}
												if (isset($name_data_paciente['Área']) && count($name_data_paciente['Área'])>0 && isset($name_data_paciente['Área'][0])) {
													$temp = array_merge( $temp,  [ 'Área' => $activas[$l][$name_data_paciente['Área'][0]] ] );
												}
												$temp = array_merge($temp, ['_id'=>$activas[$l]['_id']['oid']]);
												$tmp_ok=array_merge($tmp_ok, $temp);
												$tmp_activas[] = $tmp_ok;
											}
											if ($bandera) {
												$temp = ['fecha'=> date('Y-m',  strtotime($activas[$l][$keys_fechas[0]]))];
												$texto=[];
												if (count($name_data_paciente['Nombre'])>0) {
													for ($im=0; $im < count($name_data_paciente['Nombre']); $im++) { 
														if (isset($activas[$l][$name_data_paciente['Nombre'][$im]])) {
															$texto[]=$activas[$l][$name_data_paciente['Nombre'][$im]];
														}															
													}
												}
												if (count($name_data_paciente['Apellido'])>0) {
													for ($im=0; $im < count($name_data_paciente['Apellido']); $im++) { 
														if (isset($activas[$l][$name_data_paciente['Apellido'][$im]])) {
															$texto[]=$activas[$l][$name_data_paciente['Apellido'][$im]];
														}
													}
												}
												$temp = array_merge($temp, ['Nombre'=>join(' ', $texto)]);
												if (count($name_data_paciente['Sexo'])>0 && isset($activas[$l][$name_data_paciente['Sexo'][0]])) {
													$temp = array_merge($temp, ['Sexo'=>$activas[$l][$name_data_paciente['Sexo'][0]]]);
												}
												$texto='';
												if (count($name_data_paciente['Teléfono'])>0) {
													for ($im=0; $im < count($name_data_paciente['Teléfono']); $im++) { 
														if (isset($activas[$l][$name_data_paciente['Teléfono'][$im]]) && strlen($activas[$l][$name_data_paciente['Teléfono'][$im]])==10 && substr($activas[$l][$name_data_paciente['Teléfono'][$im]], 0, 1)=="3") {
															$texto=$activas[$l][$name_data_paciente['Teléfono'][$im]];
															break;
														}
													}
												}
												$temp = array_merge($temp, ['Teléfono'=>$texto]);
												if (count($name_data_paciente['Fecha de nacimiento'])>0 && isset($activas[$l][$name_data_paciente['Fecha de nacimiento'][0]])) {
													$temp = array_merge($temp, ['Fecha de nacimiento'=>$activas[$l][$name_data_paciente['Fecha de nacimiento'][0]]]);
												}
												if (isset($name_data_paciente['Sede']) && count($name_data_paciente['Sede'])>0 && isset($name_data_paciente['Sede'][0])) {
													$temp = array_merge( $temp,  [ 'Sede' => $activas[$l][$name_data_paciente['Sede'][0]] ] );
												}
												if (isset($name_data_paciente['Profesional']) && count($name_data_paciente['Profesional'])>0 && isset($name_data_paciente['Profesional'][0])) {
													$temp = array_merge( $temp,  [ 'Profesional' => $activas[$l][$name_data_paciente['Profesional'][0]] ] );
												}
												if (isset($name_data_paciente['Estándar']) && count($name_data_paciente['Estándar'])>0 && isset($name_data_paciente['Estándar'][0])) {
													$temp = array_merge( $temp,  [ 'Estándar' => $activas[$l][$name_data_paciente['Estándar'][0]] ] );
												}
												if (isset($name_data_paciente['Especialidad']) && count($name_data_paciente['Especialidad'])>0 && isset($name_data_paciente['Especialidad'][0])) {
													$temp = array_merge( $temp,  [ 'Especialidad' => $activas[$l][$name_data_paciente['Especialidad'][0]] ] );
												}
												if (isset($name_data_paciente['Diagnóstico']) && count($name_data_paciente['Diagnóstico'])>0 && isset($name_data_paciente['Diagnóstico'][0])) {
													$temp = array_merge( $temp,  [ 'Diagnóstico' => $activas[$l][$name_data_paciente['Diagnóstico'][0]] ] );
												}
												if (isset($name_data_paciente['Discapacidad']) && count($name_data_paciente['Discapacidad'])>0 && isset($name_data_paciente['Discapacidad'][0])) {
													$temp = array_merge( $temp,  [ 'Discapacidad' => $activas[$l][$name_data_paciente['Discapacidad'][0]] ] );
												}
												if (isset($name_data_paciente['Procedencia']) && count($name_data_paciente['Procedencia'])>0 && isset($name_data_paciente['Procedencia'][0])) {
													$temp = array_merge( $temp,  [ 'Procedencia' => $activas[$l][$name_data_paciente['Procedencia'][0]] ] );
												}
												if (isset($name_data_paciente['Área']) && count($name_data_paciente['Área'])>0 && isset($name_data_paciente['Área'][0])) {
													$temp = array_merge( $temp,  [ 'Área' => $activas[$l][$name_data_paciente['Área'][0]] ] );
												}
												$temp = array_merge($temp, ['_id'=>$activas[$l]['_id']['oid']]);
												$tmp_bad=array_merge($tmp_bad, $temp);
												$tmp_activas_bad[] = $tmp_bad;
											}												
										}
										if (count($tmp_activas)>0) {
											$data_ok = array_merge($data_ok, $tmp_activas);
										}
										if (count($tmp_activas_bad)>0) {
											$data_bad = array_merge($data_bad, $tmp_activas_bad);
										}											
									}										
								}								
								$total_registros_sin_otros = count($data_bad);
								/*echo "<br>TOTAL SIN FECHAS ".$total_registros_sin_fecha."<br>";
								echo "<br>TOTAL SIN OTROS  FILTROS  ".count($data_bad)."<br>";
								echo "<br>TOTAL OK ".count($data_ok)."<br>";*/
								//afrupar la data por meses y organizar de menor a mayor
								$data_bad = groupByDate($data_bad);
								$data_bad_total = $data_bad['total'];
								$data_bad_pacientes = $data_bad['pacientes'];
								/*echo "<pre>";
								print_r($data_bad_pacientes);
								echo "</pre>";*/
								
								$total_registros_ok = count($data_ok);
								$data_ok = groupByDate($data_ok);
								$data_ok_total = $data_ok['total'];
								$data_ok_pacientes = $data_ok['pacientes'];
								$fecha_total = array_unique(array_merge(array_column($data_bad_total, 'fecha'), array_column($data_ok_total, 'fecha')));
								sort($fecha_total);
								if (!array_key_exists($acronym."_".remove_accent($name_categoria[0]), $info_punto)) {
									$info_punto=array_merge($info_punto, array($acronym."_".remove_accent($name_categoria[0])=>array()));
								}
								if (!array_key_exists($acronym."_".remove_accent($name_categoria[0])."_bad", $info_punto)) {
									$info_punto=array_merge($info_punto, array($acronym."_".remove_accent($name_categoria[0])."_bad"=>array()));
								}
								$data_puntos = setInfoPuntoByColor($color_azul, $copy_azul, $acronym."_".remove_accent($name_categoria[0]), "Cumple", $info_punto, false);
								$color_azul = $data_puntos['color'];
								$info_punto = $data_puntos['info'];
								$data_puntos = setInfoPuntoByColor($color_rojo, $copy_rojo, $acronym."_".remove_accent($name_categoria[0])."_bad", "No cumple", $info_punto, true);
								$color_rojo = $data_puntos['color'];
								$info_punto = $data_puntos['info'];
								/*echo "<br>info_punto";
								echo "<pre>";
								print_r($info_punto);
								echo "</pre>";*/
								$rank_standar = [];
								$fecha_final = [];
								//SACA LOS PORCENTAJES POR FECHAS
								for ($ij=0; $ij < count($fecha_total); $ij++) { 
									$campo = $fecha_total[$ij];
									$sum_bad = getTotalByDate($data_bad_total, $campo);
									$sum_ok = getTotalByDate($data_ok_total, $campo);
									$total_by_indicador = ($info_name_type_indicator[$ii] == 'Indicador Proceso') ? $total_registros : $sum_bad+$sum_ok;
									$sum_bad = ($info_name_type_indicator[$ii] == 'Indicador Proceso') ? $total_registros-$sum_ok : $sum_bad;
									$total_porcentaje_ok = ($sum_ok!=0 && ($total_by_indicador)!=0)? (float)round((($sum_ok/($total_by_indicador)) * 100),2) : (float) 0;
									$total_porcentaje_bad = 100-$total_porcentaje_ok;									
									//CLASIFICACION
									$data_clasificada = [];
									if (array_key_exists($campo, $data_bad_pacientes) && count($reglas_by_grupos)>0) {
										$some_data_bad_pacientes = $data_bad_pacientes[$campo];
										foreach ($some_data_bad_pacientes as $k_sdbp => $v_sdbp) {
											foreach ($reglas_by_grupos as $k_rbg => $v_rgb) {
												$type_ruler = $v_rgb['type_ruler'];
												$name_ruler = $v_rgb['name'];
												$sum_activas = 0;
												$total_ = 0;
												foreach ($v_rgb['datos'] as $k_datos => $v_datos) {
													//por reglas
													$total_ = count($v_datos);
													$activas = getComparisonData($v_datos, $v_sdbp);
													if (count($activas)>0) {
														$sum_activas=count($activas);
														break;
													}
												}
												//echo "<br> $type_ruler ---- $sum_activas ---- $total_ ---- $name_ruler ";
												if ($type_ruler=='y' && $sum_activas==$total_) {
													//echo " y ";
													$some_data_bad_pacientes[$k_sdbp] = array_merge($some_data_bad_pacientes[$k_sdbp], ['Clasificación'=>$name_ruler]);
												}
												if ($type_ruler=='y/o' && $sum_activas>=1) {
													//echo " y/o ";
													$some_data_bad_pacientes[$k_sdbp] = array_merge($some_data_bad_pacientes[$k_sdbp], ['Clasificación'=>$name_ruler]);
												}
											}
										}
										/*echo "<pre>";
										print_r($some_data_bad_pacientes);
										echo "</pre>";*/
										$clasificacion_ = array_column($some_data_bad_pacientes, 'Clasificación');
										$count_clasificacion = array_count_values($clasificacion_);
										$total_clasificacion = count($some_data_bad_pacientes);
										$uu=0;
										//echo "<pre>";print_r($count_clasificacion);echo "</pre>";
										//echo "<pre>";print_r($name_ranking);echo "</pre>";
										if (count($count_clasificacion)>0) {
											foreach ($name_ranking as $k_nr => $v_nr) {
												if (!in_array($v_nr, $count_clasificacion)) {
													$words_ = explode(" ", remove_accent($v_nr));
													$acronym_ = "";
													foreach ($words_ as $w) {
													  $acronym_ .= $w[0];
													}
													$data_clasificada=array_merge($data_clasificada, [
														"num_".$acronym."_".remove_accent($name_categoria[0])."_".$acronym_ => 0,
														"denominador_".$acronym."_".remove_accent($name_categoria[0])."_".$acronym_ => $total_clasificacion,
														"$v_nr"=>0
													]);

													if (!array_key_exists($acronym."_".remove_accent($name_categoria[0])."_".$acronym_, $info_punto)) {
														$info_punto=array_merge($info_punto, array($acronym."_".($name_categoria[0])."_".$acronym_=>array()));
													}
													$stacked_=($k_nr==0)?false:true;
													$data_puntos = setInfoPuntoByColor($color_rojo_intenso, $copy_rojo_intenso, $acronym."_".remove_accent($name_categoria[0])."_".$acronym_, "$v_nr", $info_punto, $stacked_);
													$color_rojo_intenso = $data_puntos['color'];
													$info_punto = $data_puntos['info'];
													$uu++;
												}
											}
										}
										foreach ($count_clasificacion as $k_cc => $v_cc) {
											$words_ = explode(" ", remove_accent($k_cc));
											$acronym_ = "";
											foreach ($words_ as $w) {
											  $acronym_ .= $w[0];
											}
											$data_clasificada=array_merge($data_clasificada, [
												"num_".$acronym."_".remove_accent($name_categoria[0])."_".$acronym_ => $v_cc,
												"denominador_".$acronym."_".remove_accent($name_categoria[0])."_".$acronym_ => $total_clasificacion,
												"$k_cc"=>(float)round((($v_cc/$total_clasificacion) * 100),2)
											]);

											if (!array_key_exists($acronym."_".remove_accent($name_categoria[0])."_".$acronym_, $info_punto)) {
												$info_punto=array_merge($info_punto, array($acronym."_".($name_categoria[0])."_".$acronym_=>array()));
											}
											$stacked_=($k_cc==$name_ranking[0])?false:true;
											$data_puntos = setInfoPuntoByColor($color_rojo, $copy_rojo, $acronym."_".remove_accent($name_categoria[0])."_".$acronym_, "$k_cc", $info_punto, $stacked_);
											$color_rojo = $data_puntos['color'];
											$info_punto = $data_puntos['info'];
											$uu++;
										}
										$data_bad_pacientes[$campo]=$some_data_bad_pacientes;									
									}else{
										$data_bad_pacientes[$campo]=array();
									}	
									$activas = $activas=array_filter($array_standar, function ($tT) use ($total_porcentaje_ok) 
									{
										if (($tT->operator=='='  && $total_porcentaje_ok==$tT->value) || 
											($tT->operator=='<'  && $total_porcentaje_ok< $tT->value) || 
											($tT->operator=='<=' && $total_porcentaje_ok<=$tT->value) || 
											($tT->operator=='>'  && $total_porcentaje_ok> $tT->value) || 
											($tT->operator=='>=' && $total_porcentaje_ok>=$tT->value) ||
											($tT->operator=='!=' && $total_porcentaje_ok!=$tT->value) || 
											($tT->operator=='rango' && 
											((($tT->operator1=='<=' && $tT->operator2=='<=') && (($total_porcentaje_ok<$tT->value1 && $total_porcentaje_ok<$tT->value2) || $total_porcentaje_ok==$tT->value1 || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='<=' && $tT->operator2=='>=') && (($total_porcentaje_ok < $tT->value1 && $total_porcentaje_ok > $tT->value2) || $total_porcentaje_ok==$tT->value1 || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='>=' && $tT->operator2=='<=') && (($total_porcentaje_ok > $tT->value1 && $total_porcentaje_ok < $tT->value2) || $total_porcentaje_ok==$tT->value1 || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='>=' && $tT->operator2=='>=') && (($total_porcentaje_ok > $tT->value1 && $total_porcentaje_ok > $tT->value2) || $total_porcentaje_ok==$tT->value1 || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='>=' && $tT->operator2=='>') && (($total_porcentaje_ok > $tT->value1 && $total_porcentaje_ok > $tT->value2) || $total_porcentaje_ok==$tT->value1)) ||
											( ($tT->operator1=='>=' && $tT->operator2=='<') && (($total_porcentaje_ok > $tT->value1 && $total_porcentaje_ok < $tT->value2) || $total_porcentaje_ok==$tT->value1)) || 
											( ($tT->operator1=='<=' && $tT->operator2=='<') && (($total_porcentaje_ok < $tT->value1 && $total_porcentaje_ok < $tT->value2) || $total_porcentaje_ok==$tT->value1)) ||
											( ($tT->operator1=='<=' && $tT->operator2=='>') && (($total_porcentaje_ok < $tT->value1 && $total_porcentaje_ok > $tT->value2) || $total_porcentaje_ok==$tT->value1)) ||
											( ($tT->operator1=='>' && $tT->operator2=='>=') && (($total_porcentaje_ok > $tT->value1 && $total_porcentaje_ok > $tT->value2) || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='<' && $tT->operator2=='>=') && (($total_porcentaje_ok < $tT->value1 && $total_porcentaje_ok > $tT->value2) || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='>' && $tT->operator2=='<=') && (($total_porcentaje_ok > $tT->value1 && $total_porcentaje_ok < $tT->value2) || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='<' && $tT->operator2=='<=') && (($total_porcentaje_ok < $tT->value1 && $total_porcentaje_ok < $tT->value2) || $total_porcentaje_ok==$tT->value2)) ||
											( ($tT->operator1=='<' && $tT->operator2=='<') && ($total_porcentaje_ok < $tT->value1 && $total_porcentaje_ok < $tT->value2)) ||
											( ($tT->operator1=='>' && $tT->operator2=='>') && ($total_porcentaje_ok > $tT->value1 && $total_porcentaje_ok > $tT->value2))
											))
											) {
											return $tT;
										}
									});
									$activas=array_values($activas);
									if (count($activas)>0) {
										/*$name_standar=$activas[0]->type_standar->name;
										$rank_standar["$name_categoria"][$ij] = ['label'=>$campo, 'name_standar'=>$name_standar];*/									
										$data_ = [
											'label'=>$campo, 
											"num_".$acronym."_".remove_accent($name_categoria[0])=>$sum_ok, 
											"denominador_".$acronym."_".remove_accent($name_categoria[0])=>($total_by_indicador), 
											"num_".$acronym."_".remove_accent($name_categoria[0])."_bad"=>$sum_bad, 
											"denominador_".$acronym."_".remove_accent($name_categoria[0])."_bad"=>($total_by_indicador),
											"Cumple"=>$total_porcentaje_ok, 
											"No cumple"=>$total_porcentaje_bad,
											'data_ok' => (array_key_exists($campo, $data_ok_pacientes)) ? $data_ok_pacientes[$campo] : array(),
											'data_bad' => $data_bad_pacientes[$campo],
											'name_chart' => 'chartdiv'.$num_grafica
										];
										//"$name_categoria"=>$total_porcentaje_ok, "$name_categoria BAD"=>$total_porcentaje_bad,

										$fecha_final[] = array_merge($data_, $data_clasificada);							
									}		
								}
								/*echo "<pre>";
								print_r($fecha_final);
								echo "</pre>";*/
								$graficas[]=['puntos_grafica'=>$fecha_final, 'label'=>$info_punto, 'total_registros'=>$total_registros, 'total_registros_sin_fecha'=>$total_registros_sin_fecha, 'total_registros_sin_otros'=>$total_registros_sin_otros, 'total_registros_ok'=>$total_registros_ok, 'name'=>$get_indicador['name']." - ".$info_name_type_indicator[$ii], 'description'=>$get_indicador['description'], 'info_standard'=>$array_standar]; //'standar'=>$rank_standar, 
								$num_grafica++;
							}							
						}						
					}
				}
			}//foreach  ids_eventos
			//printArray($graficas);
			if (count($graficas)>0) {
				print json_encode(array('res'=>'ok', 'graficas'=>$graficas));
			}else{
				print json_encode(array('res'=>'bad', 'msj'=>'Debe agregar contenido al indicador'));
			}
		}else{
			print json_encode(array('res'=>'bad', 'msj'=>'Debe crear la configuración de indicadores para le entidad.'));
		}		
	}

	public function exportTablasPacientesProcesadaBienMal()
	{

		$info = json_decode($this->input->post('info'));
		$info = objectToArray($info);

		$writer = WriterEntityFactory::createXLSXWriter();
		// $writer = WriterEntityFactory::createODSWriter();
		// $writer = WriterEntityFactory::createCSVWriter();
		$fileName='/var/www/html/ips_virtual/export_excel/reporte.xlsx';
		//chmod($fileName, 0777);
		$url='export_excel/reporte.xlsx';
		$writer->openToFile($fileName); // write data to a file or to a PHP stream
		//$writer->openToBrowser($fileName); // stream data directly to the browser

		$style_ok = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(15)
           ->setFontColor(Color::GREEN)
           ->build();

        $style_bad = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(15)
           ->setFontColor(Color::RED)
           ->build();

		if (count($info['data_ok'])>0) {
			$singleRow = WriterEntityFactory::createRow([ WriterEntityFactory::createCell('Pacientes controlados') ], $style_ok);
			$writer->addRow($singleRow);

			$keys = array_keys($info['data_ok'][0]);
			$rowFromValues = WriterEntityFactory::createRowFromArray($keys);
			$writer->addRow($rowFromValues);

			$pacientes = [];
			for ($i=0; $i < count($info['data_ok']); $i++) { 
				$limpis = [];
				for ($j=0; $j < count($keys); $j++) { 
					$limpis[]=(isset($info['data_ok'][$i][$keys[$j]]))?$info['data_ok'][$i][$keys[$j]]:'';
				}
				$rowFromValues = WriterEntityFactory::createRowFromArray($limpis);
				$writer->addRow($rowFromValues);
			}
		}

		if (count($info['data_bad'])>0) {
			$singleRow = WriterEntityFactory::createRow([ WriterEntityFactory::createCell('Pacientes no controlados') ], $style_bad);
			$writer->addRow($singleRow);

			$keys = array_keys($info['data_bad'][0]);
			$rowFromValues = WriterEntityFactory::createRowFromArray($keys);
			$writer->addRow($rowFromValues);

			$pacientes = [];
			for ($i=0; $i < count($info['data_bad']); $i++) { 
				$limpis = [];
				for ($j=0; $j < count($keys); $j++) { 
					$limpis[]=$info['data_bad'][$i][$keys[$j]];
				}
				$rowFromValues = WriterEntityFactory::createRowFromArray($limpis);
				$writer->addRow($rowFromValues);
			}
		}		

		$writer->close();

		print json_encode(array('res'=>'ok', 'url'=>$url));
	}

	public function exportAllDataTabla()
	{
		$name_excel = $this->input->post('name');
		$id_entidad = $this->input->post('id_entidad');
		$info_1 = json_decode($this->input->post('info'));
		$info_1 = objectToArray($info_1);

		$name_genericos_ = [];
		$arrays_keys = [];
		foreach ($info_1 as $k_1 => $v_1) {
			if (count($v_1['data_ok'])>0 ) {
				foreach ($v_1['data_ok'] as $k_2 => $v_2) {
					$keys_ = array_keys($v_2);
					$arrays_keys = array_merge($arrays_keys, $keys_);
				}
			}
		}
		$arrays_keys = array_unique($arrays_keys);
		
		foreach ($arrays_keys as $k_a => $v_a) {
			if ($v_a=='fecha' || $v_a=='Sexo' || $v_a=='Nombre' || $v_a=='Teléfono' || $v_a=='Fecha de nacimiento' || $v_a=='_id') {
				unset($arrays_keys[$k_a]);
			}
		}
		$arrays_keys = array_values($arrays_keys);
		

		//OBTIENE LOS NOMBRES GENERICOS
		$name_genericos = $this->collection_model->get_collection('columns_configuration', ['$and' => [ ['entity.id'=>$id_entidad], ['columns'=>['$in' => $arrays_keys]], ['state'=>'1'] ]], ['projection'=>['name_generico'=>'1', 'columns'=>'1']]);
		$name_genericos=objectToArray($name_genericos);
		$all_names = array_column($name_genericos, 'name_generico');
		
		if ( count($all_names)==0 ) {
			print json_encode(array('res'=>'bad', 'msj'=>'Debe configurar los nombres genéricos para el reporte de datos.'));
			exit();
		}
		
		$info = [];
		for ($i=0; $i < count($info_1); $i++) { 
			if ( count($info_1[$i]['data_ok'])>0 ) {
				for ($j=0; $j < count($info_1[$i]['data_ok']); $j++) { 
					$tmp=[];
					foreach ($info_1[$i]['data_ok'][$j] as $k_1 => $v_1) {	
						$campo = $k_1;					
						$activas_1=array_filter($name_genericos, function ($tT) use ($campo) 
						{
							if (in_array($campo, $tT['columns'])) {
								return $tT;
							}
						});
						$activas_1=array_values($activas_1);
						if (count($activas_1)>0) {
							$name = $activas_1[0]['name_generico'];
							$info_1[$i]['data_ok'][$j][$name] = $v_1;
							unset($info_1[$i]['data_ok'][$j][$k_1]);
						}
					}
					$info[] =  $info_1[$i]['data_ok'][$j];
				}				
			}

			if ( count($info_1[$i]['data_bad'])>0 ) {
				for ($j=0; $j < count($info_1[$i]['data_bad']); $j++) { 
					$tmp=[];
					foreach ($info_1[$i]['data_bad'][$j] as $k_1 => $v_1) {	
						$campo = $k_1;					
						$activas_1=array_filter($name_genericos, function ($tT) use ($campo) 
						{
							if (in_array($campo, $tT['columns'])) {
								return $tT;
							}
						});
						$activas_1=array_values($activas_1);
						if (count($activas_1)>0) {
							$name = $activas_1[0]['name_generico'];
							$info_1[$i]['data_bad'][$j][$name] = $v_1;
							unset($info_1[$i]['data_bad'][$j][$k_1]);
						}	
					}
					$info[] =  $info_1[$i]['data_bad'][$j];
				}				
			}
		}

		$writer = WriterEntityFactory::createXLSXWriter();
		// $writer = WriterEntityFactory::createODSWriter();
		// $writer = WriterEntityFactory::createCSVWriter();
		$fileName='/var/www/html/ips_virtual/export_excel/reporte.xlsx';
		//chmod($fileName, 0777);
		$url='export_excel/reporte.xlsx';
		$writer->openToFile($fileName); // write data to a file or to a PHP stream
		//$writer->openToBrowser($fileName); // stream data directly to the browser

		$style_ok = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(15)
           ->build();

        $style_bad = (new StyleBuilder())
           ->setFontBold()
           ->setFontSize(15)
           ->setFontColor(Color::RED)
           ->build();

		if (count($info)>0) {
			$singleRow = WriterEntityFactory::createRow([ WriterEntityFactory::createCell($name_excel) ], $style_ok);
			$writer->addRow($singleRow);

			$keys = array_keys($info[0]);
			$rowFromValues = WriterEntityFactory::createRowFromArray($keys);
			$writer->addRow($rowFromValues);

			$pacientes = [];
			for ($i=0; $i < count($info); $i++) { 
				$limpis = [];
				for ($j=0; $j < count($keys); $j++) { 
					$limpis[]=( isset($info[$i][$keys[$j]]) ) ? $info[$i][$keys[$j]] : '' ;

				}
				$rowFromValues = WriterEntityFactory::createRowFromArray($limpis);
				$writer->addRow($rowFromValues);
			}
		}		

		$writer->close();

		print json_encode(array('res'=>'ok', 'url'=>$url));
	}

}

function objectToArray ( $object ) {
  	if(!is_object($object) && !is_array($object)) {
    	return $object;
  	}  
  	return array_map( 'objectToArray', (array) $object );
}

function addColumnasConfiguracion($tags, $id){
	$ci =& get_instance();
	$i = 0;
	foreach ($tags as $t) {
		$data = array('id_columnas'=>$t,'id_columnas_variables'=>$id);
		$query=$ci->db->insert('columnas_configuracion',$data);
		if (!$query) {
			$i++;
		}
	}
	return $i;
}

function filterByFecha($activas, $campo, $modo)
{
	$activas=array_filter($activas, function ($tT) use ($campo, $modo) 
	{
		if ( ($tT->$campo!='' && $tT->$campo!='NULL' && $modo=='1') || (($tT->$campo=='' || $tT->$campo=='NULL') && $modo=='2') ) {
			return $tT;
		}
	});
	return array_values($activas);
}

function groupByDate($data)
{
	$rta = ['total'=>array(),'pacientes'=>array()];
	if (count($data)>0) {
		$array_data=array_map(null, array_column($data, 'fecha'), $data, array_column($data, '_id'));
		//se organizan de menor a mayor por fecha
		array_multisort(array_column($data, 'fecha'), SORT_ASC, $data, SORT_ASC, $array_data);
		$fechas_unique = array_values(array_unique(array_column($array_data, 0)));
		for ($j=0; $j < count($fechas_unique); $j++) { 
			$campo = $fechas_unique[$j];
			$activas=array_filter($array_data, function ($tT) use ($campo) 
			{
				if ($campo==$tT[0]) {
					return $tT;
				}
			});
			$activas = array_values($activas);
			$rta['total'][] = ['fecha'=>$campo, 'total'=>count($activas) ];
			$rta['pacientes'][$campo] = array_column($activas, 1);
		}
	}

	return $rta;
}

function getTotalByDate($data, $campo)
{
	$rta = 0;
	$activas=array_filter($data, function ($tT) use ($campo) 
	{
		if ($campo==$tT['fecha']) {
			return $tT;
		}
	});
	$activas = array_values($activas);
	if (count($activas)>0) {
		$rta = $activas[0]['total'];
	}
	return $rta;
}

function setInfoPuntoByColor($color, $color_copy, $name, $full_name, $info_punto, $stacked ) {
	if (count($color)==0) {
		$color=$color_copy;
	}
	$value_azul=array_rand($color);
	$info_punto[$name]=array_merge($info_punto[$name], array(
		'name'=>$full_name, //porcentaje
		'num'=>$name, //num
		'full_name'=>$full_name, //porcentaje
		'stacked'=>$stacked,
		'color'=>$color[$value_azul]
	));
	unset($color[$value_azul]);
	return array('color'=>$color, 'info'=>$info_punto);
}
//array, regla
function getComparisonData($data_regla, $tT)
{	
	/*echo "<pre>";
	print_r($data_regla);
	echo "</pre>";
	echo "<pre>";
	print_r($tT);
	echo "</pre>";*/
	$activas=array_filter($data_regla, function ($regla) use ($tT) 
	{	
		$columna = $regla['colums'][0];
		if (array_key_exists($columna, $tT)) {
			///echo "<br>".$tT[$columna]." ".$regla['operator1']." ".$regla['operator2']." ".$regla['value1']." ".$regla['value2']." ".$regla['value'];
			if ($tT[$columna]!='' && strtolower($tT[$columna])!='null' && strtolower($tT[$columna])!=null) {
				if (($regla['operator']=='='  && $tT[$columna]==$regla['value']) || 
					($regla['operator']=='<'  && $tT[$columna]< $regla['value']) || 
					($regla['operator']=='<=' && $tT[$columna]<=$regla['value']) || 
					($regla['operator']=='>'  && $tT[$columna]> $regla['value']) || 
					($regla['operator']=='>=' && $tT[$columna]>=$regla['value']) || 
					($regla['operator']=='!=' && $tT[$columna]!=$regla['value']) || 
					($regla['operator']=='rango' && 
						((($regla['operator1']=='<=' && $regla['operator2']=='<=') && (($tT[$columna]<$regla['value1'] && $tT[$columna]<$regla['value2']) || $tT[$columna]==$regla['value1'] || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='<=' && $regla['operator2']=='>=') && (($tT[$columna] < $regla['value1'] && $tT[$columna] > $regla['value2']) || $tT[$columna]==$regla['value1'] || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='>=' && $regla['operator2']=='<=') && (($tT[$columna] > $regla['value1'] && $tT[$columna] < $regla['value2']) || $tT[$columna]==$regla['value1'] || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='>=' && $regla['operator2']=='>=') && (($tT[$columna] > $regla['value1'] && $tT[$columna] > $regla['value2']) || $tT[$columna]==$regla['value1'] || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='>=' && $regla['operator2']=='>') && (($tT[$columna] > $regla['value1'] && $tT[$columna] > $regla['value2']) || $tT[$columna]==$regla['value1'])) ||
							( ($regla['operator1']=='>=' && $regla['operator2']=='<') && (($tT[$columna] > $regla['value1'] && $tT[$columna] < $regla['value2']) || $tT[$columna]==$regla['value1'])) || 
							( ($regla['operator1']=='<=' && $regla['operator2']=='<') && (($tT[$columna] < $regla['value1'] && $tT[$columna] < $regla['value2']) || $tT[$columna]==$regla['value1'])) ||
							( ($regla['operator1']=='<=' && $regla['operator2']=='>') && (($tT[$columna] < $regla['value1'] && $tT[$columna] > $regla['value2']) || $tT[$columna]==$regla['value1'])) ||
							( ($regla['operator1']=='>' && $regla['operator2']=='>=') && (($tT[$columna] > $regla['value1'] && $tT[$columna] > $regla['value2']) || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='<' && $regla['operator2']=='>=') && (($tT[$columna] < $regla['value1'] && $tT[$columna] > $regla['value2']) || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='>' && $regla['operator2']=='<=') && (($tT[$columna] > $regla['value1'] && $tT[$columna] < $regla['value2']) || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='<' && $regla['operator2']=='<=') && (($tT[$columna] < $regla['value1'] && $tT[$columna] < $regla['value2']) || $tT[$columna]==$regla['value2'])) ||
							( ($regla['operator1']=='<' && $regla['operator2']=='<') && ($tT[$columna] < $regla['value1'] && $tT[$columna] < $regla['value2'])) ||
							( ($regla['operator1']=='>' && $regla['operator2']=='>') && ($tT[$columna] > $regla['value1'] && $tT[$columna] > $regla['value2']))
						))) {
					//echo "string";
					return $regla;
				}
			}else{
				if ($regla['operator']=='='  && $tT[$columna]==$regla['value']) {
					return $regla;
				}
			}				
		}
	});

	$activas=(count($activas)>0) ? array_values($activas) : $activas;
	/*echo "<br>activas";
	echo "<pre>";
	print_r($activas);
	echo "</pre>";*/
	return $activas;
}

function remove_accent($str){
	$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
	$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	return str_replace($a, $b, $str); 
}

function printArray($e)
{
	echo "<pre>";
	print_r($e);
	echo "</pre>";
}
?>