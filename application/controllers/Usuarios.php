<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('backend_administrador');
		$this->load->model('collection_model');
	}

	public function login(){
		$email = $this->input->post('email');
		if($email=="" ){
			print json_encode(array("res"=>"bad","msj" =>"El correo electrónico es un campo obligatorio, por favor inténtalo de nuevo"));
			exit();
		}
		$password = $this->input->post('contrasena');
		if($password==""){
			print json_encode(array("res"=>"bad","msj" =>"La contraseña es un campo obligatorio, por favor inténtalo de nuevo"));
			exit();
		}
		$user = $this->backend_administrador->get_user_login($email, $password);
		if (!is_null($user)) {
			$this->session->set_userdata('_id', (String) $user->_id);
			$this->session->set_userdata('email', $user->email);
			$this->session->set_userdata('rol', $user->rol->name);
			$this->session->set_userdata('image', (isset($user->imagen))?$user->imagen:'https://s3.amazonaws.com/ipsvirtual/user_default.png');
			$this->session->set_userdata('logged_in', TRUE);
			$this->session->set_userdata('id_rol', $user->rol->id);
			$this->session->set_userdata('id_entidad', (isset($user->entity->id))?$user->entity->id:'0');
			$this->session->set_userdata('nombre', $user->name);

			$permisos = $this->collection_model->get_collection('roles', ['_id' => new MongoDB\BSON\ObjectId($user->rol->id)], ['projection' => ['views'=>1]]);
			if (count($permisos)>0) {
				$permisos= objectToArray($permisos[0]->views);
				$permisos = array_column($permisos, 'id');
				$this->session->set_userdata('vistas',  $permisos);
				print json_encode(array("res"=>"ok"));
			}else{
				print json_encode(array("res"=>"bad", "msj"=>"Aún no tiene permisos en la herramienta, por favor, comunicarse con el Administrador"));
			}
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"La contraseña o el password incorrecto, o el usuario no existe."));
		}		
	}

	public function logout(){
		$this->session->sess_destroy();
 		print json_encode(array("res"=>"ok", "msj" => "La sesion ha terminado."));
 		redirect(base_url());
	}

	public function deleteUsuario(){
		$_id=$this->input->post('_id');
		$result=$this->collection_model->update_collection($_id, 'users', array('state'=>'2'));
		if ($result) {
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"No se pudo eliminar el usuario."));
		}
	}

	//new-update
	public function saveUsuario(){
		$_id = $this->input->post('id');
		$correo = $this->input->post('correo');
		$contrasena = $this->input->post('contrasena');
		$id_rol = $this->input->post('id_rol');
		$name_rol = $this->input->post('name_rol');
		$id_entidad = $this->input->post('id_entidad');
		$name_entidad = $this->input->post('name_entidad');
		$nombre = $this->input->post('nombre');

		$data=array('name'=>$nombre,'email'=>$correo,'password'=>$contrasena,'rol'=>array('id'=>$id_rol, 'name'=>$name_rol),'entity'=>array('id'=>$id_entidad, 'name'=>$name_entidad), 'image'=>'https://s3.amazonaws.com/ipsvirtual/user_default.png', 'state'=>'1');
		$tipo=($_id!='')?'actualiar':'guardar';
		$result=($_id!='')?$this->collection_model->update_collection($_id, 'users', $data):$this->collection_model->insert_collection('users', $data);

		if ($result) {
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"No se pudo ".$tipo." el usuario."));
		}
	}
	
	public function deleteEntidad(){
		$_id=$this->input->post('id', TRUE);

		$result=$this->collection_model->update_collection($_id, 'entities', array('state'=>'2'));

		if ($result) {
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"No se pudo ".$tipo." el usuario."));
		}

		/*$this->db->select('id');
		$this->db->where('id_entidad',$id);
		$result = $this->db->get('columnas_variables');
		if ($result->num_rows()>0) {
			$columnas_variables = $result->result();
			$columnas_variables = objectToArray($columnas_variables);
			$columnas_variables = implode(',',array_column($columnas_variables, 'id'));

			$this->db->select('id');
			$this->db->where_in('id_columnas_variables',$columnas_variables);
			$result = $this->db->get('columnas_configuracion');
			if ($result->num_rows()>0) {
				print json_encode(array('res'=>'bad', 'msj'=>'No se puede eliminar la entidad porque ya hay parametros configurados'));
				exit();
			}
		}
		$this->db->where('id', $id);
		$result=$this->db->delete('entidad');
		if ($result) {
			$this->db->where('id_entidad',$id);
			$this->db->delete('columnas_variables');
			$this->db->where('id_entidad',$id);
			$this->db->delete('entidad_indicador');
			$this->db->where('id_entidad',$id);
			$this->db->delete('entidad_periodicidad');
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"No se pudo eliminar la entidad."));
		}*/		
	}

	public function saveEntidad(){
		$_id = $this->input->post('id', TRUE);
		$nombre = $this->input->post('nombre', TRUE);

		$data=array('nombre'=>$nombre);
		$tipo=($_id!='')?'actualiar':'guardar';
		$result=($_id!='') ? $this->collection_model->update_collection($_id, 'entities', array('name'=>$nombre)) : $this->collection_model->insert_collection('entities', array('name'=>$nombre, 'state'=>'1'));
		if ($result && $_id=='') {
			$insert_id = (String) $result;
			$data = array( array('name'=>'Nombre', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Apellido', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Tipo documento', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Número documento', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Dirección', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Teléfono', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Municipio', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Sexo', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'IPS', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Departamento Residencia', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array('name'=>'Fecha de nacimiento', 'required'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ));
			$this->collection_model->insert_many_collection('columns_configuration', $data);
			$data = array( array('name'=>'Último año', 'required'=>'1', 'state'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre) ), array('name'=>'Último 6 meses', 'required'=>'1', 'state'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre) ), array('name'=>'Último semestre', 'required'=>'1', 'state'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre) ));
			$this->collection_model->insert_many_collection('entities_periodicity', $data);
			$data = array( array('name'=>'Normal', 'required'=>'1', 'state'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre) ), array('name'=>'Indicador de resultado', 'required'=>'1', 'state'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre) ), array('name'=>'Indicador Proceso', 'required'=>'1', 'state'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre) ));
			$this->collection_model->insert_many_collection('entities_indicators', $data);
			$data = array( array( 'name'=>'Desde', 'mode'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Hasta', 'mode'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Género', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Edad', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Sede', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Profesional', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Especialidad', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Eventos', 'mode'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Estándar', 'mode'=>'1', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Diagnóstico', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Discapacidad', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Procedencia', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ), array( 'name'=>'Área', 'mode'=>'0', 'entity'=>array('id'=>$insert_id, 'name'=>$nombre), 'columns'=>array(), 'state'=>'1' ));
			$this->collection_model->insert_many_collection('entities_filters', $data);
		}

		if ($result) {
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"No se pudo ".$tipo." la entidad."));
		}
	}

	public function deleteRol(){
		$_id=$this->input->post('_id');
		$result=$this->collection_model->update_collection($_id, 'roles', array('state' => '2'));
		if ($result) {
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"No se pudo eliminar el usuario."));
		}
	}

	public function saveRol(){
		$_id = $this->input->post('_id');
		$nombre = $this->input->post('nombre');

		$result=($_id!='')?$this->collection_model->update_collection($_id, 'roles', array('name' => $nombre)):$this->collection_model->insert_collection('roles', array('name' => $nombre, 'state' => '1', 'views' => array()));
		$tipo=($_id!='')?'actualizar':'guardar';

		if ($result) {
			print json_encode(array("res"=>"ok"));
		}else{
			print json_encode(array("res"=>"bad", "msj"=>"No se pudo ".$tipo." el rol."));
		}
	}

	public function permisoVista(){
		$vista = $this->input->post('vista');
		if($vista==""){
		   print json_encode(array("res"=>"bad","msj" =>"No viene vista"));
		   exit();
		}
		$rol = $this->input->post('rol');
		if($rol==""){
		   print json_encode(array("res"=>"bad","msj" =>"No viene rol"));
		   exit();
		}
		$estado = $this->input->post('estado');
		if($estado==""){
		   print json_encode(array("res"=>"bad","msj" =>"No viene estado"));
		   exit();
		}
		if($estado == "1"){//inserta
			$result=$this->backend_administrador->get_permisos_by_view_rol($vista, $rol);
			if($result>0){
				print json_encode(array("res"=>"ok","msj"=>"Permiso agregado con éxito"));
			}else{
				$result=$this->collection_model->update_push_collection($rol, 'roles', array('views'=>(object)array('id'=>$vista)));
				if($result){
					print json_encode(array("res"=>"ok","msj"=>"Permiso agregado con éxito"));
				}else{
					print json_encode(array("res"=>"bad","msj"=>"me pidio insertar y no pude"));
				}
			}
		}else{
			$result=$this->collection_model->update_pull_collection($rol, 'roles', array('views'=>(object)array('id'=>$vista)));
			if ($result) {
				print json_encode(array("res"=>"ok"));
			}else{
				print json_encode(array("res"=>"bad", "msj"=>"No se pudo eliminar el usuario."));
			}
		}
	}
}

function objectToArray ( $object ) {
  	if(!is_object($object) && !is_array($object)) {
    	return $object;
  	}  
  	return array_map( 'objectToArray', (array) $object );
}