<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacientes extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('collection_model');
	}

	public function guardarPaciente(){
		$id_entidad = $this->input->post('id_entidad');
		$info = $this->input->post('info');

		$all_columns = $this->collection_model->get_collection('columns_name', ['entity.id'=>$id_entidad], ['projection'=>['name'=>'1']]);
		$all_columns = objectToArray($all_columns);
		$name_columns = array_column($all_columns, 'name');

		$data_patient = array_fill_keys($name_columns, '');

		if (count($info)>0) {
			foreach ($info as $k => $v) {
				$data_patient[$v['column']] = $v['valor'];
			}
			$data_patient = array_merge($data_patient, ['entity'=>['id'=>$id_entidad]]);

			$result = $this->collection_model->insert_collection('patients', $data_patient);

			if ($result) {
				print json_encode(array('res'=>'ok', 'msj'=>'Paciente creado.'));
			}else{
				print json_encode(array('res'=>'bad', 'msj'=>'Problemas para crear el paciente.'));
			}
		}else{
			print json_encode(array('res'=>'bad', 'msj'=>'Debe agregar algún campo para crear el paciente.'));
		}
	}

	public function editarPaciente()
	{
		$id_paciente = $this->input->post('id_paciente');
		$info = $this->input->post('info');

		if (count($info)>0) {
			$result = $this->collection_model->update_collection($id_paciente, 'patients', [ $info['column'] => $info['valor'] ]);
			if ($result) {
				print json_encode(array('res'=>'ok', 'msj'=>'Paciente actualizado.'));
			}else{
				print json_encode(array('res'=>'bad', 'msj'=>'Problemas para actualizar el paciente.'));
			}
		}else{
			print json_encode(array('res'=>'bad', 'msj'=>'Debe agregar algún campo para actualziar el paciente.'));
		}
	}
	public function encuesta(){
		$this->load->view("pacientes/encuesta");
	}
	public function poblacionMapaDeCalor(){
		//hay que obtener latitud y longitud
		$estado=$this->input->post('estado',TRUE);
		$id_entidad=$this->input->post('entidad', TRUE);

		//$this->session->set_userdata('searchentidad6', $id_entidad);
		if ('Rojo intenso'==$estado) {
			$name=0;
		}elseif ('Rojo'==$estado) {
			$name=1;
		}elseif ('Amarillo'==$estado) {
			$name=2;
		}else{
			$name=3;
		}

		$all_pacients = $this->collection_model->get_total('patients', ['entity.id'=>$id_entidad]);
		$total = $this->collection_model->get_total('patients', ['$and'=>[['state_risk.name'=>$estado], ['entity.id'=>$id_entidad]]]);

		print json_encode(array('res'=>'ok', 'total'=>$total, 'total_pacientes'=>$all_pacients, 'name'=>$name));
	}
	public function pacienteDatoNoMedico(){
		$this->load->view("pacientes/pacienteDatoNoMedico");
	}
	public function pacienteTrazadoraRelevantes(){
		$id = $this->input->post('id');
		$datos = $this->input->post('datos');
		$observaciones = $this->input->post('observaciones');
		$orig_date = new DateTime();
		$orig_date=$orig_date->getTimestamp();

		if (isset($datos['condiciones_trazadoras'])) {
			$condiciones_trazadoras = $datos['condiciones_trazadoras'];			
			foreach ($condiciones_trazadoras as $k_ct => $v_ct) {
				$query = setDataTrazadoras($id, $v_ct);
				if (!$query) {
					print  json_encode(array('res'=>'bad', 'msj'=>'Problemas para  guardar las condiciones trazadoras.'));
					exit();
				}
			}
		}

		if (isset($datos['antecedentes_relevantes'])) {
			$antecedentes_relevantes = $datos['antecedentes_relevantes'];		
			foreach ($antecedentes_relevantes as $k_ar => $v_ar) {
				$query = setDataRelevante($id, $v_ar);
				if (!$query) {
					print  json_encode(array('res'=>'bad', 'msj'=>'Problemas para guardar los antecedentes relevantes.'));
					exit();
				}
			}
		}

		if (isset($observaciones) && $observaciones!='') {
			$total = $this->collection_model->get_total( 'historial_observaciones_patient', ['patient.id'=>$id] );
			if ($total>0) {
				$query = $this->collection_model->update_push_collection(false, 'historial_observaciones_patient', ['observaciones'=>[['observacion'=>$observaciones, 'registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)]]], ['patient.id'=>$id]);
			}else{
				$query = $this->collection_model->insert_collection('historial_observaciones_patient', ['patient'=>['id'=>$id], 'observaciones'=>[['observacion'=>$observaciones, 'registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)]]]);
			}			
			if (!$query) {
				print  json_encode(array('res'=>'bad', 'msj'=>'Problemas para guardar las observaciones.'));
				exit();
			}
		}

		print json_encode(array('res' =>'ok'));
		
	}
	public function pacienteIncidencia(){
		$id_patient = $this->input->post('id');
		$datos = $this->input->post('data');
		$modo = $this->input->post('modo');

		if ($modo == 'trazadora') {
			$query = setDataTrazadoras($id_patient, $datos[0]);
			if (!$query) {
				print  json_encode(array('res'=>'bad', 'msj'=>'Problemas para  guardar las condiciones trazadoras.'));
				exit();
			}
		}else{
			$query = setDataRelevante($id_patient, $datos[0]);
			if (!$query) {
				print  json_encode(array('res'=>'bad', 'msj'=>'Problemas para guardar los antecedentes relevantes.'));
				exit();
			}
		}

		print json_encode(array('res' =>'ok'));
	}
	public function pacienteDatosGrafica()
	{
		$id_indicator = $this->input->post('id_indicator');
		$id_patient = $this->input->post('id_patient');
		$id_entidad = $this->input->post('id_entity');

		/*$id_indicator = "5e38d14a0365000094001917";
		$id_patient = "5e373b1bb95400006500273c";
		$id_entidad = "5e338f93434c00008a006ab3";
		$estandar='';*/

		if ($id_patient=="5e373b1bb95400006500273c") {
			$id_patient="5e373b1bb954000065002793";
		}elseif ($id_patient=="5e373b1bb95400006500273d") {
			$id_patient="5e373b1bb9540000650027d0";
		}elseif ($id_patient=="") {
			$id_patient="5e373b1bb95400006500292e";
		}elseif ($id_patient=="5e373b1bb95400006500273f"){
			$id_patient="5e373b1bb954000065002822";
		}

		$ids_eventos = array($id_indicator);

		$graficas = [];
		$num_grafica = 1;
		if ($ids_eventos!='') {
			foreach ($ids_eventos as $k_ie => $v_ie) {
				$get_indicador = [];
				$indicador = $this->collection_model->get_collection('indicators', ['$and'=>[['_id' => new MongoDB\BSON\ObjectId($v_ie)], ['state'=>['$in'=>['1','0']]]]]);
				if (count($indicador)>0) {
					//Global ESTANDAR - está por tipo de indicador - son porcentajes [%]
					$global_standar = $indicador[0]->global_standar;
					$info_name_type_indicator = []; //[#][id_type_standar]
					if (count($global_standar)>0) {
						//obtengo los tipo de indicadores unicos globales
						for ($i=0; $i < count($global_standar); $i++) { 
							if (!in_array($global_standar[$i]->type_indicator->name, $info_name_type_indicator)) {
								$info_name_type_indicator[]=$global_standar[$i]->type_indicator->name;
							}
						}						
					}

					$global_datos = $indicador[0]->datos;
					$global_datos = objectToArray($global_datos);					
					if (count($global_datos)>0) {
						for ($i=0; $i < count($global_datos); $i++) { 	
							$name_categoria = $global_datos[$i]['name_categoria'];
							for ($ii=0; $ii < count($info_name_type_indicator); $ii++) {
								//echo "<br>".$info_name_type_indicator[$ii];
								$filter_data=[];
								$data=array('array_sql'=>array(), 'array_grupos'=>array(), 'name_columns'=>array());
								for ($k=0; $k < count($global_datos[$i]['grupos']); $k++) { 
									if ($global_datos[$i]['grupos'][$k]['type_indicator']['name']==$info_name_type_indicator[$ii]) {
										$array_sql = [];
										$array_normal = [];
										$array_grupos = ['fechas'=>array(), 'normal'=>array()];
										$tmp1 = [];
										$tmp2 = [];
										$array_name_columns=[];
										for ($l=0; $l < count($global_datos[$i]['grupos'][$k]['datos']); $l++) {
											if ($global_datos[$i]['grupos'][$k]['datos'][$l]['type_fecha']=='true') {
												for ($m=0; $m < count($global_datos[$i]['grupos'][$k]['datos'][$l]['colums']); $m++) { 
													if ($global_datos[$i]['grupos'][$k]['datos'][$l]['operator'] == '') {
														$tmp1[] = $global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m];
													}
													$array_sql = [$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m] => '1'];
												}
											}else{
												for ($m=0; $m < count($global_datos[$i]['grupos'][$k]['datos'][$l]['colums']); $m++) { 
													$array_sql = [$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m] => '1'];	
													$tmp2[] = $global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m];
													$data['name_columns']=array_merge($data['name_columns'], [$global_datos[$i]['grupos'][$k]['datos'][$l]['colums'][$m]]);
												}
											}
											$data['array_sql']=array_merge($data['array_sql'], $array_sql);
										}
										$array_grupos['fechas']=$tmp1;
										$array_grupos['normal']=$tmp2;
										$data['array_grupos'][]=$array_grupos;
									}
								}
								/*echo "<pre>";
								print_r($data);
								echo "</pre>";*/

								//OBTIENE LOS NOMBRES GENERICOS
								$name_genericos = $this->collection_model->get_collection('columns_configuration', ['$and' => [ ['entity.id'=>$id_entidad], ['columns'=>['$in' => $data['name_columns']]], ['state'=>'1'] ]], ['projection'=>['name_generico'=>'1', 'columns'=>'1']]);
								$name_genericos=objectToArray($name_genericos);

								//OBTIENE TODOS LOS DATOS DEL PACIENTE
								$filter_data = ['$and'=> [['_id' => new MongoDB\BSON\ObjectId($id_patient)], ['entity.id'=>$id_entidad]]];
								$patients_by_fecha = $this->collection_model->get_collection('patients', $filter_data, ['projection' => $data['array_sql']]);
								/*echo "<pre>";
								print_r($patients_by_fecha);
								echo "</pre>";*/
								
								//verifica con las reglas creadas
								$array_grupos_reglas = $data['array_grupos'];
								$total_registros = $total_registros_sin_fecha = $total_registros_sin_otros = $total_registros_ok = 0;
								$data_ok = $array_label = $array_full_data = [];

								for ($j=0; $j < count($array_grupos_reglas); $j++) {
									$keys_fechas = $array_grupos_reglas[$j]['fechas']; //suponiendo que sea colo una fecha
									$keys_normal = $array_grupos_reglas[$j]['normal'];
									$activas = [];
									//filtra las fechas que tengan algo
									for ($k=0; $k < count($keys_fechas); $k++) { 
										$campo = $keys_fechas[$k];
										$activas =  (count($activas)>0) ? $activas : $patients_by_fecha;
										$ini = count($activas);
										$activas=array_filter($activas, function ($tT) use ($campo) 
										{												
											if ( $tT->$campo!='' && $tT->$campo!='NULL' && !is_null($tT->$campo) ) {	
												return $tT;
											}
										});
										$activas=array_values($activas);
										$total_bad = $ini - count($activas);
										$total_registros_sin_fecha = $total_registros_sin_fecha + $total_bad;
									}
									//filta que el campo tengan algo
									for ($k=0; $k < count($keys_normal); $k++) { 
										$campo = $keys_normal[$k];
										$activas =  (count($activas)>0) ? $activas : $patients_by_fecha;
										$ini = count($activas);
										$activas=array_filter($activas, function ($tT) use ($campo) 
										{												
											if ( $tT->$campo!='' && $tT->$campo!='NULL' && !is_null($tT->$campo) ) {	
												return $tT;
											}
										});
										$activas=array_values($activas);
										$total_bad = $ini - count($activas);
										$total_registros_sin_otros = $total_registros_sin_otros + $total_bad;
									}
									
									if (count($activas)>0) {
										$total_registros_ok = $total_registros_ok + count($activas);
										$activas = objectToArray($activas);									
										for ($l=0; $l < count($activas); $l++) { 
											$temp=[];
											$temp = [ 'fecha' => date('Y-m',  strtotime($activas[$l][$keys_fechas[0]])) ];
											for ($k=0; $k < count($keys_normal); $k++) {
												$campo = $keys_normal[$k];
												$activas_1=array_filter($name_genericos, function ($tT) use ($campo) 
												{
													if (in_array($campo, $tT['columns'])) {
														return $tT;
													}
												});
												$activas_1=array_values($activas_1);
												if ( count($activas_1)>0 ) {
													if ( !isset($activas_1[0]['name_generico']) ) {
														print json_encode(array('res'=>'bad', 'msj'=>'Se debe configurar los nombres genéricos para los datos.'));
														exit();
													}
													$name = $activas_1[0]['name_generico'];
													$array_label[] = $name;
													$temp = array_merge($temp, [ $name => $activas[$l][$campo] ]);
												}
													
											}
											$data_ok[] = $temp;
										}
									}
								}

								if ( count($data_ok)==0 ) {
									print json_encode(array('res'=>'bad', 'msj'=>'Se debe configurar los nombres genéricos para los datos.'));
									exit();
								}
								$data_ok = groupByDate($data_ok);
								$array_label = array_unique($array_label);
								$total_registros = $total_registros_sin_fecha + $total_registros_sin_otros + $total_registros_ok;
								/*echo "<pre>";
								print_r($data_ok);
								echo "</pre>";*/
								$graficas[] = [ 'name'=>$indicador[0]->information->name." - ".$info_name_type_indicator[$ii], 'description'=>$indicador[0]->information->description, 'puntos_grafica'=>$data_ok, 'label'=>$array_label, 'total_registros'=>$total_registros, 'total_registros_sin_fecha'=>$total_registros_sin_fecha, 'total_registros_sin_otros'=>$total_registros_sin_otros, 'total_registros_ok'=>$total_registros_ok ];
							}							
						}						
					}
				}
			}//foreach  ids_eventos
			/*echo "<pre>";
			print_r($graficas);
			echo "</pre>";*/
			if (count($graficas)>0) {
				print json_encode(array('res'=>'ok', 'graficas'=>$graficas));
			}else{
				print json_encode(array('res'=>'bad', 'msj'=>'Debe agregar contenido al indicador'));
			}
		}else{
			print json_encode(array('res'=>'bad', 'msj'=>'Debe crear la configuración de indicadores para le entidad.'));
		}
	}

	//es un cron que calcula el estado por paciente, los datos que se harán la comparactión debe ser el último registro del paciente tanto a lo horizontal de columnas como a lo vertical por registros
	//esto deberia ejecutarse cuando suben una sabana masiva de pacientes y que esten configurados las columnas
	public function cronGetStatePatients()
	{
		$unique_entities_by_patients = $this->collection_model->get_aggregate('patients', [['$group'=>['_id'=>'$entity.id']]]);
		
		if (count($unique_entities_by_patients)>0) {
			foreach ($unique_entities_by_patients as $k_uebp => $v_uebp) {
				$id_entity = $v_uebp->_id;
				$name_columns = [];
				$name_data_paciente = ['Sexo', 'Fecha de nacimiento', 'Fumador', 'Tensión Arterial Sistólica (Último valor)', 'Diabetes Mellitus', 'Colesterol total (Último valor)', 'Peso (Último valor)', 'Creatinina (Último valor)']; //, 'HDL (Último valor)', 'Trigliceridos (Último valor)', , 'Talla (Último valor)'
				$data_paciente_projection = [];
				$data_paciente = ['entity.id'=>$id_entity, 'score_risk'=>['$exists'=>false]];				
				$name_columns_by_entity = $this->collection_model->get_collection('columns_configuration', ['$and'=>[['required'=>'1'], ['entity.id'=>$id_entity]]], ['projection'=>['columns'=>'1', 'name'=>'1']]);
				if (count($name_columns_by_entity)>0) {
					foreach ($name_columns_by_entity as $k_ncbe => $v_ncbe) {
						if (count($v_ncbe->columns)>0 && in_array($v_ncbe->name, $name_data_paciente)) {
							for ($j=0; $j < count($v_ncbe->columns); $j++) { 
								$data_paciente_projection = array_merge($data_paciente_projection, [$v_ncbe->columns[$j]=>'1']);
								$data_paciente = array_merge($data_paciente, [$v_ncbe->columns[$j]=>['$ne'=>null]]);
								$name_columns[$v_ncbe->name][]=$v_ncbe->columns[$j];
							}
						}
					}

					//si son iguales se puede obtener el estado por pacientes
					if (count($name_columns) == count($name_data_paciente)) {
						$patients = $this->collection_model->get_collection('patients', ['$and'=>[$data_paciente]], ['projection'=>$data_paciente_projection]); //'limit'=>1, 
						$patients = objectToArray($patients);

						foreach ($patients as $k_p => $v_p) {
							$edad = (int) date('Y') - (int) date('Y', strtotime($v_p[$name_columns['Fecha de nacimiento'][0]]));
							if($edad >= 40 && $edad <= 49){
								$age_f =1;
							}else if($edad >= 50 && $edad <= 54){
								$age_f =2;
							}else if($edad >= 55 && $edad <= 59){
								$age_f =3;
							}else if($edad >= 60 && $edad <= 64){
								$age_f =4;
							}else if($edad >= 65){
								$age_f =5;
							}
							$tension_arterial = (int) $v_p[$name_columns['Tensión Arterial Sistólica (Último valor)'][0]];
							if($tension_arterial <= 120){
								$tension_arterial=1;
							}else if($tension_arterial >= 121 && $tension_arterial <= 140){
								$tension_arterial=2;
							}else if($tension_arterial >= 141 && $tension_arterial <= 160){
								$tension_arterial=3;
							}else if($tension_arterial >= 161){
								$tension_arterial=4;
							}
							$search_score = ['fumador'=>($v_p[$name_columns['Fumador'][0]] == 'Si') ? 1 : 0, 'genero'=>($v_p[$name_columns['Sexo'][0]] == '0') ? 1 : 2, 'edad'=>$age_f, 'colesterol'=>(round(intval($v_p[$name_columns['Colesterol total (Último valor)'][0]]) * 0.026) >= 8) ? 8 : 4, 'tension_arterial'=>$tension_arterial];
							$score = $this->collection_model->get_collection('score', ['data'=>$search_score], ['limit'=>1, 'projection'=>['score'=>'1']]);
							if (count($score)>0) {
								$score = (int)$score[0]->score;
								$creatinina = (float)($v_p[$name_columns['Creatinina (Último valor)'][0]]);
								$peso = (int)($v_p[$name_columns['Peso (Último valor)'][0]]);
								if($v_p[$name_columns['Sexo'][0]] == '1'){
									$filtracion = ($creatinina != 0) ? round(((140 - $edad ) * $peso) / (72 * $creatinina)) : '';
								}else{
									$filtracion = ($creatinina != 0) ? round((((140 - $edad ) * $peso) / (72 * $creatinina)) * 0.85) : '';
								}
								$diabetes = $v_p[$name_columns['Diabetes Mellitus'][0]];
								if($filtracion < 30 || $score >= 10){									
									$estado_paciente='Rojo intenso';//rojo intenso
								}else if(($filtracion >= 30 && $filtracion <= 59) || ($score >= 5 && $score < 10) || $diabetes == 'Si'){
									$estado_paciente='Rojo';
								}else if($score >= 1 && $score < 5){
									$estado_paciente='Amarillo';
								}else if($score < 1){
									$estado_paciente='Verde';
								}
								//echo $v_p['_id']['oid'];
								$q=$this->collection_model->update_collection($v_p['_id']['oid'], 'patients', ['state_risk'=>['name'=>$estado_paciente], 'score_risk'=>$score, 'filtracion_glomerular'=>$filtracion]);
							}								
						}
					}
				}				
			}
		}
	}
}
//php index.php pacientes cronGetStatePatients
function objectToArray ( $object ) {
  	if(!is_object($object) && !is_array($object)) {
    	return $object;
  	}  
  	return array_map( 'objectToArray', (array) $object );
}

//[0]->fecha,  [1]->data (fecha, campos)
function groupByDate($data)
{
	$rta = [];
	if (count($data)>0) {
		$array_data=array_map(null, array_column($data, 'fecha'), $data);	
		/*echo "<pre>";
		print_r($array_data);
		echo "</pre>";*/
		//se organizan de menor a mayor por fecha
		array_multisort(array_column($data, 'fecha'), SORT_ASC, $data, SORT_ASC, $array_data);
		$fechas_unique = array_values(array_unique(array_column($array_data, 0)));
		for ($j=0; $j < count($fechas_unique); $j++) { 
			$campo = $fechas_unique[$j];
			$tmp=[];
			$tmp = ['fecha'=>$campo];
			foreach ($array_data as $k_ad => $v_ad) {
				foreach ($v_ad[1] as $k_vad => $v_vad) {
					if ($k_vad!='fecha') {
						$tmp = array_merge( $tmp, [ $k_vad=>$v_vad ] );
					}
				}										
			}
			$rta[] = $tmp;
		}
	}
	return $rta;
}

function setDataRelevante($id, $v_ar)
{
	$ci =& get_instance();
	$orig_date = new DateTime();
	$orig_date=$orig_date->getTimestamp();
	$result = $ci->collection_model->get_collection('historial_antecedentes_relevantes', ['$and'=>[['patient.id'=>$id], ['antecedente_relevante.id'=>$v_ar['valores']['id']]]], ['projection'=>['_id'=>'1']]);				
	if (count($result)>0) { //update
		$id_condition = (String) $result[0]->_id;
		$query = $ci->collection_model->update_push_collection($id_condition, 'historial_antecedentes_relevantes', ['data'=>[['respuesta_paciente'=>$v_ar['valores']['rta_pa'], 'respuesta_doctor'=>$v_ar['valores']['rta_doc'], 'year'=>$v_ar['valores']['ano'], 'registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)]]]);
	}else{ //insert
		$data=['patient'=>['id'=>$id], 'antecedente_relevante'=>['id'=>$v_ar['valores']['id'], 'name'=>$v_ar['condicion']], 'data'=>[['respuesta_paciente'=>$v_ar['valores']['rta_pa'], 'respuesta_doctor'=>$v_ar['valores']['rta_doc'], 'year'=>$v_ar['valores']['ano'], 'registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)]]];
		$query = $ci->collection_model->insert_collection('historial_antecedentes_relevantes', $data);
	}
	return $query;
}

function setDataTrazadoras($id, $v_ct)
{
	$ci =& get_instance();
	$orig_date = new DateTime();
	$orig_date=$orig_date->getTimestamp();
	$result = $ci->collection_model->get_collection('historial_condiciones_trazadoras', ['$and'=>[['patient.id'=>$id], ['condicion_trazadora.id'=>$v_ct['valores']['id']]]], ['projection'=>['_id'=>'1']]);
	if (count($result)>0) {
		$id_condition = (String) $result[0]->_id;
		$query = $ci->collection_model->update_push_collection($id_condition, 'historial_condiciones_trazadoras', ['data'=>[['respuesta'=>$v_ct['valores']['rta'], 'observacion'=>$v_ct['valores']['obs'], 'year'=>$v_ct['valores']['ano'], 'registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)]]]);
	}else{
		$data=['patient'=>['id'=>$id], 'condicion_trazadora'=>['id'=>$v_ct['valores']['id'], 'name'=>$v_ct['condicion']], 'data'=>[['respuesta'=>$v_ct['valores']['rta'], 'observacion'=>$v_ct['valores']['obs'], 'year'=>$v_ct['valores']['ano'], 'registration_date'=>new MongoDB\BSON\UTCDateTime($orig_date*1000)]]];
		$query = $ci->collection_model->insert_collection('historial_condiciones_trazadoras', $data);
	}
	return $query;
}

function printArray($e)
{
	echo "<pre>";
	print_r($e);
	echo "</pre>";
}